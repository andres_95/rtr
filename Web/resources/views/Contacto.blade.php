@extends('layouts.main')

@section('titulo')
    contacto
@stop



@section('breadcrumb')
    <li><a href="#">SIIF</a></li>
    <li>Contacto</li> 
@stop

@section('Content')
    <div class="row">
        <div class="col-6" style="padding:5%;margin-top:4%;">
            <form  id="contacta-form">
                {{ csrf_field() }}
                <div class="md-form">
                    <i class="fa fa-user fa-sm prefix grey-text"></i>
                    <input type="text"id="name" class="form-control contacta"  name='name'
                           required pattern="[a-zA-Z0-9_]+([.][a-zA-Z0-9_]+)*@[a-zA-Z0-9_]+([.][a-zA-Z0-9_]+)*[.][a-zA-Z]{1,5}">
                    <label for="name">Nombre y apellido</label>
                </div>
                <div class="md-form">
                    <i class="fa fa-envelope fa-sm prefix grey-text"></i>
                    <input type="email"id="email" class="form-control contacta"  name='email'
                           required pattern="[a-zA-Z0-9_]+([.][a-zA-Z0-9_]+)*@[a-zA-Z0-9_]+([.][a-zA-Z0-9_]+)*[.][a-zA-Z]{1,5}">
                    <label for="email">Correo electronico</label>
                </div>
                <div class="md-form">
                    <i class="fas fa-edit fa-sm prefix grey-text"></i>
                    <input type="text" id='Asunto'class=" form-control contacta"  name='asunto'
                           required pattern="[A-Za-z0-9]">
                    <label for="asunto">Asunto</label>
                </div>
                <div class="md-form" style="display:none">
                    <textarea name="message" id="message" cols="30" rows="10"></textarea>
                </div>
            </form>
            <br>
            <div>
                <i class="fab fa-facebook fa-2x prefix grey-text"></i><a href=""> Servicios Integrales IF</a>&nbsp;&nbsp;
                <i class="fab fa-twitter fa-2x prefix grey-text"></i><a href=""> @integralesIF</a><br><br>
                <i class="fab fa-instagram fa-2x prefix grey-text"></i><a href=""> serviciosintegralesif</a>&nbsp;&nbsp;
                <i class="fab fa-linkedin fa-2x prefix grey-text"></i><a href=""> Servicios Integrales IF</a><br>
            </div>
        </div>
        <div class="col-5">
            <label for="mesage" style="margin-top:55%;"><b> Mensaje:</b></label>
            <textarea id="mesage" cols="30" rows="10"></textarea>
            <button id="boton" type="submit" onclick="event.preventDefault(); document.getElementById('contacta-form').submit();" class="btn btn-w btn-siif float-right btn-w">Enviar</button>
        </div>
    </div>

@endsection