@extends('layouts.main')


@section('titulo')
    Configuracion RTR
@stop


@section('breadcrumb')
        <li><a href="#">Procesos</a></li>
        <li>Configuración</li>
@stop


@section('Content')
<div class="row">
    <div class="col-12">
        <div class="card card-cascade narrower mt-1 p-2" style="z-index: 20;position: relative;left: 0%;top: -1%;max-width: 100%;max-height: fit-content;">
            {{--  <div class="view gradient-card-header purple-gradient narrower py-0 mx-4 mb-1 d-flex align-content-center">
                <h6 class="black-text font-up font-bold mb-0" style="font-weight:700;">Configuracion</h6>
            </div>  --}}
            <div style="border:1px solid gray;">
                <div class="row px-4" >
                    <div class="col-1 md-form">
                        <label for="codigo">Pais</label>
                    </div>
                    <div class="col-3 md-form">
                        <select class="form-control" name="pais" id="pais">
                            <option value="1">Venezuela</option>
                        </select>
                    </div>
                    <div class="col-1 md-form">
                        <label for="codigo">Idioma</label>
                    </div>
                    <div class="col-3 md-form">
                        <select class="form-control" name="idioma" id="idioma">
                            <option value="1">Español</option>
                        </select>
                    </div>
                    <div class="col-1 md-form">
                        <label for="codigo">Moneda</label>
                    </div>
                    <div class="col-3 md-form">
                        <select class="form-control" name="moneda" id="moneda">
                            <option value="1">BSF</option>
                        </select>
                    </div>
                </div>
                <div class="row px-4 pt-2" >
                    <div class="col-2 md-form">
                        <label for="codigo">Nombre del Sitio</label>
                    </div>
                    <div class="col-4 md-form">
                        <input type="text" id='dateH'class=" form-control"  name='dateH' 
                        required>
                    </div>
                    <div class="col-2 md-form">
                        <label for="Ruta">Ruta del logo</label>
                    </div>
                    <div class="col-4 md-form">
                        <input type="text" name="logo" id="logo" class=" form-control">
                    </div>
                </div>
                <br>
            </div>
            <!--<div style="border:1px solid gray;padding:1%;">
                <div class="row px-1" >
                    <div class="col-3 md-form">
                        <select id='OrdenPD'class=" form-control"  name='OrdenPD'>
                                <option value="">Orden-Pedido</option>
                                <option value="170">N° 170</option>
                        </select>
                    </div>
                    <div class="col-3 md-form">
                        <select id='EstatusOrdenPD'class=" form-control"  name='EstatusOrdenPD'>
                                <option value="">Estatus-Orden-Pedido</option>
                                <option value="001">Recibidas</option>
                                <option value="002">Facturadas</option>
                                <option value="003">Firmadas</option>
                        </select>
                    </div>
                </div>
            </div>
            <div style="border:1px solid gray;padding:1%;">
                    <div class="row px-1" >
                        <div class="col-3 md-form">
                            <select id='EstatusOrdenPD'class=" form-control"  name='EstatusOrdenPD'>
                                    <option value="">Factura</option>
                                    <option value="170">N° 170</option>
                                    <option value="171">N° 171</option>
                            </select>
                        </div>
                        <div class="col-3 md-form">
                            <select id='EstatusFactD'class=" form-control"  name='EstatusFactD'>
                                    <option value="">Estatus-Factura</option>
                                    <option value="001">Entregada</option>
                                    <option value="002">Pendiente</option>
                            </select>
                        </div>
                    </div>
                </div>
            <br>
            <div class="row">
                <div class="col-11"></div>
                <div class="col-1">
                    <button class="btn btn-ct victoria">Consultar</button>
                </div>
            </div>-->
        </div>
    </div>
</div>
    
@endsection