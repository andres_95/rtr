@extends('layouts.main')

@section('Head')


@endsection

@section('titulo')
    Pase a Historicos
@stop


@section('breadcrumb')
    <li><a href="#">Procesos</a></li>
    <li><a href="#">Información Historica</a></li>
    <li>Pase a Historicos</li>
@stop



@section('Content')

<div class="row">
    <div class="col-12">
        <div class="card card-cascade narrower mt-1 p-2 cuadro-tabla">

           <!-- <div class="view gradient-card-header purple-gradient narrower py-0 mx-4 mb-1 d-flex align-content-center">
                    <h6 class="black-text font-up font-bold mb-0" style="font-weight:700;">Preguntas Frecuentes</h6>
            </div>
            <br>-->
            <div class="content">
                <div class="col-md-12 col-sm-12">
                    <div class="panel">
                        <h6 class="black-text font-up font-bold mb-0 h5" style="font-weight:700;">Personas</h6><br>
                        <div class="progress" style="height: 20px">
                                <div class="progress-bar " role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100" style="width: 20%">20% completado</div>
                        </div>
                    </div>
                    <div class="panel">
                        <h6 class="black-text font-up font-bold mb-0 h5" style="font-weight:700;">Productos</h6><br>
                        <div class="progress" style="height: 20px">
                                <div class="progress-bar " role="progressbar" aria-valuenow="45" aria-valuemin="0" aria-valuemax="100" style="width: 45%">45% completado</div>
                        </div>
                    </div>
                    <div class="panel">
                        <h6 class="black-text font-up font-bold mb-0 h5" style="font-weight:700;">Ordenes</h6><br>
                        <div class="progress" style="height: 20px">
                                <div class="progress-bar " role="progressbar" aria-valuenow="75" aria-valuemin="0" aria-valuemax="100" style="width: 20%">75% completado</div>
                        </div>
                    </div>
                    <div class="panel">
                        <h6 class="black-text font-up font-bold mb-0 h5" style="font-weight:700;">Items de Ordenes</h6><br>
                        <div class="progress" style="height: 20px">
                                <div class="progress-bar " role="progressbar" aria-valuenow="45" aria-valuemin="0" aria-valuemax="100" style="width: 45%">45% completado</div>
                        </div>
                    </div>
                    <div class="panel">
                        <h6 class="black-text font-up font-bold mb-0 h5" style="font-weight:700;">Facturas</h6><br>
                        <div class="progress" style="height: 20px">
                                <div class="progress-bar " role="progressbar" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100" style="width: 45%">50% completado</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>    
    </div>
</div>
@endsection