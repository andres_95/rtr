@extends('layouts.main')


@section('titulo')
    Monitor
@stop


@section('breadcrumb')
                <li><a href="#">Procesos</a></li>
                <li>Monitor de Archivos</li>
            </ul>
@stop


@section('Content')
    <div class="jumbotron rounded">
            <monitor :empresas="{{$empresas}}"></monitor>
    </div>
@endsection
