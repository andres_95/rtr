@extends('layouts.main')


@section('titulo')
    Consulta Reportes
@stop


@section('breadcrumb')
                <li><a href="#">SIIF</a></li>
                <li>Consultas - reportes</li>
@stop

@section('Content')

<div class="row">
    <div class="col-12">
        <div class="jumbotron rounded p-2">
            <!--<div class="view gradient-card-header purple-gradient narrower py-0 mx-4 mb-1 d-flex align-content-center">
                <h6 class="black-text font-up font-bold mb-0" style="font-weight:700;">Consulta de Reportes</h6>
            </div>-->
            <div style="border:1px solid gray;">
                <div class="row px-3" >
                    <div class="col-2 md-form">
                        <label for="codigo">Fecha Desde</label>
                    </div>
                    <div class="col-4 md-form">
                        <input type="date" id='dateD'class=" form-control"  name='dateD' 
                        required>
                    </div>
                    <div class="col-2 md-form">
                        <label for="codigo">Fecha Hasta</label>
                    </div>
                    <div class="col-4 md-form">
                        <input type="date" id='dateH'class=" form-control"  name='dateH' 
                        required>
                    </div>
                </div>
                <!--<div class="row px-1" >
                    <div class="col-6 md-form">
                        <label for="codigo">Empresa Desde</label>
                        <input type="text" id='empresaD'class=" form-control"  name='empresaD' 
                        required pattern="[0-9]{4,6}">
                    </div>
                    <div class="col-6 md-form">
                        <label for="empresa">Empresa Hasta</label>
                        <input type="text" id='empresaH'class=" form-control"  name='empresaH' 
                        required pattern="[0-9]{4,6}">
                    </div>
                </div>-->
                <div class="row px-3" >
                    <div class="col-3 md-form">
                        <select class="form-control" id="RFV" name="RFV">
                                <option value="">RFV</option>
                                <option value="23685332">Luis Caceres</option>
                                <option value="23563653">Victoria Da Silva</option>
                        </select>
                    </div>
                    <div class="col-3 md-form">
                        <select id='rankingD'class=" form-control"  name='rankingD'>
                            <option value="">Ranking</option>
                            <option value="001">Alta</option>
                            <option value="002">Baja</option>
                        </select>
                    </div>
                    <div class="col-3 md-form">
                        <select id='lineaD'class=" form-control"  name='lineaD'>
                            <option value="">Linea</option>
                            <option value="001">Veterinaria</option>
                            <option value="002">Cardiologia</option>
                        </select>
                    </div>
                    <div class="col-3 md-form">
                        <select id='especialidadD'class=" form-control"  name='especialidadD'>
                            <option value="">Especialidad</option>
                            <option value="001">Medico</option>
                        </select>
                    </div>
                    <div class="col-3 md-form">
                        <select class="form-control" id="Muestra" name="Muestra">
                                <option value="">Muestra</option>
                                <option value="1234">Tabeletas DOLL</option>
                                <option value="3254">Taza</option>
                        </select>
                    </div>
                    <div class="col-3 md-form">
                        <select id='ActividadD'class=" form-control"  name='ActividadD'>
                                <option value="">Actividad</option>
                                <option value="01">Visita</option>
                                <option value="02">Charla</option>
                                <option value="03">Entrega de Muestra</option>
                        </select>
                    </div>
                    <div class="col-3 md-form">
                        <select id='SituacionD'class=" form-control"  name='SituacionD'>
                                <option value="">Situación </option>
                        </select>
                    </div>
                    <div class="col-3 md-form">
                        <select id='ZBRD'class=" form-control"  name='ZBRD'>
                                <option value="">Zona-Brick-Ruta</option>
                                <option value="001">Distrito Capital</option>
                                <option value="002">Carabobo</option>
                        </select>
                    </div>
                </div>
            </div>
            <div style="border:1px solid gray;padding:1%;">
                <div class="row px-1" >
                    <div class="col-3 md-form">
                        <select id='OrdenPD'class=" form-control"  name='OrdenPD'>
                                <option value="">Orden-Pedido</option>
                                <option value="170">N° 170</option>
                        </select>
                    </div>
                    <div class="col-3 md-form">
                        <select id='EstatusOrdenPD'class=" form-control"  name='EstatusOrdenPD'>
                                <option value="">Estatus-Orden-Pedido</option>
                                <option value="001">Recibidas</option>
                                <option value="002">Facturadas</option>
                                <option value="003">Firmadas</option>
                        </select>
                    </div>
                    <div class="col-3 md-form">
                        <select id='MayoristaD'class=" form-control"  name='MayoristaD'>
                            <option value="">Mayorista</option>
                            <option value="001">IMP</option>
                            <option value="002">BPM</option>
                        </select>
                    </div>
                </div>
            </div>
            <div style="border:1px solid gray;padding:1%;">
                    <div class="row px-1" >
                        <div class="col-3 md-form">
                            <select id='EstatusOrdenPD'class=" form-control"  name='EstatusOrdenPD'>
                                    <option value="">Factura</option>
                                    <option value="170">N° 170</option>
                                    <option value="171">N° 171</option>
                            </select>
                        </div>
                        <div class="col-3 md-form">
                            <select id='EstatusFactD'class=" form-control"  name='EstatusFactD'>
                                    <option value="">Estatus-Factura</option>
                                    <option value="001">Entregada</option>
                                    <option value="002">Pendiente</option>
                            </select>
                        </div>
                    </div>
                </div>
            <br>
            <div class="row">
                <div class="col-11"></div>
                <div class="col-1">
                    <button class="btn btn-ct victoria" style="height: 29px; margin-left: -16px;">Consultar</button>
                </div>
            </div>
        </div>
    </div>
</div>
    
@endsection