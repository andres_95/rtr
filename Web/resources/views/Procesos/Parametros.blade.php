@extends('layouts.main')


@section('titulo')
    Parametros
@stop


@section('breadcrumb')
    <li><a href="#">Procesos</a></li>
    <li>Parametros</li>
@stop

@section('Content')
        <div class="jumbotron rounded p-2">
            <div style="border:1px solid gray;">
                <div class="row px-3" >
                    <div class="col-4 md-form">
                        <select class="form-control" id="Parametro" name="Parametro">
                                <option value="">Idioma</option>
                                <option value="ESP">Español</option>
                                <option value="ENG">Ingles</option>
                        </select>
                    </div>
                    <div class="col-4 md-form">
                        <select class="form-control" id="Parametro" name="Parametro">
                                <option value="">Moneda</option>
                                <option value="0">BsF</option>
                                <option value="1">USD</option>
                        </select>
                    </div>
                    <div class="col-4 md-form">
                        <select class="form-control" id="Parametro" name="Parametro">
                                <option value="">Formato de Fecha</option>
                                <option value="1">dd-mm-aaaa</option>
                                <option value="0">aaaa-mm-dd</option>
                        </select>
                    </div>
                    <div class="col-4 md-form">
                        <select class="form-control" id="Parametro" name="Parametro">
                                <option value="">Operador</option>
                                <option value="0">BsF</option>
                                <option value="1">USD</option>
                        </select>
                    </div>
                </div>
                <div class="row px-3" >
                    <div class="col-4 md-form">
                    </div>
                </div>
            </div>
            <br>
            <div class="row">
                <div class="col-11"></div>
                <div class="col-1">
                    <button class="btn btn-ct victoria" style="height: 29px; margin-left: -16px;">Aplicar</button>
                </div>
            </div>
        </div>
@endsection