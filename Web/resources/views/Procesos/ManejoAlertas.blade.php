@extends('layouts.main')


@section('titulo')
    Manejo de Alertas
@stop


@section('breadcrumb')
    <li><a href="#">Procesos</a></li>
    <li>Manejo de Alertas</li>
@stop

@section('Content')

<div class="row">
    <div class="col-12">
        <div class="jumbotron rounded p-2">
            <div style="border:1px solid gray;">
                <div class="row px-3 align-items-center" >
                    <div class="col-3 md-form">
                        <select class="form-control" id="Parametro" name="Parametro">
                                <option value="">Parametro de Alerta</option>
                                <option value="23685332">Cumplimiento de visitas del RFV</option>
                                <option value="23563653">Cantidad de ventas del RFV</option>
                        </select>
                    </div>
                    <div class="col-4">
                        <v-layout row wrap>
                            <v-flex xs9>
                                <v-slider label="Meta" thumb-label step="1" v-model="metamnima" ></v-slider>
                            </v-flex>
                            <v-flex xs3>
                                <v-text-field v-model="metamnima" type="number"></v-text-field>
                            </v-flex>
                        </v-layout>
                    </div>
                    <div class="col-3 ">
                        <v-layout row wrap>
                            <v-flex xs9>
                                <v-slider label="%" thumb-label step="1" v-model="porcalertas" ></v-slider>
                            </v-flex>
                            <v-flex xs3>
                                <v-text-field v-model="porcalertas" type="number"></v-text-field>
                            </v-flex>
                        </v-layout>
                    </div>
                    <div class="col-2">
                        <button class="btn btn-md victoria">
                        <i class="fa fa-plus"></i> Agregar mas</button>
                    </div>
                </div>
            </div>
            <br>
            <div class="row">
                <div class="col-11"></div>
                <div class="col-1">
                    <button class="btn btn-ct victoria" style="height: 29px; margin-left: -16px;">Aplicar</button>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection