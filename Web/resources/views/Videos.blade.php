@extends('layouts.main')

@section('titulo')
Videos de Sistema
@stop


@section('breadcrumb')
<li><a href="#">SIIF</a></li>
<li>Video del sistema</li>
@stop

@section('Content')

<div class="row justify-content-center">
    <div class="col-8">
        <div class="card">
            <div class="card-body">
                    <div class="embed-responsive embed-responsive-16by9">
                        <iframe width="550" height="400"
                            src="https://www.youtube.com/embed/J0Ukn3UC3ps">
                        </iframe>
                    </div>
            </div>
        </div>
    </div>
</div>
@endsection