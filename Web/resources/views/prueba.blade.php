@extends('layouts.main')

@section('titulo')
    Consulta Gerencial
@stop


@section('breadcrumb')
    <li><a href="#">SIIF</a></li>
    <li>Consulta Gerencial</li>
@stop

@section('Content')
    <div class="jumbotron rounded py-1">
        <div>
            <div class="row justify-content-center">
                <div class="col-sm-6 col-md-3">
                    <v-menu
                            ref="menuD"
                            lazy
                            :close-on-content-click="false"
                            v-model="menuD"
                            transition="scale-transition"
                            offset-y
                            full-width
                            :nudge-right="40"
                            min-width="290px"
                            :return-value.sync="dateD"
                    >
                        <v-text-field
                                slot="activator"
                                label="Fecha desde"
                                v-model="dateD"
                                prepend-icon="event"
                                readonly
                                name="dateD"
                        ></v-text-field>
                        <v-date-picker v-model="dateD" no-title scrollable locale="es-ES" @change="$refs.menuD.save(dateD)">
                        </v-date-picker>
                    </v-menu>
                </div>
                <div class="col-sm-6 col-md-3">
                    <v-menu
                            ref="menuH"
                            lazy
                            :close-on-content-click="false"
                            v-model="menuH"
                            transition="scale-transition"
                            offset-y
                            full-width
                            :nudge-right="40"
                            min-width="290px"
                            :return-value.sync="dateH"
                    >
                        <v-text-field
                                slot="activator"
                                label="Fecha hasta"
                                v-model="dateH"
                                prepend-icon="event"
                                readonly
                                name="dateH"
                        ></v-text-field>
                        <v-date-picker v-model="dateH" no-title scrollable locale="es-ES" @change="$refs.menuH.save(dateH)">
                        </v-date-picker>
                    </v-menu>
                </div>
                <div class="col-sm-6 col-md-4 md-form">
                        <v-select
                                :items="{{$RFV}}"
                                label="Seleccione un RFV"
                                required
                                font-size="8"
                                item-text="nombre_completo_razon_social"
                                item-value="idPersona"
                                autocomplete
                                multiple
                                chips
                                v-model=rfv
                                dense
                        ></v-select>
                </div>
            </div>
            <!--<div class="narrower py-0 mx-2 mb-1 d-flex row align-items-center">
                <div class="col-2"><h6 class="black-text font-bold font-up mb-0" style="font-weight:700;">Reportes de Visitas</h6></div>
            </div>-->
            <div class="row border border-dark py-0">
                <div class="col-sm-6 col-md-3 md-form">
                    <v-select
                            v-bind:items="{{ $zona }}"
                            label="Zona"
                            item-text="descripcion_zona"
                            item-value="idzona"
                            return-object
                            autocomplete
                            multiple
                            chips
                            v-model=confirm
                            dense
                    ></v-select>
                </div>
                <div class="col-sm-6 col-md-3 md-form">
                    <v-select
                            v-bind:items="{{ $brick }}"
                            label="Brick-Ruta"
                            item-text="Descripcion"
                            item-value="idbrick"
                            return-object
                            autocomplete
                            multiple
                            chips
                            v-model=factura
                            dense
                            :disabled="!confirm"
                    ></v-select>
                </div>
                <div class="col-6">
                    <button class="btn siif btn-w float-right " @click="consultaGerencial()" >Consultar</button>
                </div>
            </div>
        </div>
    </div>

@endsection