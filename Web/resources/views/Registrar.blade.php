@extends('layouts.header_basic')

@section('Content')

<img src="img/Registro.png" alt="" style="height:100%;width:100%;z-index:-1;position:absolute;">

    <div class="container-fluid">
        <div class="row">
            <div class="col-6">
                <div class="register-form mt-5">
                    <form action="{{route('post_register')}}"  method="post" id="formregister">
                        {{ csrf_field() }}
                        <div class="h1-responsive mb-2" align="center">Registro</div>
                        <div class="row">
                            <div class="col-12">
                                <div class="md-form">
                                    <input type="text" class="form-control" id="empresaregister" name="empresa" >
                                    <label for="empresaregister">Empresa</label>
                                    <div class="errorempresa"></div>
                                </div>
                            </div>
                        </div>
                        
                        <div class="row">
                            <div class="col-6">
                                <div class="md-form">
                                    <input type="text" class="form-control" id="nombresregister" name="nombres">
                                    <label for="nombresregister">Nombre</label>
                                </div>
                            </div>
                            <div class="col-6">
                                <div class="md-form">
                                    <input type="text" class="form-control" id="apellidoregister" name="apellidos">
                                    <label for="apellidoregister">Apellido</label>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-6">
                                <div class="row">
                                    <div class="col-3">
                                        <div class="md-form form-group form-md-line-input has-info">
                                            <select class="form-control" id="tipopersona" name="tipo_persona">
                                                <option value="V-">V-</option>
                                                <option value="E-">E-</option>
                                                <option value="J-">J-</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-9">
                                        <div class="md-form">
                                            <input type="text" class="form-control" id="documentoregister" name="documento">
                                            <label for="documentoregister">Documento</label>
                                        </div>
                                    </div>
                                </div>

                            </div>
                            <div class="col-6">
                                <div class="md-form">
                                    <input type="email" class="form-control" id="emailregister" name="email">
                                    <label for="emailregister">Email</label>
                                </div>

                            </div>
                        </div>
                        
                        <div class="row">
                            <div class="col-6">
                                <div class="md-form">
                                    <input type="text" class="form-control" id="usernameregister" name="username" >
                                    <label for="usernameregister">Username</label>
                                    <div class="errorusername"></div>
                                </div>
                            </div>
                            <div class="col-6">
                                <div class="md-form">
                                    <input type="password" class="form-control" id="passwordregister" name="password">
                                    <label for="passwordregister">Contrasena</label>
                                </div>

                            </div>
                        </div>
                        
                        <div class="text-right">
                            <button class="btn victoria btn-lg" type="submit">Registrar</button>
                        </div>
                    </form>
                </div>
            </div>
            <div class="col-6">
                <div style="background-color: #f1f0f0;height: 51%;position: absolute;top: 84%;left: 21%;width: 72%;">
                    <img src="img/logo.png" alt="" style="height: 200%;width: auto;position: absolute;top: -84px;left: 40%">
                </div>
                
            </div>
        </div>
    </div>

@endsection