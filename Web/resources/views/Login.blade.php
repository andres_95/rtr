@extends('layouts.header_basic')

@section('Content')
    <div class="container-fluid">
        <div class="row login">
            <div class="col-sm-6 col-md-8 align-self-center justify-content-center">
                <img src="{{asset('img/logo2.png')}}" alt="RTR System" class="img-fluid w-75" style=" " >
            </div>
            <div class="col-sm-6 col-md-4 align-self-center">
                 
                <form action="{{route('postlogin')}}" method="post" class="px-3 card animated fadeInRight">
                     {{csrf_field()}}
                        <div class="card-body">
                            <p class="h3-responsive titulo text-center py-3">Iniciar Sesion</p>
                            <div class="md-form">
                                <i class="fa fa-user fa-sm prefix grey-text"></i>
                                <input type="text" name="username" id="username" class="form-control ">
                                <label for="username">Usuario</label>
                            </div>
                            <div class="md-form">
                                <i class="fa fa-lock fa-sm prefix grey-text"></i>
                                <input type="password" name="password" id="password" class="form-control">
                                <label for="password">Password</label>
                            </div>
                            <div class="text-center py-4 mt-3">
                                <button class="btn victoria btn-w" type="submit">Ingresar</button>
                            </div>
                            @if (count($errors) > 0)
                                    <p class="red-text text-center" >El usuario y/o la contraseña son invalidos</p>
                                
                            @endif
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection