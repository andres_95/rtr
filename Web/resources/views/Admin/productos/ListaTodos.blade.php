@extends('layouts.main')


@section('titulo')
    Reportes
@stop


@section('breadcrumb')
    <li><a href="#">Administrar</a></li>
    <li><a href="{{route('Productos')}}">Administrar Productos</a></li>
    <li>Lista</li>
@stop

@section('Content')
    <div class="jumbotron rounded">
        <div class="row mt-3 ">
            <p class="h3-responsive font-bold w-100 ml-3" >Tu lista de {{$categoria}}</p>
            <p class="font-bold ml-3">Fabricante: <span class="font-weight-light">{{$Empresa}}</span> </p>
          
            <div class="col-12">
                <listatodos 
                    fabricante="{{$Empresa}}" 
                    categ="{{$categoria}}"
                    :linea="{{$linea}}"
                    :mayoristas="{{$mayorista}}"
                    :tipos="{{$tipos}}"
                >
                </listatodos> 
            </div>
        </div>
    </div>

@endsection