@extends('layouts.main')


@section('titulo')
    Agregar Productos
@stop


@section('breadcrumb')
    <li><a href="#">Administrar</a></li>
    <li><a href="{{route('Productos')}}">Administrar Productos</a></li>
    <li>Agregar Productos</li>
@stop
@section('Content')
    <div class="jumbotron">
        <div class="">
            <p class="h4-responsive font-weight-light border-title">Agregar Productos</p>
        </div>
        @if(count($PM)==0)
            <form action="{{route('storePr')}}" method="post" class="mt-2 pt-3" >
                {{csrf_field()}}
                <div class="row p-3 border border-dark" style="min-height: 200px;">
                    <p class="h6-responsive mt-1 font-bold w-100 pl-3">Datos Basicos:</p>
                    <div class="col-md-2 col-sm-6">
                        <div class="md-form">
                            <v-text-field
                                
                                name="fabricante"
                                label="Fabricante"
                                value="{{$Fabrica}}"
                                disabled
                                
                            ></v-text-field>
                            <input type="hidden" name="fabricante" value="{{$Fabrica}}">
                        </div>
                    </div>
                    <div class="col-md-2 col-sm-6">
                        <div class="md-form">
                            <v-text-field
                            name="idproducto"
                            label="Id Producto"
                            ></v-text-field>
                        </div>
                    </div>
                    <div class="col-md-2 col-sm-6">
                        <div class="md-form">
                            <v-text-field
                                name="categoria"
                                label="Categoria"
                                value="{{  $categorias }}"
                                disabled
                            ></v-text-field>
                            <input type="hidden" name="categoria" value="{{$categorias}}">
                        </div>
                    </div>
                    <div class="col-md-2 col-sm-6">
                        <div class="md-form">
                            <v-text-field
                            name="Nombre Producto"
                            label="Nombre Producto"
                            ></v-text-field>
                        </div>
                    </div>
                    <div class="col-md-2 col-sm-6">
                        <div class="md-form">
                            <v-text-field
                            name="Cantidad"
                            label="Cantidad"
                            type="number"
                            min=0
                            ></v-text-field>
                        </div>
                    </div>
                    <div class="col-md-2 col-sm-6">
                        <div class="md-form">
                            <v-select
                            :items="{{  $linea }}"
                            label="Linea Producto"
                            item-text="descripcion_linea_producto"
                            item-value="id"
                            return-object
                            v-model="codigo"
                        ></v-select>
                        <input type="hidden" name="linea" v-model="codigo.id">
                        </div>
                    </div>
                    <!--<div class="col-md-2 col-sm-6">
                        <div class="md-form">
                            <v-select
                            v-bind:items="{{  $linea }}"
                            label="Unidades"
                            item-text="descripcion_unidades"
                            item-value="idunidades"
                            return-object
                            autocomplete
                            name="unidades"
                        ></v-select>
                        </div>
                    </div>-->
                    <div class="col-sm-3 col-md-2">
                        <v-menu
                                ref="menuD"
                                lazy
                                :close-on-content-click="false"
                                v-model="menuD"
                                transition="scale-transition"
                                offset-y
                                full-width
                                :nudge-right="40"
                                min-width="290px"
                                :return-value.sync="dateD"
                        >
                            <v-text-field
                                    slot="activator"
                                    label="Fecha Expedicion"
                                    v-model="dateD"
                                    readonly
                            ></v-text-field>
                            <v-date-picker v-model="dateD" no-title scrollable locale="es-ES" @change="$refs.menuD.save(dateD)">
                            </v-date-picker>
                        </v-menu>
                    </div>
                    <div class="col-sm-3 col-md-2">
                        <v-menu
                                ref="menuH"
                                lazy
                                :close-on-content-click="false"
                                v-model="menuH"
                                transition="scale-transition"
                                offset-y
                                full-width
                                :nudge-right="40"
                                min-width="290px"
                                :return-value.sync="dateH"
                        >
                            <v-text-field
                                    slot="activator"
                                    label="Fecha Vencimiento"
                                    v-model="dateH"
                                    readonly
                                    name="dateV"
                            ></v-text-field>
                            <v-date-picker v-model="dateH" no-title scrollable locale="es-ES" @change="$refs.menuH.save(dateH)">
                            </v-date-picker>
                        </v-menu>
                    </div>
                    @if($categorias=='PROD')
                        <div class="col-md-2 col-sm-6">
                            <div class="md-form">
                                <v-text-field
                                    name="Precio"
                                    label="Precio"
                                    single-line
                                    type="number"
                                    min=0
                                ></v-text-field>
                            </div>
                        </div>
                        <div class="col-md-2 col-sm-6">
                            <div class="md-form">
                                <v-text-field
                                    name="Descuento"
                                    label="Descuento"
                                    single-line
                                    type="number"
                                    min=0
                                    max=99
                                ></v-text-field>
                            </div>
                        </div>
                    @else
                        <div class="col-md-2 col-sm-6">
                            <div class="md-form">
                                <v-text-field
                                    name="Lote"
                                    label="Lote"
                                ></v-text-field>
                            </div>
                        </div>
                    @endif
                </div>

                <div align="right">
                    <button type="submit" class="btn btn-w btn-siif">Registrar</button>
                </div>
            </form>
        @else
            <!--<form action="{{route('storePr')}}" method="post" class="mt-2 pt-3" >
                {{csrf_field()}}
                <div class="row p-3 border border-dark" style="min-height: 200px;">
                    <p class="h6-responsive mt-1 font-bold w-100 pl-3">Datos Basicos:</p>
                    <div class="col-md-2 col-sm-6">
                        <div class="md-form">
                            <v-text-field
                                name="fabricante"
                                label="Fabricante"
                                value="{{$fabricante}}"
                                disabled
                            ></v-text-field>
                            <input type="hidden" name="fabricante" value={{$fabricante}}>
                        </div>
                    </div>
                    <div class="col-md-2 col-sm-6">
                        <div class="md-form">
                                <v-text-field
                                name="idproducto"
                                label="Id Producto"
                                disabled
                            ></v-text-field>
                            <input type="hidden" name="idproducto" value={{$fabricante}}>
                        </div>
                    </div>
                    <div class="col-md-2 col-sm-6">
                        <div class="md-form">
                            <v-text-field
                                name="categoria"
                                label="Categoria"
                                value="{{  $categorias }}"
                                disabled
                            ></v-text-field>
                            <input type="hidden" name="categoria" value={{$categorias}}>
                        </div>
                    </div>
                    <div class="col-md-2 col-sm-6">
                        <div class="md-form">
                            <v-text-field
                                name="Nombre_Producto"
                                label="Nombre Producto"
                                disabled
                            ></v-text-field>
                            <input type="hidden" name="Nombre_Producto" value={{$fabricante}}>
                        </div>
                    </div>
                    <div class="col-md-2 col-sm-6">
                        <div class="md-form">
                            <v-text-field
                                name="Cantidad"
                                label="Cantidad"
                                type="number"
                                min=0
                                value=0
                            ></v-text-field>
                        </div>
                    </div>
                    <div class="col-md-2 col-sm-6">
                        <div class="md-form">
                            <v-select
                            v-bind:items="{{  $linea }}"
                            label="Linea Producto"
                            item-text="descripcion_linea_producto"
                            item-value="idlinea_producto"
                            return-object
                            name="linea"
                        ></v-select>
                        <input type="hidden" name="linea" value={{$fabricante}}>
                        </div>
                    </div>
                    <div class="col-md-2 col-sm-6">
                        <div class="md-form">
                            <v-select
                            v-bind:items="{{  $categorias }}"
                            label="Unidades"
                            item-text="NombreCategorias"
                            item-value="idcategoria"
                            return-object
                            autocomplete
                            name="NombreCategorias"
                        ></v-select>
                        <input type="hidden" name="NombreCategorias" value={{$fabricante}}>
                        </div>
                    </div>
                    <div class="col-sm-3 col-md-2">
                        <v-menu
                                ref="menuD"
                                lazy
                                :close-on-content-click="false"
                                v-model="menuD"
                                transition="scale-transition"
                                offset-y
                                full-width
                                :nudge-right="40"
                                min-width="290px"
                                :return-value.sync="dateD"
                        >
                            <v-text-field
                                    slot="activator"
                                    label="Fecha Expedicion"
                                    v-model="dateD"
                                    readonly
                                    name="dateE"
                            ></v-text-field>
                            <v-date-picker v-model="dateD" no-title scrollable locale="es-ES" @change="$refs.menuD.save(dateD)">
                            </v-date-picker>
                        </v-menu>
                    </div>
                    <div class="col-sm-3 col-md-2">
                        <v-menu
                                ref="menuH"
                                lazy
                                :close-on-content-click="false"
                                v-model="menuH"
                                transition="scale-transition"
                                offset-y
                                full-width
                                :nudge-right="40"
                                min-width="290px"
                                :return-value.sync="dateH"
                        >
                            <v-text-field
                                    slot="activator"
                                    label="Fecha Vencimiento"
                                    v-model="dateH"
                                    readonly
                                    name="dateV"
                            ></v-text-field>
                            <v-date-picker v-model="dateH" no-title scrollable locale="es-ES" @change="$refs.menuH.save(dateH)">
                            </v-date-picker>
                        </v-menu>
                    </div>
                    @if($categorias=='PROD')
                        <div class="col-md-2 col-sm-6">
                            <div class="md-form">
                                <v-text-field
                                    name="Precio"
                                    label="Precio"
                                    single-line
                                    type="number"
                                    min=0
                                ></v-text-field>
                            </div>
                        </div>
                        <div class="col-md-2 col-sm-6">
                            <div class="md-form">
                                <v-text-field
                                    name="Descuento"
                                    label="Descuento"
                                    single-line
                                    type="number"
                                    min=0
                                    max=99
                                ></v-text-field>
                            </div>
                        </div>
                    @else
                        <div class="col-md-2 col-sm-6">
                            <div class="md-form">
                                <v-text-field
                                    name="Lote"
                                    label="Lote"
                                ></v-text-field>
                            </div>
                        </div>
                    @endif
                </div>

                <div>
                    <button type="submit" class="btn btn-success mt-3">Registrar</button>
                </div>
            </form>-->
        @endif
    </div>
@endsection
