@extends('layouts.main')


@section('titulo')
    Administrar Ordenes
@stop


@section('breadcrumb')
    <li><a href="#">Administrar</a></li>
    <li>Administrar Ordenes</li>
@stop

@section('Content')
    <div class="jumbotron rounded">

            <div class="row">
                <div class="col-md-4 md-form">
                    <v-select
                            :items="{{  $Empresa }}"
                            label="Seleccione Fabricante"
                            required
                            item-text="nombre_completo_razon_social"
                            item-value="idPersona"
                            return-object
                            v-model="Fabrica"
                            single-line
                            id="empresa"
                            dense
                    ></v-select>
                </div>
                <div class="col-2">
                    <button class="btn siif btn-w float-right " @click="OrdenFabricante(Fabrica.idPersona)" >Seleccionar</button>
                </div>
                <div class="col-md-4 md-form">
                    <v-progress-circular :indeterminate="loading" color="primary" v-if="loading"></v-progress-circular>
                </div>
            </div>
        <div  id="categorias" style="display: none">
            <v-data-table
                    :headers="headers_tablas.ordenes"
                    :items="mayorista"
                    class="table-bordered table-responsive-md"
                    no-data-text="Sin Orden"
                    :loading="loadermuestras"
                    rows-per-page-text="Registros por pagina"
            >
                <v-progress-linear slot="progress" :indeterminate="loadermuestras"></v-progress-linear>
                <template slot="items" slot-scope="props">
                    <td>@{{ props.item.nOrden}}</td>
                    <td>@{{ props.item.persona}}</td>
                    <td >@{{ props.item.cliente}}</td>
                    <td >@{{ props.item.fecha }}</td>
                    <td >@{{ props.item.mayoristas[0].nombre}}</td>
                    <td align="right">@{{ props.item.cantidad}}</td>
                    <td align="right">@{{ props.item.Total.toLocaleString()}}</td>
                    <td >@{{ props.item.estatus}}</td>
                    <td class="justify-center layout px-0">
                        <v-btn icon class="mx-0" @click="deleteOrden(props.item)">
                            <v-icon color="accent" >fa-ban</v-icon>
                        </v-btn>
                    </td>
                </template>
            </v-data-table>
        </div>



    </div>
@endsection