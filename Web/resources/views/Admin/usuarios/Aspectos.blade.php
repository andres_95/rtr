@extends('layouts.main')


@section('titulo')
    Administrar Persona
@stop


@section('breadcrumb')
    <li><a href="#">Administrar</a></li>
    <li>Administrar Persona - {{$T}}</li>
@stop

@section('Content')
    <div class="jumbotron rounded">
        <personas :valores="{{$tipo}}" :tipo="'{{$T}}'"></personas>
    </div>


@endsection