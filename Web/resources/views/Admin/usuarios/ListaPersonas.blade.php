@extends('layouts.main')

@section('titulo')
    Administrar Personas
@stop


@section('breadcrumb')
    <li>Administrar</li>
    <li><a href="{{route('Personas')}}">Administrar Personas</a></li>
    <li>Lista de Personas</li>
@stop

@section('Content')
    <div class="jumbotron rounded">
        <div class="row mt-3 ">
            <p class="h3-responsive font-bold w-100 ml-3" >Tu lista de {{$DescripGrupopersona->descripciongrupoPersona}}</p>
            <p class="font-bold ml-3">Fabricante: <span class="font-weight-light">{{$Empresa}}</span> </p>
            <div class="col-12">
                <listatodaspersona token="{{csrf_token()}}" categ="{{$grupopersona}}" personas="{{$listaPersona}}" fabricante="{{$Empresa}}"></listatodaspersona>
            </div>
        </div>
    </div>
@endsection