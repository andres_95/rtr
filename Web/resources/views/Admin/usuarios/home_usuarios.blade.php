@extends('layouts.main')


@section('titulo')
    Administrar Personas
@stop


@section('breadcrumb')
    <li>Administrar</li>
    <li>Administrar Personas</li>
@stop



@section('Content')
    <div class="jumbotron rounded">
        <form id="new" action="{{route('newU')}}" method="post">
            {{csrf_field()}}
            <div class="row">
                <div class="col-md-4 md-form">
                    <v-select
                        :items="{{  $Empresa }}"
                        label="Seleccione Fabricante"
                        required
                        item-text="nombre_completo_razon_social"
                        item-value="idPersona"
                        id="empresa"
                        return-object
                        autocomplete
                        name=incidente
                    ></v-select>
                </div>
                <input id="tipo" name="tipo" type="text" hidden>
            </div>
        </form>
        <form id="list" action="" method="post">
            {{csrf_field()}}
            <input id="fab" name="fabrica" type="text" hidden>
            <input id="type" name="tipo" type="text" hidden>
        </form>
        <div class="row mt-3">
            <div class="col-md-4 col-sm-4 col-xs-12">
                <div class="dashboard-stat blue">
                    <div class="visual">
                            <i class="fas fa-users fa-3x" style="color: rgb(218, 230, 227); fill-opacity: 0.3;"></i>
                    </div>
                    <div class="details">
                        <div class="number h3-responsive">
                            RFV
                        </div>
                        <div class="desc">
                            
                        </div>
                    </div>
                    <a class="more" onclick="document.getElementById('tipo').value='RFV';
                        document.getElementById('new').submit()">
                        Agregar<i class="m-icon-swapright m-icon-white fa fa-angle-right"></i>
                    </a>
                    <br>
                    <a class="more" onclick="document.getElementById('type').value='RFV';
                    document.getElementById('fab').value=document.getElementById('empresa').value;
                    document.getElementById('list').submit()">
                        Listar Todos <i class="m-icon-swapright m-icon-white fa fa-angle-right"></i>
                    </a>
                </div>
            </div>
            <div class="col-md-4 col-sm-4 col-xs-12">
                <div class="dashboard-stat blue">
                    <div class="visual">
                        <i class="fas fa-users fa-3x" style="color: rgb(218, 230, 227); fill-opacity: 0.3;"></i>
                    </div>
                    <div class="details">
                        <div class="number h3-responsive">
                            Supervisor
                        </div>
                        <div class="desc">
                        
                        </div>
                    </div>
                    <a class="more" onclick="document.getElementById('tipo').value='SUP';
                        document.getElementById('new').submit()">
                        Agregar<i class="m-icon-swapright m-icon-white fa fa-angle-right"></i>
                    </a>
                    <br>
                    <a class="more" onclick="document.getElementById('type').value='SUP';
                        document.getElementById('fab').value=document.getElementById('empresa').value;
                        document.getElementById('list').submit()">
                        Listar Todos <i class="m-icon-swapright m-icon-white fa fa-angle-right"></i>
                    </a>
                </div>
            </div>
        </div>
    </div>
@endsection