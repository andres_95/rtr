@extends('layouts.main')


@section('titulo')
    Agregar Usuarios
@stop


@section('breadcrumb')
    <li>Administrar</li>
    <li><a href="{{route('Personas')}}">Administrar Personas</a></li>
    <li>Agregar Operador</li>
@stop

@section('Content')
    <div class="jumbotron">
        <form action="{{route('storeO')}}" method="post">
          {{csrf_field()}}
          <div class="row mt-1 p-1 border-dark border">
                    <p class="h6-responsive mt-1 font-bold w-100 pl-3">Datos del Operador:</p>
                
                    <div class="col-md-1 col-sm-6 ">
                        <div class="md-form">
                            <v-text-field
                                name="id"
                                label="Cod."
                                single-line
                            ></v-text-field>
                        </div>
                    </div>
                        <div class="col-md-2 col-sm-6 ">
                            <div class="md-form">
                                <v-text-field
                                    name="nombre"
                                    label="nombre"
                                    single-line
                                ></v-text-field>
                            </div>
                      </div>
                     
                      <div class="col-md-2 col-sm-6 ">
                        <div class="md-form">
                            <v-text-field
                                name="apellido"
                                label="Apellidos"
                                single-line
                            ></v-text-field>
                        </div>
                    </div>

                    <div class="col-md-2 col-sm-6 ">
                        <div class="md-form">
                            <v-text-field
                                name="alias"
                                label="Alias"
                                single-line
                            ></v-text-field>
                        </div>
                    </div>

                    <div class="col-md-3 col-sm-6 ">
                        <div class="md-form">
                            <v-text-field
                                name="identidad"
                                label="Documento de identidad"
                                single-line
                            ></v-text-field>
                        </div>
                    </div>

                   
                    
                    <div class="col-md-4 col-sm-6 ">
                  
                        <div class="md-form">
                        
                            <v-text-field
                                type="date"
                                name="expiracion"
                                label="fecha de expiracion"
                                single-line
                            ></v-text-field>
                        </div>
                    </div>

                    <div class="col-md-4 col-sm-6 ">
                        <div class="md-form">
                          
                            <v-text-field
                                name="contrato"
                                label="referencia de contrato"
                                single-line
                            ></v-text-field>
                        </div>
                    </div>
                   
                
            

            </div>
            <div class="row mt-1 p-1 border-dark border">
            <p class="h6-responsive mt-1 font-bold w-100 pl-3">Datos de contacto:</p>
                 <div class="col-md-4 col-sm-6 ">
                        <div class="md-form">
                            <v-text-field
                                name="email"
                                label="Email de contacto"
                                single-line
                            ></v-text-field>
                        </div>
                    </div>

                    

                    <div class="col-md-4 col-sm-6 ">
                        <div class="md-form">
                            <v-text-field
                                name="pcontacto"
                                label="persona de contacto"
                                single-line
                            ></v-text-field>
                        </div>
                    </div>
                   
                    <div class="col-md-4 col-sm-6 ">
                        <div class="md-form">
                            <v-text-field
                                type="number"
                                name="telefono"
                                label="telefono"
                                single-line
                            ></v-text-field>
                        </div>
                    </div>

                    <div class="col-md-5 col-sm-6 ">
                        <div class="md-form">
                            <v-text-field
                                textarea
                                rows="400"
                                counter="100"
                                name="Direccion"
                                label="Direccion de domicilio"
                                single-line
                            ></v-text-field>
                        </div>
                    </div>
                </div>
                <div class="row mt-3 p-1 border border-dark">
                <p class="h6-responsive mt-1 font-bold w-100 pl-3">Ubicacion</p>
                <div class="col-md-2 col-sm-6">
                    <div class="md-form">
                        <v-select
                        :items="{{  $paises }}"
                        label="Pais"
                        item-text="nombrePais"
                        item-value="idpais"
                        v-model="nombre"
                        return-object
                        autocomplete
                        dense
                        @focusout="buscaestado()"
                        ></v-select>
                    </div>
                    <input type="hidden" name="pais" v-model="nombre.idpais">
                </div>
                <div class="col-md-2 col-sm-6">
                    <div class="md-form">
                        <v-select
                        :items="mayoristas"
                        label="Estado"
                        item-text="nombreCiudad"
                        item-value="idestado"
                        return-object
                        autocomplete
                        dense
                        :disabled="!mayoristas"
                        v-model="comentario"
                        @focusout="buscaciudad()"
                        ></v-select>
                        <input type="hidden" name="estado" v-model="comentario.idestado">
                    </div>
                </div>
                <div class="col-md-2 col-sm-6">
                    <div class="md-form">
                        <v-select
                        :items="mayorista"
                        label="Ciudad"
                        item-text="nombreCiudad"
                        item-value="idCiudad"
                        return-object
                        autocomplete
                        dense
                        :disabled="!mayorista"
                        v-model="incidente"
                        ></v-select>
                        <input type="hidden" name="ciudad" v-model="incidente.idCiudad">
                    </div>
                </div>
            </div>

            <div  align="right">
                    <button type="submit" class="btn btn-w siif">Registrar</button>
                </div>
        </form>
        

    </div>
@endsection


@section('script')

    
@endsection