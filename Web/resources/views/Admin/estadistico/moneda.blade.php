@extends('layouts.main')


@section('titulo')
    Administrar
@stop


@section('breadcrumb')
    <li><a href="#">Administrar</a></li>
    <li>Administrar - {{$T}}</li>
@stop

@section('Content')
    <div class="jumbotron rounded">

        <monedas :Empresa="{{$Empresa}}" tipo="{{$T}}" fabrica="{{$fabrica}}"></monedas>

    </div>
@endsection