@extends('layouts.main')


@section('titulo')
    Administrar
@stop


@section('breadcrumb')
    <li><a href="#">Administrar</a></li>
    <li>Administrar - {{$T}}</li>
@stop

@section('Content')
<div class="jumbotron rounded">

    <sf tipo="{{$T}}" :valores="{{$valor}}"></sf>

</div>
@endsection