@extends('layouts.main')


@section('titulo')
    Productos Promocionados
@stop


@section('breadcrumb')
    <li><a href="#">Administrar</a></li>
    <li>Productos promocionados</li>
@stop



@section('Content')
<div class="jumbotron rounded">
        <div class="row mt-3 ">
          <div class="col-4">
           <v-select
          
             label="Selecciona fabricante"
            ></v-select>
          </div>
          <div class="col-4">
           <v-select
              :items="{{$productos}}"
              label="Selecciona producto"
              item-text="nombre_producto"
              item-value="idproducto"
            ></v-select>
          </div>
          <div class="col-4">
           <v-select
              :items="{{$personas}}"
              label="Selecciona mayorista"
              item-text="nombre_completo_razon_social"
              item-value="idPersona"
            ></v-select> 
          </div>
          
        </div>
        <div class="row px-8">
        <v-data-table
                            :headers="headers_tablas.promociones"
                            :items="promociones"
                            class="table-hover table-bordered table-responsive-md"
                            no-data-text="Sin Promociones"
                            :loading="loadermuestras"
                    >
                        <v-progress-linear slot="progress" :indeterminate="loadermuestras"></v-progress-linear>
                        <template slot="items" slot-scope="props">
                          <!--  <td>@{{ props.item.nOrden}}</td>
                            <td >@{{ props.item.persona}}</td>
                            <td >@{{ props.item.cliente}}</td>
                            <td >@{{ props.item.mayoristas[0].nombre}}</td>
                            <td >@{{ props.item.fecha}}</td>-->
                            <td class="justify-center layout">
                                <v-btn class="mx-0 p-0 btn-w" data-toggle="modal" data-target="#myModal" color="siif" @click="detalle(props.item.nOrden,props.item.fecha,props.item.mayoristas[0].nombre,props.item.cliente)">
                                    Ver Detalle
                                </v-btn>
                            </td>
                        </template>
                    </v-data-table>
        </div>
    </div>
@endsection