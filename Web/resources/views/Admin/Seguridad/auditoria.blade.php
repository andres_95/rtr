@extends('layouts.main')


@section('titulo')
    Administrar
@stop


@section('breadcrumb')
    <li><a href="#">Administrar</a></li>
    <li>Administrar - {{$T}}</li>
@stop

@section('Content')
    <div class="jumbotron rounded">

        <auditorias :Empresa="{{$Empresa}}" tipo="{{$T}}" fabrica="{{$fabrica}}"></auditorias>

    </div>
@endsection