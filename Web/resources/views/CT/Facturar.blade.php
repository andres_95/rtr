@extends('layouts.main')


@section('titulo')
    Facturar
@stop


@section('breadcrumb')
    <li><a href="#">Centro de Transferencias (CT)</a></li>
    <li>Conciliar Facturas</li>
@stop


@section('Content')
    <div class="jumbotron p-4">
        <div v-if="loading">
            <v-progress-linear :indeterminate="loading"></v-progress-linear>
            <p class="text-center">Buscando...</p>
        </div>
        <v-alert type="error" dismissible v-model="alertordenbuscar">
            No existe orden con ese numero
        </v-alert>
        <v-alert type="warning" dismissible v-model="alertorden">
            Error : Complete todos los campos
        </v-alert>
        <div class="border border-dark">
            <div class="row px-4 mt-2">
                <div class="col-sm-8 col-md-4 md-form">
                    <v-select
                        label="Nro Orden"
                        :items="{{$Corden}}"
                        :rules="[() => codigo != '' || 'Seleccione un Codigo de Orden']"
                        item-text="idorden"
                        item-value="idorden"
                        return-object
                        autocomplete
                        v-model=codigo
                    ></v-select>
                </div>
                <div class="col-sm-4 col-md-3 md-form">
                    <button  class="btn siif btn-w btn-conciliar" 
                        @click="buscaorden(codigo.idorden)">Buscar</button>
                </div>
            </div>
            <div class="row px-4 mt-2 mb-2">
                <div class="col-sm-3 col-md-3 md-form">
                    <v-text-field
                        label="N° de Factura"
                        id="n-factura"
                        v-model=nombre
                        :disabled = "findorden"
                        required
                        :rules="[() => nombre != '' || 'Nº de factura es requerido']"

                    ></v-text-field>
                </div>
                <div class="col-3 md-form">
                    <v-text-field
                        label="Almacen de despacho"
                        id="almacen"
                        v-model=comentario
                        :disabled = "findorden"
                        required
                        :rules="[() => comentario != '' || 'Debe haber un almacen de despacho']"
                    ></v-text-field>
                </div>
                <div class="col-3 md-form">
                    <v-select
                            label="Forma de pago"
                            autocomplete
                            required
                            :rules="[() => formaPago != '' || 'Seleccione una fomra de pago']"
                            :items="{{$formapago}}"
                            return-object
                            item-text="descripcion"
                            item-value="idformaPago"
                            v-model="formaPago"
                            dense
                            :disabled = "findorden"
                    ></v-select>
                </div>
                <div class="col-3 md-form">
                    <v-menu
                        ref="menuD"
                        lazy
                        :close-on-content-click="false"
                        v-model="menuD"
                        transition="scale-transition"
                        offset-y
                        full-width
                        :nudge-right="40"
                        min-width="290px"
                        :return-value.sync="dateD"
                        :disabled = "findorden"
                    >
                        <v-text-field
                                slot="activator"
                                label="Fecha"
                                v-model="dateD"
                                prepend-icon="event"
                                readonly
                                name="dateD"
                        ></v-text-field>
                        <v-date-picker v-model="dateD" no-title scrollable locale="es-ES" @change="$refs.menuD.save(dateD)">
                        </v-date-picker>
                    </v-menu>
                </div>
            </div>
            <div class="row px-4 my-3" v-if="conciliar">
                <div class="col-sm-6 col-md-3">
                    <div class="font-bold"><span>Cliente:</span> @{{cliente}}</div>
                </div>
                <div class="col-sm-6 col-md-3">
                    <p class="font-bold"><span>RFV:</span> @{{rfv}}</p>
                </div>
                <div class="col-sm-6 col-md-3">
                    <p class="font-bold"><span>Mayorista:</span> @{{especialidad}}</p>
                </div>
                <div class="col-sm-6 col-md-3">
                    <p class="font-bold"><span>Registrada por:</span> @{{searchmuestras}}</p>
                </div>
            </div>    
        </div>
        <br>
        <div class="row">
            <div class="col-12">
                <v-data-table
                        :headers="headers_tablas.facturar"
                        :items="facturaP"
                        class="table-bordered table-responsive-sm"
                        no-data-text="Sin Productos"
                        :loading="loadermuestras"
                        :rows-per-page-items = '[15,25,50,{"text":"All","value":-1}]'
                >
                    <v-progress-linear slot="progress" :indeterminate="loadermuestras"></v-progress-linear>
                    <template slot="items" slot-scope="props">
                        <td class=" px-2 text-xs-center">@{{ props.item.id}}</td>
                        <td class=" px-2 ">@{{ props.item.nombre}}</td>
                        <td class=" px-2" align="right">@{{ props.item.cantidad}}</td>
                        <td class=" px-2" align="right">
                            <v-text-field
                                    v-model=props.item.precio
                                    type="number"
                                    class="text-xs-center"
                                    min=1
                            ></v-text-field>
                        </td>
                        <td class=" px-2" align="right">@{{calcularcosto(props.item.precio,props.item.cantidad,props.item).toLocaleString()}}</td>
                        <td class=" px-2" align="right">
                            <v-text-field
                                    v-model=props.item.descuento
                                    type="number"
                                    min=0
                                    max=100
                            ></v-text-field>
                        </td>
                        <td class=" px-2" align="right">
                            <v-text-field
                                    v-model=props.item.conciliada
                                    type="number"
                                    class="text-xs-center"
                                    min=0
                                    :max=props.item.cantidad
                            ></v-text-field>
                        </td>
                        <td class=" px-2" align="right">@{{calcularfaltante(props.item.cantidad,props.item.conciliada,props.item)}}</td>
                        <td class=" px-2" align="right">@{{calcularsubtotal(props.item.precio,props.item.conciliada,props.item.descuento,props.item).toLocaleString()}}</td>
                    </template>
                </v-data-table>
                <div>
                    <button v-if="conciliar" class="btn siif btn-w btn-conciliar float-right" @click="validarconciliar()" id="conciliar" :disabled="!comentario || !nombre || !formaPago || !conciliar">Conciliar</button>
                </div>
            </div>

        </div>

    </div>

    <div v-if="modal_conciliar"class="modal fade bs-example-modal-lg" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header accent">
                    <h4 class="modal-title" id="myModalLabel" style="color:white;">Orden</h4>
                    <button type="button" class="close btn-w" data-dismiss="modal" @click="refrescar()" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                </div>
                <div class="modal-body" v-if="rfv == 'pasa'">
                    <div class="row">
                        <div class="col-12">
                            <!--Table-->
                            <table class="table table-bordered table-hover ">
                                <tr>
                                    <td style="width:50%; font-weight: 700;">EMPRESA</td>
                                    <td style="width:35%; font-weight: 700;">MAYORISTA</td>
                                    <td style="width:10%; font-weight: 700;">DESCUENTO</td>
                                </tr>
                                <tr>
                                    <td style="width:50%; ">
                                        {{Auth::user()->nombre_completo_razon_social}}
                                    </td>
                                    <td style="width:35%; ">
                                        @{{mayoristas.nombre}}
                                    </td>
                                    <td style="width:10%; ">
                                        @{{mayoristas.descuento}}
                                    </td>
                                </tr>
                            </table>
                            <br>
                            <table class="table table-bordered table-hover ">
                                <tr>
                                    <td style="width:45%; font-weight: 700;">NUMERO DE ORDEN</td>
                                    <td style="width:45%; font-weight: 700;">FECHA</td>
                                </tr>
                                <tr>
                                    <td style="width:45%; ">
                                        @{{codigo}}
                                    </td>
                                    <td style="width:45%; ">
                                        @{{date}}
                                    </td>
                                </tr>
                            </table>
                            <br>
                            <table class="table table-bordered table-hover ">
                                <tr align="center">
                                    <td style="width: 14%; font-weight: 700;">CODIGO</td>
                                    <td style="width: 48%; font-weight: 700;">DESCRIPCION</td>
                                    <td style="width: 7%; font-weight: 700;">DESCUENTO</td>
                                    <td style="width: 7%; font-weight: 700;">CANT.</td>
                                    <td style="width: 14%; font-weight: 700;">PRECIO</td>

                                </tr>
                                <tr v-for="(productos,index) in mayorista">
                                    <td style="width: 14%;">@{{productos.id}}</td>
                                    <td style="width: 48%;">@{{productos.nombre}}</td>
                                    <td align="right" style="width: 7%;">@{{productos.descuento}}</td>
                                    <td align="right" style="width: 7%;">@{{productos.conciliada}}</td>
                                    <td align="right" style="width: 14%;">@{{productos.precio.toLocaleString()}}</td>
                                </tr>
                            </table>
                            <br>
                            <table class="table table-bordered table-hover ">
                                <tr>
                                    <td style="width: 45%; text-align: center;"></td>
                                    <td class="accent" style="width: 45%; text-align: center; font-weight: 700; color:white;">TOTAL</td>
                                </tr>
                                <tr>
                                    <td style="width: 45%; text-align: center;"></td>
                                    <td style="width: 45%; text-align: center;">@{{total.toLocaleString()}}</td>
                                </tr>
                            </table>
                            <!--Table-->
                        </div>
                    </div>
                    <div id="loader" style="position: absolute;	text-align: center;	top: 55px;	width: 100%;display:none;"></div><!-- Carga gif animado -->
                    <div class="outer_div" ></div><!-- Datos ajax Final -->
                </div>
                <div v-else align="center">
                    <h5 class="modal-title"> @{{rfv}}</h5>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn siif btn-w"  data-dismiss="modal" @click="refrescar()" >Cerrar</button>
                </div>
            </div>
        </div>
    </div>



@endsection

@section('script')

@endsection