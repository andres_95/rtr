@extends('layouts.main')


@section('titulo')
    Seguimiento
@stop


@section('breadcrumb')
    <li><a href="{{route('home')}}">
    @if((Auth::user()->idperfil==4 || Auth::user()->idperfil==2) && $value != 'SIIF')
        {{$value}} -
    @endif
    Centro de Transferencias (CT)</a></li>
    <li>Seguimiento de Pedidos</li>
@stop


@section('Content')
    <div class="jumbotron">
        <div class="row" >
            
            <div class="col-4">
                <v-select
                        :items="{{$estatus}}"
                        v-model=nombre
                        label="Selecciona Estatus"
                        item-text="descripcion"
                        item-value="idestatus"
                        return-object
                        single-line
                ></v-select>
            </div>
            <div class="col-2">
                <button class="btn siif btn-w float-right " @click="status(nombre.idestatus)" >Seleccionar</button>
            </div>
            <div class="col-12">
                <div class="px-4">

                    <v-data-table
                            :headers="headers_tablas.seguimientos"
                            :items="muestrasfinal"
                            class="table-hover table-bordered table-responsive-md"
                            no-data-text="Sin Ordenes"
                            :loading="loadermuestras"
                            rows-per-page-text="Registros por pagina"
                    >
                        <v-progress-linear slot="progress" :indeterminate="loadermuestras"></v-progress-linear>
                        <template slot="items" slot-scope="props">
                            <td>@{{ props.item.nOrden}}</td>
                            <td >@{{ props.item.persona}}</td>
                            <td >@{{ props.item.cliente}}</td>
                            <td >@{{ props.item.mayoristas}}</td>
                            <td >@{{ props.item.fecha}}</td>
                            <td class="justify-center layout">
                                <v-btn class="mx-0 p-0 btn-w" data-toggle="modal" data-target="#myModal" color="siif" @click="detalle(props.item.nOrden,props.item.fecha,props.item.cliente)">
                                    Ver Detalle
                                </v-btn>
                            </td>
                        </template>
                    </v-data-table>

                </div>
                <hr class="my-0">
                <!--Bottom Table UI-->
            </div>
        </div>
    </div>

    <div class="modal fade bs-example-modal-lg" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header accent">
                    <h4 class="modal-title" id="myModalLabel" style="color:white;">Orden</h4>
                    <button type="button" class="close btn-w" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-12">
                            <!--Table-->
                            <table class="table table-bordered table-hover ">
                                <tr>
                                    <td style="width:50%; font-weight: 700;">EMPRESA</td>
                                    <td style="width:35%; font-weight: 700;">MAYORISTA</td>
                                    <td style="width:10%; font-weight: 700;">DESCUENTO</td>
                                </tr>
                                <tr>
                                    <td style="width:50%; ">
                                        @{{cliente}}
                                    </td>
                                    <td style="width:35%; ">
                                        @{{comentario.nombre}}
                                    </td>
                                    <td style="width:10%; ">
                                        @{{comentario.descuento}}
                                    </td>
                                </tr>
                            </table>
                            <br>
                            <table class="table table-bordered table-hover ">
                                <tr>
                                    <td style="width:45%; font-weight: 700; ">NUMERO DE ORDEN</td>
                                    <td style="width:45%; font-weight: 700; ">FECHA</td>
                                </tr>
                                <tr>
                                    <td style="width:45%; ">
                                       @{{codigo}}
                                    </td>
                                    <td style="width:45%; ">
                                        @{{date}}
                                    </td>
                                </tr>
                            </table>
                            <br>
                            <table class="table table-bordered table-hover ">
                                <tr align="center">
                                    <td style="width: 14%; font-weight: 700;">CODIGO</td>
                                    <td style="width: 48%; font-weight: 700;">DESCRIPCION</td>
                                    <td style="width: 7%; font-weight: 700;">DESCUENTO</td>
                                    <td style="width: 7%; font-weight: 700;">CANT.</td>
                                    <td style="width: 14%; font-weight: 700;">PRECIO</td>

                                </tr>
                                <tr v-for="(productos,index) in mayorista">
                                    <td style="width: 14%;">@{{productos.id}}</td>
                                    <td style="width: 48%;">@{{productos.nombre}}</td>
                                    <td align="right" style="width: 7%;">@{{productos.descuento}}</td>
                                    <td align="right" style="width: 7%;">@{{productos.conciliada}}</td>
                                    <td align="right" style="width: 14%;">@{{productos.precio.toLocaleString()}}</td>
                                </tr>
                            </table>
                            <br>
                            <table class="table table-bordered table-hover ">
                                <tr>
                                    <td style="width: 45%; text-align: center; font-weight: 700;">COMENTARIO</td>
                                    <td class="accent" style="width: 45%; text-align: center; font-weight: 700; color:white;">TOTAL</td>
                                </tr>
                                <tr>
                                    <td style="width: 45%; text-align: center;">@{{confirm?confirm:"S/C"}}</td>
                                    <td style="width: 45%; text-align: center;">@{{total.toLocaleString()}}</td>
                                </tr>
                            </table>
                            <!--Table-->
                        </div>
                    </div>
                    <div id="loader" style="position: absolute; text-align: center; top: 55px;  width: 100%;display:none;"></div><!-- Carga gif animado -->
                    <div class="outer_div" ></div><!-- Datos ajax Final -->
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn siif btn-w"  data-dismiss="modal">Cerrar</button>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('script')

@endsection