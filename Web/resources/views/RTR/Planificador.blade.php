@extends('layouts.main')



@section('titulo')
    Planificador
@stop


@section('breadcrumb')
    <li><a href="#">Real Time Report (RTR)</a></li>
    <li>Planificador</li>
@stop



@section('Content')
    <div class="jumbotron">
        <!--<button class="btn btn-ct btn-w siif waves-effect waves-light" @click="agenda()" data-toggle="modal" data-target="#myModal">Agendar&nbsp;<i class="fas fa-pencil-alt"></i></button>-->
        <div class="row">
            <div class="col-sm-12">
                <calendar rfv={{Auth::user()->idPersona}} fabrica={{Auth::user()->idFabricante}} tipo={{Auth::user()->idgrupo_persona}}></calendar>
            </div>
        </div>
    </div>

    <div class="modal fade bs-example-modal-lg" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content" style="width: 110%;">
                <div class="modal-header">
                    <h4 class="modal-title" id="myModalLabel" >Agendar Citas</h4>
                    <v-spacer></v-spacer>
                    <v-text-field
                            append-icon="search"
                            label="Buscar"
                            single-line
                            hide-details
                            v-model="searchmuestras"
                    ></v-text-field>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                </div>
                <div class="modal-body">
                    <div v-if="loading">
                        <v-progress-linear :indeterminate="loading"></v-progress-linear>
                        <p class="text-center" style="display: none;">@{{ mensajes.procesando }}</p>
                    </div>
                    <v-alert type="success" dismissible v-model="alertreport" style="display: none;">
                        <p class="text-center">@{{ mensajes.citaexito }}</p>
                    </v-alert>
                    <v-alert type="error" dismissible v-model="alertreporterror" style="display: none;">
                        <p class="text-center">@{{ mensajes.error }}</p>
                    </v-alert>
                    <div class="row">
                        <div class="col-12">
                            <v-data-table
                            :headers="headers_tablas.agenda"
                            :items="clientes"
                            class="table-bordered table-responsive-md"
                            no-data-text = "buscando..."
                            :search="searchmuestras"
                            :loading="loadermuestras"
                            >
                                <v-progress-linear slot="progress" :indeterminate="loadermuestras"></v-progress-linear>
                                <template slot="items" slot-scope="props">
                                    <td>@{{ props.item.nombre_completo_razon_social}}</td>
                                    <td >
                                        <input type="time" v-model="props.item.hora">
                                    </td>
                                    <td >
                                        <input type="date" v-model="props.item.fecha">
                                    </td>
                                    <td class="justify-center layout">
                                        <v-btn flat icon class="mx-0 p-0" color="blue" @click="agendarcita(props.item)">
                                            <v-icon color="accent">add</v-icon>
                                        </v-btn>
                                    </td>
                                </template>
                                <v-alert slot="no-results" :value="true" color="error" icon="warning">
                                    Tu busqueda para "@{{ searchmuestras }}" no tuvo resultados.
                                </v-alert>
                            </v-data-table>
                        </div>
                    </div>
                    <div id="loader" style="position: absolute;	text-align: center;	top: 55px;	width: 100%;display:none;"></div><!-- Carga gif animado -->
                    <div class="outer_div" ></div><!-- Datos ajax Final -->
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-ct btn-w siif"  data-dismiss="modal"  @click="refrescar()">Cerrar</button>
                </div>
            </div>
        </div>
    </div>


@endsection