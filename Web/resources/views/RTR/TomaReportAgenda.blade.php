@extends('layouts.main')


@section('titulo')
    Reportes
@stop


@section('breadcrumb')
    <li><a href="#">Real Time Report (RTR)
        </a></li>
    <li><a href="{{route('nuevo_reporte')}}">Toma de Reporte
        </a></li>
    <li>Visitas</li>
@stop

@section('Content')
    <div class="jumbotron rounded">
        <div class="row mt-3">
            <p class="h4-responsive font-bold w-100 " style="padding-left: 15px">Citas Agendadas</p>
            <div class="col-12">
            <clienteconcitas token="{{csrf_token()}}" :clientes="{{$result}}"></clienteconcitas>
            </div>
        </div>
    </div>

@endsection