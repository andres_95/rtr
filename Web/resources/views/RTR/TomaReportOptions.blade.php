@extends('layouts.main')


@section('titulo')
    Reportes
@stop


@section('breadcrumb')
    <li><a href="#">Real Time Report (RTR)
        </a></li>
    <li>Toma de Reporte</li>
@stop

@section('Content')
    <div class="jumbotron rounded">
        <div class="row mt-3">
            <div class="col-md-6 col-sm-6 col-xs-12">
                <div class="dashboard-stat dashboard-stat-blue">
                    <div class="dashboard-stat__visual">
                            <i class="fas fa-edit fa-3x" style="color: rgb(218, 230, 227); fill-opacity: 0.3;"></i>
                    </div>
                    <div class="dashboard-stat__details">
                        <div class="dashboard-stat__details__number h3-responsive">
                            Crear Nuevo Reporte
                        </div>
                        <div class="dashboard-stat__details__desc">
                            
                        </div>
                    </div>
                    <a class="dashboard-stat__more" href="{{route('Reportar')}}">
                        Ir <i class="m-icon-swapright m-icon-white fa fa-angle-right"></i>
                    </a>
                </div>
            </div>
            <div class="col-md-6 col-sm-6 col-xs-12">
                <div class="dashboard-stat dashboard-stat-blue">
                    <div class="dashboard-stat__visual">
                        <i class="fas fa-clipboard-list fa-3x" style="color: rgb(218, 230, 227); fill-opacity: 0.3;"></i>
                    </div>
                    <div class="dashboard-stat__details">
                        <div class="dashboard-stat__details__number h3-responsive">
                            Reportar Actividad de la Agenda
                        </div>
                        <div class="dashboard-stat__details__desc">
                           
                        </div>
                    </div>
                    <a class="dashboard-stat__more" href="{{route('ReportarAgenda')}}">
                        Ir <i class="m-icon-swapright m-icon-white fa fa-angle-right"></i>
                    </a>
                </div>
            </div>
        </div>
    </div>


@endsection