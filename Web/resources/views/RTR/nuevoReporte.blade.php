@extends('layouts.main')


@section('titulo')
    Reportes
@stop


@section('breadcrumb')
    <li><a href="#">Real Time Report (RTR)
        </a></li>
    <li><a href="{{route('nuevo_reporte')}}">Toma de Reporte
        </a></li>
    <li>Nuevo Reporte</li>
@stop

@section('Content')
    <div class="jumbotron py-3">
        <nuevoreporte :actividades="{{$actividades}}" :incidentes="{{$incidentes}}" rfv="{{Auth::user()->nombre_completo_razon_social}}"></nuevoreporte>
    </div>


@endsection