@extends('layouts.main')





@section('Content')

<div class="row justify-content-center">
    <div class="col-8">
        <div class="card">
            <div class="card-header blue">
                <div class="card-title">
                    <div class="row">
                        <div class="col-12">
                            <p class="h2-responsive text-center text-white mb-0">No autorizado</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="card-body">
                <div class="row mt-3 justify-content-center">
                        <i class="fas fa-exclamation-triangle fa-7x text-grey grey-text text-center"></i>
                </div>
                <div class="row mt-3">
                    <div class="col-12">
                          <p class="h4-responsive text-center"> Disculpe, usted no tiene permisos para realizar esta operación</p>
                
                    </div>
                  </div>
            </div>
        </div>
    </div>
</div>
@endsection