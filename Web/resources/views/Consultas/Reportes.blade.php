@extends('layouts.main')


@section('titulo')
    Inicio
@stop



@section('breadcrumb')
    <li><a href="{{route('home')}}">
    @if((Auth::user()->idperfil==4 || Auth::user()->idperfil==2) && $fabrica != null)
        {{$fabrica}} -
    @endif
    SIIF</a></li>
    <li>Consultas - reportes</li>
@stop

@section('Content')
    <div class="jumbotron rounded">
        <div class="alert alert-info d-block" role="alert">
            
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            <h4 class="alert-heading font-bold" >Información</h4>
            <p style="line-height: 1.5; font-size: 1rem">Las consultas que pueden ser realizadas corresponden a reportes de visitas realizadas, ordenes y sus facturas, se deben consultar estas por separado.</p>
            <p style="line-height: 1.5; font-size: 1rem">Si lo desea puede seleccionar un rango de fecha para las cosultas asi como los distintos filtros segun el tipo de consulta</p>
            <hr class="my-3">
            <p class="mb-0"><span class="font-bold">¡Importante!</span> De no seleccionar un filtro la consulta sera generica para dicho filtro</p>
        </div>
        <reporte-basico 
            :rankings="{{  $ranking }}" 
            :especialidades="{{ $especialidades }}"
            :actividades="{{ $actividades }}"
            :eventos="{{   $incidentes }}"
            :zonas="{{   $zona }}"
            :rutas="{{   $brick }}"
            :estatusorden="{{ $estatus }}"
            :mayoristas="{{  $Mayorista }}"
            @if(Auth::user()->idgrupo_persona=='RFV')
            :rfv="{{Auth::user()}}"
            @else
            :rfv="{{$RFV}}"
            @endif
        ></reporte-basico>
    </div>
@endsection