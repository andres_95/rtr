@extends('layouts.main')

@section('titulo')
    Consulta Gerencial
@stop


@section('breadcrumb')
    <li><a href="{{route('home')}}">
    @if((Auth::user()->idperfil==4 || Auth::user()->idperfil==2) && $fabrica != null)
        {{$fabrica}} -
    @endif
    SIIF</a></li>
    <li>Consultas - Gerencial</li>
@stop

@section('Content')
    <div class="jumbotron rounded py-1">
        <gerencial
            :zonas="{{$zona}}"
            :rutas="{{$brick}}"
            :sups="{{$sup}}"
            :rfvs="{{$rfv}}"
        >
        </gerencial>
    </div>

@endsection