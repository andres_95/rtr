
@extends('layouts.header')

@section('main')
    <div>
        <div class="row mt-3 mr-2">
            <div class="col-2 mt-1">
                @include('layouts.menu')
            </div>
            <div class="col-10">
                <div class="position-absolute panel-title">
                    <div class="titulo-opcion3">
                        <h3 class="text-center">@yield('titulo')</h3>
                    </div>
                </div>
                <div class="row">
                    <div class="col-12">
                        <ul class="breadcrumb">
                            @yield('breadcrumb')
                        </ul>
                    </div>
                </div>
                    <div class="position-relative">
                        @yield('Content')
                    </div>
            </div>
        </div>
        @yield('Footer')
    </div>
@endsection

