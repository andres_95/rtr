
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

import Vuetify from 'vuetify';

window.Vue = require('vue');
import fullCalendar from 'vue-fullcalendar';
import JsonExcel from 'vue-json-excel';
import vue2Dropzone from 'vue2-dropzone';
Vue.use(Vuetify, {
    theme: {
        primary: '#4285F4',
        secondary: '#b0bec5',
        accent: '#005ab6',
        error: '#b71c1c',
        siif: '#63c10d',
    }
});

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */


window.EventBus = new Vue();
Vue.component('vueDropzone',vue2Dropzone);
Vue.component('full-calendar', fullCalendar);
Vue.component('downloadExcel', JsonExcel);
Vue.component('nuevoreporte', require('./components/NuevoReporte.vue'));
Vue.component('clienteconcitas',require('./components/ClientesCitas.vue'));
Vue.component('tomareporte',require('./components/TomaReporte.vue'));
Vue.component('calendar', require('./components/calendar.vue'));
Vue.component('listatodos',require('./components/listatodos.vue'));
Vue.component('sf',require('./components/sf.vue'));
Vue.component('personas', require('./components/personas.vue'));
//Vue.component('personas', require('./components/personas.vue'));
Vue.component('reporte-basico',require('./components/reportebasico.vue'));
Vue.component('monitor',require('./components/monitor.vue'));
Vue.component('crf', require('./components/CRF.vue'));
Vue.component('gerencial', require('./components/Gerencial.vue'));
Vue.component('prueba', require('./components/prueba.vue'));
Vue.component('esquema', require('./components/esquema.vue'));
//Vue.component('listanegra', require('./components/listanegra.vue'));
Vue.component('monedas',require('./components/moneda.vue'));
Vue.component('auditorias',require('./components/auditorias.vue'));
Vue.component('logs',require('./components/logs.vue'));
Vue.component('rolperfil',require('./components/rolperfil.vue'));
Vue.component('clienterfv',require('./components/clientesrfv.vue'));

const app = new Vue({
    el: '#app',
    data: function () {
        return {
        	mensajes : {
                procesando : "Procesando...",
                exito: "Se ha procesado con exito",
                citaexito: "Se ha agendado su cita con exito",
                error: "Hubo un error al procesar",
                dataerror: "Datos incorrectos, verifique la data ingresada"
            },
            dropzoneOptions: {
                url: 'https://httpbin.org/post',
                thumbnailWidth: 150,
                maxFilesize: 0.5,
                headers: { "My-Awesome-Header": "header value" }
            },
            Fabrica: {
        	    idPersona: 'DEMO'
            },
            Empresa:'',
            loadermuestras: false,
            loadingselect: false,
            codigo:"",
            idPersona:'',
            actividad:{},
            incidente:{},
            comentario:"",
            searchmuestras: '',
            searchmuestras2: '',
            date:"",
            mayorista:[],
            mayoristas:[],
            nombre:"",
            cliente:"",
            cantidad:0,
            clientes:[],
            loading:false,
            search: null,
            alertreport:false,
            alertreporterror:false,
            alertbuscar:false,
            alertordenbuscar:false,
            alertorden:false,
            rfv:"",
            confirm:"",
            muestrasfinal:[],
            muestras:[],
            total:0,
            pass:"",
            impuesto:12,
            formaPago:"",
            findorden:true,
            dateD: null,
            menuD:false,
            dateH: null,
            menuH:false,
            conciliar:false,
            especialidad:"",
            modal_conciliar:false,
            itemFactura:[],
            factura:"",
            statusF:"",
            vereportes:0,
            veordenes:0,
            vefacturas:0,
            descarga: 0,
            estadoF: [
                {id:4,text:'Pendientes'},
                {id:5,text:'Parcial'},
                {id:6,text: 'Enviada Total'},
                {id:7,text: 'Anulada'},
                {id:8,text: 'Pagada'}
            ],
            xlsclientes : {
                'RFV': 'idRFV',
                'Cliente': 'nombre_completo_razon_social',
                'Ranking': 'idranking',
                'Especialidad': 'idactividad_negocio',
                'Dias de visita': 'descripcion_dias_visita',
                'Horario': 'descripcion_horarios',
                'Visitas': 'visitado',
                'Frecuencia': 'frecuencia',
            },
            headers_tablas : {
            	agenda: [
                    {text:"Cliente" , value: "nombre_completo_razon_social"},
                    {text:"Hora" , value: ''},
                    {text:"Fecha", value: "fecha"},
                    {text: 'Accion', value: '', sortable: false}
                ],
                reportes : [
                    {text: 'RFV',value: 'RFV',},
                    { text: 'Cliente', value: 'Cliente' },
                    { text: 'Fecha', value: 'fecha_actividad' },
                    { text: 'Descripcion de actividad', value: 'tipo_actividades' },
                    { text: 'Comentarios', value: 'observaciones_cliente' },
                    {text: 'Accion', value: '', sortable: false}
                ],
                mayoristas : [
                    {text: 'Codigo',value: 'id',},
                    { text: 'Mayorista', value: 'nombre' },
                    { text: 'Descuento', value: 'descuento' },
                    {text: 'Accion', value: '', sortable: false}
                ],
                productos : [
                    {text: 'Codigo',value: 'id',},
                    { text: 'Producto', value: 'nombre' },
                    { text: 'Unidades', value: 'cantidad' },
                    { text: 'Descuento', value: 'descuento' },
                    { text: 'Precio', value: 'precio' },
                    {text: 'Accion', value: '', sortable: false}
                ],
                seguimientos : [
                    {text: 'Nro Orden',value: 'nOrden',},
                    { text: 'RFV', value: 'persona' },
                    { text: 'Cliente', value: 'cliente' },
                    { text: 'Mayorista', value: 'mayoristas' },
                    { text: 'Fecha', value: 'fecha.date' },
                    {text: 'Accion', value: '', sortable: false}
                ],
                facturar : [
                    { text: 'Codigo', value: 'id',align: 'center'},
                    { text: 'Producto',value: 'nombre',align: 'center'},
                    { text: 'Cantidad', value: 'cantidad',align: 'center'},
                    { text: 'PrecioxUnd',value:'precio',align: 'center'},
                    { text: 'Costo',value:'costo',align: 'center'},
                    { text: 'Descuento',value: '',align: 'center'},
                    { text: 'Despachadas', value: '',align: 'center' },
                    { text: 'Faltantes', value: 'cantidad_faltante',align: 'center' },
                    {text: 'Accion', value: '', sortable: false}
                ],
                ordenes : [
                    { text: 'N Orden', value: 'nOrden'},
                    { text: 'RFV',value: 'persona'},
                    { text: 'Cliente', value: 'cliente' },
                    { text: 'Fecha', value: 'fecha' },
                    { text: 'Mayorista', value: 'nombre' },
                    { text: 'Unidades', value: 'cantidad' },
                    { text: 'Valor', value: 'Total' },
                    { text: 'Estatus', value: 'idestatus' },
                    {text: 'Accion', value: '', sortable: false}
                ],
                reporte : [
                    { text: 'N Reporte', value: 'idreporte'},
                    { text: 'RFV',value: 'persona'},
                    { text: 'Cliente', value: 'cliente' },
                    { text: 'Fecha', value: 'fecha' },
                    { text: 'Estatus', value: 'idestatus' },
                    {text: 'Accion', value: '', sortable: false}
                ],
                facturas : [
                    { text: 'Factura', value: 'id'},
                    { text: 'RFV',value: 'RFV'},
                    { text: 'Cliente', value: 'cliente' },
                    { text: 'Fecha', value: 'date' },
                    { text: 'Unidades', value: 'cantP' },
                    { text: 'Fallas', value: 'cantF' },
                    { text: 'Monto', value: 'costo' },
                    { text: 'Estado', value: 'idestatus' },
                    {text: 'Accion', value: '', sortable: false}
                ],
                categorias : [
                    { text: 'Codigo', value:'idcategorias'},
                    { text: 'Nombre Categoria', value:'NombreCategorias'},
                    { text: 'Fecha', value:'FechaCreacion'},
                    {text: 'Accion', value: '', sortable: false}
                ],
                clientesvisita : [
                    {text:"Cliente" , value: "nombre_completo_razon_social"},
                    {text:"Ranking" , value: ''},
                    {text:"Hora" , value: ''},
                    {text:"Fecha", value: "fecha"},
                    {text: 'Accion', value: '', sortable: false}
                ]
            },

        }
    },
    watch: {
        search (val) {
            val && this.buscarcliente(val)
        },
        impuesto (val){
            if (val>100) {
                this.impuesto=100;
            }
            if (val<0) {
                this.impuesto=0;
            }
        }
    },
    methods : {
        enviar2 (index){
            var aux = {
                id:index.id,
                nombre:index.nombre,
                cantidad:index.cant,
                precio:index.precio,
                descuento:index.descuento,
                impuesto: 0,
                unidades: index.unidades
                };
            var obj = this.muestrasfinal.find(function (obj) { return obj.id === index.id; });
            if(!obj){
                if(aux.cantidad && aux.cantidad>0)
                {
                    this.muestrasfinal.push(aux)
                    this.cantidad+=(index.cant*1)
                    this.total+=(index.cant*index.precio)-(((index.cant*index.precio)*index.descuento)/100);
                }
            }
        },
        enviar3 (index){
            var aux = {
                id:index.id,
                nombre:index.nombre,
                descunt:index.descuento,
                descuento:index.descuento
            }
            var obj = this.mayoristas.find(function (obj) { return obj.id === index.id; });
            if(!obj){
                this.mayoristas.push(aux)
            }
        },
        borrar (index){
            this.muestrasfinal.splice(index, 1);
        },
        borrar2 (data,index,costo,item){
            if(costo){
                this.total-=costo-((costo/data)*item.item.descuento)/100;
                this.cantidad-=data
            }
            this.muestrasfinal.splice(index, 1);
            if(this.total<0) this.total=0; 
        },
        borrar3 (index){
            this.mayoristas.splice(index, 1);
        },
        calcularcosto(a,b,c){
            c.Costo=a*b;
            return a*b;
        },
        calcularfaltante(a,b,c){
            c.Faltante = a-b;
            return a-b;
        },
        calcularsubtotal(a,b,c,d)
        {
            d.SubTotal = (a*b)-(((a*b)*c)/100);
            return (a*b)-(((a*b)*c)/100);
        },
        calcularFacturaTotal()
        {
            setTimeout(function() {
                this.total=0;
                for(let index of this.itemFactura)
                {
                    this.total+=index.SubTotal;
                }
                if (!this.total) {
                    this.total=this.pass
                }
            }.bind(this), 10);
            
        },
        refrescar (){
                this.mayoristas=[],
                this.muestrasfinal=[],
                this.mayorista=[],
                this.codigo="",
                this.nombre="",
                this.rfv="",
                this.comentario="",
                this.date="",
                this.total=0,
                this.cantidad=0,
                this.clientes=[],
                this.itemFactura=[],
                this.alertreport=false,
                this.alertreporterror=false
        },
    	detallereporte(rfv){
            rfv.loading=true;
            axios.post('/services/actividad/detalle',{'rfv': rfv.idRFV,'reporte':rfv.idreporte})
                .then((res)=>{
                    if(res.data.muestras){
                        this.cantidad=1;
                    }else{
                        this.cantidad=0;
                    }
                    this.mayorista=res.data.muestras;
                    this.codigo=res.data.cliente;
                    this.date=res.data.fecha_actividad;
                    this.comentario=res.data.observaciones_cliente;
                    this.actividad=res.data.actividad;
                    this.nombre=res.data.Firma_cliente;
                    this.confirm=rfv.RFV;
                },(error)=>{

                })
        },
        agenda (){
            if (!this.clientes.length) {
                this.loadermuestras = true;
                axios.get('/services/agenda/clientes')
                    .then((res)=>{
                        this.clientes=res.data.Clientes;
                        this.loadermuestras = false;
                    },(error)=>{
                        this.loadermuestras = false;
                    })
            }
            
        },
        agendarcita(data){
            this.loading=true;
            this.alertreporterror=false;
            this.alertreport=false;
            axios.post('/services/agenda/crear',{'rfv':this.rfv,'cliente':data.idPersona,'fecha':data.fecha, 'hora':data.hora})
                .then((res)=>{
                    this.loading=false;
                    this.alertreport=true;
                },(error)=>{
                    this.alertreport=false;
                    this.alertreporterror=true;
                    this.loading=false;
                })
        },
        codigoE (data){
            this.loading=true;
            axios.post('/services/clientes/searchbyid',{'id': data })
                .then((res)=>{
                        if(res.data.nombre_completo_razon_social)
                        {
                            //this.search(res.data)
                            this.clientes.push(res.data)
                            this.cliente = res.data;

                            this.loading=false;
                            this.alertbuscar=false;
                        }
                        else{
                            this.loading=false;
                            this.alertbuscar=true;
                        }

                    },
                    (error)=>{
                        this.loading=false;
                    })
        },
        buscarcliente (val){
            let obj = this.clientes.find(o => {
                                    if(o.nombre_completo_razon_social.toLowerCase().indexOf(val.toLowerCase())!=-1){
                                            return true;
                                        }
                                    if(o.nombre_completo_razon_social.toLowerCase()=== val.toLowerCase()){
                                        return true;
                                    }

                                    });
            if(!obj){
                this.loadingselect=true;
                axios.post('/services/search-cliente',{data: val})
                    .then((res)=>{
                        this.clientes=res.data;
                        this.loadingselect=false;
                    },(error)=>{
                        this.loadingselect=false;
                    });
            }
            
        },
        inputcodigo(data){
            this.codigo=data
        },
        buscamayorista (){
            this.loadermuestras = true;
            axios.get('/services/mayoristas',{})
                .then((res)=>{
                    this.loadermuestras = false;
                    this.mayorista=res.data.Mayoristas;
                },(error)=>{
                    this.loadermuestras = false;

                })
        },
        buscaproductos (){
            this.loadermuestras = true;
            axios.post('/services/producto/consult',{'categoria':"null",'producto': "null"})
                .then((res)=>{
                    this.loadermuestras = false;
                    this.muestras=res.data.productos;
                },(error)=>{
                    this.loadermuestras = false;
                })
        },
        validarenviopedido(){
            if(this.codigo && this.mayorista.length>0 && this.muestrasfinal.length>0)
            {
                this.enviarpedido();
            }else
            {
                this.alertreporterror=true;
            }
        },
        enviarpedido (){
            this.loading=true;
            axios.post('/services/orden/create',{
                'cliente': this.codigo,
                'RFV': jQuery('#representante').val(),
                'mayoristas': this.mayoristas,
                'comentario': this.comentario,
                'productos': this.muestrasfinal,
                'impuesto':this.impuesto,
                'cantidad':this.cantidad,
                'costo':this.total
            })
                .then((res)=>{
                    this.loading=false;
                    this.refrescar();
                    this.confirm=res.data.norden;
                    this.alertreport=true;
                },(error)=>{

                    this.loading=false;
                    this.alertreporterror=true;
                })
        },
        status (id){
            axios.post('/services/orden/filterestatus',{'id': id})
                .then((res)=>{
                    if(!res.data.error){
                        this.muestrasfinal=res.data.data;
                    }else{
                        this.muestrasfinal=[];
                    }

                },(error)=>{

                })
        },
        detalle (data,fecha,cli){
            axios.post('/services/orden/show',{'id': data})
                .then((res)=>{
                    if(res.data)
                    {
                        this.mayorista=res.data.data.productos;
                        this.codigo=data;
                        this.date=fecha;
                        this.comentario= res.data.data.mayoristas[0];
                        this.cliente=cli;
                        this.total=res.data.data.Total;
                        this.confirm=res.data.data.comentario;
                    }

                },(error)=>{

                })
        },
        buscaorden (data){
            this.loading=true;
            this.alertordenbuscar=false;
            axios.post('/services/factura/orden/show',{'id': data})
                .then((res)=>{
                    res=res.data;
                    this.itemFactura=res.data.productos;
                    this.loading=false;
                    this.conciliar=true;
                    this.findorden=false;
                    this.pass=res.data.Total;
                    this.total=res.data.Total;
                    this.cliente=res.data.cliente;
                    this.rfv=res.data.persona;
                    this.impuesto=res.data.Impuesto;
                    this.especialidad=res.data.mayoristas[0].nombre?res.data.mayoristas[0].nombre:"Sin Mayorista";
                    this.searchmuestras=res.data.Registrado_Por;
                },(error)=>{
                    this.alertordenbuscar=true;
                    this.loading=false;
                })
        },
        validarconciliar(){
            if(this.codigo && this.nombre && this.comentario && this.formaPago)
            {
                this.modal_conciliar=true;
                this.conciliarF()
            }else{
                this.alertorden=true;
            }
        },
        conciliarF (){
            this.loading=true;
            axios.post('/services/orden/conciliar',{
                'productos': this.itemFactura,
                'norden': this.codigo.idorden,
                'nfactura': this.nombre,
                'almacen':this.comentario,
                'formaPago':this.formaPago.idformaPago,
                'fecha':this.dateD
            })
                .then((res)=>{
                    console.log(res.data);
                    
                    if (!res.data.exito) {
                        res=res.data;
                        this.muestrasfinal=res.data;
                        this.mayoristas=res.data.mayoristas[0];
                        this.total=res.data.Total;
                        this.date=res.data.fecha;
                        this.codigo=res.data.nOrden;
                        this.mayorista=res.data.productos;
                        this.rfv="pasa";
                    }
                    else{
                        this.rfv=res.data.exito?res.data.exito:res.data.Invalido;
                    }
                    $('#myModal').modal('show');
                    this.conciliar=false;

                    this.loading=false;
                },(error)=>{

                    this.loading=false;
                })
        },
        notificacion(data){
            this.loading=true;
            axios.post('/services/notification_add',{
                'idrfv': this.codigo,
                'descripcion': this.comentario,
                'idOperador':data.idOperador,
                'idfabricante':data.fabricante
            })
                .then((res)=>{
                    this.codigo="";
                    this.comentario="";
                    this.loading=false;
                    this.alertreport=true;
                },(error)=>{
                    this.loading=false;
                })
        },
        CategoriPersonaFabricante (id)
        {
            this.loading = true;
            axios.post('/services/Admin/CategoriaPersona',{'fabricante':id})
                .then((res)=>{
                    this.mayorista = res.data;
                    jQuery('#categorias').show();
                    this.loading = false;
                },(error)=>{
                    this.loading = false;
                })
        },
        ListaNegra(id){
            this.loading = true;
            this.idPersona=id;
            axios.post('/services/Admin/grupos/persona',{'fabricante':id})
                .then((res)=>{
                    this.mayorista = res.data;
                    jQuery('#categorias').show();
                    this.loading = false;
                },(error)=>{
                    this.loading = false;
                })
        },
        CategoriProductFabricante (id)
        {
            this.loading = true;
            axios.post('/services/Admin/CategoriaProducto',{'fabricante':id})
                .then((res)=>{
                this.mayorista = res.data;
                    jQuery('#categorias').show();
                    this.loading = false;
                },(error)=>{
                    this.loading = false;
                })
        },
        OrdenFabricante (id)
        {
            this.loading = true;
            axios.post('/services/Admin/ordenes',{'fabricante':id})
                .then((res)=>{
                    res=res.data
                    this.mayorista = res.data;
                    jQuery('#categorias').show();
                    this.loading = false;
                },(error)=>{
                    this.loading = false;
                })
        },
        ReporteFabricante (id)
        {
            this.loading = true;
            axios.post('/services/Admin/reporte',{'fabricante':id})
                .then((res)=>{
                    res=res.data
                    this.mayorista = res.data;
                    jQuery('#categorias').show();
                    this.loading = false;
                },(error)=>{
                    this.loading = false;
                })
        },
        VisitasADM (id,rfv){
            if(rfv!=null)
                rfv=this.rfv;

            this.loading = true;
            axios.post('/services/visitar',{'fabricante':id,'rfv':rfv})
                .then((res)=>{
                    this.clientes = res.data;
                    jQuery('#categorias').show();
                    this.loading = false;
                },(error)=>{
                    this.loading = false;
                })
        },
        deleteReporte(item){
             let result=confirm("anular la orden?")
                if(!result)
                   return;

             this.loading = true;
            axios.post('/admin/reportes/delete',{'id':item.idreporte})
                .then((res)=>{
                    let index=this.mayorista.indexOf(item);
                    
                    this.mayorista.splice(index,1);
                    jQuery('#categorias').show();
                    this.loading = false;
                },(error)=>{
                    this.loading = false;
                })
        },
        deleteOrden(item){
            let result=confirm("anular la orden?")
            if(!result)
                return;

            this.loading = true;
            axios.post('/admin/ordenes/delete',{'id':item.nOrden})
                .then((res)=>{
                    let index=this.mayorista.indexOf(item);

                    this.mayorista.splice(index,1);
                    jQuery('#categorias').show();
                    this.loading = false;
                },(error)=>{
                    this.loading = false;
                })
        },
        deleteFactura(item){
            let result=confirm("anular la factura?")
            if(!result)
               return;

         this.loading = true;
                axios.post('/admin/facturas/delete',{'id':item.id})
                    .then((res)=>{
                        let index=this.mayorista.indexOf(item);
                        
                        this.mayorista.splice(index,1);
                        jQuery('#categorias').show();
                        this.loading = false;
                    },(error)=>{
                        this.loading = false;
                    })
        },
        deleteCategoria(item){
            let result=confirm("anular la categoria?")

            if(!result)
               return;

         this.loading = true;
                axios.post('/admin/productos/delete_categoria',{'id':item.idcategorias})
                    .then((res)=>{
                        let index=this.mayorista.indexOf(item);
                        
                        this.mayorista.splice(index,1);
                        jQuery('#categorias').show();
                        this.loading = false;
                    },(error)=>{
                        this.loading = false;
                    })
        },
        FacturaFabricante (id)
        {
            this.loading = true;
            axios.post('/services/Admin/facturas',{'fabricante':id})
                .then((res)=>{
                    this.mayorista = res.data.data;
                    jQuery('#categorias').show();
                    this.loading = false;
                },(error)=>{
                    this.loading = false;
                })
        },
        consultaGerencial(){
            axios.post('/services/consultaGerencial',{'filtros':{
                'fechaIni':this.dateD,
                'fechaFin':this.dateH,
                'ciclo': this.rfv,
                'ranking':this.comentario,
                'actividad':this.actividad,
                'incidente':this.incidente,
                'zonas':this.confirm,
                'bricks':this.factura,
                'especialidad':this.especialidad
            }})
                .then((res)=>{
                    if(!res.data.error){
                        this.vereportes=1;
                        this.veordenes=0;
                        this.vefacturas=0;
                        this.descarga=1;
                        this.mayoristas=res.data;
                    }else{
                        this.mayoristas=[];
                    }
                },(error)=>{

                })
        },
        CambiarFabricante(nombre){
            axios.post('/services/CambioF',{'fabricante':nombre})
                .then((res)=>{
                    this.comentario=nombre;
                },(error)=>{

                })
        },
        ClaseFabricante (id,aspecto)
        {
            this.loading = true;
         //   debugger
            axios.post('/services/Admin/'+aspecto.toLowerCase(),{'fabricante':id})
                .then((res)=>{
                    this.mayorista = res.data;

                       if(aspecto=='Grupo'){
                           
                            this.mayorista.forEach(function(item){
                                item.id=item.idgrupo_persona
                                item.descripcion=item.descripciongrupoPersona
                            })
                        }
                    jQuery('#categorias').show();
                    this.loading = false;
                },(error)=>{
                    this.loading = false;
                })
        },
        password (){
            axios.post('/services/confirmpass',{'pass':this.pass})
                .then((res)=>{
                    this.validarenviopedido();
                    jQuery('#cerrar').click();
                },(error)=>{
                    this.pass='';
                })
        }
    },
    created: function () {
        var a = jQuery('#app').attr('fabricante')
        this.Fabrica.idPersona = a;

    }

});
