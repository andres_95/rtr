<?php

use Illuminate\Http\Request;
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('/estadistico','API\Estadistico\EstadisticaController@Estadistico');

Route::post('/login','Auth\LoginAPIController@login');
Route::middleware('auth:api')->get('/logout','Auth\LoginAPIController@logout');

Route::middleware('auth:api')->prefix('/actividades')->group(function () {
    Route::get('/consult','API\Actividades\TipoActividadesController@show');
});
Route::middleware('auth:api')->get('/incidente','Admin\Incidencia\incidenciacontroller@index');

      //  Route::get('/consult', 'ControladorTipoActividades@show');
Route::middleware('auth:api')->prefix('/orden')->group(function () {
    Route::post('/create','API\Ordenes\OrdenController@crear');
    Route::post('/consult','API\Ordenes\OrdenController@index_json');
    Route::post('/productos','API\Ordenes\OrdenController@show');
});

Route::middleware('auth:api')->prefix('/agenda')->group(function(){
//santiago
    Route::get('/clientes','API\Agenda\PlanificadorController@mostrarDisponible');
    Route::post('/crear','API\Agenda\PlanificadorController@create');
    Route::get('/consultar','API\Agenda\PlanificadorController@index');
    Route::get('/citas','API\Agenda\PlanificadorController@citaReporte');
//josue
    Route::post('/update','API\Agenda\PlanificadorController@update');
    Route::post('/delete','API\Agenda\PlanificadorController@delele');
    Route::post('/','API\Agenda\PlanificadorController@index');
});

Route::middleware('auth:api')->prefix('/actividad')->group(function(){
    Route::post('/add','API\Actividades\ReporteController@store');
    Route::post('/add_new','API\Actividades\ReporteController@new');
    Route::post('/','API\Actividades\ActividadController@index');
    Route::post('/firma','API\Actividades\actividadcontroller@saveFirma');
    Route::post('/detalle','API\Actividades\actividadcontroller@detalleReporte');
    Route::post('/reporte','API\Actividades\ActividadController@consultaReporte');
});

Route::middleware('auth:api')->prefix('/producto')->group(function(){
    Route::post('/consult','API\Producto\ProductoController@index');
    Route::post('/','API\Producto\ProductoController@index');
    Route::post('/crear','API\Producto\ProductoController@create');
    Route::post('/modificar','API\Producto\ProductoController@update');
});

Route::middleware('auth:api')->post('/info/cliente','API\Persona\ClienteController@show');
Route::middleware('auth:api')->get('/representantes','API\Persona\RepresentastesController@index');

Route::middleware('auth:api')->get('/mayoristas','API\Persona\MayoristaController@index');

Route::middleware('auth:api')->prefix('/clientes')->group(function(){
    Route::post('/','API\Persona\ClienteController@index');
    Route::post('/materiales','API\Persona\ClienteController@get_materiales');
    Route::post('/count','API\Persona\ClienteController@muestracount');
    Route::post('/searchbyid','API\Persona\ClienteController@SearchByID');
    Route::post('/searchbyname','API\Persona\ClienteController@SearchByName');
});

Route::middleware('auth:api')->prefix('/notificacion')->group(function(){
    Route::post('/','API\Notificaciones\NotificacionController@index');
    Route::post('/add','API\Notificaciones\NotificacionController@store');
    Route::post('/update','API\Notificaciones\NotificacionController@update');
    Route::post('/delete','API\Notificaciones\NotificacionController@delete');
    Route::post('/notice','API\Notificaciones\NotificacionController@notificar');
});

Route::post('/Admin/Monitor/Persona','Admin\Persona\PersonaController@exportExcel');
Route::post('/Admin/Monitor/LoadPersona','Admin\Persona\PersonaController@LoadExcel');


