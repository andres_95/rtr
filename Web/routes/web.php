<?php

Route::get('/', 'Auth\LoginController@showLoginForm')->name('login');
Route::post('/login','Auth\LoginController@login')->name('postlogin');
Route::post('/logout','Auth\LoginController@logout')->name('logout');

Route::get('prueba','Admin\Localizacion\PaisController@exportExcel');

Route::get('/import_parse', function (){
    return view('prueba2');
});

Route::post('/import_process', 'prueba@parseImport')->name('import_parse');


Route::get('/clave',function (){
    return bcrypt('AdminSIIF');
});


Route::group(['middleware'=>['auth']],function (){
    Route::get('/home','Web\HomeController@index')->name('home');

    Route::group(['prefix'=>'reportes'],function (){
        Route::get('/consulta','Web\Consultas\ReporteBasicoController@index')->name('ConsultaReporte');
        Route::get('/Gerencial','Web\ConsultReporteController@consulGerencial')->name('Gerencial');
    });

    Route::group(['prefix'=>'services'],function (){
        Route::post('/CambioF', 'Admin\Persona\PersonaController@SetF')->name('service0');
        Route::post('search-cliente','API\Persona\ClienteController@search')->name('service1');
        Route::get('/RFV_clientes','API\Agenda\AgendaController@index')->name('service2');
        Route::post('/clientes/searchbyid','API\Persona\ClienteController@SearchByID')->name('service3');
        Route::post('/clientes/searchbyname','API\Persona\ClienteController@SearchByName')->name('service4');
        Route::post('/Reporte/add','API\Actividades\ReporteController@store')->name('service5');
        Route::post('/Reporte/add-new','API\Actividades\ReporteController@new')->name('service6');
        Route::post('/orden/create','API\Ordenes\OrdenController@crear')->name('service7');
        Route::post('/orden/conciliar','API\Ordenes\FacturarController@conciliarFactura')->name('service8');
        Route::post('/orden/filterestatus','API\Ordenes\OrdenController@FilterEstatus')->name('service9');
        Route::post('/orden/show','API\Ordenes\OrdenController@show')->name('service10');
        Route::post('/actividad/detalle','API\Actividades\ActividadController@detalleReporte')->name('service11');
        Route::get('/mayoristas','API\Persona\MayoristaController@index')->name('service12');
        Route::post('/cliente/materiales','API\Persona\ClienteController@get_materiales')->name('service13');
        Route::post('/producto/consult','API\Producto\ProductoController@index')->name('service14');
        Route::post('/producto/consult2','Admin\Producto\ProductoController@listprod')->name('service15');
        Route::get('/agenda/clientes','API\Agenda\PlanificadorController@mostrarDisponible')->name('service16');
        Route::post('/agenda/crear','API\Agenda\PlanificadorController@create')->name('service17');
        Route::get('/agenda','API\Agenda\PlanificadorController@index2')->name('service18');
        Route::post('/reporte','API\Actividades\ActividadController@consultaReporte')->name('service19');
        Route::post('/consultaOrden','API\Ordenes\OrdenController@consultaOrden')->name('service20');
        Route::post('/consultaFactura','API\Ordenes\FacturarController@consultaFactura')->name('service21');
        Route::post('/notification_add','API\Notificaciones\NotificacionController@store')->name('service22');
        Route::post('/generarpdf','Web\Consultas\ReporteBasicoController@Reporte_PDF')->name('service23');
        Route::post('/Admin/CategoriaProducto','Admin\Producto\CategoryController@show')->name('service24');
        Route::post('/Admin/CategoriaPersona','Admin\Persona\PersonaController@show')->name('service25');
        Route::post('/Admin/personas','Admin\Persona\PersonaController@showpersonas')->name('service26');
        Route::post('/pais','Admin\Localizacion\PaisController@show')->name('service27');
        Route::post('/estado','Admin\Localizacion\EstadoController@show')->name('service28');
        Route::post('/Admin/ordenes', 'API\Ordenes\OrdenController@all_admin')->name('service29');
        Route::post('/Admin/facturas', 'Admin\Facturacion\FacturaController@show')->name('service30');
        Route::post('/consultaGerencial','EstadisticaController@Estadistico')->name('service31');
        Route::post('/Admin/clase', 'Admin\Persona\PersonaController@showclase')->name('service32');
        Route::post('/Admin/grupo','Admin\Persona\PersonaController@showgrupo')->name('service33');
        Route::post('/agenda/cliente/detalle','API\Agenda\AgendaController@detalle_cliente')->name('service34');
        Route::post('/factura/orden/show','API\Ordenes\FacturarController@show')->name('service35');
        Route::post('/Admin/ciclos','Administrativo\CRFController@showciclo')->name('service36');
        Route::post('/Admin/frecuencias','Administrativo\CRFController@showfrec')->name('service37');
        Route::post('/Admin/ranking','Administrativo\CRFController@showrank')->name('service38');
        Route::post('/ciclos/add','Administrativo\CRFController@storeC')->name('service39');
        Route::post('/ciclos/upd','Administrativo\CRFController@updateC')->name('service40');
        Route::post('/ciclos/del','Administrativo\CRFController@deleteC')->name('service41');
        Route::post('/persona/tipo/add','Admin\user\tipopersonacontroller@store')->name('service42');
        Route::post('/persona/tipo/upd','Admin\user\tipopersonacontroller@update')->name('service43');
        Route::post('/persona/tipo/del','Admin\user\tipopersonacontroller@delete')->name('service44');
        Route::post('/persona/clase/add','Admin\user\clasepersonacontroller@store')->name('service45');
        Route::post('/persona/clase/upd','Admin\user\clasepersonacontroller@update')->name('service46');
        Route::post('/persona/clase/del','Admin\user\clasepersonacontroller@destroy')->name('service47');
        Route::post('/persona/especialidad/add','Admin\especialidades\especialidadcontroller@store')->name('service48');
        Route::post('/persona/especialidad/upd','Admin\especialidades\especialidadcontroller@update')->name('service49');
        Route::post('/persona/especialidad/del','Admin\especialidades\especialidadcontroller@destroy')->name('service50');
        Route::post('/persona/titulo/add','Admin\titulos\titulocontroller@store')->name('service51');
        Route::post('/persona/titulo/upd','Admin\titulos\titulocontroller@update')->name('service52');
        Route::post('/persona/titulo/del','Admin\titulos\titulocontroller@destroy')->name('service53');
        Route::post('/persona/grupo/add','Admin\user\grupopersonacontroller@store')->name('service54');
        Route::post('/persona/grupo/upd','Admin\user\grupopersonacontroller@update')->name('service55');
        Route::post('/persona/grupo/del','Admin\user\grupopersonacontroller@destroy')->name('service56');
        Route::post('/Admin/tipoactividad','Admin\Actividades\tipoactividadcontroller@show')->name('service57');
        Route::post('/Admin/tipoincidente','Admin\Incidencia\incidenciacontroller@show')->name('service58');
        Route::post('/Admin/redessociales','Admin\Redes\redsocialcontroller@show')->name('service59');
        Route::post('/Admin/horarios','Admin\Horarios\horariocontroller@show')->name('service60');
        Route::post('/Admin/localizacion','Admin\Localizacion\TipoLocalizacionController@show')->name('service61');
        Route::post('/Admin/tipolocalizacion','Admin\Localizacion\TipoLocalizacionController@show2')->name('service62');
        Route::post('/Admin/cadenas','Admin\cadenas\cadenacontroller@show')->name('service63');
        Route::post('/Admin/tipoproducto', 'Admin\Producto\tipoproductocontroller@show2')->name('service64');
        Route::post('/Admin/lineaproducto', 'Admin\Producto\lineaproductocontroller@show2')->name('service65');
        Route::post('/tipoactividad/add','Admin\Actividades\tipoactividadcontroller@store')->name('service66');
        Route::post('/tipoactividad/upd','Admin\Actividades\tipoactividadcontroller@update')->name('service67');
        Route::post('/tipoactividad/del','Admin\Actividades\tipoactividadcontroller@destroy')->name('service68');
        Route::post('/frecuencias/add','Administrativo\CRFController@storeF')->name('service69');
        Route::post('/frecuencias/upd','Administrativo\CRFController@updateF')->name('service70');
        Route::post('/frecuencias/del','Administrativo\CRFController@destroyF')->name('service71');
        Route::post('/ranking/add','Administrativo\CRFController@storeR')->name('service72');
        Route::post('/ranking/upd','Administrativo\CRFController@updateR')->name('service73');
        Route::post('/ranking/del','Administrativo\CRFController@destroyR')->name('service74');
        Route::post('/Admin/diasvisita','\Admin\planificador\habilescontroller@show')->name('service75');
        Route::post('/Admin/diasferiados','Admin\planificador\feriadocontroller@show')->name('service76');
        Route::post('/persona/add','Admin\Persona\PersonaController@store')->name('service77');
        Route::post('/persona/upd','Admin\Persona\PersonaController@update')->name('service78');
        Route::post('/persona/del','Admin\Persona\PersonaController@destroy')->name('service79');
        Route::post('/producto/add','Admin\Producto\ProductoController@store')->name('service80');
        Route::post('/producto/upd','Admin\Producto\ProductoController@update')->name('service81');
        Route::post('/producto/del','Admin\Producto\ProductoController@destroy')->name('service82');
        Route::post('/localizacion/add','Admin\Localizacion\TipoLocalizacionController@store')->name('service83');
        Route::post('/localizacion/upd','Admin\Localizacion\TipoLocalizacionController@update')->name('service84');
        Route::post('/localizacion/del','Admin\Localizacion\TipoLocalizacionController@destroy')->name('service85');
        Route::post('/paises/add','Admin\Localizacion\PaisController@store')->name('service86');
        Route::post('/paises/upd','Admin\Localizacion\PaisController@update')->name('service87');
        Route::post('/paises/del','Admin\Localizacion\PaisController@destroy')->name('service88');
        Route::post('/idioma/add','Admin\Bricks\idiomacontroller@store')->name('service89');
        Route::post('/idioma/upd','Admin\Bricks\idiomacontroller@update')->name('service90');
        Route::post('/idioma/del','Admin\Bricks\idiomacontroller@destroy')->name('service91');
        Route::post('/moneda/add','Admin\moneda\monedacontroller@store')->name('service92');
        Route::post('/moneda/upd','Admin\moneda\monedacontroller@update')->name('service93');
        Route::post('/moneda/del','Admin\moneda\monedacontroller@destroy')->name('service94');
        Route::post('/diasferiados/add','Admin\planificador\feriadocontroller@store')->name('service95');
        Route::post('/diasferiados/upd','Admin\planificador\feriadocontroller@update')->name('service96');
        Route::post('/diasferiados/del','Admin\planificador\feriadocontroller@destroy')->name('service97');
        Route::post('/horarios/add','Admin\Horarios\horariocontroller@store')->name('service98');
        Route::post('/horarios/upd','Admin\Horarios\horariocontroller@update')->name('service99');
        Route::post('/horarios/del','Admin\Horarios\horariocontroller@destroy')->name('service100');
        Route::post('/tipoincidente/add','Admin\Incidencia\incidenciacontroller@store')->name('service101');
        Route::post('/tipoincidente/upd','Admin\Incidencia\incidenciacontroller@update')->name('service102');
        Route::post('/tipoincidente/del','Admin\Incidencia\incidenciacontroller@destroy')->name('service103');
        Route::post('/Admin/esquema', 'Admin\esquemas\esquemacontroller@show')->name('service104');
        Route::post('/esquema/add', 'Admin\esquemas\esquemacontroller@create')->name('service105');
        Route::post('/esquema/upd', 'Admin\esquemas\esquemacontroller@update')->name('service106');
        Route::post('/esquema/del', 'Admin\esquemas\esquemacontroller@destroy')->name('service107');
        Route::post('/visitar', 'Admin\Persona\PersonaController@clientes')->name('service108');
        Route::post('/Admin/pais','Admin\Localizacion\PaisController@pais')->name('service109');
        Route::post('/redessociales/add','Admin\Redes\redsocialcontroller@store')->name('service110');
        Route::post('/redessociales/upd','Admin\Redes\redsocialcontroller@update')->name('service111');
        Route::post('/redessociales/del','Admin\Redes\redsocialcontroller@destroy')->name('service112');
        Route::post('/redessociales','Admin\Redes\redsocialcontroller@show')->name('service113');
        Route::post('/diasdevisita/add','Admin\planificador\habilescontroller@store')->name('service114');
        Route::post('/diasdevisita/upd','Admin\planificador\habilescontroller@update')->name('service115');
        Route::post('/diasdevisita/del','Admin\planificador\habilescontroller@destroy')->name('service116');
        Route::post('/Admin/categorias','Admin\Producto\CategoryController@list')->name('service117');
        Route::post('/lineaproducto/add','Admin\Producto\lineaproductocontroller@store')->name('service118');
        Route::post('/lineaproducto/upd','Admin\Producto\lineaproductocontroller@update')->name('service119');
        Route::post('/lineaproducto/del','Admin\Producto\lineaproductocontroller@destroy')->name('service120');
        Route::post('/tipoproducto/add','Admin\Producto\tipoproductocontroller@store')->name('service121');
        Route::post('/tipoproducto/upd','Admin\Producto\tipoproductocontroller@update')->name('service122');
        Route::post('/tipoproducto/del','Admin\Producto\tipoproductocontroller@destroy')->name('service123');
        Route::post('/unidades/add','Admin\unidades\unidadcontroller@store')->name('service124');
        Route::post('/unidades/upd','Admin\unidades\unidadcontroller@update')->name('service125');
        Route::post('/unidades/del','Admin\unidades\unidadcontroller@destroy')->name('service126');
        Route::post('/Admin/unidades','Admin\unidades\unidadcontroller@show2')->name('service127');
        Route::post('/cadenas/add','Admin\cadenas\cadenacontroller@store')->name('service128');
        Route::post('/cadenas/upd','Admin\cadenas\cadenacontroller@update')->name('service129');
        Route::post('/cadenas/del','Admin\cadenas\cadenacontroller@delete')->name('service130');
        Route::post('/Admin/zonas','Admin\Bricks\zonacontroller@index')->name('service131');
        Route::post('/zonas/add','Admin\Bricks\zonacontroller@store')->name('service132');
        Route::post('/zonas/upd','Admin\Bricks\zonacontroller@update')->name('service133');
        Route::post('/zonas/del','Admin\Bricks\zonacontroller@destroy')->name('service134');
        Route::post('/parametros/add','Admin\Parametros\parametrocontroller@store')->name('service135');
        Route::post('/parametros/upd','Admin\Parametros\parametrocontroller@update')->name('service136');
        Route::post('/parametros/del','Admin\Parametros\parametrocontroller@destroy')->name('service137');
        Route::post('/Admin/parametros','Admin\Parametros\parametrocontroller@show')->name('service138');
        Route::post('/Admin/listanegra','Admin\user\listanegracontroller@show')->name('service139');
        Route::post('/categorias/add','Admin\Categorias\CategoriaController@store')->name('service140');
        Route::post('/categorias/upd','Admin\Categorias\CategoriaController@update')->name('service141');
        Route::post('/categorias/del','Admin\Categorias\CategoriaController@destroy')->name('service142');
        Route::post('/Admin/moneda','Admin\moneda\monedacontroller@show')->name('service143');
        Route::post('/listanegra/add','Admin\user\listanegracontroller@store')->name('service144');
        Route::post('/listanegra/upd','Admin\user\listanegracontroller@update')->name('service145');
        Route::post('/listanegra/del','Admin\user\listanegracontroller@destroy')->name('service146');
        Route::post('/Admin/grupos/persona','Admin\Persona\PersonaController@personaByGrupo')->name('service147');
        Route::post('/agenda/TMP','API\Agenda\PlanificadorController@TMP')->name('service148');
        Route::get('/agenda/RFV','API\Agenda\PlanificadorController@rfvs')->name('service149');
        Route::post('/agenda/TMPdelete','API\Agenda\PlanificadorController@tmpdelete')->name('service150');
        Route::post('/confirmpass', 'Admin\Persona\PersonaController@pass')->name('service151');
        Route::post('/Admin/logs','logcontroller@show')->name('service152');
        Route::post('/Admin/auditoria','Admin\Seguridad\AuditoriasController@show')->name('service153');
        Route::post('/auditoria','Admin\Seguridad\AuditoriasController@destroy')->name('service154');
        Route::post('/Admin/reporte', 'API\Actividades\ReporteController@all_admin')->name('service155');

    });

    Route::group(['prefix'=>'RTR'],function (){
        Route::get('/toma-reporte','Web\RTR\TomaReporteController@show_report_option')->name('nuevo_reporte');
        Route::get('/nuevo-reporte','Web\RTR\TomaReporteController@show_new_report')->name('Reportar');
        Route::get('/reportes-agenda','Web\RTR\TomaReporteController@show_report_agend')->name('ReportarAgenda');
        Route::post('/reporte-agenda-nuevo','Web\RTR\TomaReporteController@show_new_report_agend')->name('RFVClientes');
        Route::get('/agenda','Web\RTR\AgendaController@show_resume_agenda')->name('agenda');
        Route::get('/lista-reportes','Web\RTR\ReportesController@index')->name('reportes');
        Route::get('/planificar-cita','Web\RTR\PlanificadorController@index')->name('planificar');
    });

    Route::group(['prefix'=>'CT'],function (){
        Route::get('/toma_pedido','Web\CT\CTController@Pedido')->name('tomapedido');
        Route::get('/Seguimiento', 'Web\CT\CTController@Seguimiento')->name('seguimiento');
    });

    Route::group(['prefix'=>'contacto'],function (){
        Route::get('/','Web\ContactoController@index')->name('contacto');
    });
    Route::group(['prefix'=>'procesos'],function(){

        Route::group(['prefix'=> 'monitor'],function (){
            Route::get('/','Web\Procesos\MonitorController@index')->name('monitor');
            Route::post('ZonaFormato','Admin\Bricks\zonacontroller@exportExcel')->name('ZonaFormato');
            Route::post('RutaFormato','Admin\Bricks\BrickController@exportExcel')->name('RutaFormato');
            Route::post('CadenaFormato','Admin\cadenas\cadenacontroller@exportExcel')->name('CadenaFormato');
            Route::post('CategoriaPFormato','Admin\Categorias\CategoriaController@exportExcel')->name('CategoriaPFormato');
            Route::post('LineaFormato','Admin\Producto\lineaproductocontroller@exportExcel')->name('LineaFormato');
            Route::post('TipoProductoFormato','Admin\Producto\tipoproductocontroller@exportExcel')->name('TipoProductoFormato');
            Route::post('ProductoFormato','Admin\Producto\ProductoController@exportExcel')->name('ProductoFormato');
            Route::post('CliMatFormato','Admin\Producto\ClienteMuestra@exportExcel')->name('CliMatFormato');
            Route::post('CliRfvFormato','Admin\Persona\ClienteRFV@exportExcel')->name('CliRfvFormato');
            Route::post('LoadZona', 'Admin\Bricks\zonacontroller@LoadExcel')->name('LoadZona');
            Route::post('LoadRuta', 'Admin\Bricks\Brickcontroller@LoadExcel')->name('LoadRuta');
            Route::post('LoadCadena', 'Admin\cadenas\cadenacontroller@LoadExcel')->name('LoadCadena');
            Route::post('LoadCliMat','Admin\Producto\ClienteMuestra@LoadExcel')->name('LoadCliMat');
            Route::post('LoadCliRfv','Admin\Persona\ClienteRFV@LoadExcel')->name('LoadCliRfv');
            Route::post('LoadProducto','Admin\Producto\ProductoController@LoadExcel')->name('LoadProducto');
            Route::post('PersonaFormato','Admin\Persona\PersonaController@exportExcel')->name('PersonaFormato');
            Route::post('LoadPersona','Admin\Persona\PersonaController@LoadExcel')->name('LoadPersona');
            Route::post('ClienteDiasFormato','Admin\Persona\PersonaController@exportClienteDiasExcel')->name('ClienteDiasFormato');
            Route::post('LoadClienteDisponible','Admin\Persona\PersonaController@LoadClienteDisponibleExcel')->name('LoadClienteDisponible');
        });

        Route::get('/parametros','Web\Procesos\ParametrosController@index')->name('parametros');
        Route::get('/manejo_alertas','Web\Procesos\AlertasController@index')->name('alertas');
        Route::get('/InfHistorica','Web\Procesos\HistoricoController@index')->name('historico');
        Route::get('/notificacion','Web\Procesos\NotificacionController@index')->name('notificacion');
    });

    Route::group(['prefix' => 'admin'], function() {
        //Facturacion
        Route::group(['prefix'=>'Facturar'],function ()
        {
            Route::get('/','Web\Facturacion\FacturaController@Facturar')->name('facturar');
            Route::post('/conciliar','Web\Facturacion\FacturaController@show_view_conciliar')->name('conciliar');
        });

        Route::group(['prefix'=>'operador'],function(){
            Route::get('/','Web\Administrador\operadorcontroller@crear')->name('crearO');
            Route::post('/add','Web\Administrador\operadorcontroller@store')->name('storeO');
        });
        //Personas

        Route::group(['prefix'=>'personas'],function (){
            Route::get('/', 'Admin\Persona\PersonaController@index')->name('Personas');
            Route::post('/new_persona', 'Admin\Persona\PersonaController@crearP')->name('newP');
            Route::post('/lista_persona', 'Admin\Persona\PersonaController@showList')->name('ListaPer');
            Route::post('/store_persona', 'Admin\Persona\PersonaController@store' )->name('storeP');
            Route::post('/update_persona','Admin\Persona\PersonaController@update')->name('updateP');
            Route::post('/delete_persona','Admin\Persona\PersonaController@delete')->name('deleteP');
            Route::get('/upd_persona','Admin\Persona\PersonaController@updP')->name('updateP');
            Route::get('/tipo', 'Admin\Persona\PersonaController@tipo')->name('Tipo');
            Route::get('/clase', 'Admin\Persona\PersonaController@clase')->name('Clase');
            Route::get('/grupo','Admin\user\grupopersonacontroller@ver')->name('Grupo');
            Route::get('/especialidad', 'Admin\Persona\PersonaController@especialidades')->name('Especialidad');
            Route::get('/titulo', 'Admin\Persona\PersonaController@titulos')->name('Titulo');
            
            Route::get('/listaNegra','Admin\Persona\PersonaController@listanegra')->name('ListadoNegro');
          //  Route::post('listaNegra/lista','Admin\user\listanegracontroller@index')->name('ListadoNegro');
        });
        //Productos
        Route::group(['prefix'=>'productos'],function (){
            Route::get('/', 'Admin\Producto\ProductoController@index')->name('Productos');
            Route::post('/new', 'Admin\Producto\ProductoController@create')->name('newPr');
            Route::post('/lista', 'Admin\Producto\ProductoController@showList')->name('Lista');
            Route::post('/store_producto', 'Admin\Producto\ProductoController@store' )->name('storePr');
            Route::get('/categorias', 'Admin\Producto\CategoryController@index')->name('Categorias');
            Route::post('/store_categoria','Admin\Producto\CategoryController@store')->name('newCat');
            Route::post('/update_categoria', 'Admin\Producto\CategoryController@update')->name('updCat');
            Route::post('/delete_categoria','Admin\Producto\CategoryController@destroy')->name('delCat');

        });

        Route::group(['prefix'=>'reportes'],function (){
            Route::get('/', 'API\Actividades\ReporteController@index')->name('Reportes');
            Route::post('/delete','API\Actividades\ReporteController@destroy')->name('deleteReporte');
        });

        Route::group(['prefix'=>'ordenes'],function (){
            Route::get('/', 'API\Ordenes\OrdenController@index')->name('Ordenes');
            Route::post('/delete','API\Ordenes\OrdenController@destroy')->name('deleteOrden');
        });

        Route::group(['prefix'=>'facturas'],function (){
            Route::get('/', 'Admin\Facturacion\FacturaController@index')->name('Facturas');
            Route::post('/delete','API\Ordenes\FacturarController@destroy')->name('deleteFactura');
        });

        //Estadistico
        Route::group(['prefix'=>'estadistico'],function(){
            Route::get('/Ciclos', 'Administrativo\CRFController@index')->name('Ciclos');
            Route::get('/Frecuencias', 'Administrativo\CRFController@indexF')->name('Frecuencias');
            Route::get('/Ranking', 'Administrativo\CRFController@indexR')->name('Ranking');
            Route::get('/Parametro','Admin\Parametros\parametrocontroller@index')->name('Parametros');
        });

        //Geografico
        Route::group(['prefix'=>'geografico'],function(){
            Route::get('/Localizacion', 'Admin\Localizacion\TipoLocalizacionController@index')->name('Localizacion');
            Route::get('/TLocalizacion', 'Admin\Localizacion\TipoLocalizacionController@index2')->name('TLocalizacion');
            Route::get('/Cadenas', 'Admin\cadenas\cadenacontroller@index2')->name('Cadenas');
            Route::get('/zonas','Admin\Bricks\zonacontroller@index2')->name('Zonas');
            Route::get('/visitas', 'Admin\Persona\PersonaController@adminvisita')->name('ADMVisitas');
        });
        //configuracion
        Route::group(['prefix'=>'configuracion'],function(){
            Route::get('/Pais','Admin\Localizacion\PaisController@index')->name('Pais');
            Route::get('/Idioma', 'Admin\Bricks\idiomacontroller@index2')->name('Idioma');
            Route::get('/Moneda','Admin\moneda\monedacontroller@index')->name('Moneda');
            Route::get('/TipoActividad', 'Admin\Actividades\tipoactividadcontroller@index2')->name('Actividades');
            Route::get('/TipoIncidente', 'Admin\Incidencia\incidenciacontroller@index2')->name('Incidentes');
            Route::get('/Horario', 'Admin\Horarios\horariocontroller@index')->name('Horario');
            Route::get('/DiasVisita', 'Admin\planificador\habilescontroller@index')->name('DiasV');
            Route::get('/DiasFeriado', 'Admin\planificador\feriadocontroller@index')->name('DiasF');
            Route::get('/RedesSociales', 'Admin\Redes\redsocialcontroller@index2')->name('RedesS');
        });

        //Productos
        Route::group(['prefix'=>'Productos'],function(){
            Route::get('/Esquema', 'Admin\esquemas\esquemacontroller@index')->name('Esquema');
            Route::get('/Tipo', 'Admin\Producto\tipoproductocontroller@index2')->name('TipoProduct');
            Route::get('/Linea', 'Admin\Producto\lineaproductocontroller@index2')->name('LineaProduct');
            Route::get('/Unidades', 'Admin\unidades\unidadcontroller@index2')->name('Unidades');
        });

        Route::group(['prefix'=>'Seguridad'],function(){
            Route::get('/Auditoria', 'Admin\Seguridad\AuditoriasController@index')->name('Auditoria');
            Route::get('/Logs', 'Admin\Seguridad\LogsController@index')->name('Logs');
            Route::get('/Roles', 'API\Roles\RolesController@index')->name('Roles');
            Route::get('/Perfiles', 'Auth\perfilcontroller@index')->name('Perfil');
        });



    });

    Route::get('/Manual',function (){
        return view('Manual');
    })->name('manual');
    Route::get('/FQ',function (){
        return view('PreguntasFrecuentes');
    })->name('FQ');
    Route::get('/Videos',function (){
        return view('Videos');
    })->name('videos');
    Route::get('/unauthorized',function(){
        return view('unauthorized');
    })->name('unauthorized');
});

