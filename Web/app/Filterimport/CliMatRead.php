<?php


namespace RTR\Filterimport;

use PhpOffice\PhpSpreadsheet\Reader\IReadFilter;

class CliMatRead implements IReadFilter
{

    public function readCell($column, $row, $worksheetName = 'Cliente_Muestras')
    {
        if (in_array($column,range('A', 'B'))){
            return true;
        }
        return false;
    }
}