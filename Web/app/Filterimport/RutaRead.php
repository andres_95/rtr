<?php
/**
 * Created by PhpStorm.
 * User: usuario
 * Date: 03/05/2018
 * Time: 11:38
 */

namespace RTR\Filterimport;


use PhpOffice\PhpSpreadsheet\Reader\IReadFilter;

class RutaRead implements IReadFilter
{

    public function readCell($column, $row, $worksheetName = 'Rutas')
    {
        if (in_array($column,range('A', 'C'))){
            return true;
        }
        return false;
    }
}