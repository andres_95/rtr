<?php
/**
 * Created by PhpStorm.
 * User: usuario1
 * Date: 17/04/2018
 * Time: 02:01 PM
 */

namespace RTR\Filterimport;


use PhpOffice\PhpSpreadsheet\Reader\IReadFilter;

class ClienteDiasRead implements IReadFilter
{

    public function readCell($column, $row, $worksheetName = 'DiasDisponibles')
    {
        if (in_array($column,range('A', 'B'))){
            return true;
        }
        return false;
    }
}