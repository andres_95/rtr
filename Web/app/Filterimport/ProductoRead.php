<?php
/**
 * Created by PhpStorm.
 * User: usuario
 * Date: 25/04/2018
 * Time: 8:56
 */
namespace RTR\Filterimport;


use PhpOffice\PhpSpreadsheet\Reader\IReadFilter;

class ProductoRead implements IReadFilter
{

    public function readCell($column, $row, $worksheetName = 'Productos')
    {
        if (in_array($column,range('A', 'R'))){
            return true;
        }
        return false;
    }
}