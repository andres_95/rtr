<?php
/**
 * Created by PhpStorm.
 * User: usuario1
 * Date: 17/04/2018
 * Time: 02:01 PM
 */

namespace RTR\Filterimport;


use PhpOffice\PhpSpreadsheet\Reader\IReadFilter;

class PersonaRead implements IReadFilter
{

    public function readCell($column, $row, $worksheetName = 'zonas')
    {
        $arr = ['AA','AB','AC','AD','AE','AF','AG','AH','AI','AJ','AK','AL','AM','AN',];
        if (in_array($column,array_merge(range('A', 'Z'),$arr))){
            return true;
        }
        return false;
    }
}