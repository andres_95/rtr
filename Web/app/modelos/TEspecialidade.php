<?php

namespace RTR\modelos;

use Illuminate\Database\Eloquent\Model;

class TEspecialidade extends Model
{

	public $timestamps = false;
    public $incrementing = false;

	protected $guarded = ['id'];
	protected $fillable = [
        'id',
        'descripcion_especialidad',
        'estatus'
	];

	 public static $validators=[
		 'id'=>'required|unique:t_especialidades,id',
		 'descripcion'=>'required|unique:t_especialidades,descripcion_especialidad'
	 ];

    public function personas(){
		return $this->hasMany('RTR\modelos\TPersona','id','idactividad_negocio');
	}
}
