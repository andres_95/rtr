<?php

namespace RTR\modelos;

use Illuminate\Database\Eloquent\Model;

class TCiudade extends Model
{
    //
    public $incrementing = false;
	public $timestamps = false;
    protected $primaryKey='idCiudad';

	protected $fillable = [
		'idOperador',
		'nombreCiudad',
		'idestatus'
	];

	public function t_estado()
	{
		return $this->belongsTo(\App\modelos\TEstado::class, 'idestado')
					->where('t_estados.idestado', '=', 't_ciudades.idestado')
					->where('t_estados.idpais', '=', 't_ciudades.idpais');
	}
}
