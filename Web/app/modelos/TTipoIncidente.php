<?php

namespace RTR\modelos;

use Illuminate\Database\Eloquent\Model;
use RTR\Scopes\OperadorFabricante;

class TTipoIncidente extends Model
{
    protected $primaryKey = 'idtipo_incidentes';
	public $incrementing = true;
	public $timestamps = false;

	protected $hidden =['idOperador','idestatus'];

	protected $guarded = ['idtipo_incidentes'];

	protected $fillable = [
		'idOperador',
		'idFabricante',
		'idtipo_incidentes',
		'descripcion_tipo_incidentes',
		'idestatus'
	];

	public function getDescripcionTipoIncidentesAttribute($value)
    {
        return  utf8_encode($value);
    }

	/* GLOBAL FILTER OPERADOR , FABRICANTE TO CONSULT MYSQL*/

    protected static function boot()
    {
        parent::boot();
        static::addGlobalScope(new OperadorFabricante);
    }
}
