<?php

namespace RTR\modelos;

use Illuminate\Database\Eloquent\Model;
use RTR\Scopes\OperadorFabricante;
use RTR\Traits\DML;

class TBrickRuta extends Model
{
	use DML;
    protected $table='t_brick_rutas';
	protected $primaryKey='idbrick';
	public $incrementing = false;
	public $timestamps = false;

	protected $fillable = [
		'idOperador',
		'idFabricante',
		'idbrick' ,
		'Descripcion' ,
		'idzona'  ,
		'idestatus',
	];
	 public function zona(){
		 return $this->belongsTo(\RTR\modelos\TZona::class,'idzona','idzona');
	 }

    public function personas(){
        return $this->belongsToMany(\RTR\modelos\TPersona::class,'t_brick_ruta_personas','idbrick','idPersona');
    }
	
	/* GLOBAL FILTER OPERADOR , FABRICANTE TO CONSULT MYSQL*/

	protected static function boot()
    {
        parent::boot();
        static::addGlobalScope(new OperadorFabricante);
    }
}
