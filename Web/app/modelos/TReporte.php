<?php

namespace RTR;

use Illuminate\Database\Eloquent\Model;

class TReporte extends Model
{
    //
      protected $table='t_reportes';
      protected $primaryKey='idReporte';
      public $timestamps = true;

      protected $fillable=[
        'idReporte' ,
        'idRFV' ,
        'idCliente' ,
        'fecha' ,
        'idestatus' 
      ];

      public function rfv(){
          return $this->belongsTo('RTR\modelos\TPersona','idPersona','idRFV');
      }
      public function cliente(){
          return $this->belongsTo('RTR\modelos\TPersona','idPersona','idCliente');
      }
     public function Actividades(){
         return $this->hasMany('RTR\modelos\TActividadRepresentante','idReporte','idReporte');
     }
}
