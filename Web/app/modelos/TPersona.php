<?php

namespace RTR\modelos;


use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Laravel\Passport\HasApiTokens;
use RTR\Traits\DML;
use RTR\Scopes\OperadorFabricante;
use RTR\Events\UserEvent;
use RTR\Events\UpdateUserEvent;

class TPersona extends Authenticatable
{
    use Notifiable,HasApiTokens;
    use DML;
    protected $table='t_personas';
    protected $primaryKey='idPersona';
    public $incrementing=false;
    public $timestamps=false;
    public $modulos='050201';
    protected $dates = [
      'fecha_nacimiento_registro'
    ];
    protected $casts = [
        'nombre_completo_razon_social' => 'string',
    ];
    protected $searchable = [
        'nombre_completo_razon_social'
    ];
    protected $hidden = [
        'idOperador',
        'idFabricante',
        'password',
        'idempresa_Grupo',
        'idsupervisor',
        'idcadenas',
        'idpais',
        'ididioma',
        'idclase_persona',
        'idranking',
        'idciclos',
        'idfrecuencia',
        'idestado',
        'idciudad',
        'zona_postal',
        'banco_persona',
        'cuenta_principal_persona',
        'banco_persona_internacional',
        'cuenta_internacional_persona',
        'aba',
        'switf',
        'url_foto',
        'url_logo',
        'coordenadas_l',
        'coordenadas_a',
        'terminos_condiciones',
        'idestatus',
        'remember_token'
    ];

    protected $fillable = [
        'idOperador',
        'idFabricante',
        'idPersona',
        'username',
        'password',
        'idempresa_Grupo',
        'idsupervisor',
        'idcadenas',
        'idpais',
        'ididioma',
        'cod_tipo_persona',
        'idespecialidad',
        'idactividad_negocio',
        'idsubespecialidad',
        'documento_identidad',
        'idgrupo_persona',
        'idclase_persona',
        'idranking',
        'idperfil',
        'idtitulo',
        'nombre_persona',
        'apellido_persona',
        'nombre_completo_razon_social',
        'sexo_genero_persona',
        'fecha_nacimiento_registro',
        'telefono_persona',
        'movil_persona',
        'email_persona',
        'idciclos',
        'idfrecuencia',
        'direccion_domicilio',
        'idestado',
        'idciudad',
        'zona_postal',
        'banco_persona',
        'cuenta_principal_persona',
        'banco_persona_internacional',
        'cuenta_internacional_persona',
        'aba',
        'switf',
        'url_foto',
        'url_logo',
        'coordenadas_l',
        'coordenadas_a',
        'terminos_condiciones',
        'persona_contacto',
        'telefono_contacto',
        'email_contacto',
        'descuento',
        'idestatus',
        'remember_token'
    ];
   
    protected $request=[
        'idFabricante'=>'fabricante',
        'nombre_persona'=>'nombre',
        'apellido_persona'=>'apellido',
        'idgrupo_persona'=>'Tpersona',
        'cod_tipo_persona'=>'tipopersona',
        'email_persona'=>'email',
        'telefono_persona'=>'telf',
        'direccion_domicilio'=>'Direccion',
        'sexo_genero_persona'=>'Genero',
        'idgrupo_persona'=>'Tpersona',
        'idpais'=>'pais',
        'idespecialidad'=>'especialidad',
        'idranking'=>'factura',
        'idclase_persona'=>'confirm'
        

    ];
  /*  "nombre":this.editedItem.nombre,
    "apellido":this.editedItem.apellido,
    "tipo":tipo,
    "CI":this.editedItem.CI,
    "email":this.editedItem.email,
    "telf":this.editedItem.telf,
    "direccion":this.editedItem.direccion,
    "genero":this.editedItem.genero,
    "especialidad":esp,
    "clase":clas,
    "ranking":rank,
    "frecuencia":frec,
    "descuento":this.editedItem.descuento,
    "username":this.editedItem.username,
    "password":this.editedItem.password,
    "ciclo":ciclo,
    "supervisor":sup,
    "pais":pais,
    "estado":estado,
    "ciudad":ciudad,
    "categoria":this.categ,
    "fabricante":this.fabricante*/
    public static $validator=[
        'fabricante'=>'required|exists:t_personas,idPersona',
        'CI'=>'required|unique:t_personas,idPersona|max:11',
        'username'=>'required|unique:t_personas,username',
        'password'=>'required|max:11',
    //    'idempresa_Grupo'=>'required|exists:t_grupopersonas,idgrupo_persona|max:5',
        'supervisor'=>'nullable|exists:t_personas,idPersona',
       // 'cadenas',
        'pais'=>'required|exists:t_paises,idpais',
     //   'idioma',
        'tipo'=>'required|exists:t_tipo_personas,id',
      //  'especialidad'=>'required|exists:t_tipo_especialidad,idespecialidad',
        'especialidad'=>'required|exists:t_especialidades,id',
        'categoria'=>'required|exists:t_grupopersonas,idgrupo_persona',
        'ranking'=>'required|int',
        'perfil'=>'required|string',
        'titulo'=>'required|string',
        'nombre'=>'required|string',
        'apellido'=>'required|string',
       // 'nombre_completo_razon_social'=>'required|string',
        'genero'=>'required|in:M,F',
     //   'fecha_nacimiento_registro'=>'required|date',
        'telf'=>'nullable|numeric',
      //  'movil_persona'=>'required|regex:^[0-9]{3}-[0-9]{7}$',
       'email'=>'email',
        'ciclo'=>'nullable|exists:t_ciclos,id',
        'frecuencia'=>'required|alpha_num',
        'direccion'=>'nullable|string',
        'estado'=>'required|int|exists:t_estados,idestado',
        'ciudad'=>'required|int|exists:t_ciudades,idCiudad',
        'clase'=>'required|exists:t_clase_personas,id',
     //   'zona'=>'required|int',
       /* 'coordenadas_l'=>'required',
        'coordenadas_a'=>'required',*/
   //     'persona_contacto'=>'required|string',
  //      'telefono_contacto'=>'required|regex:^[0-9]{3}-[0-9]{7}$',
 //       'email_contacto'=>'required|email',
        'descuento'=>'nullable|numeric|between:0,100',
        'categoria'=>'required|string',
        'email'=>'nullable|regex:^[a-zA-Z0-9]+\@[a-zA-Z0-9]+\.[a-z]{3}$^',
        'password'=>'required|min:5',

    ];

     public static $messages=[
         'nombre_persona.string'=>'El nombre no puede contener numeros',
         'apellido_persona.string'=>'El apellido no puede conteneer numeros'
     ];

     protected $dispatchesEvents=[
           'created'=>UserEvent::class,
           'updated'=>UpdateUserEvent::class,
          
     ];

     

    /*  GETTERS ENCODE */

    public function getNombreCompletoRazonSocialAttribute($value)
    {
        return  utf8_encode($value);
    }

    public function getNombrePersonaAttribute($value)
    {
        return  utf8_encode($value);
    }

    public function getApellidoPersonaAttribute($value)
    {
        return  utf8_encode($value);
    }

    public function getDireccionDomicilioAttribute($value)
    {
        return  utf8_encode($value);
    }

    /* Full - Text Search Scope */
    protected function fullTextWildcards($term)
    {
        return str_replace(' ', '*', $term) . '*';
    }

    public function scopeSearch($query, $term)
    {
        $columns = implode(',',$this->searchable);
        $query->whereRaw("MATCH ({$columns}) AGAINST (? IN BOOLEAN MODE)" , $this->fullTextWildcards($term));
        return $query;
    }

    /* QUERY BUILDER SCOPES */


    public function scopeTypeRFV($builder)
    {
        return $builder->where('idgrupo_persona','RFV');
    }

    public function scopeTypeMayorista($builder)
    {
        return $builder->where('idgrupo_persona','MAY');
    }

    public  function scopeTypeCliente($builder){
        return $builder->where('idgrupo_persona','CLI');
    }

    public function scopeRawAgenda($builder){
        $builder->raw('g inner JOIN (select  idCliente, count(*) from `t_planificadores` where `idOperador` = `01` and `idFabricante` = `DEMO` and `idestatus`=1 group by `idCliente`) idP ON g.idPersona = idP.idCliente');
    }

    /* Relations */

    public function Perfil()
    {
        return $this->hasOne('RTR\modelos\TPerfile','id','idperfil');
    }

    public function Agenda()
    {
        return $this->hasOne('RTR\modelos\TPlanificadore','idCliente','idPersona');
    }

    public function Dias(){
        return $this->belongsToMany(\RTR\modelos\TDiasVisita::class,'t_persona_dias','idPersona','iddias_visita');
    }

    public function Horarios(){
        return $this->belongsToMany(\RTR\modelos\THorario::class,'t_persona_horario','idPersona','idhorarios');
    }

    public function Clientes(){
        return $this->belongsToMany(\RTR\modelos\TPersona::class,'r_cliente_rfv','id_RFV','id_cliente');
    }

    public function Materiales(){
        return $this->belongsToMany(\RTR\modelos\TProducto::class,'r_clientes_materiales','idcliente','idproducto');
    }

    public function bricks()
    {
        return $this->belongsToMany(\RTR\modelos\TBrickRuta::class,'t_brick_ruta_personas','idPersona','idbrick');
    }

    /* GLOBAL FILTER OPERADOR , FABRICANTE TO CONSULT MYSQL*/

    protected static function boot()
    {
        parent::boot();
        static::addGlobalScope(new OperadorFabricante);
    }
}
