<?php

namespace RTR\modelos;

use Illuminate\Database\Eloquent\Model;

class TTipoPersona extends Model
{
    //
    protected $primaryKey = 'id';
	public $incrementing = false;
	public $timestamps = false;

	protected $fillable = [
		'idOperador',
		'descripcion_tipo_persona',
		'idestatus'
	];
}
