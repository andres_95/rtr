<?php

namespace RTR\modelos;

use Illuminate\Database\Eloquent\Model;
use RTR\Scopes\OperadorFabricante;

class TDiasVisita extends Model
{
    protected $table='t_dias_visitas';
	protected $primaryKey='iddias_visita';
	public $incrementing = false;
	public $timestamps = false;

	protected $hidden = ['idpais','idOperador','idFabricante',
        'idestatus'];
	protected $fillable = [
		'idFabricante',
        'idOperador',
		'idpais',
		'iddias_visita',
		'descripcion_dias_visita',
		'idestatus'
	];


	public function personas(){
		return $this->belongsToMany(\RTR\modelos\TPersona::class,'t_persona_dias','iddias_visita','idPersona');
	}

    protected static function boot()
    {
        parent::boot();
        static::addGlobalScope(new OperadorFabricante);
    }
}
