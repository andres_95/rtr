<?php

namespace RTR\modelos;

use Illuminate\Database\Eloquent\Model;

class TNotificacion extends Model
{
    //
     protected $table='t_notificaciones';
     protected $primaryKey='idNotificacion';
     public $timestamps = false;
     public $incrementing=true;
       protected $date=[
           'fecha_registro'
       ];
     protected $fillable=[
        'idOperador',
        'idFabricante',
        'idPersona',
        'idFabricante',
        'idtipo',
        'descripcion_notoficacion' ,
        'fecha_registro' ,
        'idestatus'
     ];

        public function tipo(){
            return $this->belongsTo(\RTR\modelos\TTipoNotificacion::class,'idtipo','id');
        }
        public function representante(){
            return $this->belongsTo(\RTR\modelos\TPersona::class,'idPersona','idPersona');

        }
}

