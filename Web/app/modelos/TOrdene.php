<?php

namespace RTR\modelos;

use Illuminate\Database\Eloquent\Model;
use RTR\Scopes\OperadorFabricante;
use RTR\Traits\DML;
use RTR\events\ordenEvent;
use RTR\events\UpdateordenEvent;

class TOrdene extends Model
{
	use DML;
	
    protected $primaryKey = 'idorden';
	public $incrementing = true;
	public $timestamps = false;
	
   
	protected $casts = [
		'impuesto' => 'float',
		'comision' => 'float',
		'costoTotal' => 'float',
		'descuento' => 'float'
	];

    

	

	protected $guarded = [
		'idorden'
	];

	protected $dates = [
		'fechaOrden',
		'fecha_envio_orden',
		'fecha_entrega'
	];
	protected $fillable = [
		'idorden',
		'idOperador',
		'idFabricante',
		'idPersona',
		'idpasadopor',
		'idMayorista',
		'idPersona_solicitante',
		'idpais',
		'ididioma',
		'idmoneda',
		'tipo_operacion',
		'impuesto',
		'comision',
		'costoTotal',
		'descuento',
		'fechaOrden',
		'coordenadas_l', 
		'coordenadas_a',
		'fecha_envio_orden',
		'registro_entrega',
		'fecha_entrega',
		'firma_signature',
		'comentario_entrega',
		'idtipo_incidentes',
		'idfactura',
		'idestatus',
		'TotalUnidades'
	];

	 

	public function Productos(){
        return $this->belongsToMany(\RTR\modelos\TProducto::class,'t_item_ordenes','idorden','idproducto')
          ->withPivot('idOperador','idFabricante','nombreproducto','idfactura','idMayorista','cantidad_facturada','cantidad_solicitada','cantidad_conciliada','cantidad_faltante','item_price','item_total','idunidades','item_descuento');
    }

    public function Mayoristas(){
        return $this->belongsToMany('RTR\modelos\TPersona','t_orden_mayorista','idorden','idPersona')
            ->withPivot('nombre_mayorista','item_descuento','orden_total','idOperador','idFabricante','idpais','idCliente','ididioma','idmoneda','impuesto','orden_total','idestatus');
    }
	

	public function Factura(){
        return $this->hasOne('RTR\modelos\TFactura', 'idfactura', 'idorden');
    }

    public function scopeOperadorFabricante($builder,$o,$f)
    {
    	return $builder->where('idOperador',$o)->where('idFabricante',$f);
    }
	/* GLOBAL FILTER OPERADOR , FABRICANTE TO CONSULT MYSQL*/

    protected static function boot()
    {
        parent::boot();
        static::addGlobalScope(new OperadorFabricante);
    }
}
