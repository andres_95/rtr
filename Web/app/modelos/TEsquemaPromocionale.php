<?php

/**
 * Created by Illuminate Model.
 * Date: Wed, 24 Jan 2018 12:41:31 +0000.
 */

namespace RTR\modelos;
use RTR\Scopes\OperadorFabricante;
use RTR\events\EsquemaEvent;
use RTR\events\UpdateEsquemaEvent;

use Illuminate\Database\Eloquent\Model as Eloquent;


/**
 * Class TEsquemaPromocionale
 * 
 * @property string $idOperador
 * @property string $idfabricante
 * @property string $idesqprom
 * @property string $idProducto
 * @property string $idRFV
 * @property float $cantidad_desde
 * @property float $cantidad_hasta
 * @property float $monto_desde
 * @property float $monto_hasta
 * @property float $porcentaje
 * @property float $cantidad
 * @property string $Alta_Baja_Update
 * @property string $idestatus
 * 
 * @property \App\modelos\TProducto $t_producto
 *
 * @package App\modelos
 */
class TEsquemaPromocionale extends Eloquent
{
	protected $primaryKey='idesqprom';
	public $incrementing = false;
	public $timestamps = false;
	public $modulos='050801';

	protected $casts = [
		'cantidad_desde' => 'float',
		'cantidad_hasta' => 'float',
		'monto_desde' => 'float',
		'monto_hasta' => 'float',
		'porcentaje' => 'float',
		'cantidad' => 'float'
	];

	protected $fillable = [
		'idOperador' ,
		'idFabricante' ,
		'idesqprom' , 
		'idProducto' ,
		'idMayorista',
		'cantidad_desde' ,
		'cantidad_hasta' ,
		'monto_desde' ,
		'monto_hasta' ,
		'cantidad' ,
		'porcentaje',
		'Alta_Baja_Update' ,
		'idestatus' 
	];

	protected $dispatchesEvents=[
		'created'=>EsquemaEvent::class,
		'updated'=>UpdateEsquemaEvent::class
	];

	public static $validators=[
		"id"=>'required|int|unique:t_esquema_promocionales,idesqprom',
		"idproducto"=>'required|exists:t_productos,idproducto',
		"nombre"=>'required|string',
		"cantD"=>'nullable|numeric|min:0',
		"cantH"=>'nullable|numeric|min:0',
		"montoD"=>'nullable|numeric|min:0',
		"montoH"=>'nullable|numeric|min:0',
		"porcentaje"=>'numeric|between:0,100',
		"mayorista"=>'required|exists:t_personas,idPersona',
		"cant"=>'min:0',
		"fabricante"=>'required|exists:t_personas,idPersona'
	];


	public function t_producto()
	{
		return $this->belongsTo(\App\modelos\TProducto::class, 'idProducto');
	}

	protected static function boot()
    {
        parent::boot();
        static::addGlobalScope(new OperadorFabricante);
    }
}
