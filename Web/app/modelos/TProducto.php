<?php

namespace RTR\modelos;

use Illuminate\Database\Eloquent\Model;
use RTR\Scopes\OperadorFabricante;
use RTR\Traits\DML;
use RTR\Events\ProductEvent;
use RTR\Events\UpdateProductEvent;

class TProducto extends Model
{
    use DML;
    protected $primaryKey='idproducto';
	public $incrementing = false;
	public $timestamps = false;
    public $modulos='050202';

	protected $dates = [
		'fechaRegistro_producto',
		'fechaExpedicion_producto',
		'fechaVencimiento_producto',
		'FechaInicioPublicacion',
		'FechaFinPublicacion'
	];

	protected  $hidden = [
		'idOperador' ,
		'idfabricante' ,
		'idPersona' ,
        'idpais',
        'ididioma' ,
        'idmoneda' ,
    ];

	protected $fillable = [
		'idOperador',
		'idfabricante',
		'idproducto',
		'idPersona',
		'idMayorista',
		'idpais',
		'ididioma',
		'idmoneda',
		'idlinea_producto',
		'idtipo_producto',
		'nombre_producto',
		'descripcion_producto',
		'principio_producto',
		'Precio_producto',
		'Descuento_producto',
		'idcategorias',
		'presentacion',
		'cantidad_producto_existente',
		'fechaRegistro_producto',
		'fechaExpedicion_producto',
		'fechaVencimiento_producto',
		'dias_publicacion',
		'idpromocion',
		'lote',
		'FechaInicioPublicacion',
		'FechaFinPublicacion',
		'estatus_producto',
		'idunidades'
	];

	protected $request=[
		 'idfabricante'=>'fabricante',
		 'idcategorias'=>'categoria',
		 'cantidad_producto_existente'=>'cantidad',
		 'fechaExpedicion_producto'=>'dateE',
		 'fechaVencimiento_producto'=>'dateV',
		 'idlinea_producto'=>'linea'
	];
	
	public static $validators=[
		
		'fabricante'=>'required|string|exists:t_personas,idPersona',
		'id'=>'required|string|unique:t_productos,idProducto',
		'mayorista'=>'required|exists:t_personas,idPersona',
	   /* 'idpais'=>,
		'ididioma',
		'idmoneda',*/
		'linea'=>'required|exists:t_linea_productos,id',
		'tipo'=>'required|exists:t_tipo_productos,idtipo_producto',
		'nombre'=>'required|string',
		'precio'=>'required|numeric|min:1',
		'descuento'=>'nullable|numeric|between:0,100',
		'categoria'=>'required|string',
	//	'presentacion'=>'required|string',
		'cantidad'=>'nullable|int|min:1',
		//'fechaRegistro_producto'=>'required|date',
		'expedicion'=>'required|date',
		'vencimiento'=>'required|date',
		'cantidad'=>'required|int|min:1',
	  //  'dias_publicacion',
	  //  'idpromocion',
	//	'lote'=>'required|int',
		'mayorista'=>'required|string|exists:t_personas,idPersona'
	  //  'FechaInicioPublicacion',
	  //  'FechaFinPublicacion',
	  //  'estatus_producto',
	   // 'idunidades'
	];

	protected $dispatchesEvents=[
		'created'=>ProductEvent::class,
		'updated'=>UpdateProductEvent::class
	];

	public function getNombreProductoAttribute($value)
    {
        return  utf8_encode($value);
    }

    public function getPrincipioProductoAttribute($value)
    {
        return  utf8_encode($value);
    }

    public function getDescripcionProductoAttribute($value)
    {
        return  utf8_encode($value);
    }

    public function getPresentacionAttribute($value)
    {
        return  utf8_encode($value);
    }

    public function scopeList($query){
        return $query->where('idcategorias','!=',"MUES");
	}


    protected static function boot()
    {
        parent::boot();
        static::addGlobalScope(new OperadorFabricante);
    }
}
