<?php
/**
 * Created by PhpStorm.
 * User: usuario
 * Date: 14/05/2018
 * Time: 10:49
 */
namespace RTR\modelos;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;

class TTmpPlanificadore extends Model
{
    protected $primaryKey = 'Id';
    public $incrementing = true;
    public $timestamps = false;

    protected $hidden = ['idOperador'];

    protected $fillable = [
        'idOperador',
        'idFabricante',
        'idRFV',
        'idCliente',
        'Fecha',
        'Hora',
        'idSupervisor',
        'idstatus',
    ];

    protected static function boot()
    {
        parent::boot();

        static::addGlobalScope('idestatus', function (Builder $builder) {
            $builder->where('idstatus', 1);
        });
    }

    public function scopeOperadorFabricante($query, $Operador,$Fabricante)
    {
        return $query->where('idOperador', $Operador)->where('idFabricante',$Fabricante);
    }

}