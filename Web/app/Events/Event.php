<?php

namespace RTR\Events;

use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use RTR\modelos\TLogAuditoria;
use Illuminate\Support\Facades\Route;

class Event
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }
    
    public function isAuditable($model){
         
           $modulo=substr($model->modulos,0,2);
           $opcion=substr($model->modulos,-4);
         
         return (TLogAuditoria::where('cod_modulo',$modulo)
                               ->where('cod_opcion',$opcion)
                               ->where('idestatus',1)
                               ->count()==1);
    }

  /*  public function isAuditableByModulo($modulos){
        $modulo=substr($modulos,0,2);
        $opcion=substr($modulos,-4);
      
      return (TLogAuditoria::where('cod_modulo',$modulo)
                            ->where('cod_opcion',$opcion)
                            ->where('idestatus',1)
                            ->count()==1);
    }*/

   
    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel('channel-name');
    }
}
