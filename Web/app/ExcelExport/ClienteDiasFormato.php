<?php
/**
 * Created by PhpStorm.
 * User: usuario1
 * Date: 16/04/2018
 * Time: 01:49 PM
 */

namespace RTR\ExcelExport;


use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\RegistersEventListeners;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithTitle;
use Maatwebsite\Excel\Events\AfterSheet;
use PhpOffice\PhpSpreadsheet\Cell\DataValidation;
use RTR\modelos\TDiasVisita;
use RTR\modelos\TPersona;

class ClienteDiasFormato implements FromCollection, WithEvents, WithTitle, WithHeadings
{
    use Exportable, RegistersEventListeners;
    static $fabricante = '';
    public function __construct($fab = '')
    {
        self::$fabricante = $fab;
    }

    public function collection()
    {
        return collect([]);
    }

    public function title(): string
    {
        return 'DiasDisponibles';
    }

    public function headings(): array
    {
        return [
            'idPersona',
            'iddias_visita'
        ];
    }

    public static function afterSheet(AfterSheet $event)
    {
        $fondo = [
            'font' => [
                'bold' => true,
            ],
            'alignment' => [
                'horizontal' => 'center',
            ],
            'fill' => [
                'fillType' => 'solid',
                'startColor' => [
                    'argb' => 'FFFFFF00',
                ],
            ],
        ];

        $bordes = [
            'borders' => [
                'allBorders' => [
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                ]
            ],

        ];

        $event->sheet->setAutoFilter(
            $event->sheet->calculateWorksheetDimension()
        );

        $event->sheet->getColumnDimension('A')->setAutoSize(true);
        $event->sheet->getColumnDimension('B')->setAutoSize(true);

        $fabricante = TPersona::withoutGlobalScopes()->where('idOperador',Auth::user()->idOperador)->find(self::$fabricante);

        $personas = TPersona::withoutGlobalScopes()
            ->where('idOperador',$fabricante->idOperador)
            ->where('idFabricante',$fabricante->idPersona)
            ->where('idgrupo_persona','CLI')
            ->get(['idPersona','nombre_completo_razon_social'])->toArray();

        $diasDisponible = TDiasVisita::withoutGlobalScopes()
            ->where('idOperador',$fabricante->idOperador)
            ->where('idFabricante',$fabricante->idPersona)
            ->where('idestatus',1)
            ->get(['iddias_visita','descripcion_dias_visita'])->toArray();

        $event->sheet->fromArray(
            $personas,
            NULL,
            'D3'
        );

        $event->sheet->mergeCells('D1:E1');
        $event->sheet->setCellValue('D1', 'Clientes');
        $event->sheet->setCellValue('D2', 'ID');
        $event->sheet->setCellValue('E2', 'NOMBRE');
        $event->sheet->getColumnDimension('D')->setAutoSize(true);
        $event->sheet->getColumnDimension('E')->setAutoSize(true);

        $event->sheet->fromArray(
            $diasDisponible,
            NULL,
            'G3'
        );

        $event->sheet->mergeCells('G1:H1');
        $event->sheet->setCellValue('G1', 'Dias disponibles');
        $event->sheet->setCellValue('G2', 'ID');
        $event->sheet->setCellValue('H2', 'Dia');
        $event->sheet->getColumnDimension('G')->setAutoSize(true);
        $event->sheet->getColumnDimension('H')->setAutoSize(true);

        $length = count($personas);
        $event->sheet->getStyle('D1:E'.($length+2))->applyFromArray($bordes);

        $event->sheet->setDataValidation(
            'A:A',
            (new DataValidation())
                ->setType(DataValidation::TYPE_LIST)
                ->setShowDropDown(true)
                ->setErrorStyle( \PhpOffice\PhpSpreadsheet\Cell\DataValidation::STYLE_STOP )
                ->setShowErrorMessage(true)
                ->setFormula1("DiasDisponibles!$"."D"."$3:D$".($length+2))
        );

        $length = count($diasDisponible);

        $event->sheet->getStyle('G1:H'.($length+2))->applyFromArray($bordes);

        $event->sheet->setDataValidation(
            'B:B',
            (new DataValidation())
                ->setType(DataValidation::TYPE_LIST)
                ->setShowDropDown(true)
                ->setErrorStyle( \PhpOffice\PhpSpreadsheet\Cell\DataValidation::STYLE_STOP )
                ->setShowErrorMessage(true)
                ->setFormula1("DiasDisponibles!$"."G"."$3:G$".($length+2))
        );

        $event->sheet->getStyle('D1:E1')->applyFromArray($fondo);
        $event->sheet->getStyle('G1:H1')->applyFromArray($fondo);
        $event->sheet->getStyle('A1:B1')->applyFromArray($fondo);
        $event->sheet->getStyle('A1:B1')->applyFromArray($bordes);

    }

}