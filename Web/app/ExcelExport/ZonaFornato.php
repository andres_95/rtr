<?php
/**
 * Created by PhpStorm.
 * User: usuario1
 * Date: 12/04/2018
 * Time: 03:35 PM
 */

namespace RTR\ExcelExport;


use Illuminate\Support\Facades\Request;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\RegistersEventListeners;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithTitle;
use Maatwebsite\Excel\Events\AfterSheet;
use PhpOffice\PhpSpreadsheet\Cell\DataValidation;
use RTR\modelos\TCiudade;
use RTR\modelos\TEspecialidade;
use RTR\modelos\TEstado;
use RTR\modelos\TPersona;

class ZonaFornato implements FromCollection, WithHeadings, WithTitle,WithEvents
{
    use Exportable, RegistersEventListeners;


    static $fabricante;

    public function __construct($Fabricante = '')
    {
        self::$fabricante = $Fabricante;
    }
    

    public function collection()
    {
        return collect([]);
    }

    public function headings(): array
    {
        return [
            'Descripcion',
            'idesatdo',
            'idciudad'
        ];
    }
    public function title(): string
    {
        return 'zonas';
    }

    public static function afterSheet(AfterSheet $event)
    {
        $event->sheet->setAutoFilter(
            $event->sheet->calculateWorksheetDimension()
        );

        $event->sheet->getColumnDimension('A')->setAutoSize(true);
        $event->sheet->getColumnDimension('B')->setAutoSize(true);
        $event->sheet->getColumnDimension('C')->setAutoSize(true);


        $pais = TPersona::withoutGlobalScopes()->find(self::$fabricante);
        $ciudades= TCiudade::where('idpais',$pais->idpais)
            ->select(['idCiudad','nombreCiudad'])
            ->get()->toArray();
        $estados = TEstado::where('idpais',$pais->idpais)
            ->select(['idestado','nombreCiudad'])
            ->get()->toArray();

        $event->sheet->fromArray(
            $estados,
            NULL,
            'E3'
        );
        $event->sheet->mergeCells('E1:F1');
        $event->sheet->setCellValue('E1', 'Estados');
        $event->sheet->setCellValue('E2', 'ID');
        $event->sheet->setCellValue('F2', 'NOMBRE');
        $event->sheet->getColumnDimension('E')->setAutoSize(true);
        $event->sheet->getColumnDimension('F')->setAutoSize(true);

        $event->sheet->fromArray(
            $ciudades,
            NULL,
            'H3'
        );
        $event->sheet->mergeCells('H1:I1');
        $event->sheet->setCellValue('H1', 'Ciudades');
        $event->sheet->setCellValue('H2', 'ID');
        $event->sheet->setCellValue('I2', 'NOMBRE');
        $event->sheet->getColumnDimension('H')->setAutoSize(true);
        $event->sheet->getColumnDimension('I')->setAutoSize(true);

        $stringE = '"';

        foreach ($estados as $clave => $valor)
        {
            $stringE.= $valor['idestado'].',';
        }
        $stringE = trim($stringE, ',').'"';
        $event->sheet->setDataValidation(
            'A:A',
            (new DataValidation())
                ->setType(DataValidation::TYPE_TEXTLENGTH)
                ->setAllowBlank(false)
                ->setErrorStyle( \PhpOffice\PhpSpreadsheet\Cell\DataValidation::STYLE_STOP )
                ->setShowErrorMessage(true)
                ->setError('Palabra de 1 a 40 Caracteres')
                ->setFormula1(1)
                ->setFormula2(40)
        );
        $event->sheet->setDataValidation(
            'B:B',
            (new DataValidation())
                ->setType(DataValidation::TYPE_LIST)
                ->setShowDropDown(true)
                ->setErrorStyle( \PhpOffice\PhpSpreadsheet\Cell\DataValidation::STYLE_STOP )
                ->setShowErrorMessage(true)
                ->setFormula1($stringE)
        );

        $stringC = '"';

        foreach ($ciudades as $clave => $valor)
        {
            $stringC.= $valor['idCiudad'].',';
        }
        $stringC = trim($stringC, ',').'"';
        $cant = count($ciudades);
        $cant+=2;
        $event->sheet->setDataValidation(
            'C:C',
            (new DataValidation())
                ->setType(DataValidation::TYPE_LIST)
                ->setShowDropDown(true)
                ->setErrorStyle( \PhpOffice\PhpSpreadsheet\Cell\DataValidation::STYLE_STOP )
                ->setShowErrorMessage(true)
                ->setFormula1('$H3:$H'.$cant)
        );


        $fondo = [
            'font' => [
                'bold' => true,
            ],
            'alignment' => [
                'horizontal' => 'center',
            ],
            'fill' => [
                'fillType' => 'solid',
                'startColor' => [
                    'argb' => 'FFFFFF00',
                ],
            ],
        ];

        $event->sheet->getStyle('E1:F1')->applyFromArray($fondo);
        $event->sheet->getStyle('H1:I1')->applyFromArray($fondo);

        $bordes = [
            'borders' => [
                'allBorders' => [
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                ]
            ],

        ];
        $event->sheet->getStyle('H1:I'.$cant)->applyFromArray($bordes);
        $cante = count($estados);
        $cante += 2;
        $event->sheet->getStyle('E1:F'.$cante)->applyFromArray($bordes);

    }




}