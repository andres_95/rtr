<?php
/**
 * Created by PhpStorm.
 * User: usuario1
 * Date: 16/04/2018
 * Time: 01:49 PM
 */

namespace RTR\ExcelExport;


use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\RegistersEventListeners;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithTitle;
use Maatwebsite\Excel\Events\AfterSheet;
use PhpOffice\PhpSpreadsheet\Cell\DataValidation;

class CadenaFormato implements FromCollection, WithEvents, WithTitle, WithHeadings
{

    use Exportable, RegistersEventListeners;



    public function collection()
    {
        return collect([]);
    }

    public function title(): string
    {
        return 'cadenas';
    }

    public function headings(): array
    {
        return [
            'Descripcion'
        ];
    }

    public static function  afterSheet( AfterSheet $event)
    {
        $event->sheet->setAutoFilter(
            $event->sheet->calculateWorksheetDimension()
        );
        $event->sheet->setDataValidation(
            'A:A',
            (new DataValidation())
                ->setType(DataValidation::TYPE_TEXTLENGTH)
                ->setAllowBlank(false)
                ->setErrorStyle( \PhpOffice\PhpSpreadsheet\Cell\DataValidation::STYLE_STOP )
                ->setShowErrorMessage(true)
                ->setError('Palabra de 1 a 60 Caracteres')
                ->setFormula1(1)
                ->setFormula2(60)
        );

        $fondo = [
            'font' => [
                'bold' => true,
            ],
            'alignment' => [
                'horizontal' => 'center',
            ],
            'fill' => [
                'fillType' => 'solid',
                'startColor' => [
                    'argb' => 'FFFFFF00',
                ],
            ],
        ];
        $bordes = [
            'borders' => [
                'allBorders' => [
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                ]
            ],

        ];

        $event->sheet->mergeCells('E1:F1');
        $event->sheet->setCellValue('E1', 'REGLAS DEL FORMATO');
        $event->sheet->mergeCells('E2:F2');
        $event->sheet->setCellValue('E2', 'Descripcion hasta 60 caracteres');
        $event->sheet->getStyle('E1:F1')->applyFromArray($fondo);
        $event->sheet->getStyle('E1:F2')->applyFromArray($bordes);
        $event->sheet->getColumnDimension('A')->setAutoSize(true);
        $event->sheet->getColumnDimension('E')->setWidth(20);
    }


}