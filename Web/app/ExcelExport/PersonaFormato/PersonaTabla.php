<?php
/**
 * Created by PhpStorm.
 * User: usuario1
 * Date: 12/04/2018
 * Time: 03:35 PM
 */

namespace RTR\ExcelExport\PersonaFormato;

use Illuminate\Support\Facades\Request;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\RegistersEventListeners;
use Maatwebsite\Excel\Concerns\WithColumnFormatting;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithTitle;
use Maatwebsite\Excel\Events\AfterSheet;
use PhpOffice\PhpSpreadsheet\Style\NumberFormat;
use PhpOffice\PhpSpreadsheet\Cell\DataValidation;
use Carbon\Carbon;

class PersonaTabla implements  WithHeadings, WithTitle, WithEvents, WithColumnFormatting
{
    use Exportable, RegistersEventListeners;
    /**
     * ClienteFormato constructor.
     */

    public function headings(): array
    {
        return [
            'idPersona',
            'idempresa_Grupo',
            'idsupervisor',
            'idcadenas',
            'idpais',
            'ididioma',
            'idmoneda',
            'cod_tipo_persona',
            'idespecialidad',
            'idactividad_negocio',
            'idsubespecialidad',
            'documento_identidad',
            'idgrupo_persona',
            'idclase_persona',
            'idranking',
            'nombre_persona',
            'apellido_persona',
            'nombre_completo_razon_social',
            'sexo_genero_persona',
            'fecha_nacimiento_registro',
            'telefono_persona',
            'movil_persona',
            'email_persona',
            'idtitulo',
            'idciclos',
            'idfrecuencia',
            'direccion_domicilio',
            'idestado',
            'idciudad',
            'zona_postal',
            'banco_persona',
            'cuenta_principal_persona',
            'banco_persona_internacional',
            'cuenta_internacional_persona',
            'coordenadas_l',
            'coordenadas_a',
            'persona_contacto',
            'telefono_contacto',
            'email_contacto',
            'descuento'
        ];
    }

    public function columnFormats(): array
    {
        return [
            'T' => NumberFormat::FORMAT_DATE_YYYYMMDD,
            'AN' => NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1,
            'B'=>'@',
            'D'=>'@',
            'E'=>'@',
            'H'=>'@',
            'I'=>'@',
            'J'=>'@',
            'K'=>'@',
            'M'=>'@',
            'N'=>'@',
            'O'=>'@',
            'X'=>'@',
            'Y'=>'@',
            'Z'=>'@',
            'AB'=>'@',
            'AC'=>'@',
        ];
    }

    public function title(): string
    {
        return 'TablaPrincipal';
    }


    public static function afterSheet(AfterSheet $event)
    {
        $current = Carbon::now();
        $tabla = new PersonaQuery(Request::getFacadeRoot()->id);

        $fondo = [
            'font' => [
                'bold' => true,
            ],
            'alignment' => [
                'horizontal' => 'center',
            ],
            'fill' => [
                'fillType' => 'solid',
                'startColor' => [
                    'argb' => 'FFFFFF00',
                ],
            ],
        ];

        $bordes = [
            'borders' => [
                'allBorders' => [
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                ]
            ],

        ];

        $event->sheet->getStyle('A1:AN1')->applyFromArray($fondo);
        $event->sheet->getStyle('A1:AN1')->applyFromArray($bordes);

        $event->sheet->setAutoFilter(
            $event->sheet->calculateWorksheetDimension()
        );

        $start = 'A';
        $ax = 'A';

        foreach ($tabla->info as $e)
        {

            $event->sheet->getColumnDimension($ax)->setAutoSize(true);

            if(sizeOf(str_split($start)) >= 2)
            {
                $last = ++str_split($start)[1];

                if ($last === 'X' || $last === 'Z' || $last === 'AA' || $last ==='AB')
                {
                    $last = ++str_split($start)[0];
                    $start = $last . 'A';
                }
            }

            if (!$e[0])
            {
                $validate = new DataValidation();
                $validate->setErrorStyle( \PhpOffice\PhpSpreadsheet\Cell\DataValidation::STYLE_STOP )
                    ->setShowErrorMessage(true);

                if ($ax === 'T' || $ax === 'AN') {
                    if ($ax === 'T'){
                        $event->sheet->setDataValidation(
                            $ax . ':' . $ax,
                            $validate
                                ->setType(DataValidation::TYPE_DATE)
                                ->setAllowBlank(true)
                                ->setError('Introduzca una fecha en formato AAAA-MM-DD')
                                ->setFormula1('1900-01-01')
                                ->setFormula2($current->toDateString())
                        );
                    }else
                        $event->sheet->setDataValidation (
                            $ax . ':' . $ax,
                            $validate
                                ->setType(DataValidation::TYPE_DECIMAL)
                                ->setAllowBlank(true)
                                ->setError('Introduzca un numero decimal')
                        );
                } else {
                    if ($ax !== 'A' || $ax !== 'L')
                    {
                        $event->sheet->setDataValidation(
                            $ax . ':' . $ax,
                            $validate->setType(DataValidation::TYPE_TEXTLENGTH)
                                ->setAllowBlank(false)
                                ->setError('Palabra de 1 a' . $e[1] . ' Caracteres')
                                ->setFormula1(1)
                                ->setFormula2((int)$e[1])
                        );
                    } else {
                        $event->sheet->setDataValidation(
                            $ax . ':' . $ax,
                            $validate->setType(DataValidation::TYPE_TEXTLENGTH)
                                ->setAllowBlank(true)
                                ->setError('Palabra de 1 a' . $e[1] . ' Caracteres')
                                ->setFormula1(1)
                                ->setFormula2((int)$e[1])
                        );
                    }
                }
            }

            if(!$e[1])
            {
                $length = sizeOf($e[0])+2;
                $event->sheet->setDataValidation(
                    $ax.':'.$ax,
                    (new DataValidation())
                        ->setType(DataValidation::TYPE_LIST)
                        ->setShowDropDown(true)
                        ->setErrorStyle( \PhpOffice\PhpSpreadsheet\Cell\DataValidation::STYLE_STOP )
                        ->setShowErrorMessage(true)
                        ->setFormula1("TablasRelacionadas!$".$start."$3:".$start."$".$length)
                );

                if(sizeOf(str_split($start)) >= 2)
                {
                    ++$last;
                    ++$last;
                    $start = $start[0] . $last;
                    ++$last;
                }else
                {
                    ++$start;
                    ++$start;
                    ++$start;
                }
            }
            ++$ax;
        }
    }
}