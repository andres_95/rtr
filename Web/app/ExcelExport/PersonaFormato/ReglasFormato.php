<?php
/**
 * Created by PhpStorm.
 * User: usuario1
 * Date: 12/04/2018
 * Time: 03:35 PM
 */

namespace RTR\ExcelExport\PersonaFormato;

use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\RegistersEventListeners;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Concerns\WithTitle;
use Maatwebsite\Excel\Events\AfterSheet;

class ReglasFormato implements  WithTitle, WithEvents
{
    use Exportable, RegistersEventListeners;

    public function title(): string
    {
        return 'Reglas';
    }

    public static function afterSheet(AfterSheet $event)
    {
        $fondo = [
            'font' => [
                'bold' => true,
            ],
            'alignment' => [
                'horizontal' => 'center',
            ],
            'fill' => [
                'fillType' => 'solid',
                'startColor' => [
                    'argb' => 'FFFFFF00',
                ],
            ],
        ];

        $bordes = [
            'borders' => [
                'allBorders' => [
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                ]
            ],

        ];

        $event->sheet->setCellValue('A1','REGLAS DEL FORMATO');
        $event->sheet->setCellValue('A2','1.     Consideraciones generales');
        $event->sheet->setCellValue('A3','1.1     El usuario debe llenar las celdas "idPersona" y "documento_identidad", en caso contrario ocurrira un error.');
        $event->sheet->setCellValue('A4','1.2    Los datos referentes al banco pueden ser dejados en blanco.');
        $event->sheet->setCellValue('A5','1.3    Los campos que tengan listas desplegables deben ser necesariamente llenados, algunas excepciones aplican, dependiendo del registro que desee hacer.');
        $event->sheet->setCellValue('A6','1.5    Al momento de subir el archivo, debe tener el nombre Persona_Formato necesariamente, de lo contrario el sistema genera error.');
        $event->sheet->setCellValue('A7','1.6    El archivo cargado debe contener el libro TablaPrincipal con todos los campos que se incluyen en este formato.');
        $event->sheet->setCellValue('A8','1.7    Cuando cargue el excel en la pagina, el archivo debe tener un tamaño de 1MB, de lo contrario, el sistema genera error.');

        $event->sheet->setCellValue('A10','2.    Sobre el registro de Representantes de la fuerza de venta RFV');
        $event->sheet->setCellValue('A11','2.1    El valor de "documento_identidad" e "idPersona" deben ser unicos en las columnas que esta ingresando');
        $event->sheet->setCellValue('A12','2.2    El valor de "documento_identidad" e "idPersona" no deben haber sido registrados antes, de lo contrario, el sistema generara un error');
        $event->sheet->setCellValue('A13','2.3    Las celdas "idPersona" y "documento_identidad" no pueden ser dejados en blanco, de lo contrario, el sistema generara un error.');
        $event->sheet->setCellValue('A14','2.4    El usuario debe especificar el ID del supervisor.');
        $event->sheet->setCellValue('A15','2.5.    Si el supervisor no aparece en el desplegable, registrar primero los supervisores (por el servicio administrativo) y luego los RFV.');
        $event->sheet->setCellValue('A16','2.6.    No es necesario que los campos "idclase_persona", "idranking", "idespecialidad", "idactividad_negocio", o "idsubespecialidad" sean llenados.');
        $event->sheet->setCellValue('A17','2.7.    Es necesario que llene el campo de "idciclo".');

        $event->sheet->setCellValue('A19','3.    Sobre el registro de clientes CLI.');
        $event->sheet->setCellValue('A20','3.1.    Es necesario que los campos "idclase_persona", "idranking", "idespecialidad", "idactividad_negocio", "idfrecuencia", "descuento", e "idsubespecialidad" sean llenados.');
        $event->sheet->setCellValue('A21','3.1.    No es necesario que el campo del "idsupervisor" sea llenado.');

        $event->sheet->setCellValue('A23','4.    Sobre el registro de mayoristas MAY.');
        $event->sheet->setCellValue('A24','4.1.    Es necesario que los campos "idclase_persona", "idespecialidad", "idactividad_negocio", "descuento", e "idsubespecialidad" sean llenados.');
        $event->sheet->setCellValue('A25','3.1.    No es necesario que el campo del "idsupervisor" sea llenado.');

        $event->sheet->getColumnDimension('A')->setAutoSize(true);
        $event->sheet->getStyle('A1')->applyFromArray($fondo);
        $event->sheet->getStyle('A2')->applyFromArray($fondo);
        $event->sheet->getStyle('A10')->applyFromArray($fondo);
        $event->sheet->getStyle('A19')->applyFromArray($fondo);
        $event->sheet->getStyle('A23')->applyFromArray($fondo);
        $event->sheet->getStyle('A1:A25')->applyFromArray($bordes);

    }

}