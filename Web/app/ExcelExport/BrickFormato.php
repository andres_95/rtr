<?php
/**
 * Created by PhpStorm.
 * User: usuario
 * Date: 11/04/2018
 * Time: 13:55
 */

namespace RTR\ExcelExport;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Request;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\RegistersEventListeners;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithTitle;
use Maatwebsite\Excel\Events\AfterSheet;
use PhpOffice\PhpSpreadsheet\Cell\DataValidation;
use RTR\modelos\TZona;

class BrickFormato implements  FromCollection, WithHeadings, WithTitle, WithEvents
{
    use Exportable, RegistersEventListeners;

    static $fabricante;

    public function __construct(string $Fabricante)
    {
        self::$fabricante= $Fabricante;
    }

    public function collection()
    {
        return collect([]);
    }

    public function headings(): array
    {
        return [
            'Identificador',
            'Descripcion',
            'idzona'
        ];
    }


    public function title(): string
    {
        return 'Rutas';
    }

    public static function afterSheet(AfterSheet $event)
    {

        $event->sheet->setAutoFilter(
            $event->sheet->calculateWorksheetDimension()
        );
        $event->sheet->getColumnDimension('A')->setAutoSize(true);
        $event->sheet->getColumnDimension('B')->setAutoSize(true);
        $zonas = TZona::withoutGlobalScopes()
            ->select(['idzona','descripcion_zona'])
            ->where('idOperador',Auth::user()->idOperador)
            ->where('idFabricante',self::$fabricante)
            ->get()->toArray();
        $string = '"';

        foreach ($zonas as $clave => $valor)
        {
            $string.= $valor['idzona'].',';
        }
        $string = trim($string, ',').'"';
        $event->sheet->setDataValidation(
            'A:A',
            (new DataValidation())
                ->setType(DataValidation::TYPE_TEXTLENGTH)
                ->setAllowBlank(false)
                ->setErrorStyle( \PhpOffice\PhpSpreadsheet\Cell\DataValidation::STYLE_STOP )
                ->setShowErrorMessage(true)
                ->setError('Palabra de 1 a 10 Caracteres')
                ->setFormula1(1)
                ->setFormula2(10)
        );
        $event->sheet->setDataValidation(
            'B:B',
            (new DataValidation())
                ->setType(DataValidation::TYPE_TEXTLENGTH)
                ->setAllowBlank(false)
                ->setErrorStyle( \PhpOffice\PhpSpreadsheet\Cell\DataValidation::STYLE_STOP )
                ->setShowErrorMessage(true)
                ->setError('Palabra de 1 a 11 Caracteres')
                ->setFormula1(1)
                ->setFormula2(11)
        );
        $event->sheet->setDataValidation(
            'C:C',
            (new DataValidation())
                ->setType(DataValidation::TYPE_LIST)
                ->setShowDropDown(true)
                ->setErrorStyle( \PhpOffice\PhpSpreadsheet\Cell\DataValidation::STYLE_STOP )
                ->setShowErrorMessage(true)
                ->setFormula1($string)
        );

        $event->sheet->mergeCells('E1:F1');
        $event->sheet->fromArray(
            $zonas,
            NULL,
            'E2'
        );
        $event->sheet->getColumnDimension('E')->setWidth(12);
        $event->sheet->getColumnDimension('F')->setAutoSize(true);
        $fondo = [
            'font' => [
                'bold' => true,
            ],
            'alignment' => [
                'horizontal' => 'center',
            ],
            'fill' => [
                'fillType' => 'solid',
                'startColor' => [
                    'argb' => 'FFFFFF00',
                ],
            ],
        ];
        $event->sheet->getStyle('E1:F1')->applyFromArray($fondo);
        $bordes = [
            'borders' => [
                'allBorders' => [
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                ]
            ],

        ];
        $cant= count($zonas)+1;
        $event->sheet->getStyle('E1:F'.$cant)->applyFromArray($bordes);

        $event->sheet->mergeCells('H1:L1');
        $event->sheet->setCellValue('H1', 'REGLAS DEL FORMATO');
        $event->sheet->mergeCells('H2:L2');
        $event->sheet->setCellValue('H2', 'Identificador debe ser unico en el sistema');
        $event->sheet->mergeCells('H3:L3');
        $event->sheet->setCellValue('H3', 'Descripcion hasta 11 caracteres');
        $event->sheet->mergeCells('H4:L4');
        $event->sheet->setCellValue('H4', 'idzona deben ser las mostradas');
        $event->sheet->getStyle('H1:L1')->applyFromArray($fondo);
        $event->sheet->getStyle('H1:L4')->applyFromArray($bordes);

    }


}