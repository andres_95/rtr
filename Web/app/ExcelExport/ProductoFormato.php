<?php
/**
 * Created by PhpStorm.
 * User: usuario1
 * Date: 16/04/2018
 * Time: 03:43 PM
 */

namespace RTR\ExcelExport;


use Illuminate\Support\Facades\Auth;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\RegistersEventListeners;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithTitle;
use Maatwebsite\Excel\Events\AfterSheet;
use PhpOffice\PhpSpreadsheet\Cell\DataValidation;
use RTR\modelos\TCategoria;
use RTR\modelos\TLineaProducto;
use RTR\modelos\TMoneda;
use RTR\modelos\TPersona;
use RTR\modelos\TTipoProducto;
use RTR\modelos\TUnidade;

class ProductoFormato implements  FromCollection, WithHeadings, WithTitle, WithEvents
{
    use Exportable, RegistersEventListeners;

    static $fabricante;

    /**
     * ProductoFormato constructor.
     */
    public function __construct($Fabricante = '')
    {
        self::$fabricante= $Fabricante;
    }

    /**
     * @return mixed
     */
    public static function getFabricante()
    {
        return self::$fabricante;
    }




    public function collection()
    {
       return collect([]);
    }

    public function title(): string
    {
        return 'Productos';
    }

    public function headings(): array
    {
        return [
          'Identificador',
            'idmayorista',
            'idmoneda',
            'idlinea',
            'idtipo',
            'idcategoria',
            'Nombre',
            'Descripcion',
            'Principio',
            'Precio',
            'Descuento',
            'Presentacion',
            'Cantidad',
            'fechaExpedicion',
            'fechaVencimiento',
            'idpromocion',
            'lote',
            'idunidades'
        ];
    }

    public static function afterSheet(AfterSheet $event)
    {
        $user = Auth::user();
        $fondo = [
            'font' => [
                'bold' => true,
            ],
            'alignment' => [
                'horizontal' => 'center',
            ],
            'fill' => [
                'fillType' => 'solid',
                'startColor' => [
                    'argb' => 'FFFFFF00',
                ],
            ],
        ];

        $bordes = [
            'borders' => [
                'allBorders' => [
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                ]
            ],

        ];

        $event->sheet->setAutoFilter(
            $event->sheet->calculateWorksheetDimension()
        );
        $event->sheet->getColumnDimension('A')->setAutoSize(true);
        $event->sheet->getColumnDimension('B')->setAutoSize(true);
        $event->sheet->getColumnDimension('C')->setAutoSize(true);
        $event->sheet->getColumnDimension('D')->setAutoSize(true);
        $event->sheet->getColumnDimension('E')->setAutoSize(true);
        $event->sheet->getColumnDimension('F')->setAutoSize(true);
        $event->sheet->getColumnDimension('G')->setAutoSize(true);
        $event->sheet->getColumnDimension('H')->setAutoSize(true);
        $event->sheet->getColumnDimension('I')->setAutoSize(true);
        $event->sheet->getColumnDimension('J')->setAutoSize(true);
        $event->sheet->getColumnDimension('K')->setAutoSize(true);
        $event->sheet->getColumnDimension('L')->setAutoSize(true);
        $event->sheet->getColumnDimension('M')->setAutoSize(true);
        $event->sheet->getColumnDimension('N')->setAutoSize(true);
        $event->sheet->getColumnDimension('O')->setAutoSize(true);
        $event->sheet->getColumnDimension('P')->setAutoSize(true);
        $event->sheet->getColumnDimension('Q')->setAutoSize(true);
        $event->sheet->getColumnDimension('R')->setAutoSize(true);

        $mayoristas = TPersona::withoutGlobalScopes()
            ->where('idOperador',$user->idOperador)
            ->where('idFabricante',self::getFabricante())
            ->where('idgrupo_persona','MAY')
            ->select(['idPersona','nombre_completo_razon_social'])
            ->get()->toArray();

        $event->sheet->fromArray(
            $mayoristas,
            NULL,
            'T3'
        );


        $string = '"';

        foreach ($mayoristas as $clave => $valor)
        {
            $string.= $valor['idPersona'].',';
        }
        $string = trim($string, ',').'"';
        $event->sheet->setDataValidation(
            'B:B',
            (new DataValidation())
                ->setType(DataValidation::TYPE_LIST)
                ->setShowDropDown(true)
                ->setErrorStyle( \PhpOffice\PhpSpreadsheet\Cell\DataValidation::STYLE_STOP )
                ->setShowErrorMessage(true)
                ->setFormula1($string)
        );

        $event->sheet->mergeCells('T1:U1');
        $event->sheet->setCellValue('T1', 'Mayoristas');
        $event->sheet->setCellValue('T2', 'ID');
        $event->sheet->setCellValue('U2', 'NOMBRE');
        $event->sheet->getStyle('T1:U1')->applyFromArray($fondo);
        $event->sheet->getStyle('T1:U'.(count($mayoristas)+2))->applyFromArray($bordes);
        $event->sheet->getColumnDimension('T')->setAutoSize(true);
        $event->sheet->getColumnDimension('U')->setAutoSize(true);

        $moneda = TMoneda::withoutGlobalScopes()
            ->where('idoperador',$user->idOperador)
            ->where('idFabricante',self::getFabricante())
            ->select(['idmoneda','codmoneda','nombreMoneda'])
            ->get()->toArray();

        $event->sheet->fromArray(
            $moneda,
            NULL,
            'W3'
        );

        $string = '"';

        foreach ($moneda as $clave => $valor)
        {
            $string.= $valor['idmoneda'].',';
        }
        $string = trim($string, ',').'"';
        $event->sheet->setDataValidation(
            'C:C',
            (new DataValidation())
                ->setType(DataValidation::TYPE_LIST)
                ->setShowDropDown(true)
                ->setErrorStyle( \PhpOffice\PhpSpreadsheet\Cell\DataValidation::STYLE_STOP )
                ->setShowErrorMessage(true)
                ->setFormula1($string)
        );

        $event->sheet->mergeCells('W1:Y1');
        $event->sheet->setCellValue('W1', 'Monedas');
        $event->sheet->setCellValue('W2', 'ID');
        $event->sheet->setCellValue('X2', 'ALIAS');
        $event->sheet->setCellValue('Y2', 'NOMBRE');
        $event->sheet->getStyle('W1:Y1')->applyFromArray($fondo);
        $event->sheet->getStyle('W1:Y'.(count($moneda)+2))->applyFromArray($bordes);
        $event->sheet->getColumnDimension('W')->setAutoSize(true);
        $event->sheet->getColumnDimension('X')->setAutoSize(true);
        $event->sheet->getColumnDimension('Y')->setAutoSize(true);

        $lineas = TLineaProducto::withoutGlobalScopes()
            ->where('idOperador',$user->idOperador)
            ->where('idFabricante',self::getFabricante())
            ->select(['id','descripcion_linea_producto'])
            ->get()->toArray();

        $event->sheet->fromArray(
            $lineas,
            NULL,
            'AA3'
        );

        $string = '"';

        foreach ($lineas as $clave => $valor)
        {
            $string.= $valor['id'].',';
        }
        $string = trim($string, ',').'"';
        $event->sheet->setDataValidation(
            'D:D',
            (new DataValidation())
                ->setType(DataValidation::TYPE_LIST)
                ->setShowDropDown(true)
                ->setErrorStyle( \PhpOffice\PhpSpreadsheet\Cell\DataValidation::STYLE_STOP )
                ->setShowErrorMessage(true)
                ->setFormula1($string)
        );

        $event->sheet->mergeCells('AA1:AB1');
        $event->sheet->setCellValue('AA1', 'Linea Productos');
        $event->sheet->setCellValue('AA2', 'ID');
        $event->sheet->setCellValue('AB2', 'NOMBRE');
        $event->sheet->getStyle('AA1:AB1')->applyFromArray($fondo);
        $event->sheet->getStyle('AA1:AB'.(count($lineas)+2))->applyFromArray($bordes);
        $event->sheet->getColumnDimension('AA')->setAutoSize(true);
        $event->sheet->getColumnDimension('AB')->setAutoSize(true);


        $tipos = TTipoProducto::withoutGlobalScopes()
            ->where('idOperador',$user->idOperador)
            ->where('idFabricante',self::getFabricante())
            ->select(['idtipo_producto','descripcion_tipo_producto'])
            ->get()->toArray();


        $event->sheet->fromArray(
            $tipos,
            NULL,
            'AD3'
        );

        $string = '"';

        foreach ($tipos as $clave => $valor)
        {
            $string.= $valor['idtipo_producto'].',';
        }
        $string = trim($string, ',').'"';
        $event->sheet->setDataValidation(
            'E:E',
            (new DataValidation())
                ->setType(DataValidation::TYPE_LIST)
                ->setShowDropDown(true)
                ->setErrorStyle( \PhpOffice\PhpSpreadsheet\Cell\DataValidation::STYLE_STOP )
                ->setShowErrorMessage(true)
                ->setFormula1($string)
        );

        $event->sheet->mergeCells('AD1:AE1');
        $event->sheet->setCellValue('AD1', 'Tipos de Productos');
        $event->sheet->setCellValue('AD2', 'ID');
        $event->sheet->setCellValue('AE2', 'NOMBRE');
        $event->sheet->getStyle('AD1:AE1')->applyFromArray($fondo);
        $event->sheet->getStyle('AD1:AE'.(count($tipos)+2))->applyFromArray($bordes);
        $event->sheet->getColumnDimension('AD')->setAutoSize(true);
        $event->sheet->getColumnDimension('AE')->setAutoSize(true);


        $categoerias = TCategoria::withoutGlobalScopes()
            ->where('idOperador',$user->idOperador)
            ->where('idFabricante',self::getFabricante())
            ->select(['idcategorias','NombreCategorias'])
            ->get()->toArray();

        $event->sheet->fromArray(
            $categoerias,
            NULL,
            'AG3'
        );


        $string = '"';

        foreach ($categoerias as $clave => $valor)
        {
            $string.= $valor['idcategorias'].',';
        }
        $string = trim($string, ',').'"';
        $event->sheet->setDataValidation(
            'F:F',
            (new DataValidation())
                ->setType(DataValidation::TYPE_LIST)
                ->setShowDropDown(true)
                ->setErrorStyle( \PhpOffice\PhpSpreadsheet\Cell\DataValidation::STYLE_STOP )
                ->setShowErrorMessage(true)
                ->setFormula1($string)
        );

        $event->sheet->mergeCells('AG1:AH1');
        $event->sheet->setCellValue('AG1', 'Categorias de Productos');
        $event->sheet->setCellValue('AG2', 'ID');
        $event->sheet->setCellValue('AH2', 'NOMBRE');
        $event->sheet->getStyle('AG1:AH1')->applyFromArray($fondo);
        $event->sheet->getStyle('AG1:AH'.(count($categoerias)+2))->applyFromArray($bordes);
        $event->sheet->getColumnDimension('AG')->setAutoSize(true);
        $event->sheet->getColumnDimension('AH')->setAutoSize(true);

        $unidades = TUnidade::where('idestatus',1)
            ->select(['id','descripcion_unidades'])
            ->get()->toArray();

        $event->sheet->fromArray(
            $unidades,
            NULL,
            'AJ3'
        );

        $event->sheet->mergeCells('AJ1:AK1');
        $event->sheet->setCellValue('AJ1', 'Unidades de medida de Productos');
        $event->sheet->setCellValue('AJ2', 'ID');
        $event->sheet->setCellValue('AK2', 'NOMBRE');
        $event->sheet->getStyle('AJ1:AK1')->applyFromArray($fondo);
        $event->sheet->getStyle('AJ1:AK'.(count($unidades)+2))->applyFromArray($bordes);
        $event->sheet->getColumnDimension('AJ')->setAutoSize(true);
        $event->sheet->getColumnDimension('Ak')->setAutoSize(true);

        $string = '"';

        foreach ($unidades as $clave => $valor)
        {
            $string.= $valor['id'].',';
        }
        $string = trim($string, ',').'"';
        $event->sheet->setDataValidation(
            'R:R',
            (new DataValidation())
                ->setType(DataValidation::TYPE_LIST)
                ->setShowDropDown(true)
                ->setErrorStyle( \PhpOffice\PhpSpreadsheet\Cell\DataValidation::STYLE_STOP )
                ->setShowErrorMessage(true)
                ->setFormula1($string)
        );
    }
}