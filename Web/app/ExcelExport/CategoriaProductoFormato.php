<?php
/**
 * Created by PhpStorm.
 * User: usuario1
 * Date: 16/04/2018
 * Time: 02:59 PM
 */

namespace RTR\ExcelExport;


use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\RegistersEventListeners;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithTitle;
use Maatwebsite\Excel\Events\AfterSheet;
use PhpOffice\PhpSpreadsheet\Cell\DataValidation;

class CategoriaProductoFormato implements FromCollection, WithHeadings, WithTitle, WithEvents
{
    use Exportable, RegistersEventListeners;
    public function collection()
    {
        return collect([]);
    }

    public function title(): string
    {
        return 'CategotiaProductos';
    }

    public function headings(): array
    {
        return [
            'Identificador',
            'Descripcion'
        ];
    }

    public static function afterSheet(AfterSheet $event)
    {
        $event->sheet->setAutoFilter(
            $event->sheet->calculateWorksheetDimension()
        );
        $event->sheet->getColumnDimension('A')->setAutoSize(true);
        $event->sheet->getColumnDimension('B')->setAutoSize(true);
        $event->sheet->setDataValidation(
            'A:A',
            (new DataValidation())
                ->setType(DataValidation::TYPE_TEXTLENGTH)
                ->setAllowBlank(false)
                ->setErrorStyle( \PhpOffice\PhpSpreadsheet\Cell\DataValidation::STYLE_STOP )
                ->setShowErrorMessage(true)
                ->setError('Palabra unica de 1 a 5 Caracteres')
                ->setFormula1(1)
                ->setFormula2(5)
        );

        $event->sheet->setDataValidation(
            'B:B',
            (new DataValidation())
                ->setType(DataValidation::TYPE_TEXTLENGTH)
                ->setAllowBlank(false)
                ->setErrorStyle( \PhpOffice\PhpSpreadsheet\Cell\DataValidation::STYLE_STOP )
                ->setShowErrorMessage(true)
                ->setError('Palabra unica de 1 a 100 Caracteres')
                ->setFormula1(1)
                ->setFormula2(100)
        );

        $fondo = [
            'font' => [
                'bold' => true,
            ],
            'alignment' => [
                'horizontal' => 'center',
            ],
            'fill' => [
                'fillType' => 'solid',
                'startColor' => [
                    'argb' => 'FFFFFF00',
                ],
            ],
        ];
        $bordes = [
            'borders' => [
                'allBorders' => [
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                ]
            ],

        ];

        $event->sheet->mergeCells('E1:F1');
        $event->sheet->setCellValue('E1', 'REGLAS DEL FORMATO');
        $event->sheet->mergeCells('E2:F2');
        $event->sheet->setCellValue('E2', 'Descripcion hasta 100 caracteres');
        $event->sheet->mergeCells('E3:F3');
        $event->sheet->setCellValue('E3', 'Identificador unico hasta 5 caracteres');
        $event->sheet->getStyle('E1:F1')->applyFromArray($fondo);
        $event->sheet->getStyle('E1:F3')->applyFromArray($bordes);
        $event->sheet->getColumnDimension('A')->setAutoSize(true);
        $event->sheet->getColumnDimension('E')->setWidth(25);

    }
}