<?php

namespace RTR\Listeners;

use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use RTR\Events\ProductEvent;

class ProductListener extends EventListener
{
   

    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle(ProductEvent $event)
    {
        //
        if($event->isAuditable($event->producto))
           $this->log_create($event->producto);
    }
}
