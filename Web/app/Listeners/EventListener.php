<?php

namespace RTR\Listeners;

use RTR\Events\Event;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use RTR\modelos\TLog;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;



class EventListener 
{
    protected $user;
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
        $this->user = Auth::user();
    }
    public function log($descripcion){

        Tlog::create([
            'idPersona'=>$this->user->idPersona,
            'idOperador'=>$this->user->idOperador,
            'idfabricante'=>$this->user->idFabricante,
            'RegistroOcurrido'=>$descripcion,
            'fecha_registro'=>Carbon::now()->format('Y-m-d'),
            'idestatus'=>1
        ]);
    }
     private function getTKA($item){
        $table=$item->getTable();
        
      
       $attributes=$item->getAttributes();
       $keys=array_keys($attributes);
       $values=array_values($attributes);
       return ['table'=>$table,'keys'=>$keys,'values'=>$values];
     }
    public function log_create($item){
          $tokens=$this->getTKA($item);
            
          $sql='insert into '.$tokens['table'].PHP_EOL.'('.implode(",",$tokens['keys']).')'.PHP_EOL.'values('.implode(",",$tokens['values']).')';
          $this->log($sql);
    }
    
    public function log_update($item){
        $tokens=$item->getAttributes();
        $equations=[];

       foreach($tokens as $key=>$value){
          if(!($value==null || strlen($value)==0))
           array_push($equations,$key.'='.$value);   
       }
        $sql='update '.$item->getTable().PHP_EOL.'set '.implode(",",$equations).PHP_EOL.'where '.$item->getKeyName().'='.$item->getKey();
        $this->log($sql);
    }

    public function log_null($item){
        $sql='delete from '.$item->getTable().PHP_EOL.' where '.$item->getKeyName().'='.$item->getKey();
        $this->log($sql);
    }

    /**
     * Handle the event.
     *
     * @param  Event  $event
     * @return void
     */
    public function handle(Event $event)
    {
        //
    }
}
