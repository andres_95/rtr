<?php

namespace RTR\Listeners;

use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use RTR\Events\UserEvent;


class UserListener extends EventListener
{
   
    

    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle(UserEvent $event)
    {
        //
       //    dd($this->user->idPersona);
        //   $this->log('me gustas mucho');
        if($event->isAuditable($event->persona))
          $this->log_create($event->persona);

    }
}
