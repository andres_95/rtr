<?php

namespace RTR\Http\Controllers\Admin\localizaciones;
use RTR\modelos\TTipoLocalizacione;
//use RTR\modelos\TPersona;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use RTR\Http\Controllers\Controller;


class tipolocalizacionescontroller extends Controller
{

    public function __construct()
    {
        $this->middleware(function ($request, $next) {
            $this->user = Auth::user();
            return $next($request);
        });
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
         TTipoLocalizacione::create([
            'idOperador'=>$this->user->idOperador,
            'cod_tipo_localizacion'=>$request->cod,
            'descripcion_tipo_localizacion'=>$request->descripcion ,
            'idestatus'=>1
         ]);

           return response()->json(['sucess'=>'forma de localizacion registrada'],200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $localizacion=TTipoLocalizacione::where('idOperador','=',$this->user->idOperador)
                                          ->where('cod_tipo_localizacion','=',$id)
                                          ->where('idestatus','=','1');

                    if($localizacion->count()==0)
                       return response()->json(['error'=>'forma de localizacion no encontrada'],404);

                $localizacion->update([
                    'cod_tipo_localizacion'=>$request->cod,
                    'descripcion_tipo_localizacion'=>$request->descripcion
                ]);

                return response()->json(['sucess'=>'forma de localizacion actualizada'],200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
                    $localizacion= TTipoLocalizacione::where('idOperador','=',$this->user->idOperador)
                    ->where('cod_tipo_localizacion','=',$id)
                    ->where('idestatus','=','1');

            if($localizacion->count()==0)
                    return response()->json(['error'=>'forma de localizacion no encontrada'],404);

                  $localizacion->update([
                          'idestatus'=>'0'
               ]);
               return response()->json(['sucess'=>'forma de localizacion eliminada'],200);
    }
}
