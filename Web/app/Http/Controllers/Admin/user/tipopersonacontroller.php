<?php
     
     namespace RTR\Http\Controllers\Admin\user;

     use RTR\modelos\TTipoPersona as TipoPersona; //Modelo a usar
     use Illuminate\Http\Request;  //trabajar con las solicitudes http
     use Illuminate\Support\Facades\Auth; //manejo de autenticacion de usuario
     use RTR\Http\Controllers\Controller; // clase padre
     use RTR\Http\Resources\clasePersonaResource as clasePersona;
     use Validator;

      class tipopersonacontroller extends Controller{
               //consultas masivas
           public function index(){

               return response('esta es la consulta masiva',200);
           }
               //consultas individulaes
           public function show(Request $request){

               return response('esta es la consulta individual',200);
                
            }
              //registrar nuevo item
            public function store(Request $request){

              $validar=Validator::make($request->all(),[
                  'id'=>'required|int',
                  'descripcion'=>'required'
              ],
              [
                'required'=>'Se necesita el :attribute',
                'int'=>'El :attribute debe ser entero'  
              ]
            
            );
               if($validar->fails()){
                   return response()->json($validar->errors()->all(), 400); 
               }

                TipoPersona::create([
                    'descripcion_tipo_persona'=>$request->descripcion,
                    'estatus'=>'1'
                ]);
               $tipoP=TipoPersona::all()->last();
                return response($tipoP,200);
            }
                //actualizar item
           public function update(Request $request){
            $validar=Validator::make($request->all(),[
                'id'=>'required|int|exists:t_tipo_persona,cod_tipo_persona',
                'descripcion'=>'required'
            ],
            [
              'required'=>'Se necesita el :attribute',
              'int'=>'El :attribute debe ser entero'  
            ]
          
          );
           ///  "select * tipo_persona where cod_tipo_persona=".$request->idtipo." and estatus='1'";

              //update tipo_persona set descripcion='sdsdf' where cod_tipo_persona=.request->idtipo;
             $tipoPersona=TipoPersona::withoutGlobalScopes()->find($request->id);
             $tipoPersona->descripcion_tipo_persona=$request->descripcion;
             $tipoPersona->save();

             return response('esta actualiza',200);
           }
              //borrar item
           public function delete(Request $request){
            $data=$request->only('idtipo');
            $validar=Validator::make($request->all(),[
                'id'=>'required|int|exists:t_tipo_persona,id',
            ],
            [
              'required'=>'Se necesita el :attribute'
            ]
          
          );

           $tipoPersona=TipoPersona::withoutGlobalScopes()->find($request->id);
           $tipoPersona->delete();

            return response('esta elimina el item',200);
           }
             
      }

?>