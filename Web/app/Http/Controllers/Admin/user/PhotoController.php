<?php
    namespace RTR\Http\Controllers\Admin\user;


    use Validator;
    use Illuminate\Http\Response;
    use Illuminate\Http\Request;
    use Illuminate\Support\Facades\Auth;
    use App\Http\Controllers\Controller;
    use App\Services\UploadPhotoPerfil;


    class PhotoController extends Controller {
        /**
     * @var Request
     */
    protected $request;
    
        /**
        * @var UploadService
        */
        private $service;
    
        /**
         *
         * @param Request $request
         * @param UploadService $uploadService
         * @param FileUploadRequestMapper $fileUploadRequestMapper
         */
        public function __construct(Request $request, UploadPhotoPerfil $uploadService)
        {
            $this->request = $request;
            $this->service = $uploadService;
        }
    
        /**
         *
         * @return array
         */
        private function mapFileUploadRequest()
        {
           // dd($this->request->header('Upload-Content-Type'));
            $request = [
                    'file' => $this->request->getContent(),
                    'contentType' => $this->request->header('Upload-Content-Type')
            ];
            return $request;
        }

        public function upload()
        {
            try
            {
            $postRequest = $this->mapFileUploadRequest();
            $response = $this->service->uploadUserProfileImage($postRequest);
            return response($response, 200,
                            [
                                    'Content-Type' => 'application/json'
                            ]);
            }
            catch(Exception $e)
            {
                return response()->json(['Error'=> "No se pudo actualizar el usaurio, intente de nuevo"],409);
            }
        }
    }