<?php
    namespace RTR\Http\Controllers\Admin\user;


    use Validator;
    use App\modelos\TPersona;
    use App\modelos\TPerfil;
    use App\modelos\Opcion;
    use Illuminate\Http\Request;
    use Illuminate\Support\Facades\Auth;
    use App\Http\Controllers\Controller;

    class perfilcontroller extends Controller {


        public function __construct()
        {
            $this->content = array();
            $this->status = 200;
        }

        public function index()
        {
            $user = Auth::user();
            if(!$user)
            {
                $this->content = ["Error obteniendo usuario"];
                $this->status = 404;
            }else
            {
                $this->content = [
                    'Username' => $user->username,
                    'Nombre' => $user->nombre_persona,
                    'Apellido' => $user->apellido_persona,
                    'Email' => $user->email,
                    'Avatar' => $user->URLfoto,
                    'Telefono' => $user->telefono,
                    'Cedula' => $user->documento_Identidad,
                    'Direccion' => $user->direccion,
                    'Genero' => $user->genero_persona

                ];
            }
            return response()->json($this->content,$this->status);
        }

        public function update(Request $request)
        {
            $user = Auth::user();
            $validator = 
            Validator::make($request->all(), 
            [
                'Nombre' => 'required|string',
                'Apellido' => 'required|string',
                'Email' => 'required|email|unique:t_persona,email,'.$user->idpersona.',idpersona',
                'Genero' => 'required',
            ]
            , [
                'required' => 'El  :attribute es requerido.',
                'unique' => 'El :attribute ya existe en nuestros registros',
                'email' => 'El email no tiene un formato correcto',
            ]);

            if ($validator->fails()) {
                return response()->json($validator->errors()->all(), 400); 
            }

            try
            {
                $data = 
                [
                    'nombre_persona' => $request['Nombre'],
                    'apellido_persona' => $request['Apellido'],
                    'email' => $request['Email'],
                    'telefono' => $request->has('Telefono')?  $request['Telefono'] : "",
                    'genero_persona' => $request['Genero'],
                    'direccion' => $request->has('Direccion')? $request['Direccion'] : "", 
                ];

            
                $user->update($data);

                return response()->json("Usuario actualizado",200);
            }
            catch(Exception $e)
            {
                return response()->json(['Error'=> "No se pudo actualizar el usaurio, intente de nuevo"],409);
            }
        }


            public function crearPerfil(Request $request){
                    do{ $id=rand(); }while(TPerfil::find($id));
                TPerfil::create([
                        'idOperador'=>'4',
                        'idperfil'=>$id ,
                        'descripcion_perfil'=>$request->descripcion , 
                        'condicion_perfil'=>$request->condicion,
                        'idestatus'=>'1'
                ]);
                return response()->json(['sucess'=>'Perfil creado'],200);
            }

            public function updatePerfil(Request $request){
                $perfil=TPerfil::where('id_perfil','=',$request->id)->where('estatus','=','1');

                    if($perfil->count()==0)
                    return response()->json(['error'=>'no encontrado'],404);

                $perfil->update([
                    'descripcion_perfil'=>$request->descripcion , 
                    'condicion_perfil'=>$request->condicion,
                ]);
                return response()->json(['sucess'=>'Perfil actualizado'],200);
            }
            public function deletePerfil(Request $request){
                $perfil=TPerfil::where('id_perfil','=',$request->id)->where('estatus','=','1');
                
                            if($perfil->count()==0)
                            return response()->json(['error'=>'no encontrado'],404);
                $perfil->update(['estatus'=>'1']);
                return response()->json(['sucess'=>' Perfil Eliminado'],200);
            }
        
          public function addopcion(Request $request){
            do{ $id=rand(); }while(TPerfil::find($id));
            Opcion::create([
                    'idOperador'=>'6' ,
                    'id_perfil' =>$id,
                    'opciones_perfil' =>$request->opcion
            ]);
            return response()->json(['sucess'=>'Opcion de Perfil creado'],200);
          }

         public function updopcion(Request $request){
              $opcion=Opcion::where('id_perfil','=',$request->id);
             //$opcion=Opcion::where('id_perfil','=',$request->id)->where('estatus','=','1');
                
                if($opcion->count()==0)
                      return response()->json(['error'=>'Opcion no encontrada'],404);
                    
                      $opcion->update(['opciones_perfil' =>$request->opcion],200);
                    
              return response()->json(['success'=>'Opcion actualizada'],200);
         }
         public function delopcion(Request $request){
            $opcion=Opcion::where('id_perfil','=',$request->id)->where('estatus','=','1');
            
            if($opcion->count()==0)
                  return response()->json(['error'=>'Opcion no encontrada'],404);
                //Necesita estatus
                //  $opcion->update();
                
          return response()->json(['success'=>'Opcion actualizada'],200);
         }
        
    }