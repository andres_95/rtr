<?php

namespace RTR\Http\Controllers\Admin\user;

use Illuminate\Http\Request;
use Validator;
use RTR\modelos\TListanegra;
use RTR\modelos\TPersona;
use RTR\modelos\TGrupopersona;
use Illuminate\Support\Facades\Auth;
use RTR\Http\Controllers\Controller;
use RTR\Http\Resources\listaNegraRecurso as listanegra;
use RTR\Http\Resources\CRFRecurso;


class listanegracontroller extends Controller
{

    private $user;
    public function __construct(){
        $this->middleware(function ($request, $next) {
            $this->user = Auth::user();
            return $next($request);
        });
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        //
       // $grupo=TGrupopersona::find($request->tipo);
       
        $Empresa=TPersona::withoutGlobalScopes()
        ->where('idOperador',$this->user->idOperador)
        ->where('idgrupo_persona','FABR')
        ->select(['idOperador','idPersona'])
        ->get();
        $T='ListaNegra';
        return view('Admin.usuarios.Listanegra',compact('Empresa','T'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
         $validator=Validator::make($request->all(),TListanegra::$validators);
            if($validator->fails())
                return response()->json($validator->errors()->all(),404);

        $datas=$request->all();
        $datas['idOperador']=$this->user->idOperador;
        $datas['idestatus']=1;
      //   dd($request->causa);
        TListanegra::create([
            'idlistanegra'=>$request->id,
            'idOperador'=>$this->user->idOperador,
            'idFabricante'=>$request->fabricante,
            'idPersona'=>$request->CI,
            'Causas_listanegra'=>$request->descripcion
        ]);
          $persona=TPersona::withoutGlobalScopes()->find($request->CI);
      //    dd($persona);
          // dd(TGrupopersona::find($persona->idgrupo_persona));
           $categoria=TGrupopersona::find($persona->idgrupo_persona)->descripciongrupoPersona;
           return response()->json(['success'=>'persona agregada en la lista negra',
                                    'categoria'=>$categoria,
                                    'persona'=>$persona->nombre_persona
                                     ],200);
       // $datas['idfabricante']=$this->user->idfabricante;

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request){
        //
      /*  $lista=TListanegra::where('idFabricante',$request->id)->where('idestatus',1);
        $personas=TPersona::where('idgrupo_persona',$request->tipo)->get(['idPersona']);
         $lista=$lista->whereIn('idPersona',$personas);

              if($lista->count()==0)
                     return response()->json(['error'=>'no hay lista'],404);   

           return response(listanegra::collection($lista),200);*/

           $lista=TListanegra::withoutGlobalScopes()->where('idFabricante',$request->fabricante)->where('idestatus',1)->get();

             foreach($lista as $item){
                 $item->tipo='ln';
             }
             //  dd($lista);
            return response(CRFRecurso::collection($lista),200);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        //
        $validator=Validator::make($request->all(),array_merge(
              TListanegra::$validators,
              ['id'=>'required|exists:t_listanegra,idlistanegra'])
            );

        if($validator->fails())
            return response()->json($validator->errors()->all(),404);
          
         $lista=TListanegra::withoutGlobalScopes()->where('idlistanegra','=',$request->id)->where('idestatus',1);

            if($lista->count()==0)
                 return response()->json(['error'=>'persona no encontrada en la lista negra'],404);
          
           $lista->update([
                 'Causas_listanegra'=>$request->descripcion
           ]);

           $persona=TPersona::withoutGlobalScopes()->find($lista->first()->idPersona);
           //    dd($persona);
               // dd(TGrupopersona::find($persona->idgrupo_persona));
                $categoria=TGrupopersona::find($persona->idgrupo_persona)->descripciongrupoPersona;
                return response()->json(['success'=>'persona agregada en la lista negra',
                                         'categoria'=>$categoria,
                                         'persona'=>$persona->nombre_persona
                                          ],200);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        //
        $validator=Validator::make($request->all(),[
            'id'=>'required|exists:t_listanegra,idlistanegra'
        ]);
           if($validator->fails())
              return response()->json($validator->errors()->all(),404);
              

        $lista=TListanegra::withoutGlobalScopes()->where('idlistanegra','=',$request->id)->where('idestatus',1);
        
                    if($lista->count()==0)
                         return response()->json(['error'=>'persona no encontrada en la lista negra'],404);
                  
                   $lista->update([
                      'idestatus'=>0 
                   ]);
        
                     return response()->json(['sucess'=>'se ha elimina esta persona de la lista negra'],200);
    }
}
