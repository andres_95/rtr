<?php
   namespace RTR\Http\Controllers\Admin\user;


    use Validator;
    use RTR\modelos\TClasePersona;
    use Illuminate\Http\Request;
    use Illuminate\Support\Facades\Auth;
    use RTR\Http\Controllers\Controller;
    use RTR\Http\Resources\clasePersonaResource as clasePersona;

    class clasepersonacontroller extends Controller{
        
         public function index(){
               return clasePersona::collection(TClasePersona::where('estatus','=','1')->get());
         }

          public function show(Request $request){
             $user=Auth::User();
             $validar=Validator::make($request->all(),[
                 'id'=>'required|exists:t_clase_persona,id'
             ]);
               if($validar->fails())
                  return response()->json(['error'=>'esta clase de persona no se encuentra']);
          //  $clase=TClasePersona::where('id','=',$request->id)->where('estatus','=','1')->get();
              
             return new clasePersona(TClasePersona::find($request->id));
          }
           /**
            * 
            * @param  Illuminate\Http\Request $request
            * @return  Illuminate\Http\Response
            */
         public function store(Request $request){
               $user=Auth::User();

                $validador=Validator::make($request->all(),
                  [
                     'descripcion'=>'required|unique:t_clase_personas,descripcion',
                  ]
                  );
                   if($validador->fails())
                      return response()->json($validador->errors()->all(),404);
                 

                $clase=TClasePersona::create([
                    'descripcion'=>$request->descripcion,
                    'estatus'=>'1'
                ]);
                  
                 return response()->json(['success'=>'se ha agregado una nueva clase de persona','id'=>$clase['id']],200);

         }

          /**
            * 
            * @param  Illuminate\Http\Request $request
            * @return  Illuminate\Http\Response
            */
            public function update(Request $request){
                 $user=Auth::User();
               
                 $validador=Validator::make($request->all(),
                 [  
                    'id'=>'required|int',
                    'descripcion'=>'required'
                 ]
                 );
                 if($validador->fails())
                     return response()->json($validador->errors()->all(),404);

                    $clase=TClasePersona::withoutGlobalScopes()->find($request->id);
                    //dd($clase,$request->id);
                    $clase->descripcion=$request->descripcion;
                    $clase->save();
                      /* if($clase->count()==0)
                          return response()->json(['error'=>'esta clase persona no se encuentra '.$request->id],404);

                $clase->update([ 
                'descripcion'=>$request->descripcion
                ]);*/
                      return response()->json(['success'=>'clase person actualizada','id'=>$clase['id']],200);
            } 

            /**
            * 
            * @param  Illuminate\Http\Request $request
            * @return  Illuminate\Http\Response
            */

           /* public function delete(Request $request){

                $user=Auth::User();
                
                                    $clase=TClasePersona::where('id','=',$request->id)->where('estatus','=','1');
                
                                       if($clase->count()==0)
                                          return response()->json(['error'=>'esta clase persona no se encuentra'],404);
                
                                $clase->update(['estatus'=>'0']);
                                      return response()->json(['success'=>'clase persona eliminada'],200);
            }*/

            public function destroy(Request $request){
                $validador=Validator::make($request->all(),
                [  
                   'id'=>'required|int'
                ]
                );
                if($validador->fails())
                    return response()->json($validador->errors()->all(),404);

                $clase=TClasePersona::withoutGlobalScopes()->find($request->id);
                $clase->estatus=0;
                $clase->save();
                

                return response()->json(['success'=>'clase persona eliminada'],200);
            }


    }