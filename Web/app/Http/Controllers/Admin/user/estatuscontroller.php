<?php

namespace RTR\Http\Controllers\Admin\user;

use Illuminate\Http\Request;
use Validator;
use RTR\modelos\TEstatus;
use Illuminate\Support\Facades\Auth;
use RTR\Http\Controllers\Controller;

class estatuscontroller extends Controller
{

    private $user;
    public function __construct(){
        $this->middleware(function ($request, $next) {
            $this->user = Auth::user();
            return $next($request);
        });
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
           $datas=$request->all();
           $datas['idOperador']=$this->user->idOperador;

            TEstatus::create($datas);
            return response()->json(['success'=>'estatus agregado'],200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
         return response()->json(['estatus'=>TEstatus::find($id)],200);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
          $estatus=TEstatus::find($id);
          
          if($estatus!=null){
             $estatus->descripcion_estatus=$request->descripcion;
             $estatus->save();
             return response()->json(['success'=>'estatus actualizado'],200);
          }
           return response()->json(['error'=>'estatus no encontrado'],404);
            
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //

        $estatus=TEstatus::find($id);
        
        if($estatus!=null){
           $estatus->delete();
           return response()->json(['success'=>'estatus eliminado'],200);
        }
    }
}
