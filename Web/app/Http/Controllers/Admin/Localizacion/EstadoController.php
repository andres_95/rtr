<?php

namespace RTR\Http\Controllers\Admin\Localizacion;

use Illuminate\Http\Request;
use RTR\Http\Controllers\Controller;
use RTR\modelos\TCiudade;

class EstadoController extends Controller
{
    public function show(Request $request){

        $ciudad=TCiudade::where('idestado',$request->estado)->get();

        return response()->json($ciudad);
    }
}
