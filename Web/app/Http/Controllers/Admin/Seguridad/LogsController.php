<?php
/**
 * Created by PhpStorm.
 * User: usuario
 * Date: 26/04/2018
 * Time: 12:35
 */

namespace RTR\Http\Controllers\Admin\Seguridad;

use App\modelos\TLog;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use RTR\Http\Controllers\Controller;
use RTR\modelos\TPersona;

class LogsController extends Controller
{
    public function __construct()
    {
        $this->middleware(function ($request, $next) {
            $this->user = Auth::user();
            return $next($request);
        });
    }

    public function index(Request $request){
        $Empresa=TPersona::withoutGlobalScopes()
            ->where('idOperador',$this->user->idOperador)
            ->where('idgrupo_persona','FABR')
            ->select(['idOperador','idPersona'])
            ->get();
        $T="Logs";
        $fabrica = $request->session()->get('user.teams');
        return view('Admin.Seguridad.logs',compact('Empresa','T','fabrica'));
    }

    public function show(Request $request){

        $logs=TLog::withoutGlobalScopes()
            ->where('idFabricante',$request->fabricante)
            ->get();

        return response($logs,200);
    }

}