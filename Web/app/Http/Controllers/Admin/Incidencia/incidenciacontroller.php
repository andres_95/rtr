<?php

namespace RTR\Http\Controllers\Admin\Incidencia;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use RTR\Http\Controllers\Controller;
use RTR\modelos\TPersona;
use RTR\modelos\TTipoIncidente;
use RTR\Http\Resources\CRFRecurso;
use Validator;

class IncidenciaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware(function ($request, $next) {
            $this->user = Auth::user();
            return $next($request);
        });
    }

    public function index()
    {
        //
        $incidentes=TTipoIncidente::all();
        return response()->json(['incidencias'=>$incidentes],200);
    }

    public function index2(Request $request){
        $Empresa=TPersona::withoutGlobalScopes()
            ->where('idOperador',$this->user->idOperador)
            ->where('idgrupo_persona','FABR')
            ->where('idestatus',1)
            ->select(['nombre_completo_razon_social','idPersona'])
            ->get();
        $T="TipoIncidente";
        $fabrica = $request->session()->get('user.teams');
        return view('Admin.estadistico.CiRaFr',compact('Empresa','T','fabrica'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
         // dd($request->all());
         $validator=Validator::make($request->all(),
          [
             'fabricante'=>'required|exists:t_personas,idPersona',
            'id'=>'required|unique:t_tipo_incidentes,idtipo_incidentes',
            'descripcion'=>'required|unique:t_tipo_incidentes,descripcion_tipo_incidentes'  
          ]
          );
             if($validator->fails())
                return response()->json($validator->errors()->all(),404);

        $incidente=TTipoIncidente::create([
            'idOperador'=>$this->user->idOperador,
            'idFabricante'=>$request->fabricante,
            'idtipo_incidentes'=>$request->id,
            'descripcion_tipo_incidentes'=>$request->descripcion
        ]);

          return response()->json(['success'=>$incidente['idtipo_incidentes']],200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request)
    {
        $TA=TTipoIncidente::withoutGlobalScopes()->where('idFabricante',$request->fabricante)
                           ->where('idestatus',1)
                           ->get();
        foreach ($TA as $item)
            $item->tipo='ti';

        $hola=CRFRecurso::collection($TA)->toJson();
        return response($hola,200);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        //
        $validator=Validator::make($request->all(),
        [
           'fabricante'=>'required|exists:t_personas,idPersona',
          'id'=>'required|unique:t_tipo_incidentes,idtipo_incidentes',
          'descripcion'=>'required|unique:t_tipo_incidentes,descripcion_tipo_incidentes'  
        ]
        );
           if($validator->fails())
              return response()->json($validator->errors()->all(),404);

        if(!TTipoIncidente::withoutGlobalScopes()->where('idtipo_incidentes',$request->id)->update([
            'descripcion_tipo_incidentes'=>$request->descripcion
           ]))
              return response()->json(['error'=>'incidente no encontrado'],404);

             return response()->json(['success'=>'tipo de incidente actualizado'],200);
          
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request){
        $validator=Validator::make($request->all(),
        [
          
          'id'=>'required|exists:t_tipo_incidentes,idtipo_incidentes'
         
        ]
        );
           if($validator->fails())
              return response()->json($validator->errors()->all(),404);
        if(!TTipoIncidente::withoutGlobalScopes()->find($request->id)->update([
            'idestatus'=>0
           ]))
              return response()->json(['error'=>'incidente no encontrado'],404);

             return response()->json(['success'=>'tipo de incidente anulado'],200);
    }



}