<?php

namespace RTR\Http\Controllers\Admin\titulos;
use RTR\modelos\TTitulo;
use Illuminate\Support\Facades\Auth;
use RTR\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Validator;

class titulocontroller extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $titulos=TTitulo::where('idestatus','=',1);

        if($titulos->count()==0)
           return response()->json(['error'=>'no hay titulo encontrado'],404);
      
        //   dd($titulos->get()->toArray());
       //  return $titulos->get()->toArray();
   //dd($titulos);

     return response()->json(['titulos'=>$titulos->get()->toArray()],200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
       
         
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator=Validator::make($request->all(),TTitulo::$validators);

           if($validator->fails()){
                return response()->json($validator->errors()->all(),404);
           }
        //
        //  $validador=Valid
        TTitulo::create([
            'id'=>$request->id,
            'descripcion'=>$request->descripcion,
            'idestatus'=>'1'
        ]);
          return response()->json([
            
             'success'=>'titulo guardado con exito'
             
          ],200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        //
        $validator=Validator::make($request->all(),array('descripcion'=>TTitulo::$validators['descripcion']));
        
        
                   if($validator->fails()){
                        return response()->json($validator->errors()->all(),404);
                   }

          $titulo=TTitulo::where('id','=',$request->id)->where('idestatus','=','1');
    
          if($titulo->count()==0)
             return response()->json(['error'=>'titulo no encontrado'],404);

             $titulo->update([
                 'id'=>$request->id,
                 'descripcion'=>$request->descripcion
             ]);

               return response()->json([     
                'success'=>'titulo actualizado'
               ],200);           
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        //
        $validator=Validator::make($request->all(),array('id'=>'required|exists:t_titulos,id'));
        
                   if($validator->fails()){
                        return response()->json($validator->errors()->all(),404);
                   }
            
        $titulo=TTitulo::where('id','=',$request->id)->where('idestatus','=','1');
        
              if($titulo->count()==0)
                 return response()->json(['error'=>'titulo no encontrado'],404);
    
                 $titulo->update([
                     'idestatus'=>0
                      
                 ]);
    
                   return response()->json([
    
                      
                    'success'=>'titulo eliminado'
                   ],200);           
    }
}
