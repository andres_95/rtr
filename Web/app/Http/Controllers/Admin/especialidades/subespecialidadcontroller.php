<?php

namespace RTR\Http\Controllers\Admin\especialidades;

use Illuminate\Http\Request;
use RTR\modelos\TSubEspecialidad;
use RTR\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class subespecialidadcontroller extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
         return response()->json(['sub_especialidad'=>TSubEspecialidad::all()],200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        TSubEspecialidad::create([
            'id'=>$request->id,
            'descripcion_actividad_negocio'=>$request->descripcion,
            'idestatus'=>0
        ]);

          return response()->json(['success'=>'sub espcialidad registrada'],200);

    }
    

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $subespecialidad=TSubEspecialidad::where('id','=',$id);

            if($subespecialidad->count()==0)
                return response()->json(['error'=>'sub especialdad no encontrado'],404);

            $subespecialidad->update([
                'descripcion_actividad_negocio'=>$request->descripcion
                ]);

                  return response()->json(['sucess'=>'sub especialidad actualizada'],200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        //
        $subespecialidad=TSubEspecialidad::where('id','=',$id);
        
                    if($subespecialidad->count()==0)
                        return response()->json(['error'=>'sub especialdad no encontrado'],404);
        
             $subespecialidad->update([
                 'idestatus'=>0
             ]);
         
              return response()->json(['sucess'=>'sub especialidad eliminado'],200);
    }
}
