<?php

namespace RTR\Http\Controllers\Admin\especialidades;

use Illuminate\Http\Request;
use RTR\modelos\TEspecialidade;
use RTR\Http\Controllers\Controller;
use Validator;
class especialidadcontroller extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $especialidades=TEspecialidade::where('estatus','=','1');


         return response()->json(['especialidades'=>($especialidades->count()==0)?$especialidades->get():'no hay ninguna especialidad en el sistema'],200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {    $validators=Validator::make($request->all(),TEspecialidade::$validators);

           if($validators->fails())
               return response()->json($validators->errors()->all(),404);
          
         TEspecialidade::create([
             'id'=>$request->id,
             'descripcion_especialidad'=>$request->descripcion,
             'estatus'=>1
         ]);
           
           return response()->json(['success'=>'especialidad agregada'],200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {      
        $validators=Validator::make($request->all(),array('id'=>TEspecialidade::$validators['id']));
        
                   if($validators->fails())
                       return response()->json($validators->errors()->all(),404);
        //
        $especialidad=TEspecialidade::find($request->id);
        if($especialidad->count()==0)
            return response()->json(['error'=>'especialidad no encontrada'],404);

        $especialidad->update([
            
             'descripcion_especialidad'=>$request->descripcion
         ]);
           
            return response()->json(['success'=>'especialidad actualizada'],200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
          $validator=Validator::make($request->all(),array(TEspecialidade::$validators['id']));

        if($validators->fails())
        return response()->json($validators->errors()->all(),404);

        $especialidad=TEspecialidade::where('id','=',$request->id)
        ->where('estatus','=','1');

                if($especialidad->count()==0)
                    return response()->json(['error'=>'especialidad no encontrada'],404);

            $especialidad->first()->update([
                    'estatus'=>0
                    
            ]);

             return response()->json(['success'=>'especialidad eliminada'],200);
        
    }
}
