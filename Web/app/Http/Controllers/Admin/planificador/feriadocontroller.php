<?php

namespace RTR\Http\Controllers\Admin\planificador;

use Illuminate\Http\Request;
use RTR\Http\Resources\CRFRecurso;
use RTR\modelos\TDiasFeriado;
use RTR\modelos\TPaise;
use RTR\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use RTR\modelos\TPersona;
use Validator;

class feriadocontroller extends Controller
{
    private $pais;
    private $feriado;
    private $user;
    public function __construct()
    {
        $this->middleware(function ($request, $next) {
            $this->user = Auth::User();
            return $next($request);
        });
         $this->pais=new TPaise();
         $this->feriado=new TDiasFeriado();
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $Empresa=TPersona::withoutGlobalScopes()
            ->where('idOperador',$this->user->idOperador)
            ->where('idgrupo_persona','FABR')
            ->select(['nombre_completo_razon_social','idPersona'])
            ->get();
        $T="DiasFeriados";
        $pais=TPaise::all();
        $fabrica = $request->session()->get('user.teams');
        return view('Admin.estadistico.CiRaFr',compact('Empresa','T','pais','fabrica'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
           $validator=Validator::make($request->all(),TDiasFeriado::$validators);
             
              if($validator->fails())
                 return response()->json($validator->errors()->all(),404);

           $dias=TDiasFeriado::create([
               'idOperador'=>$this->user->idOperador,
               'idpais'=>$this->user->idpais,
               'idFabricante'=>$request->fabricante,
               'iddias_feriados'=>$request->id,
               'fecha_dias_Feriados'=>$request->fecha,
               'descripcion_dias_Feriados'=>$request->descripcion
           ]);
         $pais=TPaise::find($this->user->idpais);
             return response()->json(['success'=>$dias['iddias_feriados'],'pais'=>$pais->nombrePais],200);
         
          return response()->json(['success'=>'se ha ingresado un dia feriado para este pais'],200);
         
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request){
        $TA=TDiasFeriado::withoutGlobalScopes()->where('idFabricante',$request->fabricante)
                        ->where('idestatus',1)
                        ->get();
        foreach ($TA as $item)
            $item->tipo='df';

        $hola=CRFRecurso::collection($TA)->toJson();
        return response($hola,200);

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        //
        $validator=Validator::make($request->all(),[
            'id'=>'required|exists:t_dias_feriados,iddias_feriados',
            'fecha'=>'required|date',
            'descripcion'=>'required'
        ]);
        
         if($validator->fails())
            return response()->json($validator->errors()->all(),404);
         
         TDiasFeriado::withoutGlobalScopes()->where('iddias_feriados',$request->id)->update([
            'fecha_dias_Feriados'=>$request->fecha,
            'descripcion_dias_Feriados'=>$request->descripcion
        ]);
           
          $pais=TPaise::find($this->user->idpais);
       return response()->json(['success'=>$request->id,'pais'=>$pais->nombrePais],200);
     }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $validator=Validator::make($request->all(),[
            'id'=>'required|exists:t_dias_feriados,iddias_feriados'
        ]);
        
         if($validator->fails())
            return response()->json($validator->errors()->all(),404);
        //
        if(!TDiasFeriado::withoutGlobalScopes()->where('iddias_feriados',$request->id)->update([
            'idestatus'=>0
        ]))
           return response()->json(['error'=>'dia feriado inexistente'],404);

        return response()->json(['success'=>'dia feriado eliminado'],200);
    }
}
