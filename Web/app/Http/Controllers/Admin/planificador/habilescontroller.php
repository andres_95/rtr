<?php

namespace RTR\Http\Controllers\Admin\planificador;

use Illuminate\Http\Request;
use RTR\Http\Resources\CRFRecurso;
use RTR\modelos\TDiasFeriado;
use RTR\modelos\TDiasVisita;
use RTR\modelos\TPaise;
use RTR\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use RTR\modelos\TPersona;
use Validator;

class habilescontroller extends Controller
{
    private $pais;
    private $visita;
    public function __construct()
    {
        $this->middleware(function ($request, $next) {
            $this->user = Auth::user();
            return $next($request);
        });
        $this->pais=new TPaise();
        $this->visita=new TDiasVisita();
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $Empresa=TPersona::withoutGlobalScopes()
            ->where('idOperador',$this->user->idOperador)
            ->where('idgrupo_persona','FABR')
            ->select(['nombre_completo_razon_social','idPersona'])
            ->get();
        $T="DiasDeVisita";
        $pais=TPaise::all();
        $fabrica = $request->session()->get('user.teams');
        return view('Admin.estadistico.CiRaFr',compact('Empresa','T','pais','fabrica'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
          $validator=Validator::make($request->all(),[
              'id'=>'required|max:3|unique:t_dias_visitas,iddias_visita',
              'descripcion'=>'required|unique:t_dias_visitas,descripcion_dias_visita',
              'fabricante'=>'required|exists:t_personas,idPersona'
          ]);
             if($validator->fails())
                 return response()->json($validator->errors()->all(),404);

         TDiasVisita::create([
             'idOperador'=>$this->user->idOperador,
             'idFabricante'=>$request->fabricante,
             'idpais'=>$this->user->idpais,
             'iddias_visita'=>$request->id,
             'descripcion_dias_visita'=>$request->descripcion,
             'idestatus'=>1
         ]);
         $pais=TPaise::find($this->user->idpais);

        return response()->json(['success'=>$request->id,'pais'=>$pais->nombrePais],200);

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request){
        $TA=TDiasVisita::withoutGlobalScopes()->where('idFabricante',$request->fabricante)->get();
        foreach ($TA as $item)
            $item->tipo='dv';

        $hola=CRFRecurso::collection($TA)->toJson();
        return response($hola,200);

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        //   
        $validator=Validator::make($request->all(),[
            'descripcion'=>'required|unique:t_dias_visitas,descripcion_dias_visita',
        ]);
           if($validator->fails())
               return response()->json($validator->errors()->all(),404);

          if(!TDiasVisita::withoutGlobalScopes()->where('iddias_visita',$request->id)->update([
            'descripcion_dias_visita'=>$request->descripcion
        ]))
           return response()->json(['error'=>'dia de visita no encontrado'],404);

           $pais=TPaise::find($this->user->idpais);
           
                   return response()->json(['success'=>$request->id,'pais'=>$pais->nombrePais],200);
           
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        //
        $validator=Validator::make($request->all(),[
            'id'=>'required|exists:t_dias_visitas,iddias_visita'
         ]);

        if(!TDiasVisita::withoutGlobalScopes()->where('iddias_visita',$request->id)->update([
            'idestatus'=>0
        ]))
            return response()->json(['error'=>'dia de visita inexistente'],404);

        return response()->json(['success'=>'dia de visita eliminado'],200);
    }
}
