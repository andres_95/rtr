<?php

namespace RTR\Http\Controllers\Admin\cadenas;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use PhpOffice\PhpSpreadsheet\IOFactory;
use RTR\ExcelExport\CadenaFormato;
use RTR\Filterimport\CadenaRead;
use RTR\Http\Resources\CRFRecurso;
use RTR\modelos\TCadena;
use RTR\Http\Controllers\Controller;
use RTR\modelos\TPersona;
use Validator;
class cadenacontroller extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware(function ($request, $next) {
            $this->user = Auth::user();
            return $next($request);
        });
    }

    public function index()
    {
        //

        return response()->json(['cadenas'=>TCadena::all()],200);
    }

    public function index2(Request $request)
    {
        $Empresa=TPersona::withoutGlobalScopes()
            ->where('idOperador',$this->user->idOperador)
            ->where('idgrupo_persona','FABR')
            ->select(['nombre_completo_razon_social','idPersona'])
            ->get();
        $T="Cadenas";
        $fabrica = $request->session()->get('user.teams');
        return view('Admin.estadistico.CiRaFr',compact('Empresa','T','fabrica'));
    }

    public function store(Request $request)
    {
        //
          $validator=Validator::make($request->all(),TCadena::$validators);

              if($validator->fails())
                 return response()->json($validator->errors()->all(),404);

          $data=$request->all();
          $data['idOperador']=$this->user->idOperador;
          $data['estatus']=1;

        $cadena=TCadena::create([
            'idOperador'=>$this->user->idOperador,
            'idFabricante'=>$request->fabricante,
            'descripcion'=>$request->descripcion,
            'estatus'=>1
        ]);
         
             return response()->json(['success'=>$cadena['id'],'id'=>$cadena['id']],200);
    }

    public function update(Request $request){

      /*   $data=$request->all();
         $data['idOperador']=$this->user->idOperador;
         $data['estatus']=1;*/
          
         $validator=Validator::make($request->all(),array(TCadena::$validators,'id'=>'required|exists:t_cadenas,id'));
         
                       if($validator->fails())
                          return response()->json($validator->errors()->all(),404);

       if(($cadena=TCadena::withoutGlobalScopes()->where('id',$request->id)
            ->where('estatus',1)
            ->update([
                'descripcion'=>$request->descripcion
            ]))
         )
            return response()->json(['success'=>$cadena['id']],200);

        return response()->json(['error'=>'cadena no encontrada'],404);

    }

    public function delete(Request $request){
        $data=$request->all();
        $data['idOperador']=$this->user->idOperador;
        $data['estatus']=1;



    } 

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request)
    {
        $local=TCadena::withoutGlobalScopes()->where('idFabricante',$request->fabricante)->get();
        foreach ($local as $item){
            $item->tipo='ca';
        }
        $hola=CRFRecurso::collection($local)->toJson();
        return response($hola,200);
    }



    public function exportExcel(Request $request)
    {
        $validator = Validator::make($request->all(),
            [
                'id' => 'required|exists:t_personas,idFabricante'
            ]
        );

        if($validator->fails()){
            return response()->json($validator->errors()->all(), 400);
        }
        return (new CadenaFormato())->download('Formato_cadena.xlsx')->deleteFileAfterSend(true);
    }

    public function LoadExcel(Request $request){
        $validator = Validator::make($request->all(),
            [
                'FormatoCadena' => 'required|mimes:xlsx|max:1024',
                'id' => 'required|exists:t_personas,idPersona'
            ]
        );
        if($validator->fails()){
            return response()->json($validator->errors()->all(), 400);
        }

        $file = $request->file('FormatoCadena');
        $path = $file->getRealPath();

        $filter = new CadenaRead();
        $reader= IOFactory::createReaderForFile($path);
        $reader->setReadFilter($filter);

        $excel = $reader->load($path);

        $hoja= $excel->getSheetByName('cadenas');

        $cantrow = $hoja->getCellCollection()->getHighestRow();
        $data =  $hoja->rangeToArray(
            'A2:A'.$cantrow,
            NULL,
            TRUE,
            TRUE,
            TRUE
        );

        $errores = [];
        foreach ($data as $row => $record) {
            $v = Validator::make($record,
                [
                    'A' => 'required|string|max:10',
                ]
            );
            if($v->fails()){
                array_push($errores,['error'=>'Error en registro '.$row,'details'=>$v->errors()->all()]);
            }else
            {
                $find = TCadena::withoutGlobalScopes()->where('idOperador',Auth::user()->idOperador)
                    ->where('idFabricante',$request->id)
                    ->where('descripcion',$record['A'])
                    ->first();
                if($find)
                {
                    array_push($errores,['error'=>'Error en registro '.$row,'details'=>['Registro ya existente']]);
                }else{
                    TCadena::create([
                        'idOperador' => Auth::user()->idOperador,
                        'idFabricante' => $request->id,
                        'descripcion' => $record['A'],
                    ]);
                }

            }

        }

        if($errores)
            return response()->json(['error'=>$errores,'status'=>400]);
        else
            return response()->json(['error'=>$errores,'status'=>201]);
    }
}
