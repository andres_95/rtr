<?php

namespace RTR\Http\Controllers\Admin\Producto;

use Illuminate\Http\Request;
use PhpOffice\PhpSpreadsheet\IOFactory;
use RTR\Filterimport\ProductoRead;
use RTR\ExcelExport\ProductoFormato;
use RTR\Http\Controllers\Controller;
use RTR\Http\Resources\ProductoRecurso;
use Illuminate\Support\Facades\Auth;
use RTR\modelos\TPersona;
use RTR\modelos\TProducto;
use RTR\modelos\TLineaProducto;
use RTR\modelos\TTipoProducto;
use RTR\modelos\TUnidade;
use Carbon\Carbon;
use Validator;

class ProductoController extends Controller
{
    //
	
    public function __construct()
    {
        $this->middleware(function ($request, $next) {
            $this->user = Auth::user();
            return $next($request);
        });
    }
    
    public function index(Request $request){

        $Empresa=TPersona::withoutGlobalScopes()
                                ->where('idOperador',$this->user->idOperador)
                                ->where('idgrupo_persona','FABR')
                                ->select(['nombre_completo_razon_social','idPersona'])
                                ->get();
        $fabrica = $request->session()->get('user.teams');
        return view('Admin.productos.home_productos',compact('Empresa','fabrica'));
    }

    public function create(Request $request){

        $categorias=$request->tipo;
        $NombreCategoria = $request->nameCategory;
        $linea=TLineaProducto::all();
        $unidades= TUnidade::all();
        $Fabrica = $request->Fabrica;
        $PM=[];
        return view('Admin.productos.newproduct',compact('categorias','linea','NombreCategoria','unidades','Fabrica','PM'));
    }

    public function listprod(Request $request)
    {
        if($request->fabricante)
            return ProductoRecurso::collection(TProducto::withoutGlobalScopes()->where(['idFabricante'=>$request->fabricante,'idcategorias'=>$request->categoria])->where('estatus_producto',1)->get())->response();
        return ProductoRecurso::collection(TProducto::withoutGlobalScopes()->where('idcategorias',$request->categoria)->where('estatus_producto',1)->get())->response();
    }

     public function show(Request $request){

     }

    public function store(Request $request){
     //   dd($request->tipo);
     //   dd($request->all());
         /*$data=$request->all();
         $data['idOperador']=$this->user->idOperador;
         $data['idPersona']=$this->user->idPersona;
         $data['idMayorista']=$data['idPersona'];*/

      /*   "id":this.editedItem.id,
         "nombre":this.editedItem.nombre,
         "apellido":this.editedItem.apellido,
         "descuento":this.editedItem.descuento,
         "cantidad":this.editedItem.cantidad,
         "linea":this.editedItem.linea,
         "expedicion":this.editedItem.expedicion,
         "vencimiento":this.editedItem.Vencimiento,
         "lote":this.editedItem.lote,
         "precio":this.editedItem.precio,
         "descuento":this.editedItem.descuento,
         "fabricante":this.fabricante*/
          // dd($request)
          $validator=Validator::make($request->all(),TProducto::$validators);
             if($validator->fails())
                return response()->json($validator->errors()->all(),404);

            TProducto::create([
                'idOperador'=>$this->user->idOperador,
                'idfabricante'=>$request->fabricante,
                'idproducto'=>$request->id,
                'idPersona'=>$this->user->idPersona,
                'idMayorista'=>$request->mayorista,
               /* 'idpais'=>,
                'ididioma',
                'idmoneda',*/
                'idlinea_producto'=>$request->linea,
                'idtipo_producto'=>$request->tipo,
                'nombre_producto'=>$request->nombre,
                'descripcion_producto'=>$request->nombre,
               // 'principio_producto',
                'Precio_producto'=>$request->precio,
                'Descuento_producto'=>$request->descuento,
                'idcategorias'=>$request->categoria,
                'presentacion'=>($request->presentacion)?$request->presentacion:'S/N',
                'cantidad_producto_existente'=>$request->cantidad,
                'fechaRegistro_producto'=>Carbon::now()->format('Y-m-d'),
                'fechaExpedicion_producto'=>$request->expedicion,
                'fechaVencimiento_producto'=>$request->vencimiento,
              //  'dias_publicacion',
              //  'idpromocion',
                'lote'=>$request->lote
              //  'FechaInicioPublicacion',
              //  'FechaFinPublicacion',
              //  'estatus_producto',
               // 'idunidades'

            ]);

            $Empresa=$request->fabricante;
            $categoria=$request->categoria;
            $linea=TLineaProducto::all();
            //dd($categoria,$request->all());
            return ProductoRecurso::collection(TProducto::withoutGlobalScopes()->where(['idFabricante'=>$Empresa,'idcategorias'=>$categoria])->where('estatus_producto',1)->get())->response();


        
    }

     public function update(Request $request){

        //$prod=TProducto::withoutGlobalScopes()->where('idproducto',$request->id)->get();
        //dd($request);
        $validator=Validator::make($request->all(),array_diff(
            TProducto::$validators,array(TProducto::$validators['fabricante'],
                                         TProducto::$validators['id']
         )));
        if($validator->fails())
           return response()->json($validator->errors()->all(),404);

        if(!TProducto::withoutGlobalScopes()->where('idproducto',$request->id)->first()->update([

           'idMayorista'=>$request->mayorista,
           /* 'idpais'=>,
            'ididioma',
            'idmoneda',*/
          //  'idlinea_producto'=>(is_object($request->linea))?$request->linea['id']:$request->linea,
            'idtipo_producto'=>$request->tipo,
            'nombre_producto'=>$request->nombre,
            'descripcion_producto'=>$request->nombre,
           // 'principio_producto',
            'Precio_producto'=>$request->precio,
            'Descuento_producto'=>$request->descuento,
            'idcategorias'=>$request->categoria,
            'presentacion'=>($request->presentacion)?$request->presentacion:'S/P',
            'cantidad_producto_existente'=>$request->cantidad,
          //  'fechaRegistro_producto'=>Carbon::now()->format('Y-m-d'),
            'fechaExpedicion_producto'=>$request->expedicion,
            'fechaVencimiento_producto'=>$request->vencimiento,
          //  'dias_publicacion',
          //  'idpromocion',
            'lote'=>$request->lote
          //  'FechaInicioPublicacion',
          //  'FechaFinPublicacion',
          //  'estatus_producto',
           // 'idunidades'

        ]))
         {  return ProductoRecurso::collection(TProducto::withoutGlobalScopes()->where(['idFabricante'=>$request->fabricante,'idcategorias'=>$request->categoria])->where('estatus_producto',1)->get())->response();
         }

         return ProductoRecurso::collection(TProducto::withoutGlobalScopes()->where(['idFabricante'=>$request->fabricante,'idcategorias'=>$request->categoria])->where('estatus_producto',1)->get())->response();
     }

     public function destroy(Request $request){
        $validator=Validator::make($request->all(),array(
            'id'=>'required|exists:t_productos,idProducto'
         ));
            if($validator->fails()){
                return response()->json($validator->errors()->all(),404);
            }
         if(!TProducto::withoutGlobalScopes()->where('idproducto',$request->id)->first()->update(['estatus_producto'=>0]))
             return ProductoRecurso::collection(TProducto::withoutGlobalScopes()->where(['idFabricante'=>$request->fabricante,'idcategorias'=>$request->categoria,'estatus_producto'=>1])->where('estatus_producto',1)->get())->response();

         return ProductoRecurso::collection(TProducto::withoutGlobalScopes()->where(['idFabricante'=>$request->fabricante,'idcategorias'=>$request->categoria])->where('estatus_producto',1)->get())->response();
     }
    public function showList(Request $request){
        $Empresa=$request->fabrica;
        $categoria=$request->tipo;
        $linea=TLineaProducto::withoutGlobalScopes()->where('idFabricante',$Empresa)->get();
        $mayorista=TPersona::withoutGlobalScopes()->where(['idFabricante'=>$Empresa,'idgrupo_persona'=>'MAY'])->get();
        $tipos=TTipoProducto::withoutGlobalScopes()->where('idFabricante',$Empresa)->get();
        //dd($categoria,$request->all());
        return view('Admin.productos.ListaTodos',compact('categoria','Empresa','linea','mayorista','tipos'));
    }

    public function exportExcel(Request $request)
    {
        $validator = Validator::make($request->all(),
            [
                'id' => 'required|exists:t_personas,idFabricante'
            ]
        );

        if($validator->fails()){
            return response()->json($validator->errors()->all(), 400);
        }
        return (new ProductoFormato($request->id))->download('Productoformato.xlsx')->deleteFileAfterSend(true);
    }

    public function LoadExcel(Request $request){
        $validator = Validator::make($request->all(),
            [
                'FormatoProducto' => 'required|mimes:xlsx|max:1024',
                'id' => 'required|exists:t_personas,idPersona'
            ]
        );
        if($validator->fails()){
            return response()->json($validator->errors()->all(), 400);
        }

        $file = $request->file('FormatoProducto');
        //dd($file->getRealPath());
        $path = $file->getRealPath();

        $filter = new ProductoRead();
        $reader= IOFactory::createReaderForFile($path);
        $reader->setReadFilter($filter);

        $excel = $reader->load($path);

        $hoja= $excel->getSheetByName('Productos');

        $cantrow = $hoja->getCellCollection()->getHighestRow();
        $data =  $hoja->rangeToArray(
            'A2:R'.$cantrow,
            NULL,
            TRUE,
            TRUE,
            TRUE
        );

        $errores = [];
        foreach ($data as $row => $record) {
            $v = Validator::make($record,
                [
                    'A' => 'required|string|max:11',
                    'B' => 'required|exists:t_personas,idPersona',
                    'D' => 'required|exists:t_linea_productos,id',
                    'E' => 'required|exists:t_tipo_productos,idtipo_producto',
                    'F' => 'required|exists:t_categorias,idcategorias',
                    'G' => 'required|string|max:100',
                    'H' => 'required|string|max:100',
                    'I' => 'required|string|max:100',
                    'J' => 'integer',
                    'K' => 'integer',
                ]
            );
            if($v->fails()){
                array_push($errores,['error'=>'Error en registro '.$row,'details'=>$v->errors()->all()]);
            }else
            {
                $find = TProducto::withoutGlobalScopes()
                    ->where('idOperador',Auth::user()->idOperador)
                    ->where('idfabricante',$request->id)
                    ->where('idproducto',$record['A'])
                    ->where('idMayorista',$record['B'])
                    ->where('idlinea_producto',$record['D'])
                    ->where('idtipo_producto',$record['E'])
                    ->where('idcategorias',$record['F'])
                    ->first();
                if($find)
                {
                    array_push($errores,['error'=>'Error en registro '.$row,'details'=>['Registro ya existente']]);
                }else{
                    TProducto::create([
                        'idOperador' => Auth::user()->idOperador,
                        'idfabricante' => $request->id,
                        'idPersona'=> Auth::user()->idPersona,
                        'idproducto'=>$record['A'],
                        'idMayorista'=>$record['B'],
                        'idlinea_producto'=>$record['D'],
                        'idtipo_producto'=>$record['E'],
                        'idcategorias'=>$record['F'],
                        'nombre_producto'=>$record['G'],
                        'descripcion_producto'=>$record['H'],
                        'principio_producto'=>$record['I'],
                        'Precio_producto'=>$record['J'],
                        'Descuento_producto'=>$record['K'],
                        'presentacion'=>$record['L']
                    ]);
                }

            }

        }

        if($errores)
            return response()->json(['error'=>$errores,'status'=>400]);
        else
            return response()->json(['error'=>$errores,'status'=>201]);

    }

}
