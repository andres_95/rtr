<?php

namespace RTR\Http\Controllers\Admin\Producto;

use PhpOffice\PhpSpreadsheet\IOFactory;
use RTR\Filterimport\CliMatRead;
use RTR\ExcelExport\ClientMatrialFormato;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use RTR\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use RTR\modelos\TCategoria;
use RTR\modelos\TPersona;
use Validator;
class ClienteMuestra extends Controller
{
    //

    public function __construct()
    {
        $this->middleware(function ($request, $next) {
            $this->user = Auth::user();
            return $next($request);
        });
    }

    public function exportExcel(Request $request)
    {
        $validator = Validator::make($request->all(),
            [
                'id' => 'required|exists:t_personas,idFabricante'
            ]
        );

        if($validator->fails()){
            return response()->json($validator->errors()->all(), 400);
        }
        return (new ClientMatrialFormato($request->id))->download('CliMatformato.xlsx')->deleteFileAfterSend(true);
    }

    public function LoadExcel(Request $request){
        $validator = Validator::make($request->all(),
        [
            'FormatoCliMat' => 'required|mimes:xlsx|max:1024',
            'id' => 'required|exists:t_personas,idPersona'
        ]
            );
        if($validator->fails()){
            return response()->json($validator->errors()->all(), 400);
        }

        $file = $request->file('FormatoCliMat');
        //dd($file->getRealPath());
        $path = $file->getRealPath();

        $filter = new CliMatRead();
        $reader= IOFactory::createReaderForFile($path);
        $reader->setReadFilter($filter);

        $excel = $reader->load($path);

        $hoja= $excel->getSheetByName('Cliente_Muestras');

        $cantrow = $hoja->getCellCollection()->getHighestRow();
        $data =  $hoja->rangeToArray(
            'A2:B'.$cantrow,
            NULL,
            TRUE,
            TRUE,
            TRUE
        );
        //dd($cantrow);
        $errores = [];
        foreach ($data as $row => $record) {
            $v = Validator::make($record,
            [
                'A' => 'required|exists:t_personas,idPersona',
                'B' => 'required|exists:t_productos,idproducto',
            ]
            );
            if($v->fails()){
                array_push($errores,['error'=>'Error en registro '.$row,'details'=>$v->errors()->all()]);
            }else
            {
                $find = DB::table('r_clientes_materiales')->where('idOperador',Auth::user()->idOperador)
                    ->where('idFabricante',$request->id)
                    ->where('idcliente',$record['A'])
                    ->where('idproducto',$record['B'])->first();
                if($find)
                {
                    array_push($errores,['error'=>'Error en registro '.$row,'details'=>['Registro ya existente']]);
                }else{
                    DB::table('r_clientes_materiales')->insert([
                        'idOperador' => Auth::user()->idOperador,
                        'idFabricante' => $request->id,
                        'idcliente' => $record['A'],
                        'idproducto' => $record['B'],
                        ''
                    ]);
                }

            }

        }

        if($errores)
            return response()->json(['error'=>$errores,'status'=>400]);
        else
            return response()->json(['error'=>$errores,'status'=>201]);
    }


}