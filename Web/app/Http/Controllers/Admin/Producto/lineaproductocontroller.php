<?php

namespace RTR\Http\Controllers\Admin\Producto;

use Illuminate\Http\Request;
use RTR\ExcelExport\LineaFormato;
use RTR\Http\Resources\CRFRecurso;
use RTR\modelos\TLineaProducto;
use Illuminate\Support\Facades\Auth;
use RTR\Http\Controllers\Controller;
use RTR\modelos\TPersona;
use Validator;
class lineaproductocontroller extends Controller
{
    private $linea;
    public function __construct(){
        $this->middleware(function ($request, $next) {
            $this->user = Auth::user();
            return $next($request);
        });
        $this->linea=new TLineaProducto();
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //

        return response()->json([
            'lineas'=>TLineaProducto::where('idestatus',1)->get()
        ]);
    }

    public function index2(Request $request)
    {
        $Empresa=TPersona::withoutGlobalScopes()
            ->where('idOperador',$this->user->idOperador)
            ->where('idgrupo_persona','FABR')
            ->select(['nombre_completo_razon_social','idPersona'])
            ->get();
        $T="LineaProducto";
        $fabrica = $request->session()->get('user.teams');
        return view('Admin.estadistico.CiRaFr',compact('Empresa','T','fabrica'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
       
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
         $validator=Validator::make($request->all(),[
             'id'=>'required|unique:t_linea_productos,id|max:5',
             'fabricante'=>'required|exists:t_personas,idPersona',
             'descripcion'=>'required',
             'alias'=>'required|max:5'
         ]);
             if($validator->fails())
                 return response()->json($validator->errors()->all(),404);
     
        
        TLineaProducto::create([
            'id'=>$request->id,
            'idOperador'=>$this->user->idOperador,
            'idFabricante'=>$request->fabricante,
            'descripcion_linea_producto'=>$request->descripcion,
            'alias'=>$request->alias
        ]);
          return response()->json(['success'=>'linea de producto creada'],200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        
    }

    public function show2(Request $request)
    {
        $local=TLineaProducto::withoutGlobalScopes()->where('idFabricante',$request->fabricante)
                              ->where('idestatus',1)
                              ->get();
        foreach ($local as $item){
            $item->tipo='lp';
        }
        $hola=CRFRecurso::collection($local)->toJson();
        return response($hola,200);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        //
        $validator=Validator::make($request->all(),[
            'id'=>'required|exists:t_linea_productos,id|max:5',
            'fabricante'=>'required|exists:t_personas,idPersona',
            'descripcion'=>'required',
            'alias'=>'required|max:5'
        ]);
            if($validator->fails())
                return response()->json($validator->errors()->all(),404);

           if(!TLineaProducto::where('id',$request->id)->update([
            'descripcion_linea_producto'=>$request->descripcion,
            'alias'=>$request->alias
        ]))
              return response()->json(['error'=>'linea de producto no encontrda'],404);
              
            return response()->json(['success'=>'se ha cambiado una linea de producto'],200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        //
        $validator=Validator::make($request->all(),[
            'id'=>'required|exists:t_linea_productos,id|max:5'
        ]);
            if($validator->fails())
                return response()->json($validator->errors()->all(),404);

        if(!TLineaProducto::where('id',$request->id)->update([
            'idestatus'=>0
        ]))
            return response()->json(['error'=>'linea de producto no encontrda'],404);
        
      return response()->json(['success'=>'se ha eliminado una linea de producto'],200);
    }

    public function exportExcel(Request $request)
    {
        $validator = Validator::make($request->all(),
            [
                'id' => 'required|exists:t_personas,idFabricante'
            ]
        );

        if($validator->fails()){
            return response()->json($validator->errors()->all(), 400);
        }
        return (new LineaFormato())->download('zona.xlsx');

    }
}
