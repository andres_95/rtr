<?php

namespace RTR\Http\Controllers\Admin\Producto;

use Illuminate\Http\Request;
use RTR\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use RTR\modelos\TCategoria;
use RTR\modelos\TPersona;
use Validator;
class CategoryController extends Controller
{
    //

    public function __construct()
    {
        $this->middleware(function ($request, $next) {
            $this->user = Auth::user();
            return $next($request);
        });
    }

    public function show(Request $request)
    {

        $validator = Validator::make($request->all(),
            [
                'fabricante' => 'required'
            ],
            [
                'required' => 'El Atributo :attribute es requerido'
            ]
        );

        if($validator->fails()){
            return response()->json($validator->errors()->all(), 400);
        }

        $categorias = TCategoria::withoutGlobalScopes()->where('idFabricante',$request->fabricante)->get();

        return response()->json($categorias,200);
    }

    public function list(Request $request){
        $categorias = TCategoria::withoutGlobalScopes()->where('idFabricante',$request->fabricante)->get();
        foreach ($categorias as $item){
            $item->id=$item->idcategorias;
            $item->descripcion=$item->NombreCategorias;
            $item->fecha=$item->FechaCreacion->format('Y-m-d');
        }

        return response()->json($categorias,200);
    }

    public function index(Request $request){
        $Empresa=TPersona::withoutGlobalScopes()
                                ->where('idOperador',$this->user->idOperador)
                                ->where('idgrupo_persona','FABR')
                                ->select(['nombre_completo_razon_social','idPersona'])
                                ->get();
        $T="Categorias";
        $fabrica = $request->session()->get('user.teams');
        return view('Admin.estadistico.CiRaFr',compact('Empresa','T','fabrica'));
    }

    public function nuevo(){
        return view('Admin.productos.agregarcategoria');
    }

    public function store(Request $request){

        $validator=Validator::make($request->all(),TCategoria::$validators);
           if($validator->fails())
               return response()->json($validator->errors()->all(),404);

        TCategoria::create(
            [
                'idOperador'=>$this->user->idOperador,
                'idFabricante'=>$request->idFabricante,
                'idcategorias'=>$request->id,
                'NombreCategorias'=>$request->nombre,
            ]);
        
    }
   public function update(Request $request){
    $validator=Validator::make($request->all(),TCategoria::$validators);
    if($validator->fails())
        return response()->json($validator->errors()->all(),404);  

       if(
            !TCategoria::where('idcategorias',$request->id)->update(
                [
                
                    'idcategorias'=>$request->id,
                    'NombreCategorias'=>$request->nombre
                ])
                  
       ){
            return response()->json(['error'=>'categoria no encontrada'],404);
        }


         return response()->json(['success'=>'categoria registrada'],200);
   }
    public function destroy(Request $request){

        $validator=Validator::make($request->all(),array(TCategoria::$validators['id']));
        if($validator->fails())
            return response()->json($validator->errors()->all(),404);
        /*  dd(TCategoria::where('idcategorias',$request->id)
          ->update(['idestatus'=>0]));*/
         if(!TCategoria::where('idcategorias',$request->id)
            ->update(['idestatus'=>0])
         )
             return response()->json(['error'=>'categoria no encontrada'],404);

             return response()->json(['success'=>'categoria registrada'],200);
    }
}
