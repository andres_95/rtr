<?php

namespace RTR\Http\Controllers\Admin\Orden;

use Illuminate\Http\Request;
use RTR\Http\Controllers\Controller;
use RTR\Http\Resources\OrdenRecurso;
use RTR\modelos\TPersona;
use RTR\modelos\TOrdene;

class OrdenController extends Controller
{

    public function index(){

        $Empresa=TPersona::withoutGlobalScopes()->where('idFabricante','')->get(['idPersona']);
        return view('Admin.ordenes.lista_ordenes',compact('Empresa'));
    }

    public function show(Request $request){
        $orden=TOrdene::where('idFabricante',$request->fabricante)->get();
        
        return OrdenRecurso::collection($orden);
    }

}