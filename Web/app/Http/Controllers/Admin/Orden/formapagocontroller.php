<?php

namespace RTR\Http\Controllers\Admin\Orden;

use Illuminate\Http\Request;
use RTR\modelos\TFormaPago;

class formapagocontroller extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
          return response()->json(['formas de pago'=>TFormaPago::where('idestatus',1)]);
    }

    public function index2()
    {
        $Fpago=TFormaPago::where('idestatus','=',1)->get();
        foreach ($Fpago as $item)
            $item->tipo='fp';

        $T="FormaPago";
        $valor=SinFABR::collection($Fpago)->toJson();
        return view('Admin.estadistico.SinFABR',compact('valor','T'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
          (new TFormaPago())->createItem($request->all());

            return response()->json(['success'=>'forma de pago agregada'],200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        //
          if(!(new TFormaPago())->updItem($request->id,$request->all()))
             return response()->json(['error'=>'forma de pago no encontrada'],404);

             return response()->json(['success'=>'forma de pago cambiada'],200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        if(!(new TFormaPago())->delItem($request->id))
        return response()->json(['error'=>'forma de pago no encontrada'],404);

        return response()->json(['success'=>'forma de pago cambiada'],200);
    }
}
