<?php

namespace RTR\Http\Controllers\Web\Admin;

use RTR\modelos\TGrupopersona;
use RTR\modelos\TPersona;
use RTR\modelos\TTipoPersona;
use Illuminate\Http\Request;
use RTR\Http\Controllers\Controller;
use RTR\modelos\TPaise;
use RTR\modelos\TEspecialidade;
use RTR\modelos\TClasePersona;
use RTR\modelos\TRankingCliente;
use RTR\modelos\TCiclo;
use RTR\modelos\TFrecuenciaVisita;
use validate;

class UsuarioController extends Controller
{
    public function index2()
    {
        $Empresa=TPersona::withoutGlobalScopes()->where('idFabricante','')->get(['nombre_completo_razon_social','idPersona']);
        return view('Admin.usuarios.home_usuarios',compact('Empresa'));
    }
}