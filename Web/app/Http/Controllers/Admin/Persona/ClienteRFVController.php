<?php
/**
 * Created by PhpStorm.
 * User: usuario
 * Date: 17/04/2018
 * Time: 17:17
 */
namespace RTR\Http\Controllers\Admin\Persona;

use Illuminate\Support\Facades\Auth;
use RTR\ExcelExport\ClienteRfvFormato;
use RTR\Http\Controllers\Controller;
use Validator;

class ClienteRFVController extends Controller
{
    public function __construct()
    {
        $this->middleware(function ($request, $next) {
            $this->user = Auth::user();
            return $next($request);
        });
    }

    public function exportExcel(Request $request)
    {
        $validator = Validator::make($request->all(),
            [
                'id' => 'required|exists:t_personas,idFabricante'
            ]
        );

        if($validator->fails()){
            return response()->json($validator->errors()->all(), 400);
        }
        return (new ClienteRfvFormato($request->id))->download('Formato_cliente.xlsx');
    }

}