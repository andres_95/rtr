<?php
    //get user rol from json
    namespace RTR\Http\Controllers\Admin\Usuario;
    
    use Validator;
    use RTR\modelos\TRole;
    use Illuminate\Http\Response;
    use Illuminate\Http\Request;
    use RTR\Http\Controllers\Controller;
    use Exception;


        class RolesController extends Controller{

            public function create(Request $request){        
                $nuevoRol=TRole::create([
                    'idOperador'          => $request->operador,
                    'idperfil'            => $request->perfil,
                    'idrol'               => $request->rol,
                    'descripcion_rol'     => $request->descripcion,
                    'idestatus'           => $request->status,                    
                ]);       
                return response(["exito"=>"Rol creado"],200);
        
            }

            public function count (){
                $roles = TRole::all();
                $respuesta = [];
                foreach($roles as $key=>$rol){
                    $respuesta[ $rol ['descripcion'] ] = TRol::find($rol['id'])->persona()->all()->count();
                }
                return response (['Roles' => $respuesta]);
            }
            public function destroy(Request $request){
                
                $validar=Validator::make($request->all(),[
                        'idRol'=>'required|int|exists:descripcionRol',
                    ],
                    [
                        'required'=>'Se necesita el :attribute'
                    ]
                );
                $rol=TRole::where('idRol','=',$request->id)->where('estatus','=','1');
                $rol->update(['estatus'=>'0']);
                return response('esta elimina el item',200);
            }

            public function show (){
                return response(["roles" => TRole::all(['idrol','descripcion_rol'])],200);

            }







        }       
?>