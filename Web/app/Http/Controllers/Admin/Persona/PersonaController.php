<?php

namespace RTR\Http\Controllers\Admin\Persona;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\Rule;
use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Style\NumberFormat;
use RTR\ExcelExport\ClienteDiasFormato;
use RTR\ExcelExport\PersonaDiasFormato;
use RTR\Filterimport\ClienteDiasRead;
use RTR\Filterimport\PersonaRead;
use RTR\ExcelExport\ClienteRfvFormato;
use RTR\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use RTR\Http\Resources\listaRecurso;
use RTR\modelos\TCategoria;
use RTR\modelos\TDiasVisita;
use RTR\modelos\TEstatusOrdene;
use RTR\modelos\TLogAuditoria;
use RTR\modelos\TPerfile;
use RTR\modelos\TPersona;
use RTR\modelos\TListanegra;
use RTR\modelos\TGrupopersona;
use RTR\modelos\TPaise;
use RTR\modelos\TTipoActividade;
use RTR\modelos\TTipoIncidente;
use RTR\modelos\TTipoPersona;
use RTR\modelos\TEspecialidade;
use RTR\modelos\TTipoEspecialidade;
use RTR\modelos\TClasePersona;
use RTR\modelos\TRankingCliente;
use RTR\modelos\TPlanificadore;
use RTR\modelos\TCiclo;
use RTR\modelos\TTitulo;
use RTR\modelos\TFrecuenciaVisita;
use RTR\Http\Resources\personaRecurso;
use RTR\Http\Resources\ADMVisita;
use Validator;
use RTR\ExcelExport\PersonaFormato\ClienteFormato;

class PersonaController extends Controller
{
    public function __construct()
    {
        $this->middleware(function ($request, $next) {
            $this->user = Auth::user();
            return $next($request);
        });
    }

    public function index(Request $request)
    {
        $Empresa=TPersona::withoutGlobalScopes()
                                ->where('idOperador',$this->user->idOperador)
                                ->where('idgrupo_persona','FABR')
                                ->select(['nombre_completo_razon_social','idPersona'])
                                ->get();
        $fabrica = $request->session()->get('user.teams');
        return view('Admin.usuarios.home_personas',compact('Empresa','fabrica'));
    }

    public function showList(Request $request){
        $Empresa=$request->Fabrica;
        $grupopersona=$request->tipo;
        $DescripGrupopersona = TGrupopersona::find($grupopersona)->descripciongrupoPersona;
        $tipopersona = TTipoPersona::all();
        $paises = TPaise::all();

        //Datos Cliente
        // $especialidad=TTipoEspecialidade::where('idFabricante',$Empresa)->get();//
        $especialidad=TEspecialidade::all();
        $clase = TClasePersona::where('estatus',1)->get();
        $ranking = TRankingCliente::withoutGlobalScopes()->where('idFabricante',$Empresa)->get();
        $freq = TFrecuenciaVisita::withoutGlobalScopes()->where('idFabricante',$Empresa)->get();
        
        //Datos RFV y SUP
        if($grupopersona=='RFV' || $grupopersona=='SUP'){
            $freq = TCiclo::withoutGlobalScopes()->where('idFabricante',$Empresa)->get();
            if($grupopersona=='RFV')
                $ranking=TPersona::withoutGlobalScopes()->where(['idFabricante'=>$Empresa,'idgrupo_persona'=>'SUP'])->get(['idPersona','nombre_completo_razon_social']);
            if($grupopersona=='SUP')
                $ranking=TPersona::withoutGlobalScopes()->where(['idFabricante'=>$Empresa,'idgrupo_persona'=>'GRT'])->get(['idPersona','nombre_completo_razon_social']);
        }
        $accion='';
        //dd($Empresa,$grupopersona,$DescripGrupopersona,$tipopersona,$paises,$especialidad,$clase,$ranking,$freq);
        //return view('Admin.usuarios.ListaPersonas',compact('grupopersona','DescripGrupopersona','Empresa','listaPersona'));
        return view('Admin.prueba',compact('accion','grupopersona','DescripGrupopersona','Empresa','paises','tipopersona','especialidad','clase','ranking','freq'));
    }

     

    public function showpersonas(Request $request){
        //dd($request->all());
        if($request->fabricante != 'SIIF')
            $listaPersona= TPersona::withoutGlobalScopes()->where(['idFabricante'=>$request->fabricante,'idgrupo_persona'=>$request->tipo,'idestatus'=>1])->get();
        else {
            $listaPersona = TPersona::withoutGlobalScopes()->where(['idgrupo_persona' => $request->tipo, 'idestatus' => 1]);
            if ($request->tipo == 'SIIF')
                $listaPersona = $listaPersona->where('idperfil', 4)->get();
            else
                $listaPersona = $listaPersona->get();
        }
        //dd($listaPersona);
        $lista=listaRecurso::collection($listaPersona);
        return response()->json($lista,200);

        //return response()->json($listaPersona,200);
    }
    
    public function store(Request $request)
    {

      //  dd($request->all());
          $data=$request->all();
       //   dd($request->all());
          $data['idOperador']=$this->user->idOperador;
        //  $data['idFabricante']=$request->fabricante;
          $data['idPersona']=$request->documento_identidad;

          $data['nombre_completo_razon_social']=$data['nombre'].' '.$data['apellido'];

          $perfil=NULL;
              $request->descuento=($request->descuento==null)?'0':$request->descuento;

          if($request->categoria == 'RFV' || $request->categoria == 'SUP' || $request->categoria == 'GRT'){
            //  dd(TPerfile::where('descripcion_perfil','like',$request->categoria)->count());
              $aux=TPerfile::where('descripcion_perfil',$request->categoria)->get()->toArray();
              $perfil=$aux['0']['id'];
          }
          if($request->categoria == 'SIIF'){
              $aux=TPerfile::where('tipo','OPE')->get()->toArray();
              $perfil=$aux['0']['id'];
          }


        //  dd($request->categoria);
        //dd($perfil);
        if($request->categoria == 'FABR')
            $fabr=$request->CI;
        else
            $fabr=$request->fabricante;

              $constraints=TPersona::$validator;

           //  dd($request->all());

           $validator=Validator::make($request->all(),[
           'CI'=>$constraints['CI'],
           'nombre'=>$constraints['nombre'],
           'apellido'=>$constraints['apellido'],
           'supervisor'=>($request->categoria == 'RFV' || $request->categoria == 'SUP')?$constraints['supervisor']:'nullable',
          'email'=>$constraints['email'],
           'telf'=>$constraints['telf'],
        //   'direccion'=>$constraints['direccion'],
           'genero'=>($request->categoria=='CLI' || $request->categoria == 'RFV' || $request->categoria == 'SUP' || $request->categoria=='MAY' || $request->categoria == 'GRT' )?$constraints['genero']:'nullable',
           'tipo'=>$constraints['tipo'],
           'especialidad'=>($request->categoria=='CLI')?$constraints['especialidad']:'nullable',
           'clase'=>($request->categoria=='CLI')?$constraints['clase']:'nullable',
           'ranking'=>($request->categoria=='CLI')?$constraints['ranking']:'nullable',
           'frecuencia'=>($request->categoria=='CLI')?$constraints['frecuencia']:'nullable',
           'descuento'=>($request->categoria=='MAY')?$constraints['descuento']:'nullable',
           'username'=>($request->categoria == 'RFV' || $request->categoria == 'SUP' || $request->categoria == 'GRT')?$constraints['username']:'nullable',
           'password'=>($request->categoria == 'RFV' || $request->categoria == 'SUP' || $request->categoria == 'GRT')?$constraints['password']:'nullable',
           'ciclo'=>($request->categoria == 'RFV' || $request->categoria == 'SUP')?$constraints['ciclo']:'nullable' ,
        //   'supervisor'=>$constraints['supervisor'],
           'pais'=>$constraints['pais'],
           'estado'=>$constraints['estado'],
           'ciudad'=>$constraints['ciudad'],
           ],TPersona::$messages);

                if($validator->fails())
                  return response()->json($validator->errors()->all(),404);

          TPersona::create([
            'idOperador'=>$this->user->idOperador,
            'idFabricante'=>$fabr,
            'nombre_persona'=>$request->nombre,
            'apellido_persona'=>$request->apellido,
            'nombre_completo_razon_social'=>$request->nombre.' '.$request->apellido,
            'cod_tipo_persona'=>$request->tipo,
            'idPersona'=>$request->CI,
            'documento_identidad'=>$request->CI,
            'email_persona'=>$request->email,
            'telefono_persona'=>$request->telf,
            'direccion_domicilio'=>$request->direccion,
            'sexo_genero_persona'=>$request->genero,
            'idactividad_negocio'=>$request->especialidad,
            'idclase_persona'=>$request->clase,
            'idranking'=>$request->ranking,
            'idfrecuencia'=>$request->frecuencia,
            'descuento'=>($request->descuento)?$request->descuento:null,
            'username'=>$request->username,
            'password'=>bcrypt($request->password),
            'idciclos'=>$request->ciclo,
            'idsupervisor'=>($request->supervisor)?$request->supervisor:null,
            'idpais'=>$request->pais,
            'idestado'=>$request->estado,
            'idciudad'=>$request->ciudad,
            'idgrupo_persona'=>$request->categoria,
            'idperfil' => $perfil,
           
          //  "categoria"=>$request->,
          ]);

          if($request->categoria == 'FABR'){
              TTipoActividade::create([
                  'idOperador' => $this->user->idOperador,
                  'idFabricante' => $request->CI,
                  'descripcion_tipo_actividades' =>'VISITA'
              ]);
              TTipoActividade::create([
                  'idOperador' => $this->user->idOperador,
                  'idFabricante' => $request->CI,
                  'descripcion_tipo_actividades' =>'ENTREGA MATERIALES'
              ]);

              TTipoIncidente::create([
                  'idOperador' => $this->user->idOperador,
                  'idFabricante' => $request->CI,
                  'descripcion_tipo_incidentes' =>'SIN INCIDENTES'
              ]);

              TCategoria::create([
                  'idOperador' => $this->user->idOperador,
                  'idFabricante' => $request->CI,
                  'idcategorias' =>'MUES',
                  'NombreCategorias' => 'Muestras'
              ]);

              TCategoria::create([
                  'idOperador' => $this->user->idOperador,
                  'idFabricante' => $request->CI,
                  'idcategorias' =>'PROD',
                  'NombreCategorias' => 'Productos'
              ]);

              TEstatusOrdene::create([
                  'idOperador' => $this->user->idOperador,
                  'idFabricante' => $request->CI,
                  'descripcion' => 'Registrada'
              ]);

              TEstatusOrdene::create([
                  'idOperador' => $this->user->idOperador,
                  'idFabricante' => $request->CI,
                  'descripcion' => 'Generada'
              ]);

              TEstatusOrdene::create([
                  'idOperador' => $this->user->idOperador,
                  'idFabricante' => $request->CI,
                  'descripcion' => 'Facturada'
              ]);

              TEstatusOrdene::create([
                  'idOperador' => $this->user->idOperador,
                  'idFabricante' => $request->CI,
                  'descripcion' => 'Anulada'
              ]);

              TFrecuenciaVisita::create([
                  'idOperador' => $this->user->idOperador,
                  'idFabricante' => $request->CI,
                  'descripcion_frecuencia_visitas' => '2 veces al mes'
              ]);

              TRankingCliente::create([
                  'idOperador' => $this->user->idOperador,
                  'idFabricante' => $request->CI,
                  'descripcion_ranking_cliente' => 'Alto'
              ]);

              TCiclo::create([
                  'idOperador' => $this->user->idOperador,
                  'idFabricante' => $request->CI,
                  'descripcion_ciclos' => 'mensual'
              ]);

              TLogAuditoria::create([
                  'idOperador' => $this->user->idOperador,
                  'idFabricante' => $request->CI,
                  'idPersona'=>$this->user->idPersona,
                  'cod_modulo'=>'04',
                  'cod_opcion'=>'01',
                  'descripcion_detalle'=>'Toma de pedidos',
                  'fecha_detalle'=>Carbon::now()->format('Y-m-d'),
              ]);

              TLogAuditoria::create([
                  'idOperador' => $this->user->idOperador,
                  'idFabricante' => $request->CI,
                  'idPersona'=>$this->user->idPersona,
                  'cod_modulo'=>'05',
                  'cod_opcion'=>'01',
                  'descripcion_detalle'=>'Conciliar Factura',
                  'fecha_detalle'=>Carbon::now()->format('Y-m-d'),
              ]);

              TLogAuditoria::create([
                  'idOperador' => $this->user->idOperador,
                  'idFabricante' => $request->CI,
                  'idPersona'=>$this->user->idPersona,
                  'cod_modulo'=>'05',
                  'cod_opcion'=>'0201',
                  'descripcion_detalle'=>'Registro de personas',
                  'fecha_detalle'=>Carbon::now()->format('Y-m-d'),
              ]);

              TLogAuditoria::create([
                  'idOperador' => $this->user->idOperador,
                  'idFabricante' => $request->CI,
                  'idPersona'=>$this->user->idPersona,
                  'cod_modulo'=>'05',
                  'cod_opcion'=>'0801',
                  'descripcion_detalle'=>'Esquema Promocional',
                  'fecha_detalle'=>Carbon::now()->format('Y-m-d'),
              ]);

              TLogAuditoria::create([
                  'idOperador' => $this->user->idOperador,
                  'idFabricante' => $request->CI,
                  'idPersona'=>$this->user->idPersona,
                  'cod_modulo'=>'03',
                  'cod_opcion'=>'03',
                  'descripcion_detalle'=>'Planificador de Visitas',
                  'fecha_detalle'=>Carbon::now()->format('Y-m-d'),
              ]);
          }

        if($request->categoria == 'FABR')
          $listaPersona= TPersona::withoutGlobalScopes()->where(['idgrupo_persona'=>$request->categoria])->where('idestatus',1)->get();
        else
            $listaPersona= TPersona::withoutGlobalScopes()->where(['idFabricante'=>$request->fabricante,'idgrupo_persona'=>$request->categoria])->where('idestatus',1)->get();

          $lista=listaRecurso::collection($listaPersona);
          return response()->json($lista,200);
          //  $data['idpais']=$request->pais;

           //  (new TPersona())->createItem($data);
           /* if(!TPersona::create(

                [
                    'idOperador'=>$data['idOperador'],
                    'idFabricante'=>$data['idFabricante'],
                    'nombre_persona'=>$data['nombre'],
                    'apellido_persona'=>$data['apellido'],
                    'idPersona'=>$data['documento_identidad'],
                    'documento_identidad'=>$data['documento_identidad'],
                    'email_persona'=>$data['email'],
                    'telefono_persona'=>$data['telf'],
                    'direccion_domicilio'=>$data['Direccion'],
                    'idpais'=>$data['idpais'],
                  //  'cod_tipo_persona'=>$data['tipopersona'],
                    'idgrupo_persona'=>$data['Tpersona']

                ]
            ))
                return view('notfound');*/



            return response($listapersona);
    }

     public function update(Request $request){
             /*
          $persona=TPersona::withoutGlobalScopes()->find('idPersona',$request->CI);
          */
       //   dd($request->descuento==null);

        //  $request->descuento=($request->descuento==null)?0:$request->descuento;
         //  dd($request->all());

         $constraints=TPersona::$validator;
          $validator=Validator::make($request->all(),
          [
            'nombre'=>$constraints['nombre'],
            'apellido'=>$constraints['apellido'],
           'email'=>$constraints['email'],
            'telf'=>$constraints['telf'],
            'direccion'=>$constraints['direccion'],
            'genero'=>($request->categoria=='CLI' || $request->categoria == 'RFV' || $request->categoria == 'SUP' || $request->categoria=='MAY' || $request->categoria == 'GRT' )?$constraints['genero']:'nullable',
            'tipo'=>$constraints['tipo'],
            'especialidad'=>($request->categoria=='CLI')?$constraints['especialidad']:'nullable',
            'clase'=>($request->categoria=='CLI')?$constraints['clase']:'nullable',
            'ranking'=>($request->categoria=='CLI')?$constraints['ranking']:'nullable',
            'frecuencia'=>($request->categoria=='CLI')?$constraints['frecuencia']:'nullable',
            'descuento'=>($request->categoria=='MAY')?$constraints['descuento']:'nullable',
         //   'username'=>$constraints['username'],
           // 'password'=>$constraints['password'],
            'ciclo'=>($request->categoria == 'RFV' || $request->categoria == 'SUP')?$constraints['ciclo']:'nullable' ,
         //   'supervisor'=>$constraints['supervisor'],
            'pais'=>$constraints['pais'],
            'estado'=>$constraints['estado'],
            'ciudad'=>$constraints['ciudad'],
           // 'fabricante'=>$constraints['']
          ],
          TPersona::$messages);
          
          if($validator->fails())
              return response()->json($validator->errors()->all(),404);

          if(!TPersona::withoutGlobalScopes()->where('idPersona',$request->CI)->first()->update([

                'nombre_persona'=>$request->nombre,
                'apellido_persona'=>$request->apellido,
                'idPersona'=>$request->CI,
                'documento_identidad'=>$request->CI,
                'email_persona'=>$request->email,
                'telefono_persona'=>$request->telf,
                'direccion_domicilio'=>$request->direccion,
                'sexo_genero_persona'=>$request->genero,
                'cod_tipo_persona'=>$request->tipo,
                'idactividad_negocio'=>$request->especialidad,
                'idclase_persona'=>$request->clase,
                'idranking'=>$request->ranking,
                'idfrecuencia'=>$request->frecuencia,
                'descuento'=>$request->descuento,
                'username'=>$request->username,
                'password'=>bcrypt($request->password),
                'idciclos'=>$request->ciclo,
                'idsupervisor'=>$request->supervisor,
                'idpais'=>$request->pais,
                'idestado'=>$request->estado,
                'idciudad'=>$request->ciudad,
              //  'idgrupo_persona'=>$id,
              //  "categoria"=>$request->,

              ])){
                return response()->json(['error'=>'persona no encontrada'],404);
            }

        $listaPersona= TPersona::withoutGlobalScopes()->where(['idFabricante'=>$request->fabricante,'idgrupo_persona'=>$request->categoria])->get();

          $lista=listaRecurso::collection($listaPersona);
          return response()->json($lista,200);
     }

    public function destroy(Request $request){
          
        $validator=Validator::make($request->all(),[
            'fabricante'=>TPersona::$validator['fabricante'],
            'id'=>'required|exists:t_personas,idPersona'
        ]);
        
        if($validator->fails())
            return response()->json(['errors'=>$validator->errors()->all()],404);

        if(!TPersona::withoutGlobalScopes()->where(['idFabricante'=>$request->fabricante,'idPersona'=>$request->id])->first()->update(['idestatus'=>0]))
             return view('notfound');
        
             $listaPersona=TPersona::withoutGlobalScopes()->where(['idFabricante'=>$request->fabricante,'idgrupo_persona'=>$request->categoria])->where('idestatus',1)->get();

               $lista=listaRecurso::collection($listaPersona);
               return response()->json($lista,200);
    }

    public function personaByGrupo(Request $request){
       //   $lista=TListanegra::all(['idPersona']);
          $personas=TPersona::withoutGlobalScopes()
                     ->where('idFabricante',$request->empresa)
                     ->where('nombre_completo_razon_social','<>',null)
                     ->where('idestatus',1)
                     ->get();
           //   dd($request->tipo);
            //  dd($request->empresa);
            //   dd($personas->toArray());
            return response($personas,200);

          


    }
    public function show(Request $request)
    {

        $validator = Validator::make($request->all(),
            [
                'fabricante' => 'required'
            ],
            [
                'required' => 'El Atributo :attribute es requerido'
            ]
        );

        if($validator->fails()){
            return response()->json($validator->errors()->all(), 400);
        }

        $categorias = TGrupopersona::where('idgrupo_persona','!=','SIIF')->where('idgrupo_persona','!=','FABR')->where('idestatus',1)->get();
        return response()->json($categorias,200);
    }

     public function tipo(){
        $tipo=TTipoPersona::all();
        foreach ($tipo as $item){
            $item->descripcion=$item->descripcion_tipo_persona;
        }
        $T='Tipo';
        $Empresa='';
        return view('Admin.usuarios.Aspectos',compact('tipo','Empresa','T'));
    }
    
    public function titulos(){
        $tipo=TTitulo::withoutGlobalScopes()->where('idestatus',1)->get();
        $T='Titulo';
        $Empresa='';
        return view('Admin.usuarios.Aspectos',compact('tipo','Empresa','T'));
    }

    public function especialidades(){
        $tipo=TEspecialidade::withoutGlobalScopes()->where('estatus',1)->get();
        foreach ($tipo as $item){
            $item->descripcion=$item->descripcion_especialidad;
        }
        $T='Especialidad';
        $Empresa='';
        return view('Admin.usuarios.Aspectos',compact('tipo','Empresa','T'));
    }

    public function clase(){
        $Empresa='';
        $tipo=TClasePersona::withoutGlobalScopes()->where('estatus',1)->get();
        $T='Clase';
        return view('Admin.usuarios.Aspectos',compact('tipo','Empresa','T'));
    }

    public function showclase(Request $request){
        $clase=TClasePersona::withoutGlobalScopes()->where('idFabricante',$request->fabricante)
                ->where('idestatus',1)
                ->where('estatus',1)->get();

        return response()->json($clase);
    }

    public function showgrupo(Request $request){
        $grupo=TGrupopersona::withoutGlobalScopes()->where('idFabricante',$request->fabricante)
                             ->where('idestatus',1)
                             ->get();
        return response()->json($grupo); 
    }

    public function SetF(Request $request)
    {
        $request->session()->put('user.teams', $request->fabricante);
        return response('cambio',200);
    }

    public function pass(Request $request){
        if(Hash::check($request->pass,$this->user->password))
            return response('exito',200);

        return response('fallo',400);
    }

    //administrar clientes a visitar
    public function adminvisita(Request $request)
    {
        $Empresa=TPersona::withoutGlobalScopes()
            ->where('idOperador',$this->user->idOperador)
            ->where('idgrupo_persona','FABR')
            ->select(['nombre_completo_razon_social','idPersona'])
            ->get();
        $fabrica = $request->session()->get('user.teams');
        return view('Admin.usuarios.clientesavisitar',compact('Empresa','fabrica'));
    }

    public function clientes(Request $request){

        $clientes=TPersona::withoutGlobalScopes()->where(['idFabricante'=>$request->fabricante,'idgrupo_persona'=>'CLI'])->get();
        $rfv=TPersona::withoutGlobalScopes()->where(['idFabricante'=>$request->fabricante,'idgrupo_persona'=>'RFV'])->get();

        $visita=DB::table('r_cliente_rfv')->where('idFabricante',$request->fabricante)->get();

        $visitas=ADMVisita::collection($visita);

        return response(['visitas'=>$visitas,'Clientes'=>$clientes,'RFV'=>$rfv],200);
    }

    // monitor
    public function exportExcel(Request $request)
    {
        $validator = Validator::make($request->all(),
            [
                'id' => 'required|exists:t_personas,idFabricante'
            ]
        );

        if($validator->fails()){
            return response()->json($validator->errors()->all(), 400);
        }
        return (new ClienteFormato($request->id))->download('FormatoPersona.xlsx')->deleteFileAfterSend(true);
    }

    public function exportExcel2(Request $request)
    {
        $validator = Validator::make($request->all(),
            [
                'id' => 'required|exists:t_personas,idFabricante'
            ]
        );

        if($validator->fails()){
            return response()->json($validator->errors()->all(), 400);
        }
        return (new ClienteRfvFormato($request->id))->download('Formato_cliente_rfv.xlsx');
    }

    public function LoadExcel(Request $request)
    {
        $validator = Validator::make($request->all(),
            [
                'FormatoPersona' => 'required|mimes:xlsx|max:1024',
                'id' => 'required|exists:t_personas,idPersona'
            ]
        );
        if($validator->fails()){
            return response()->json($validator->errors()->all(), 400);
        }
        $user = TPersona::where('idFabricante',$request->id)->first();
        $file = $request->file('FormatoPersona');
        $path = $file->getRealPath();

        $filter = new PersonaRead();
        $reader= IOFactory::createReaderForFile($path);
        $reader->setReadFilter($filter);
        $reader->setReadDataOnly(true);
        $excel = $reader->load($path);

        //$hoja= $excel->getSheetByName('TablaPrincipal');

        $hoja = $excel->getSheetByName('TablaPrincipal');
        $cantrow = $hoja->getCellCollection()->getHighestRow();
        $data =  $hoja->rangeToArray(
            'A2:AN'.$cantrow,
            NULL,
            TRUE,
            TRUE,
            TRUE
        );
        //dd($data);
        $errores = [];
        $validos = [];
        $valArray = array(
            'A' => 'required',
            'D' => 'nullable|exists:t_cadenas,id',
            'E' => 'required|exists:t_paises,idpais',
            'F' => 'string|max:3|nullable',// falta clave foranea con t_idiomas
            'G' => 'string|max:3|nullable',//falta clave foranea con t_monedas
            'H' => 'required|exists:t_tipo_personas,id',
            'L' => 'required',
            'M' => 'required|exists:t_grupopersonas,idgrupo_persona',
            'N' => 'nullable|exists:t_clase_personas,id',
            'P' => 'required|string|max:40',
            'Q' => 'required|string|max:80',
            'R' => 'required',
            'T' => 'nullable|format_date:"Y-m-d"',
            'U' => 'nullable|string|max:20',
            'V' => 'string|max:20|nullable',
            'W' => 'string|max:191|nullable',
            'X' => 'exists:t_titulos,id|nullable',
            'AA' => 'string|max:255|nullable',
            'AB' => 'required|exists:t_estados,idestado',
            'AC' => 'nullable|exists:t_ciudades,idCiudad',
            'AD' => 'string|max:10|nullable',
            'AE' => 'string|max:30|nullable',
            'AF' => 'string|max:20|nullable',
            'AG' => 'string|max:30|nullable',
            'AH' => 'string|max:20|nullable',
            'AI' => 'string|max:20|nullable',//coordenadas_a
            'AJ' => 'string|max:20|nullable',//coordenadas_l
            'AK' => 'string|max:120|nullable',
            'AL' => 'string|max:20|nullable',
            'AM' => 'string|max:191|nullable'
        );

        $rfvArr = array(
            'C' => 'required|exists:t_personas,idPersona',
            'D' => 'nullable|exists:t_cadenas,id',
            'S' => 'required|string|max:1',
            'Y' => 'required|exists:t_ciclos,id',
            //campos no requeridos
            'B' => 'nullable|string|exists:t_empresa_grupo,id',
            'I' => 'nullable|exists:t_tipo_especialidad,idespecialidad',
            'J' => 'nullable|exists:t_especialidades,id',
            'K' => 'nullable|string|exists:t_sub_especialidad,id',
            'N' => 'nullable|exists:t_clase_personas,id',
            'O' => 'nullable|exists:t_ranking_clientes,id',
            'Z' => 'nullable|exists:t_frecuencia_visitas,idfrecuencia',
            'AN'=> 'nullable'
        );

        $cliArr = array(
            'B' => 'nullable|exists:t_empresa_grupo,id',
            'I' => 'required|exists:t_tipo_especialidad,idespecialidad',
            'J' => 'required|exists:t_especialidades,id',
            'K' => 'required|exists:t_sub_especialidad,id',
            'O' => 'required|exists:t_ranking_clientes,id',
            'Z' => 'required|exists:t_frecuencia_visitas,idfrecuencia',
            //campos no requeridos
            'C' => 'exists:t_personas,idPersona|nullable',
            'S' => 'string|max:1|nullable',
            'Y' => 'integer|exists:t_ciclos,id|nullable',
            'AN' => 'numeric|max:100|nullable'
        );

        $mayArr = array(
            'B' => 'nullable|exists:t_empresa_grupo,id',
            'I' => 'required|exists:t_tipo_especialidad,idespecialidad',
            'J' => 'required|exists:t_especialidades,id',
            'K' => 'required|exists:t_sub_especialidad,id',
            //campos no requeridos
            'N' => 'nullable|exists:t_clase_personas,id',
            'O' => 'exists:t_ranking_clientes,id|nullable',
            'Z' => 'exists:t_frecuencia_visitas,idfrecuencia|nullable',
            'C' => 'exists:t_personas,idPersona|nullable',
            'S' => 'string|max:1|nullable',
            'Y' => 'exists:t_ciclos,id|nullable',
            'AN' => 'numeric|max:100|nullable'
        );

        foreach ($data as $row => $record) {
            switch($record['M'])
            {
                case 'MAY':
                    $val = array_merge($valArray,$mayArr);
                    break;

                case 'RFV':
                    $val = array_merge($valArray,$rfvArr);
                    break;

                case 'CLI':
                    $val = array_merge($valArray,$cliArr);
                    break;
            }

            $validate = Validator::make($record, $val);

            if($validate->fails())
            {
                array_push($errores,['error'=>'Error en registro '.$row,'details'=>$validate->errors()->all()]);
            }else
            {
                $find = TPersona::withoutGlobalScopes()->where('idOperador',Auth::user()->idOperador)
                    ->where('idFabricante',$request->id)
                    ->where('idPersona',$record['A'])->first();
                if($find)
                {
                    array_push($errores,['error'=>'Error en registro '.$row,'details'=>['Registro ya existente']]);
                }else{
                    if($record['M']=='CLI' || $record['M']=='MAY'){
                        $perfil=null;
                    }else{
                        $perfil=TPerfile::where('alias',$record['M'])->first()->id;
                    }

                      $validos [] = [
                        'idOperador'=>Auth::user()->idOperador,
                        'idFabricante'=>$request->id,
                        'idPersona'=>$record['A'],
                        'idempresa_Grupo'=>$record['B'],
                        'idsupervisor'=>$record['C'],
                        'idcadenas'=>$record['D'],
                        'idpais'=>$record['E'],
                        'ididioma'=>$record['F'],
                        'idmoneda'=>$record['G'],
                        'cod_tipo_persona'=>$record['H'],
                        'idespecialidad'=>$record['I'],
                        'idperfil'=>$perfil,
                        'idactividad_negocio'=>$record['J'],
                        'idsubespecialidad'=>$record['K'],
                        'documento_identidad'=>$record['L'],
                        'idgrupo_persona'=>$record['M'],
                        'idclase_persona'=>$record['N'],
                        'idranking'=>$record['O'],
                        'nombre_persona'=>$record['P'],
                        'apellido_persona'=>$record['Q'],
                        'nombre_completo_razon_social'=>$record['R'],
                        'sexo_genero_persona'=>$record['S'],
                        'fecha_nacimiento_registro'=>$record['T'],
                        'telefono_persona'=>$record['U'],
                        'movil_persona'=>$record['V'],
                        'email_persona'=>$record['W'],
                        'idtitulo'=>$record['X'],
                        'idciclos'=>$record['Y'],
                        'idfrecuencia'=>$record['Z'],
                        'direccion_domicilio'=>$record['AA'],
                        'idestado'=>$record['AB'],
                        'idciudad'=>$record['AC'],
                        'zona_postal'=>$record['AD'],
                        'banco_persona'=>$record['AE'],
                        'cuenta_principal_persona'=>$record['AF'],
                        'banco_persona_internacional'=>$record['AG'],
                        'cuenta_internacional_persona'=>$record['AH'],
                        'coordenadas_l'=>$record['AI'],
                        'coordenadas_a'=>$record['AJ'],
                        'persona_contacto'=>$record['AK'],
                        'telefono_contacto'=>$record['AL'],
                        'email_contacto'=>$record['AM'],
                        'descuento'=>(float)$record['AN'],
                        'idestatus'=>1,
                    ];
                }

            }
        }

        TPersona::insert($validos);
        if($errores)
            return response()->json(['error'=>$errores,'status'=>400]);
        else
            return response()->json(['error'=>$errores,'status'=>201]);
    }

    public function exportClienteDiasExcel(Request $request)
    {
        $validator = Validator::make($request->all(),
            [
                'id' => 'required|exists:t_personas,idFabricante'
            ]
        );

        if($validator->fails()){
            return response()->json($validator->errors()->all(), 400);
        }
        return (new ClienteDiasFormato($request->id))->download('ClienteDiasFormato.xlsx')->deleteFileAfterSend(true);
    }

    public function LoadClienteDisponibleExcel(Request $request)
    {
        $validator = Validator::make($request->all(),
            [
                'ClienteDiasFormato' => 'required|mimes:xlsx|max:1024',
                'id' => 'required|exists:t_personas,idPersona'
            ]
        );
        if($validator->fails()){
            return response()->json($validator->errors()->all(), 400);
        }

        $file = $request->file('ClienteDiasFormato');
        $path = $file->getRealPath();

        $filter = new ClienteDiasRead();
        $reader= IOFactory::createReaderForFile($path);
        $reader->setReadFilter($filter);

        $excel = $reader->load($path);

        $hoja= $excel->getSheetByName('DiasDisponibles');

        $cantrow = $hoja->getCellCollection()->getHighestRow();
        $data =  $hoja->rangeToArray(
            'A2:B'.$cantrow,
            NULL,
            TRUE,
            TRUE,
            TRUE
        );

        $errores = [];
        foreach ($data as $row => $record) {
            $v = Validator::make($record,
                [
                    'A' => 'exists:t_personas,idPersona',
                    'B' => 'exists:t_dias_visitas,iddias_visita',
                ]
            );
            if($v->fails()){
                array_push($errores,['error'=>'Error en registro '.$row,'details'=>$v->errors()->all()]);
            }else
            {
                $find = DB::table('t_persona_dias')
                    ->where('idOperador',Auth::user()->idOperador)
                    ->where('idFabricante',$request->id)
                    ->where('idPersona',$record['A'])
                    ->where('iddias_visita',$record['B'])
                    ->first();

                if($find)
                {
                    array_push($errores,['error'=>'Error en registro '.$row,'details'=>['Registro ya existente']]);
                }else{
                    DB::table('t_persona_dias')
                        ->insert([
                            'idOperador' => Auth::user()->idOperador,
                            'idFabricante' => $request->id,
                            'idPersona' => $record['A'],
                            'iddias_visita' => $record['B']
                        ]);
                }
            }

        }

        if($errores)
            return response()->json(['error'=>$errores,'status'=>400]);
        else
            return response()->json(['error'=>$errores,'status'=>201]);
    }



    public function listaNegra(Request $request){
       /* $Empresa=$request->Fabrica;
        $grupopersona=$request->tipo;
        $DescripGrupopersona = kTGrupopersona::find($grupopersona)->descripciongrupoPersona;
        $tipopersona = TTipoPersona::all();
        $paises = TPaise::all();

        //Datos Cliente
       // $especialidad=TTipoEspecialidade::where('idFabricante',$Empresa)->get();//
         $especialidad=TEspecialidade::all();
        $clase = TClasePersona::where('estatus',1)->get();
        $ranking = TRankingCliente::withoutGlobalScopes()->where('idFabricante',$Empresa)->get();
        $freq = TFrecuenciaVisita::withoutGlobalScopes()->where('idFabricante',$Empresa)->get();

        //Datos RFV y SUP
        if($grupopersona=='RFV' || $grupopersona=='SUP'){
            $freq = TCiclo::all();
            $ranking=TPersona::where('idgrupo_persona','SUP')->get(['idPersona','nombre_completo_razon_social']);
        }
        $accion='Lista Negra';
        //dd($Empresa,$grupopersona,$DescripGrupopersona,$tipopersona,$paises,$especialidad,$clase,$ranking,$freq);
        //return view('Admin.usuarios.ListaPersonas',compact('grupopersona','DescripGrupopersona','Empresa','listaPersona'));
        return view('Admin.prueba',compact('accion','grupopersona','DescripGrupopersona','Empresa','paises','tipopersona','especialidad','clase','ranking','freq'));
        */
        $Empresa=TPersona::withoutGlobalScopes()
        ->where('idOperador',$this->user->idOperador)
        ->where('idgrupo_persona','FABR')
        ->select(['idOperador','idPersona'])
        ->get();
        $T='ListaNegra';
             return view('Admin.usuarios.Listanegra',compact('Empresa', 'T'));
    }
}
