<?php

namespace RTR\Http\Controllers\Admin\Persona;

use PhpOffice\PhpSpreadsheet\IOFactory;
use Illuminate\Support\Facades\DB;
use RTR\Filterimport\CliRfvRead;
use RTR\ExcelExport\ClientRFVFormato;
use Illuminate\Http\Request;
use RTR\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use RTR\modelos\TPersona;
use Validator;

class ClienteRFV extends Controller
{
    // 

    public function __construct()
    {
        $this->middleware(function ($request, $next) {
            $this->user = Auth::user();
            return $next($request);
        });
    }

    public function exportExcel(Request $request)
    {
        $validator = Validator::make($request->all(),
            [
                'id' => 'required|exists:t_personas,idFabricante'
            ]
        );

        if($validator->fails()){
            return response()->json($validator->errors()->all(), 400);
        }
        return (new ClientRFVFormato($request->id))->download('CliRfvformato.xlsx')->deleteFileAfterSend(true);
    }

    public function LoadExcel(Request $request){
        $validator = Validator::make($request->all(),
        [
            'FormatoCliRfv' => 'required|mimes:xlsx|max:1024',
            'id' => 'required|exists:t_personas,idPersona'
        ]
            );
        if($validator->fails()){
            return response()->json($validator->errors()->all(), 400);
        }

        $file = $request->file('FormatoCliRfv');
        //dd($file->getRealPath());
        $path = $file->getRealPath();

        $filter = new CliRfvRead();
        $reader= IOFactory::createReaderForFile($path);
        $reader->setReadFilter($filter);

        $excel = $reader->load($path);

        $hoja= $excel->getSheetByName('Cliente_RFV');

        $cantrow = $hoja->getCellCollection()->getHighestRow();
        $data =  $hoja->rangeToArray(
            'A2:B'.$cantrow,
            NULL,
            TRUE,
            TRUE,
            TRUE
        );
        //dd($cantrow);
        $errores = [];
        //dd($data);
        foreach ($data as $row => $record) {
            $v = Validator::make($record,
            [
                'A' => 'required|exists:t_personas,idPersona',
                'B' => 'required|exists:t_personas,idPersona'
            ]
            );
            if($v->fails()){
                array_push($errores,['error'=>'Error en registro '.$row,'details'=>$v->errors()->all()]);
            }else
            {
                $find = DB::table('r_cliente_rfv')->where('idOperador',Auth::user()->idOperador)
                    ->where('idFabricante',$request->id)
                    ->where('id_cliente',$record['A'])
                    ->where('id_RFV',$record['B'])->first();
                if($find)
                {
                    array_push($errores,['error'=>'Error en registro '.$row,'details'=>['Registro ya existente']]);
                }else{
                    $nuevo=TPersona::withoutGlobalScopes()->find($record['A']);
                    //dd($nuevo);
                    DB::table('r_cliente_rfv')->insert([
                        'idOperador' => Auth::user()->idOperador,
                        'idFabricante' => $request->id,
                        'id_cliente' => $record['A'],
                        'id_RFV' => $record['B'],
                        'idprofesion' => $nuevo->idespecialidad,
                        'idespecialidad' =>$nuevo->idactividad_negocio,
                        'idsubespecialidad' => $nuevo->idsubespecialidad
                    ]);
                }

            }

        }
        if($errores)
            return response()->json(['error'=>$errores,'status'=>400]);
        else
            return response()->json(['error'=>$errores,'status'=>201]);
    }

}