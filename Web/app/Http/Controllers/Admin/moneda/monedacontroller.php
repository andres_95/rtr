<?php
/**
 * Created by PhpStorm.
 * User: usuario
 * Date: 25/04/2018
 * Time: 8:15
 */

namespace RTR\Http\Controllers\Admin\moneda;

use Illuminate\Support\Facades\Auth;
use RTR\Http\Controllers\Controller;
use RTR\modelos\TMoneda;
use RTR\modelos\TPersona;
use Illuminate\Http\Request;
use Validator;

class monedacontroller extends Controller
{
    public function __construct()
    {
        $this->middleware(function ($request, $next) {
            $this->user = Auth::user();
            return $next($request);
        });
    }

    public function index(Request $request){
        $Empresa=TPersona::withoutGlobalScopes()
            ->where('idOperador',$this->user->idOperador)
            ->where('idgrupo_persona','FABR')
            ->select(['nombre_completo_razon_social','idPersona'])
            ->get();
        $T="Moneda";
        $fabrica = $request->session()->get('user.teams');
        return view('Admin.estadistico.moneda',compact('Empresa','T','fabrica'));
    }

    public function show(Request $request){
        $moneda=TMoneda::withoutGlobalScopes()
            ->where('idFabricante',$request->fabricante)
            ->where('idestatus',1)->get();

        return response($moneda,200);
    }

    public function store(Request $request){
            $validator=Validator::make(
                $request->all(),
                TMoneda::$validators
            );
              if($validator->fails())
                 return response()->json($validator->errors()->all(),404);
        $moneda=TMoneda::create([
            'idOperador'=>$this->user->idOperador,
            'idFabricante'=>$request->fabricante,
            'idmoneda'=>$request->idmoneda,
            'codmoneda'=>$request->codmoneda,
            'nombreMoneda'=>$request->nombremoneda,
            'moneda_simbolo'=>$request->moneda_simbolo,
        ]);

      /*  $moneda=TMoneda::withoutGlobalScopes()
            ->where('idFabricante',$request->fabricante)
            ->where('idestatus',1)->get();*/
          
        return response()->json(['success'=>'moneda almacenada','id'=>$moneda['idmoneda']],200);

    }

    public function update(Request $request){
        $validator=Validator::make(
            $request->all(),array_merge(TMoneda::$validators,[
            
            'nombremoneda'=>'required|exists:t_monedas,nombreMoneda',
            'moneda_simbolo'=>'required|exists:t_monedas,moneda_simbolo'])
        );
          if($validator->fails())
             return response()->json($validator->errors()->all(),404);


        $moneda=TMoneda::withoutGlobalScopes()->where('idmoneda',$request->idmoneda);
         
        $moneda->update([
            'nombreMoneda'=>$request->nombremoneda,
            'moneda_simbolo'=>$request->moneda_simbolo
            ]);

        $moneda=TMoneda::withoutGlobalScopes()
            ->where('idFabricante',$request->fabricante)
            ->where('idestatus',1)->get();

        return response($moneda,200);

    }

    public function destroy(Request $request){
       //   dd($request->idmoneda);

       $validator=Validator::make(
        $request->all(),[
         'id'=>'required|exists:t_monedas,idmoneda']
    );
      if($validator->fails())
         return response()->json($validator->errors()->all(),404);

        $moneda=TMoneda::withoutGlobalScopes()->where('idmoneda',$request->id)->where('idestatus',1);
         // dd($moneda->count());
        if(!$moneda->update([
            'idestatus'=>0
        ]))
           return response()->json(['error'=>'moneda no encontrada'],404);

        $moneda=TMoneda::withoutGlobalScopes()
            ->where('idFabricante',$request->fabricante)
            ->where('idestatus',1)->get();

        return response($moneda,200);

    }
}