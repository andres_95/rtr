<?php

namespace RTR\Http\Controllers\Admin\esquemas;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use RTR\modelos\TPersona;
use RTR\modelos\TProducto;
use RTR\modelos\TEsquemaPromocionale;
use RTR\Http\Resources\esquemaRecurso;

use Validator;

use RTR\Http\Controllers\Controller;

class esquemacontroller extends Controller
{

    public function __construct()
    {
        $this->middleware(function ($request, $next) {
            $this->user = Auth::user();
            return $next($request);
        });
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $Empresa=TPersona::withoutGlobalScopes()
            ->where('idOperador',$this->user->idOperador)
            ->where('idgrupo_persona','FABR')
            ->select(['nombre_completo_razon_social','idPersona'])
            ->get();
        $mayoristas=TPersona::withoutGlobalScopes()
                ->where('idOperador',$this->user->idOperador)
                ->where('idgrupo_persona','MAY')
                ->get();
        $fabrica = $request->session()->get('user.teams');
        return view('Admin.productos.esquema',compact('Empresa','mayoristas','fabrica'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
      //  dd($request->all());
         $validator=Validator::make($request->all(),TEsquemaPromocionale::$validators);
             if($validator->fails())
                return response()->json($validator->errors()->all(),404);
           
        TEsquemaPromocionale::create([
            'idOperador'=>$this->user->idOperador,
            'idFabricante'=>$request->fabricante, //no quiere aceptar este campo
            'idMayorista'=>$request->mayorista,
            'idesqprom'=>$request->id,
            'idProducto'=>$request->idproducto,
            'cantidad_desde'=>$request->cantD,
            'cantidad_hasta'=>$request->cantH,
            'monto_desde'=>$request->montoD,
            'monto_hasta'=>$request->montoH,
            'porcentaje'=>$request->porcentaje,
            'cantidad'=>$request->cant,
            'Alta_Baja_Update'=>'A',
            'idestatus'=>1
        ]);

        $esquema = esquemaRecurso::collection(TEsquemaPromocionale::withoutGlobalScopes()->where('idFabricante',$request->fabricante)->get());
        return response(['esquemas'=>$esquema],200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
       //  $persona=TPersona::where('idPersona',$request->idRFV)->where('idestatus',1);
         $producto=TProducto::where('idProducto',$request->idProducto)->where('estatus_producto',1);

              if($producto->count()==0)
                 return response()->json(['error'=>'persona no encontrada'],404);
        $data=$request->except('idProducto');
        $data['idestatus']=1;
        $data['idOperador']=$this->user->idOperador;
        
        $producto->first()->promocionar($this->user,$data);

           return response()->json(['success'=>'se ha creado una promocion para este producto'],200);
           
            
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request)
    {
        $productos = TProducto::withoutGlobalScopes()->where('idFabricante',$request->fabricante)->where('idcategorias','!=','MUES')->get();
        $esquema = esquemaRecurso::collection(TEsquemaPromocionale::withoutGlobalScopes()->where('idFabricante',$request->fabricante)->get());
        return response(['esquemas'=>$esquema,'productos'=>$productos]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        //
        $validator=Validator::make($request->all(),[
            'idproducto'=>TEsquemaPromocionale::$validators['idproducto'],
            'cantidad_desde'=>TEsquemaPromocionale::$validators['cantD'],
            'cantidad_hasta'=>TEsquemaPromocionale::$validators['cantH'],
            'monto_desde'=>TEsquemaPromocionale::$validators['montoD'],
            'monto_hasta'=>TEsquemaPromocionale::$validators['montoH'],
            'porcentaje'=>TEsquemaPromocionale::$validators['porcentaje'],
            'cantidad'=>TEsquemaPromocionale::$validators['cant']
        ]);

           if($validator->fails())
                return response()->json($validator->errors()->all(),404);

         $esquema=TEsquemaPromocionale::where('idesqprom',$request->id)->where('idestatus',1);

            if(!$esquema->first()->update([
                'idProducto'=>$request->idproducto,
                'cantidad_desde'=>$request->cantD,
                'cantidad_hasta'=>$request->cantH,
                'monto_desde'=>$request->montoD,
                'monto_hasta'=>$request->montoH,
                'porcentaje'=>$request->porcentaje,
                'cantidad'=>$request->cant
            ]))
               return response()->json(['error'=>'esquema promocional no encontrado o no actualizado']);

        return response()->json(['success'=>'se ha hecho una actualizacion al promocion'],200);

    }

      

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request,$id)
    {
        //
        $validator=Validator::make($request->all(),[
            'id'=>'required|exists:t_esquemas_promocionales,idesqprom'
        ]);
             if($validator->fails())
                return response()->json($validator->errors()->all(),404);

        $producto=TProducto::where('idProducto',$id)->where('estatus_producto',1);
        
                      if($producto->count()==0)
                         return response()->json(['error'=>'producto no encontrada'],404);

       $data=$request->except('id');
       
       $producto->first()->cancelarPromocion($request->id);

        return response()->json(['success'=>'promocion cancelada'],200);
    }

    public function exportExcel(Request $request)
    {
        $validator = Validator::make($request->all(),
            [
                'id' => 'required|exists:t_personas,idFabricante'
            ]
        );

        if($validator->fails()){
            return response()->json($validator->errors()->all(), 400);
        }
        return (new EsquemaFormato($request->id))->download('Esquemaformato.xlsx');
    }

}
