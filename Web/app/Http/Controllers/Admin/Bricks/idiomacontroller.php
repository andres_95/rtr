<?php

namespace RTR\Http\Controllers\Admin\Bricks;

use Illuminate\Http\Request;
use RTR\modelos\TIdioma;
use RTR\Http\Controllers\Controller;
use RTR\Http\Resources\SinFABR;
use Validator;
class idiomacontroller extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
         return response()->json(['idiomas'=>TIdioma::where('idestatus',1)->get()],200);
    }

    public function index2()
    {
        $idioma=TIdioma::where('idestatus','=',1)->get();
        foreach ($idioma as $item)
            $item->tipo='i';

        $T="Idioma";
        $valor=SinFABR::collection($idioma)->toJson();
        return view('Admin.estadistico.SinFABR',compact('valor','T'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $validator=Validator::make($request->all(),TIdioma::$validators);
           if($validator->fails())
              return response()->json($validator->errors()->all(),404);


        TIdioma::create([
            'ididioma'=>$request->id,
            'descripcion'=>$request->descripcion,
            'idestatus'=>1
        ]);
        return response()->json(['success'=>'idioma creado'],200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
           return response()->json((new TIdioma())->findItem($id),200);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
         $validator=Validator::make($request->all(),
          array_merge(TIdioma::$validators,
           ['id'=>'required|max:3|exists:t_idiomas,ididioma']
        ));
           if($validator->fails())
              return response()->json($validator->errors()->all(),404);
        
           if(! TIdioma::where('ididioma',$request->id)->update([
            'ididioma'=>$request->id,
            'descripcion'=>$request->descripcion,
            'idestatus'=>1
            ]))
              return response()->json(['error'=>'idioma no existente'],404);

              return response()->json(['success'=>'idioma modificado'],200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        //
        $validator=Validator::make($request->all(),
        ['id'=>'required|max:3|exists:t_idiomas,ididioma']);
        if($validator->fails())
           return response()->json($validator->errors()->all(),404);
        $idioma=new TIdioma();
        if(!$idioma->delItem($request->id))
           return response()->json(['error'=>'idioma no existente'],404);
        
           return response()->json(['success'=>'idioma eliminado'],200);
    }
}
