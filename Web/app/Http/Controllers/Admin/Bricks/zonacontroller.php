<?php

namespace RTR\Http\Controllers\Admin\Bricks;
     

    use PhpOffice\PhpSpreadsheet\IOFactory;
    use RTR\ExcelExport\ZonaFornato;
    use RTR\Filterimport\ZonaRead;
    use RTR\modelos\TPersona;
    use Validator;
    use Illuminate\Http\Request;
    use RTR\Http\Controllers\Controller;
    use Illuminate\Support\Facades\Auth;
    use RTR\modelos\TZona;
    use RTR\modelos\TCiudade;
    use RTR\modelos\TPaise;
    use RTR\modelos\TBrickRuta;
    use RTR\Http\Resources\CRFRecurso;

     class zonacontroller extends Controller{

            private $user;
            public function __construct(){
                $this->middleware(function ($request, $next) {
                    $this->user = Auth::user();
                    return $next($request);
                });
            }

            public function index(Request $request){
               $zona=TZona::withoutGlobalScopes()->where('idFabricante',$request->fabricante)
                            ->where('idestatus',1)->get();
                                                  

                    if($zona->count()==0)
                        return response()->json(['error'=>'No hay zonas'],404);
                 foreach($zona as $zon){
                      $zon->tipo='z';
                 }
                    
                   return response(CRFRecurso::collection($zona),200);

            }

            public function index2(Request $request){
                $Empresa=TPersona::withoutGlobalScopes()
                        ->where('idOperador',$this->user->idOperador)
                        ->where('idgrupo_persona','FABR')
                        ->select(['nombre_completo_razon_social','idPersona'])
                        ->get();
                  $T="Zonas";
                $pais=TPaise::all();
                $fabrica = $request->session()->get('user.teams');
                return view('Admin.estadistico.CiRaFr',compact('Empresa','T','pais','fabrica'));
            }

            public function store(Request $request){
              //   $id=0; do{ $id=rand(); }while(TZona::find($id));
           //  dd($request->all());
              $validator=Validator::make($request->all(),TZona::$validators);
                 if($validator->fails())
                    return response()->json($validator->errors()->all(),404);

                $zona=TZona::create([
                    'idOperador'=>$this->user->idOperador,
                    'idFabricante'=>$request->fabricante,
                    'idzona'=>$request->id,
                    'idestado'=>$request->idestado,
                    'idpais'=>$request->idpais,
                    'idciudad'=>$request->idciudad,
                    'descripcion_zona'=>$request->descripcion,
                    'idestatus'=>'1'
                ]);
                 // dd($ciudad);
             /*   $ciudad->first()
                ->zonas()->associate([
                    'idOperador'=>$this->user->idOperador,
                    'idzona'=>$request->idzona,
                    'idestado'=>$ciudad->first()->idestado,
                    'idpais'=>$ciudad->first()->idpais,
                    'descripcion_zona'=>$request->descripcion,
                    'idestatus'=>'1'
                ]);*/

                    return response()->json(['success'=>'zona agregada','id'=>$zona['id']],200);

            }

            public function update(Request $request){
              
                $validator=Validator::make($request->all(),array(
                    'id'=>'required|exists:t_zonas,idzona',
                    'descripcion'=>TZona::$validators['descripcion'],
                    'idpais'=>TZona::$validators['idpais'],
                    'idciudad'=>TZona::$validators['idciudad'],
                    'idestado'=>TZona::$validators['idestado']
                  ));
                if($validator->fails())
                   return response()->json($validator->errors()->all(),404);


                  $zona=TZona::withoutGlobalScopes()
                  ->where('idzona',$request->id)
                  ->where('idestatus',1);
                  
                      //dd($request->all());
                  if(!$zona->update([
                     'idestado'=>$request->idestado,
                     'idpais'=>$request->idpais,
                     'idCiudad'=>$request->idciudad,
                     'descripcion_zona'=>$request->descripcion
                  ]))
                      return response()->json(['error'=>'zona no encontrada'],404);

                  return response()->json(['success'=>'se ha corregido los datos de la zona'],200);

            }

            public function destroy(Request $request){
                  $zona=TZona::withoutGlobalScopes()->where('idzona','=',$request->id)->where('idestatus','=','1');
                  
                       if($zona->count()==0)
                               return response()->json(['error'=>'zona no encontrada'],404);
                  
                  $zona->update([
                       'idestatus'=>'0'
                    ]);
                  
                         return response()->json(['success'=>'se ha eliminado los datos de la zona'],200);
            }

            public function addRuta(Request $request){
                //  do{ $id=rand()%1000; }while(TBrickRuta::find($id)!=null);
                  
                           /*   $brick=TBrickRuta::create([
                                   'idbrick'=>$id,
                                   'Descripcion'=>$request->descripcion,
                                   'estatus'=>'1'
                               ]);*/
                       $zona=TZona::find($request->id);
                         if($zona==null)
                            return response()->json(['error'=>'zona no existente'],404);
                 
                       $zona->rutas()->create([
                              'idbrick'=>$request->id,
                              'Descripcion'=>$request->descripcion,
                              'idestatus'=>'1'
                       ]);
                         
                      
                        
                      //   return $brick;
                    /*    $brick->zona()->associate($zona);
                        $brick->save();*/
                      //  return response()->json(['result'=>$brick->zona()->get()]);
                  /* $brick->zona()->create([
                        

                   ]);
                   $brick->save();*/

                  return response()->json(['success'=>'se ha creado una ruta'],200);
            }
   
            public function updRuta(Request $request){

                  $ruta=TBrickRuta::where('idbrick','=',$request->id)->where('idestatus','=','1');

                      if($ruta->count()==0)
                        return response()->json(['error'=>'ruta no encontrada'],404);
                
                   $ruta->update([
                     'Descripcion'=>$request->descripcion
                   ]);

                   return response()->json(['success'=>'ruta actualizada'],200);
                  
            }
         public function delRuta(Request $request){
            $ruta=TBrickRuta::where('idbrick','=',$request->id)->where('idestatus','=','1');
            
                                  if($ruta->count()==0)
                                    return response()->json(['error'=>'ruta no encontrada'],404);
                            
                               $ruta->update([
                                 'idestatus'=>'0'
                               ]);
            
                return response()->json(['success'=>'ruta eliminada'],200);
                              

         }

         public function exportExcel(Request $request)
         {
             $validator = Validator::make($request->all(),
                 [
                     'id' => 'required|exists:t_personas,idFabricante'
                 ]
             );

             if($validator->fails()){
                 return response()->json($validator->errors()->all(), 400);
             }
             return (new ZonaFornato($request->id))->download('zona.xlsx')->deleteFileAfterSend(true);
         }


         public function LoadExcel(Request $request)
         {
             $validator = Validator::make($request->all(),
                [
                    'FormatoZona' => 'required|mimes:xlsx|max:1024',
                    'id' => 'required|exists:t_personas,idPersona'
                ]
                 );
             if($validator->fails()){
                 return response()->json($validator->errors()->all(), 400);
             }

             $file = $request->file('FormatoZona');
             $path = $file->getRealPath();

             $filter = new ZonaRead();
             $reader= IOFactory::createReaderForFile($path);
             $reader->setReadFilter($filter);

             $excel = $reader->load($path);

             $hoja= $excel->getSheetByName('zonas');

             $cantrow = $hoja->getCellCollection()->getHighestRow();
             $data =  $hoja->rangeToArray(
                 'A2:C'.$cantrow,
                 NULL,
                 TRUE,
                 TRUE,
                 TRUE
             );

             $errores = [];
             foreach ($data as $row => $record) {
                 $v = Validator::make($record,
                 [
                     'A' => 'required|string|max:40',
                     'B' => 'required|integer|exists:t_estados,idestado',
                     'C' => 'required|integer|exists:t_ciudades,idCiudad'
                 ]
                 );
                 if($v->fails()){
                     array_push($errores,['error'=>'Error en registro '.$row,'details'=>$v->errors()->all()]);
                 }else
                 {
                     $find = TZona::withoutGlobalScopes()->where('idOperador',Auth::user()->idOperador)
                         ->where('idFabricante',$request->id)
                         ->where('descripcion_zona',$record['A'])->first();
                     if($find)
                     {
                         array_push($errores,['error'=>'Error en registro '.$row,'details'=>['Registro ya existente']]);
                     }else{
                         TZona::create([
                             'idOperador' => Auth::user()->idOperador,
                             'idFabricante' => $request->id,
                             'descripcion_zona' => $record['A'],
                             'idciudad' => $record['C'],
                             'idestado' => $record['B'],
                             'idpais' => TPersona::withoutGlobalScopes()->find($request->id)->idpais
                         ]);
                     }

                 }

             }

             if($errores)
                 return response()->json(['error'=>$errores,'status'=>400]);
             else
                 return response()->json(['error'=>$errores,'status'=>201]);
         }
           
     }