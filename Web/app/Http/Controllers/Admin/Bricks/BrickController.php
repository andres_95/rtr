<?php

namespace RTR\Http\Controllers\Admin\Bricks;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use PhpOffice\PhpSpreadsheet\IOFactory;
use RTR\ExcelExport\BrickFormato;
use RTR\Http\Controllers\Controller;
use RTR\modelos\TBrickRuta;
use RTR\Filterimport\RutaRead;
use Validator;

class BrickController extends Controller
{
    //
    public function exportExcel(Request $request)
    {
        $validator = Validator::make($request->all(),
            [
                'id' => 'required|exists:t_personas,idFabricante'
            ]
            );

        if($validator->fails()){
            return response()->json($validator->errors()->all(), 400);
        }

        return (new BrickFormato($request->id))->download('Formato_ruta.xlsx')->deleteFileAfterSend(true);
    }

    public function LoadExcel(Request $request){
        $validator = Validator::make($request->all(),
            [
                'FormatoRuta' => 'required|mimes:xlsx|max:1024',
                'id' => 'required|exists:t_personas,idPersona'
            ]
        );
        if($validator->fails()){
            return response()->json($validator->errors()->all(), 400);
        }

        $file = $request->file('FormatoRuta');
        $path = $file->getRealPath();

        $filter = new RutaRead();
        $reader= IOFactory::createReaderForFile($path);
        $reader->setReadFilter($filter);

        $excel = $reader->load($path);

        $hoja= $excel->getSheetByName('Rutas');

        $cantrow = $hoja->getCellCollection()->getHighestRow();
        $data =  $hoja->rangeToArray(
            'A2:C'.$cantrow,
            NULL,
            TRUE,
            TRUE,
            TRUE
        );

        $errores = [];
        foreach ($data as $row => $record) {
            $v = Validator::make($record,
                [
                    'A' => 'required|string|max:10',
                    'B' => 'required|string|max:11',
                    'C' => 'required|integer|exists:t_zonas,idzona'
                ]
            );
            if($v->fails()){
                array_push($errores,['error'=>'Error en registro '.$row,'details'=>$v->errors()->all()]);
            }else
            {
                $find = TBrickRuta::withoutGlobalScopes()->where('idOperador',Auth::user()->idOperador)
                    ->where('idFabricante',$request->id)
                    ->where('idbrick',$record['A'])
                    ->where('Descripcion',$record['B'])
                    ->where('idzona',$record['C'])
                    ->first();
                if($find)
                {
                    array_push($errores,['error'=>'Error en registro '.$row,'details'=>['Registro ya existente']]);
                }else{
                    TBrickRuta::create([
                        'idOperador' => Auth::user()->idOperador,
                        'idFabricante' => $request->id,
                        'idbrick' => $record['A'],
                        'Descripcion' => $record['B'],
                        'idzona' => $record['C'],
                    ]);
                }

            }

        }

        if($errores)
            return response()->json(['error'=>$errores,'status'=>400]);
        else
            return response()->json(['error'=>$errores,'status'=>201]);
    }
}
