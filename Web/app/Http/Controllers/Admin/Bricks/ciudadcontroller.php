<?php

namespace RTR\Http\Controllers\Admin\Bricks;

use Illuminate\Http\Request;
use RTR\modelos\TCiudade;
use RTR\modelos\TPaise;
use RTR\modelos\TEstado;
use Illuminate\Support\Facades\Auth;
use RTR\Http\Controllers\Controller;

class ciudadcontroller extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $ciudad=TCiudade::where('idestatus','=',1)->get();
        foreach ($ciudad as $item)
            $item->tipo='c';

        $T="Paises";
        $valor=SinFABR::collection($ciudad)->toJson();
        return view('Admin.estadistico.SinFABR',compact('valor','T'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
         $count=TCiudade::all()->count()+2;
         //  return response($request->idpais,200);
         TCiudade::create([
             'idCiudad'=>$count,
             'idpais'=>$request->idpais,
             'idestado'=>$request->idestado,
             'nombreCiudad'=>$request->ciudad
         ]);

            return response()->json(['success'=>'ciudad agregada'],200);
         
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
         $ciudad=TCiudade::where('idCiudad','=',$id)
                    ->where('idestado','=',$request->idestado)
                    ->where('idpais','=',$request->idpais);

                    if($ciudad->count()==0)
                        return response()->json(['error'=>'no existe informacion asociada a esta ciudad'],404);

            $ciudad->update([
                'nombreCiudad'=>$request->ciudad
            ]);

                return response()->json(['success'=>'ciudad actualizada'],200);
                       
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @param \Illuminate\Http\Request
     * @return \Illuminate\Http\Response $response
     */
    public function destroy(Request $request,$id)
    {
        //
        $ciudad=TCiudade::where('idCiudad','=',$id)
        ->where('idestado','=',$request->idestado)
        ->where('idpais','=',$request->idpais)
        ->where('idestatus','=',1);

        if($ciudad->count()==0)
            return response()->json(['error'=>'no existe informacion asociada a esta ciudad'],404);

         $ciudad->update([
             'idestatus'=>0
         ]);
             return response()->json(['success'=>'ciudad eliminada'],200);
    }
}
