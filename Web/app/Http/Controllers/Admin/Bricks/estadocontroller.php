<?php

namespace RTR\Http\Controllers\Admin\Bricks;

use Illuminate\Http\Request;
use RTR\modelos\TPaise as Pais;
use RTR\modelos\TEstado;
use Illuminate\Support\Facades\Auth;
use RTR\Http\Controllers\Controller;
use Validator;


class estadocontroller extends Controller
{

    public function __construct()
    {
        $this->middleware(function ($request, $next) {
            $this->user = Auth::user();
            return $next($request);
        });
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $estado=TEstado::all();
        foreach ($estado as $item)
            $item->tipo='i';

        $T="Estados";
        $valor=SinFABR::collection($estado)->toJson();
        return view('Admin.estadistico.SinFABR',compact('valor','T'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * 
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
           $validator=Validator::make($request->all(),[
               'idpais'=>'required|exists:t_paises,idpais'
           ]);
              if($validator->fails())
                  return response()->json(['errors'=>$validator->errors()->all()],404);

         $pais=Pais::where('idpais','=',$request->idpais)
                       ->where('idestatus','=',1);

                if($pais->count()==0)
                   return response()->json(['error'=>'pais no encontrado'],404);

        $id=TEstado::all()->count()+1;
        $estado=$pais->first()->estados()->create(
            [
               'idestado'=>$id,
               'nombreCiudad'=>$request->estado,
               'idestatus'=>1 
            ]
        );
         
            return response()->json(['sucess'=>'estado agregado'],200);
    }
  
     
    
  

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
          $estado=Pais::where('idpais','=',$request->idpais)
                 ->where('idestatus','=',1)->first()
                 ->estados()
                 ->where('idestado','=',$id)  
                 ->where('idestatus','=',1);
                
                if($estado->count()==0)
                   return response()->json(['
                   error'=>'estado no encontrado'],404);
            
          $estado->update([
              'nombreCiudad'=>$request->estado
          ]);

            return response()->json(['success'=>'estado actualizado'],200);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request,$id)
    {
        //
        $estado=TEstado::where('idestado','=',$id)
                     ->where('idpais','=',$request->idpais)
                     ->where('idestatus','=',1);
                    
                if($estado->count()==0)
                   return response()->json(['error'=>'estado no encontrado'],404);

          $estado->update(['idestatus'=>0]);

             return response()->json(['success'=>'estado eliminado'],200);
    }

     /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id){
        $estado=TEstado::where('idestado','=',$id)->where('idestatus','=',1);

             return response()->json(['estado'=>($estado->count()>0)?$estado->first():'no existe este estado',
                                       'ciudades'=>($estado->count()>0 && $estado->first()->ciudades()->count()>0)?$estado->first()->ciudades->get():'no hay ciudades asociadas'],200);

                 //->ciudades()
                
    }

    
}
