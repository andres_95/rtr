<?php
namespace RTR\Http\Controllers\Admin\Actividades;


    use Validator;
    use RTR\modelos\TTipoActividade;
    use RTR\modelos\TPersona;
    use Illuminate\Http\Request;
    use Illuminate\Support\Facades\Auth;
    use RTR\Http\Controllers\Controller;
    use RTR\Http\Resources\CRFRecurso;

    class tipoactividadcontroller extends Controller{
             
      public function __construct()
      {
          $this->middleware(function ($request, $next) {
              $this->user = Auth::user();
              return $next($request);
          });
      }
         public function index(){
             return response()->json(['tipos_actividades'=>TTipoActividade::all()],200);
         }

         public function index2(Request $request){
             $Empresa=TPersona::withoutGlobalScopes()
                 ->where('idOperador',$this->user->idOperador)
                 ->where('idgrupo_persona','FABR')
                 ->select(['nombre_completo_razon_social','idPersona'])
                 ->get();
             $T="TipoActividad";
             $fabrica = $request->session()->get('user.teams');
             return view('Admin.estadistico.CiRaFr',compact('Empresa','T','fabrica'));
         }

         public function show(Request $request){
             $TA=TTipoActividade::withoutGlobalScopes()->where('idFabricante',$request->fabricante)
                                 ->where('idestatus',1)
                                 ->where('idOperador',$this->user->idOperador)
                                 ->get();
             foreach ($TA as $item)
                 $item->tipo='ta';

             $hola=CRFRecurso::collection($TA)->toJson();
             return response($hola,200);

         } 

         public function store(Request $request){
         
            //   do{ $id=rand(); }while(TTipoActividade::find($id));
              $validator=Validator::make($request->all(),[
                  'fabricante'=>'required|exists:t_personas,idPersona',
                  'descripcion'=>'required|unique:t_tipo_actividades,descripcion_tipo_actividades'
              ]);
                  if($validator->fails())
                     return response()->json($validator->errors()->all(),404);

              TTipoActividade::create([
                'idOperador'=>$this->user->idOperador,
                'idFabricante'=>$request->fabricante,
                'idtipo_actividades'=>$request->id,
                'descripcion_tipo_actividades'=>$request->descripcion,
                'idestatus'=>1
              ]);
                return response()->json(['success'=>'tipo de actividad registrado'],200);
         }
        public function update(Request $request){
            $validator=Validator::make($request->all(),[
                'fabricante'=>'required|exists:t_personas,idPersona',
                'id'=>'required|exists:t_personas,idtipo_actividades',
                'descripcion'=>'required|exists:t_tipo_actividades,descripcion_tipo_actividades'
            ]);
                if($validator->fails())
                   return response()->json($validator->errors()->all(),404);

            $tipo=TTipoActividade::withoutGlobalScopes()->find($request->id);
                   
                    if($tipo==null || $tipo->idFabricante!=$request->idFabricante)
                        return response()->json(['error'=>'tipo de actividad no encontrada'],404);
                              

                $tipo->descripcion_tipo_actividades=$request->descripcion;
                $tipo->save();

                return response()->json(['success'=>'tipo de actividad actualizada'],200);
        }
      public function destroy(Request $request){
        $validator=Validator::make($request->all(),[
            'fabricante'=>'required|exists:t_personas,idPersona',
            'id'=>'required|exists:t_personas,idtipo_actividades',
        ]);
            if($validator->fails())
               return response()->json($validator->errors()->all(),404);
           
            
        $tipo=TTipoActividade::withoutGlobalScopes()->where('idtipo_actividades',$request->id)
                          ->where('idFabricante',$request->fabricante)->first();
                   if(!$tipo->update(['idestatus'=>0]))
                        return response()->json(['error'=>'tipo de actividad no encontrada'],404);
        
                      

               // $tipo->update(['idestatus'=>'0']);
              
                return response()->json(['success'=>'tipo de actividad eliminada'],200);
      }
    }