<?php

namespace RTR\Http\Controllers\Admin\website;

use Illuminate\Http\Request;
use RTR\modelos\TConfiguracionSitio;
use RTR\Http\Controllers\Controller;

class websitecontroller extends Controller
{

    private $user;
    public function __construct(){
        $this->middleware(function ($request, $next) {
            $this->user = Auth::user();
            return $next($request);
        });
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
          $data=$request->all();
          $data['idOperador']=$this->user->idOperador;
          $data['idpais']=$this->user->idpais;
          $data['ididioma']=$this->user->ididioma;
          $data['idmoneda']=$this->user->idmoneda;

          TConfiguracionSitio::create($request->all());
          return response()->json(['success'=>'se ha creado unas configuraciones al sitio web'],200);
          
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
           if(!(new TConfiguracionSitio())->updItem($id,$request->all()))
                 return response()->json(['error'=>'configuracion no encontrada'],404);

                return response()->json(['success'=>'se ha hecho cambio en el sitio',200]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        if(!(new TConfiguracionSitio())->delItem($id))
           return response()->json(['error'=>'configuracion no encontrada'],404);

           return response()->json(['success'=>'se ha hecho eliminaciones en el sitio'],200);
    }
}
