<?php

namespace RTR\Http\Controllers\Admin\Redes;

use Illuminate\Http\Request;
use RTR\Http\Resources\CRFRecurso;
use RTR\modelos\TPersona;
use RTR\modelos\TRedesSociale;
use RTR\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class redsocialcontroller extends Controller
{

    public function __construct()
    {
        $this->middleware(function ($request, $next) {
            $this->user = Auth::user();
            return $next($request);
        });
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $redes=TRedesSociale::where('idOperador','=',$this->user->idOperador)->where('idestatus','=','1');

             if($redes->count()==0)
               return response()->json(['error'=>'red no encontrada'],404);

        return response()->json(['redes'=>$redes->get()],200);
    }

    public function index2(Request $request){
        $Empresa=TPersona::withoutGlobalScopes()
            ->where('idOperador',$this->user->idOperador)
            ->where('idgrupo_persona','FABR')
            ->select(['idOperador','idPersona'])
            ->get();
        $T="RedesSociales";
        $fabrica = $request->session()->get('user.teams');
        return view('Admin.estadistico.CiRaFr',compact('Empresa','T','fabrica'));
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
            
        $red=TRedesSociale::create([
            'idOperador'=>$this->user->idOperador,
            'cod_redes_sociales' =>$request->id,
            'idFabricante'=>$request->fabricante, 
            'descripcion_redes_sociales' => $request->descripcion,
            'idestatus' =>1 
        ]);

          return response()->json(['success'=>$red['cod_redes_sociales']],200);
        
    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request){
        $TA=TRedesSociale::withoutGlobalScopes()->where('idFabricante',$request->fabricante)
                ->where('idestatus',1)
                ->get();
        foreach ($TA as $item)
            $item->tipo='rs';

        $hola=CRFRecurso::collection($TA)->toJson();
        return response($hola,200);

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  string  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        //
         $redes=TRedesSociale::where('idOperador','=',$this->user->idOperador)
                               ->where('cod_redes_sociales','=',$request->id)
                               ->where('idestatus','=',1);

              if($redes->count()==0)
                 return response()->json(['error'=>'red social no encontrado'],404);

           $redes->update([
            'descripcion_redes_sociales' =>$request->descripcion
           ]);

             return response()->json(['success'=>'red social actualizado'],200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        //
        $redes=TRedesSociale::where('idOperador','=',$this->user->idOperador)
                           ->where('cod_redes_sociales','=',$request->id)
                           ->where('idestatus','=',1);

                if($redes->count()==0)
                      return response()->json(['error'=>'red social no encontrado'],404);
           $redes->update([
               'idestatus'=>0
           ]);

              return response()->json(['success'=>'red social eliminado'],200);
    }
}
