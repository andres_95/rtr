<?php

namespace RTR\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
 use PDF;
use RTR\Http\Middleware\header;
use RTR\modelos\TPersona;

use View;
use Carbon\Carbon;

class PDFController extends Controller
{
    public function __construct()
    {
        $this->middleware(function ($request, $next) {
            $this->user = Auth::user();
            return $next($request);
        });
    }

    public function PDF (Request $request){
        //dd($request->all());
        //dd(count($request->filtros));
   //     return response(public_path(),200);
        $title = $request->titulo;
        $fecha = Carbon::now();
        $html = '
        <!DOCTYPE html>
        <html>
        <body style ="margin:0px; font-size:13px">
        <table style="border:solid 1px black; width:100%">
            <tr>
                <td style =" width:10%" >
                    <img src="'.public_path().'\img\logo.png" style="height:105px; width:105px" >
                </td>
                <td style ="margin:0px; width:90%">
                    <table style="width:100%">
                        <tr align="left">
                            <th style="font-size:15px"> 
                                SERVICIOS INTEGRALES IF.
                            </th>    
                        </tr>
                        <tr>
                            <td>
                                SOLICITANTE
                            </td>
                            <td style="text-align:right">'
                                .strtoupper($this->user['nombre_completo_razon_social']).   
                            '</td>
                        </tr>
                        <tr>
                            <td>
                                SUPERVISOR
                            </td>
                            <td style="text-align:right">'
                                .strtoupper(TPersona::find($this->user->idsupervisor)['nombre_completo_razon_social'])
                                /*strtoupper($request->supervisor)*/.   
                            '</td>
                        </tr>
                        <tr>
                            <td>
                                FECHA DE SOLICITUD
                            </td>
                            <td style="text-align:right">'
                                .$fecha.   
                            '</td>
                        </tr>
                        <tr>
                            <td>
                                EMPRESA
                            </td>
                            <td style="text-align:right">'
                                .strtoupper(TPersona::find($this->user->idempresa_Grupo)['nombre_completo_razon_social'])
                                /*strtoupper($request->fabricante)*/.   
                            '</td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
        <h3 style= "text-align:center; width:100% "> RESULTADOS DE LA BUSQUEDA DE '.strtoupper($title).'.</h3>
        <table style="width:100%, align:center">
        <tr>
        ';


        foreach($request->cabecera as $cab){
            $html.='<th>';
            $html.=strtoupper($cab['text']).'</th>
            ';
        }
        $html.='</tr>
        <tr>';
        foreach($request->busqueda as $bus){
            $html.='<td>';
            if($request->tipo=='r')
                $html.=$bus['reporte'].'</td>';
            if($request->tipo=='o')
                $html.=$bus['nOrden'].'</td>';
            if($request->tipo=='f')
                $html.=$bus['id'].'</td>';

            $html.='<td>';
            if($request->tipo=='o'){
                $html.=$bus['persona'].'</td>';
            }else{
                $html.=$bus['RFV'].'</td>';
            }
            
            $html.='<td>';
            $html.=$bus['cliente'].'</td>
            ';      
            $html.='<td>';

            $html.=$bus['fecha'].'</td>';


            if($request->tipo=='f'){
                $html.='<td>'.$bus['cantP'].'</td>';
                $html.='<td>'.$bus['cantF'].'</td>';
            }

            $html.='<td>';
            if($request->tipo=='r')
                $html.=$bus['observaciones'].'</td></tr><tr>';
            if($request->tipo=='o')
                $html.=$bus['mayoristas']['0']['nombre'].'</td>';
            if($request->tipo=='f')
                $html.=$bus['costo'].'</td></tr><tr>'; 

            if($request->tipo=='o')
                $html.='<td>'.$bus['Total'].'</td></tr><tr>';
        }
        if(count($request->filtros)>=1){
            $html=trim($html,'<tr>').
            '</table>
            <table style="border: solid 1px black; width:100%; margin-top:20px" >
                <tr>
                    <th style="font-size:15px">
                        FILTROS APLICADOS
                    </th>
                </tr>
                <tr>';
            foreach($request->filtros as $key=>$fil){
                if (($key+1) % 3 === 0){
                    $html.='</tr><tr>';    
                }
                $html.='<td><strong>'.strtoupper($fil['nombre']).':</strong></td><td>'.strtoupper($fil['contenido']).'</td>';
            }
        }
        if($request->fechaIni!="" || $request->fechaFin!=""){
            $html.='</tr>
            </table>
            <table style="width:100%; border:solid black 1px; margin-top:10px">
                <tr>
                    <td><strong>
                        FECHA INICIO:
                    </strong></td>
                    <td>'.
                        $request->fechaIni.
                    '</td>
                    <td><strong>
                        FECHA FIN:
                    </strong></td>
                    <td>'.
                        $request->fechaFin.
                    '</td>
                </tr>
            </table>
            </body>
            </html>';
        }else{
            $html.='</tr>
            </table>
            </body>
            </html>'; 
        }
       
      

       return  PDF::loadHtml($html)->setPaper('a4', 'landscape')->stream('Reporte.pdf');


    }
}