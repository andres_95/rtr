<?php 
       namespace RTR\Http\Controllers\Productos;

        use RTR\modelos\TTipoProducto;
        use Validator;
     //   use App\Http\Resources\agendaResource;
        use Illuminate\Http\Request;
        use RTR\Http\Controllers\Controller;
        use Illuminate\Support\Facades\Auth;
        use \Carbon\Carbon;
        use Exception;

    class tipoproductocontroller extends Controller{


        public function __construct()
        {
            $this->middleware(function ($request, $next) {
                $this->user = Auth::user();
                return $next($request);
            });
        }

         public function index(){
              $actividad=TTipoProducto::where('estatus','=','1')->get();
            
               return response()->json(['Tipo de producto'=>$actividad],200);
         }
         public function show(Request $request){
            $actividad=TTipoProducto::where( 'idtipo_producto','=',$request->id)->where('idestatus','=','1')
            ->get();
            
               return response()->json(['Tipo de producto'=>$actividad],200);
         }

         public function store(Request $request){
             
          
            TTipoProducto::create([
                'idOperador' =>$this->user->idOperador, 
                'descripcion_tipo_producto'=>$request->descripcion ,
                'idestatus' =>'1'
               
              ]);
               return response()->json(['success'=>'Tipo de producto registrado'],200);

         }
        public function update(Request $request,$id){
              $validar=Validator::make($request->all(),[
                  
                  'descripcion'=>'required'
                  ]);
              if($validar->fails())
                 return response()->json(['error'=>$validar->errors()->all()],400);

             $actividad=TTipoProducto::where('idtipo_producto','=',$id)->where('idestatus','=','1');
                if($actividad->count()==0)
                   return response()->json(['errors'=>'Tipo de producto no encontrado'],404);

           $actividad->update([
               'descripcion_tipo_producto'=>$request->descripcion
           ]);
            return response()->json(['success'=>'Tipo de producto actualizada'],200);
        }

         public function destroy(Request $request,$id){
                    
                  

                $actividad=TTipoProducto::where('idtipo_producto','=',$id)->where('idestatus','=','1');
                    if($actividad->count()==0)
                        return response()->json(['errors'=>'Tipo de producto no encontrada'],404);

                $actividad->update([
                    'idestatus'=>'0'
                ]);
          return response()->json(['success'=>'Tipo de producto eliminada'],200);
       }
    }