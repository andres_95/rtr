<?php

namespace RTR\Http\Controllers\Auth;

use RTR\modelos\TPerfile;
use Illuminate\Support\Facades\Auth;
use RTR\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Validator;

class perfilcontroller extends Controller
{

    public function __construct()
    {
        $this->middleware(function ($request, $next) {
            $this->user = Auth::user();
            return $next($request);
        });
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(){

        $roles=TPerfile::all();
        foreach ($roles as $item){
            $item->descripcion=$item->descripcion_perfil;
        }
        $T="Perfil";
        return view('Admin.Seguridad.rolesperfil',compact('roles','T'));
    }

      public function store(Request $request){
        $data=$request->all();
        $data['idOperador']=$this->user->idOperador;
        $data['idestatus']=1;

           TPerfile::create($data);

             return response()->json(['success'=>'perfil creado']);
          
      }

      public function update(Request $request,$id){

      
            $perfil=TPerfile::where('id',$id)->where('idestatus',1);

                if(!$perfil)
                   return response()->json(['error'=>'configuracion de perfil no encontrado'],404);

            $perfil->update($request->all());
        

              return response()->json(['success'=>'configuracion de perfil actualizado' ],200);
      }

      public function destroy($id){
        $perfil=TPerfile::where('id',$id)->where('idestatus',1);
        
                        if(!$perfil)
                           return response()->json(['error'=>'configuracion de perfil no encontrado'],404);
        
                    $perfil->update([
                        'idestatus'=>0
                     ]);
                
        
                      return response()->json(['success'=>'configuracion de perfil eliminado'],200);
      }

 
}
