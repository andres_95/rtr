<?php

namespace RTR\Http\Controllers\Auth;

use Illuminate\Http\Request;
use RTR\modelos\TTipoPersona;
use Illuminate\Support\Facades\Auth;
use RTR\Http\Controllers\Controller;

class tipopersonacontroller extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
          $inst=TTipoPersona::create(
              [
                  'descripcion_tipo_persona'=>$request->descripcion,
                  'estatus'=>1
              ]
              );
       //   dd($inst);
              return response()->json(['sucess'=>'tipo de persona registrado'],200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
          $tipo=TTipoPersona::where('estatus', '=', '1')
                             ->where('id', '=',$id);

            if($tipo->count()==0)
               return response()->json(['error'=>'tipo de persona no encontrado'],404);

               $tipo->update(
                [
                    'descripcion_tipo_persona'=>$request->descripcion
                ]
                );
               
          return response()->json(['sucess'=>'tipo de persona actualizado'],200);

    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
                        $tipo=TTipoPersona::where('estatus', '=', '1')
                               ->where('id', '=',$id);

                if($tipo->count()==0)
                      return response()->json(['error'=>'tipo de persona no encontrado'],404);

                $tipo->update(
                [
                'estatus'=>0
                ]
                );
                return response()->json(['sucess'=>'tipo de persona eliminada'],200);
                
    }
}
