<?php

namespace RTR\Http\Controllers\Auth;

use Illuminate\Http\Request;
use Validator;
use RTR\modelos\TPersona;
use RTR\modelos\TDiasVisita;
use RTR\modelos\THorario;
use RTR\modelos\TEspecialidade;
use RTR\modelos\TGrupopersona;
use Illuminate\Support\Facades\Auth;
use RTR\Http\Controllers\Controller;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Hash;
use Illuminate\Hashing\BcryptHasher;
use RTR\Http\Resources\personaResource;
use RTR\Http\Resources\clienteResource;
use RTR\Http\Resources\MayoristaRecurso;
use Illuminate\Http\Response;
use RTR\modelos\TLocalizacione;

class personacontroller extends Controller
{
   
   /* public function __construct()
    {
        $this->middleware(function ($request, $next) {
            $this->user = Auth::user();
            return $next($request);
        });
    }*/
  
        
        public function __construct()
        {
           // $this->content = array();
           // $this->status = 200;
            $this->middleware(function ($request, $next) {
                $this->user = Auth::User();
                return $next($request);
            });
        }


     /*   public function getInfoCliente(Request $request){
               $persona=TPersona::where('documento_Identidad','=',$request->id)
               ->orwhere('nombre_persona','=',$request->id)->first();
                return new clienteResource($persona);
            }*/
         public function major(Request $request){
            /*    $mayorista=TPersona::where('idPersona',$request->id)->first();
                return new personaResource($mayorista);*/
                $persona=TGrupopersona::where('idgrupo_persona','=','MAY')->first()->personas()
                         ->where('idPersona','=',$request->id)
                         ->orwhere('nombre_completo_razon_social','=',$request->id)
                         ->where('idestatus','=','1')->first(['idPersona','nombre_completo_razon_social']);
               return new personaResource($persona);
        }

        /*
        public function mayoristas(){
            $mayoristas=TGrupopersona::where('idgrupo_persona','=','MAY')->first()->personas();
             return response()->json(['mayoristas'=>$mayoristas->get(['idPersona','nombre_completo_razon_social'])],200);
        }
        */

        public function especializar(Request $request){
            $especialidad=TEspecialidade::find($request->idespecialidad);
            $persona=TPersona::find($this->user->idPersona);
            $persona->especialidad()->associate($especialidad);
            $persona->save();
      }
       /**
        *
        *@return Illuminate\Http\Response
        * json con formato {rfv}
        */
      public function clientes(Request $request){

         $persona=TPersona::find($request->rfv);
            if ($persona->count == 0)
                return response(['error'=>'el rfv aun no tiene clientes'],200);
            return response()->json(['clientes'=>$persona->clientes()->get()],200);
      }

     /**
      *
      * implemented by Josue Becerra
      * hora 1:21 p.m.
      */
      /**
       * @param Illuminate\Http\Request $request
       * @return Illuminate\Http\Response
       *
       **/
         public function getRFV(Request $request){
          $rfv=TPersona::find($request->id)->rfv();

         if($rfv->count()==0)
              return response()->json(['error'=>'no posee representantes de venta'],404);

           return response()->json(['representantes'=>$rfv->get(['nombre_persona'])],200);
         }
        /**
         * 
         * @return Illuminate\Http\Response
         */
        public function getClientes(){
          /* $validador=Validator::make($request->all(),[
                'rfv'=>'required|exists:t_personas,idPersona'

           ]);*/
            /*  if($validador->fails()){
                  return response()->json(['error'=>'representante de venta no encontrado'],404);
              }*/
           $clientes=TPersona::find($this->user->idPersona)->clientes();
              if($clientes->count()==0)
                 return response()->json(['error'=>'no posee clientes que atender'],404);

            return response()->json(['clientes'=>$clientes->get(['idPersona','nombre_completo_razon_social'])],200);
        }

        public function getRFVS(Request $request){
             $rfvs=TPersona::where('idempresa_Grupo','=',$request->empresa);
              return response()->json(['representantes'=>$rfvs->get()],200);
        }

        public function getMayo(){
            if (TPersona::where('idgrupo_persona','MAY')->get()->count() == 0)
                return response('No hay mayoristas registrados.',404);
            return response(['Mayoristas'=>MayoristaRecurso::collection(TPersona::where('idgrupo_persona','MAY')->get())],200);
        }

        public function representantes(){
            $representantes=TGrupopersona::where('idgrupo_persona','=','RFV')->first()->personas()
            ->get(['idPersona','nombre_completo_razon_social']);

                if($representantes->count()==0)
                  return response()->json(['error'=>'no hay representantes de ventas'],404);

              return response()->json(['representantes'=>$representantes],200);
        }

        public function getInfoCliente(Request $request){

            $persona=TPersona::select('idPersona','idRFV','nombre_completo_razon_social')
            ->where('idPersona','=',$request['data'])
            ->orwhere('nombre_persona','like',$request['data'])
            ->orwhere('nombre_completo_razon_social','like',$request['data'])->where('idestatus','=','1')->first();

             return response()->json($persona,200);
         }

         public function store(Request $request){
           //  $valida
              TPersona::create($request->all()); 
              
                return response()->json(['success'=>'persona registrada'],200);
         }
        
         public function update(Request $request,$id){
               $persona=TPersona::where('idPersona',$id)->where('idestatus',$id);

                    

               if($persona->update($request->all()))
                  return response()->json(['error'=>'persona no encontrada'],404);

                 return response()->json(['success'=>'datos de la persona actualizada'],200);

         }

         public function destroy($id){
            $persona=TPersona::where('idPersona',$id)->where('idestatus',$id);
            
                    

                   if( !$persona->update([
                        'idestatus'=>0
                    ]))
                    return response()->json(['error'=>'persona no encontrada'],404);

                    return response()->json(['success'=>'datos de la persona aanulada'],200);  
                    
         }

         public function index(){

         }

         public function setLocalizacion(Request $request){
             $data=$request->except('idpersona','idlocalizacion');
             $data['idOperador']=$this->user->idOperador;
             $data['idestatus']=1;
             $persona=(new TPersona())->findItem($request->idpersona);
               if($persona){
                    $persona->contactos()->attach($request->idlocalizacion,
                        $data
                    );
             return response()->json(['success'=>'se ha agregado un contacto de esta persona'],200);
            }

               return response()->json(['error'=>'persona no encontrada'],404);
         }

         public function updLocalizacion(Request $request,$id){
           
              $persona=(new TPersona())->findItem($request->idpersona);
                $data=$request->except('idlocalizacion','idpersona','idestatus');
              //  return $persona->contactos()->wherePivot('idestatus',1);
                   // dd($persona->contactos());
               if(!$persona->contactos()->wherePivot('idestatus',1)->updateExistingPivot($id,$data)){
                   return response()->json(['error'=>'este contacto no existe o no se ha hecho cambio alguno'],404);
               }
                 return response()->json(['success'=>'se ha actualizado el contacto de este usuario'],200);
         }

         public function delLocalizacion(Request $request,$id){
            $persona=(new TPersona())->findItem($request->idpersona);
            if(!$persona->contactos()->wherePivot('idestatus',1)->updateExistingPivot($id,['idestatus'=>0])){
                return response()->json(['error'=>'este contacto no existe'],404);
            }
              return response()->json(['success'=>'se ha eliminado el contacto de este usuario'],200);
         }

            /**
             * 
             * Crear un microservicio de coberturas
             */


           /**
            * 
            * microservicios de asignacion de dias disponibles
            */
         public function setDias(Request $request){
             $dias=TDiasVisita::find($request->iddia);
                 if($dias==null || $dias->idestatus=='0')
                   return response()->json(['error'=>'dias no encontrado'],404);

            $persona=TPersona::where('idPersona',$request->idpersona)->where('idestatus',1);

              if($persona->count()==0)
                return response()->json(['error'=>'persona no encontrada'],404);

                $persona->first()->dias()->attach($dias);

              return response()->json(['success'=>'esta persona tiene dias asignados'],200);
         }

        public function updDias(Request $request){
            $dias=TDiasVisita::find($request->iddia);
            $nuevo=TDiasVisita::find($request->idnuevoDia);
            if(($dias==null || $dias->idestatus=='0')||($nuevo==null || $nuevo->idestatus=='0'))
              return response()->json(['error'=>'dias no encontrado'],404);

       $persona=TPersona::where('idPersona',$request->idpersona)->where('idestatus',1);

         if($persona->count()==0)
           return response()->json(['error'=>'persona no encontrada'],404);

           if(!$persona->first()->dias()->detach($dias))
             return response()->json(['error'=>'esta persona no tiene este dia disponible'],404);
           $persona->first()->dias()->attach($nuevo);

         return response()->json(['success'=>'dias actualizados'],200);
        }

        public function delDias(Request $request){
            $dias=TDiasVisita::find($request->iddia);
            if($dias==null || $dias->idestatus=='0')
              return response()->json(['error'=>'dias no encontrado'],404);

       $persona=TPersona::where('idPersona',$request->idpersona)->where('idestatus',1);

         if($persona->count()==0)
           return response()->json(['error'=>'persona no encontrada'],404);

           if(!$persona->first()->dias()->detach($dias))
              return response()->json(['error'=>'esta persona no tiene este dia disponible'],404);

         return response()->json(['success'=>'dia borrado'],200);
        }


       public function setHorario(Request $request){
               $horario=THorario::find($request->idhorario);
               if($horario==null || $horario->idestatus=='0')
                    return response()->json(['error'=>'dias no encontrado'],404);

            $persona=TPersona::where('idPersona',$request->idpersona)->where('idestatus',1);

                if($persona->count()==0)
                return response()->json(['error'=>'persona no encontrada'],404);

                $persona->first()->horarios()->attach($horario);

                return response()->json(['success'=>'esta persona tiene horas disponibles'],200);
       }

        public function updHorario(Request $request){ 
            $horario=THorario::find($request->idhorario);
            $nuevo=THorario::find($request->idnuevohora);
            if($horario==null || $horario->idestatus=='0')
                 return response()->json(['error'=>'horario no encontrado'],404);

         $persona=TPersona::where('idPersona',$request->idpersona)->where('idestatus',1);

             if($persona->count()==0)
             return response()->json(['error'=>'persona no encontrada'],404);

             if(!$persona->first()->horarios()->detach($horario))
                      return response()->json(['error'=>'este cliente no tiene este horario'],404) ;
             $persona->first()->horarios()->attach($nuevo);

             return response()->json(['success'=>'esta persona se le cambio su horario'],200);

        }

        public function delHorario(Request $request){
            $horario=THorario::find($request->idhorario);
            if($horario==null || $horario->idestatus=='0')
                 return response()->json(['error'=>'horario no encontrado'],404);

         $persona=TPersona::where('idPersona',$request->idpersona)->where('idestatus',1);

             if($persona->count()==0)
             return response()->json(['error'=>'persona no encontrada'],404);

             if(!$persona->first()->horarios()->detach($horario))
                    return response()->json(['error'=>'este cliente no tiene este horario'],404) ;

             return response()->json(['success'=>'esta persona dejo de estar  disponible en este horario'],200);
        }
        
}
