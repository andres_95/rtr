<?php

namespace RTR\Http\Controllers\Auth;

use Illuminate\Http\Request;
use RTR\Http\Controllers\Controller;
use RTR\modelos\TPersona;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Hash;
use Illuminate\Hashing\BcryptHasher;
use Validator;

class LoginAPIController extends Controller
{
    
      public function __construct()
        {
            $this->content = array();
            $this->status = 200;
        }


        public function login(Request $request){


            $credentials = $request->only('username', 'password');

            $validator =
                Validator::make($credentials,
                    [
                        'username'    => 'required',

                        'password'    => 'required'
                    ],
                    [
                        'required'=>'el :attribute es requerido'
                    ]
                );

            if ($validator->fails()) {
                $this->content =[
                    'error' => 'Error'
                ];
                return response()->json($validator->errors()->all(), 400);
                $this->status=400;
            }
            else
            {
                $user = TPersona::where('username', $credentials['username'])->first();
                if (!$user) {
                    $this->content =[
                        'error' => 'Invalid credentials'
                    ];
                    $this->status=401;
                }else
                {
                    if (Hash::check($credentials['password'], $user->password)) {
                        
                        $this->content =[
                            'token' =>  $user->createToken($user->username)->accessToken,
                            'username' =>$user->username,
                            'nombre' => $user->nombre_completo_razon_social,
                            'idRFV' => $user->idPersona
                        ];
                        $this->status=200;
                    }else
                    {
                        $this->content =[
                            'error' => 'Invalid credentials'
                        ];
                        $this->status=401;
                    }
                }
            }


            return response()->json($this->content, $this->status);
        }
   
        public function logout()
        {
           $user = Auth::User();
            if(!$user)
            {
                return response()->json(null,204);
            }
            else
            {
               $user->destroyToken();
            }
             return response()->json(['data'=>'deslogueado'],200);
        }

      
         public function profileUser(Request $request){
             $persona=TPersona::where('idPersona','=',$request->idrfv)->where('idestatus','=','1');

                 if($persona->count()==0)
                      return response()->json(['error'=>'representante no encontrado'],404);

             return response()->json(['perfil'=>$persona->first()]);


         }
}
