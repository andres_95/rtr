<?php


    namespace App\Http\Controllers\auth;


    use Validator;
    use App\modelos\TPersona;
    use App\modelos\LocalizacionOperador;
    use App\modelos\LocalizacionPersona;
    use Illuminate\Http\Request;
    use Illuminate\Support\Facades\Auth;
    use App\Http\Controllers\Controller;
    use App\Resources\userResources;
    use Illuminate\Support\Facades\Crypt;
    use Illuminate\Support\Facades\Hash;
    use Illuminate\Hashing\BcryptHasher;
    use Response;

    class contactocontroller extends Controller
    {
        
        public function __construct()
        {
            $this->content = array();
            $this->status = 200;
        }
       public function index(Request $request){
           
           $val=Validator::make($request->all(),[
            'empresarial'=>'required',
            'id'=>'required|int'
           ]);
               if($val->fails()){
                    return response()->json(['errors'=>$val->errors()->fails()],200);

               }
              if($request->empresarial==false){
                  $localizacion=LocalizacionPersona::where('idPersona','=',$request->id);
                  //return response()->json(['contactos'=>$localizacion])
              }
       }
       public function store(Request $request){
           
          $tipo=$request->only('empresarial','id');
            $val=Validator::make($tipo,[
                'empresarial'=>'required|bool',
                'required'=>'Necesitas definir el tipo de localizacion si es empresarial(true) o personal(false)'
                ]);
             if($val->fails()){
                 return response()->json(['errors'=>$val->errors()->all()],200);
             }
          if($request->empresarial==false)
          { 
           

              $validor=Validator::make($request->all(),[
                'operador'=>'required|int',
                'telefono'=>'required',
                'direccion'=>'required',
                'movil'=>'required',
                'email'=>'required',
                'direccion'=>'required',
                'estado'=>'required',
                'ciudad'=>'required',
                'urbanizacion'=>'required' ,
                'postal' =>'required'    
              ],
              [
                'required'=>'Se requiere :attribute'
              ]
              );
              do{ $id=rand(); }while(LocalizacionPersona::find($id));
                LocalizacionPersona::create([
                    'idOperador'=> $request->operador,
                    'cod_localizacion'=>$id,
                    'idpersona'=>'432',
                    'telefono' =>$request->telefono,
                    'movil_'=>$request->movil,
                    'email_' =>$request->email,
                    'direccion' =>$request->direccion,
                    'estado' =>$request->estado,
                    'ciudad'=> $request->ciudad,
                    'urbanizacion' =>$request->urbanizacion,
                    'zona_postal'=>$request->postal,
                    'estatus' =>'1'
                ]);

          }  
        else{
            $validor=Validator::make($request->all(),[
                'descripcion'=>'required' 
              ],
              [
                'required'=>'Se requiere :attribute'
              ]
              );
              if($validor->fails()){
                  return response()->json(['errors'=>$validor->errors()->all()],200);
               }
            do{ $id=rand(); }while(LocalizacionOperador::find($id));
            LocalizacionOperador::create([
                'idOperador' =>'154',
                'idlocalizacion'=>$id,  
                'descripcion_localizacion'=>$request->descripcion,
                'estatus'=>'1' 
            ]);
        }  
        
           return response()->json(['success'=>'localizacion registrada'],200);
       }






       public function update(Request $request){

        $tipo=$request->only('empresarial','id');
        $val=Validator::make($tipo,[
            'empresarial'=>'required|bool',
            'id'=>'required|int',
            'required'=>'Necesitas definir el tipo de localizacion si es empresarial(true) o personal(false)'
            ]);
         if($val->fails()){
             return response()->json(['errors'=>$val->errors()->all()],200);
         }
      if($request->empresarial==false)
      { 
       

          $validor=Validator::make($request->all(),[
            'operador'=>'required|int',
            'telefono'=>'required',
            'direccion'=>'required',
            'movil'=>'required',
            'email'=>'required',
            'direccion'=>'required',
            'estado'=>'required',
            'ciudad'=>'required',
            'urbanizacion'=>'required' ,
            'postal' =>'required'    
          ],
          [
            'required'=>'Se requiere :attribute'
          ]
          );
            $localizacion=LocalizacionPersona::where('cod_localizacion','=',$request->id)->where('estatus','=','1');
              if($localizacion->count()==0)
                  return response()->json(['error'=>'Esta localizacion de persona no se encuentra y por lo tanto no se pudo actualizar'],200);
           
            $localizacion->update([
                'idOperador'=> $request->operador,
                'idpersona'=>'432',
                'telefono' =>$request->telefono,
                'movil_'=>$request->movil,
                'email_' =>$request->email,
                'direccion' =>$request->direccion,
                'estado' =>$request->estado,
                'ciudad'=> $request->ciudad,
                'urbanizacion' =>$request->urbanizacion,
                'zona_postal'=>$request->postal,
                'estatus' =>'1'
            ]);

      }  
    else{
        $validor=Validator::make($request->all(),[
            'descripcion'=>'required' 
          ],
          [
            'required'=>'Se requiere :attribute'
          ]
          );
          if($validor->fails()){
              return response()->json(['errors'=>$validor->errors()->all()],200);
           }
           $localizacion=LocalizacionOperador::where('idlocalizacion','=',$request->id)->where('estatus','=','1');
           if($localizacion->count()==0)
               return response()->json(['error'=>'Esta localizacion no se encuentra y por lo tanto no se pudo actualizar'],200);
       
        $localizacion->update([
            'idOperador' =>'154',
            'descripcion_localizacion'=>$request->descripcion,
            'estatus'=>'1' 
        ]);
    }  
    
       return response()->json(['success'=>'localizacion actualizada'],200);
               
              
         
       }

     public function delete(Request $request){
        $tipo=$request->only('empresarial','id');
        $val=Validator::make($tipo,[
            'empresarial'=>'required|bool',
            'id'=>'required|int',
            'required'=>'Necesitas definir el tipo de localizacion si es empresarial(true) o personal(false)'
            ]);
         if($val->fails()){
             return response()->json(['errors'=>$val->errors()->all()],200);
         }
      if($request->empresarial==false)
      { 
       

        
            $localizacion=LocalizacionPersona::where('cod_localizacion','=',$request->id)->where('estatus','=','1');
              if($localizacion->count()==0)
                  return response()->json(['error'=>'Esta localizacion de persona no se encuentra'],200);
           
            $localizacion->update([
                
                'estatus' =>'0'
            ]);

      }  
    else{
       
          
           $localizacion=LocalizacionOperador::where('idlocalizacion','=',$request->id)->where('estatus','=','1');
           if($localizacion->count()==0)
               return response()->json(['error'=>'Esta localizacion no se encuentra '],200);
       
        $localizacion->update([
            'estatus'=>'0' 
        ]);
           
     }
        return response()->json(['success'=>'Localizacion eliminada'],200);
    }

    public function email(){
       // $user=Auth::guard('web')->check();
        return response()->json(['hola'=>'234'],200);
    }
}
    