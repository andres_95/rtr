<?php

namespace RTR\Http\Controllers\Productos;

use Illuminate\Http\Request;
use RTR\modelos\TLineaProducto;
use Illuminate\Support\Facades\Auth;
use RTR\Http\Controllers\Controller;

class lineaproductocontroller extends Controller
{
    private $linea;
    public function __construct(){
        $this->middleware(function ($request, $next) {
            $this->user = Auth::user();
            return $next($request);
        });
        $this->linea=new TLineaProducto();
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //

        return response()->json([
            'lineas'=>TLineaProducto::where('idestatus',1)->get()
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
       
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
       $data=$request->all();
        $data['idOperador']=$this->user->idOperador;
        $data['idestatus']=1;
        
        TLineaProducto::create($data);
          return response()->json(['success'=>'linea de producto creada'],200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
          return response(['linea'=>$this->linea->findItem($id)],200);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
           if(!$this->linea->updItem($id,$request->all()))
              return response()->json(['error'=>'linea de producto no encontrda'],404);
              
            return response()->json(['success'=>'se ha cambiado una linea de producto'],200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        if(!$this->linea->delItem($id))
            return response()->json(['error'=>'linea de producto no encontrda'],404);
        
      return response()->json(['success'=>'se ha eliminado una linea de producto'],200);
    }
}
