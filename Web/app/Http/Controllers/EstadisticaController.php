<?php

//CONTROLADOR COBERTURA

namespace RTR\Http\Controllers;

use Illuminate\Http\Request;
use RTR\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

use RTR\modelos\TPersona;
use RTR\modelos\TCoberturaRv;
use RTR\modelos\TBrickRutaPersona;
use RTR\modelos\TCiclo;

use Validator;
use Carbon\Carbon;

use Illuminate\Support\Facades\Auth;
class EstadisticaController extends Controller
{
    public function __construct()
    {
        $this->middleware(function ( $request, $next ) {
            $this->user = Auth::user();
            return $next($request);
        });
    }

    /**
     * Esta es la funcion para organizar los rfv por zona y brick de ruta
     * @param $zonas es un arreglo de las zonas en las que se hara la busqueda
     **/
    /** AGREGAR UNIDADES A BASE DE DATOS
     *  FILTRAR POR CIUDAD
     *  FILTRAR POR ESTADO
     */
    public function busquedaRFV ($query)
    {
        $Sup=[];
        $Rfv=[];
        $Zona=[];
        $Ruta=[];
        $cobertura = "SELECT idRFV, idsupervisor, idzona, idestado, idciudad, idruta, fechaRegistro, porce_cobertura, monto_facturado, monto_esperado, productoEsperado, productoFacturado, MesRegistro FROM `t_cobertura_rfv` WHERE `idFabricante`= '".$this->user->idFabricante."' AND (`fechaRegistro` BETWEEN '".$query['fechaIni']."' AND '".$query['fechaFin']."')";

        if (!empty($query['supervisores']))
        {
            foreach($query['supervisores'] as $item){
                array_push($Sup,"'".$item['idPersona']."'");
            }
            $cobertura = $cobertura." AND `idsupervisor` IN (".implode(",",$Sup).")";
        }

        if(!empty($query['rfv']))
        {
            foreach($query['rfv'] as $item){
                array_push($Rfv,"'".$item['idPersona']."'");
            }
            $cobertura = $cobertura." AND `idRFV` IN (".implode(",",$Rfv).")";
        }

        if (!empty($query['zonas']))
        {
            foreach($query['zonas'] as $item){
                array_push($Zona,"'".$item['idzona']."'");
            }
            $cobertura = $cobertura." AND `idzona` IN (".implode(',',$Zona).")";
        }

        if(!empty($query['rutas']))
        {
            foreach($query['rutas'] as $item){
                array_push($Ruta,"'".$item['idbrick']."'");
            }
            $cobertura = $cobertura." AND `idruta` IN (".implode(',',$Ruta).")";
        }

        $calculos = "cover.idRFV, cover.idsupervisor, MesRegistro AS Mes, cover.idzona, cover.idruta, SUM(monto_facturado) AS montoFact, SUM(monto_esperado) AS montoEsp, AVG(porce_cobertura) AS cobertura, productoEsperado AS cantEsp , productoFacturado AS cantFact";
        $cobertura = "SELECT ".$calculos." FROM (".$cobertura.") as cover GROUP BY cover.idRFV, cover.idsupervisor, cover.idzona, cover.idruta, Mes, cantEsp, cantFact";

        return $cobertura;
    }

    /**
     * Esta es la funcion para actualizacion automatica y chequeo del ciclo
     * (ganancia generada)
     * @param $rfvs es un arreglo de los rfv previamente digerido por filtrado
     **/
    public function ConsolidadoDiario()
    {
        /**
         * Plantear la posibilidad de incluir a la tabla t_cobertura_rfv un campo que se actualice
         * automaticamente cada vez que se concilie una factura, que sea incremental y que contenga
         * la informacion del total generado por ventas, para evitar la consulta masiva de todas las ordenes
         **/
        $current = Carbon::now();
        $yesterday = Carbon::yesterday();

        $ordenBase = "SELECT idPersona, SUM(TotalUnidades) AS unidades, SUM(costoTotal) AS total, COUNT(*) AS cantidad FROM `t_ordenes` WHERE `fechaOrden` = DATE('".$current."')";
        $reporteBase = "SELECT idRFV, COUNT(*) AS cantidad FROM `t_planificadores` WHERE `idFabricante` = 'FCPHARMA' AND `fecha_agenda` = DATE('".$current."')";

        $esperadas = "SELECT esp.idPersona, 0 AS cantFact, 0 AS montoFact, 0 AS unidFact,  esp.cantidad AS cantEsp, esp.total AS montoEsp, esp.unidades AS unidEsp FROM (".$ordenBase." AND `idestatus` NOT IN ('6','8','7') GROUP BY idPersona) AS esp";
        $facturadas ="SELECT fact.idPersona, fact.cantidad AS cantFact, fact.total AS montoFact, fact.unidades AS unidFact, 0 AS cantEsp,  0 AS montoEsp, 0 AS unidEsp FROM (".$ordenBase." AND `idestatus` = '6' GROUP BY idPersona) AS fact";

        $totalOrden = "SELECT res.idPersona, SUM(res.cantFact) AS cantFact, SUM(res.montoFact) AS montoFact, SUM(res.unidFact) AS unidFact, SUM(res.cantEsp) AS cantEsp, SUM(res.montoEsp) AS montoEsp, SUM(res.unidEsp) AS unidEsp, 0 AS sinVisitar, 0 AS visitados FROM ((".$facturadas.") UNION (".$esperadas.")) AS res GROUP BY res.idPersona";

        $visitados = "SELECT vis.idRFV, vis.cantidad AS visitados, 0 AS sinVisitar FROM (".$reporteBase." AND `idestatus` = '2' GROUP BY idRFV) AS vis";
        $sinVisitar = "SELECT sv.idRFV, 0 AS visitados, sv.cantidad AS sinVisitar FROM (".$reporteBase." AND `idestatus` = '1' GROUP BY idRFV) AS sv";

        $totalReporte = "SELECT res.idRFV AS idPersona, 0 AS cantFact, 0 AS montoFact, 0 AS unidFact, 0 AS cantEsp,  0 AS montoEsp, 0 AS unidEsp, SUM(res.sinVisitar) AS sinVisitar, SUM(res.visitados) AS visitados FROM ((".$visitados.") UNION (".$sinVisitar.")) AS res GROUP BY res.idRFV";

        $totalPersonas = DB::select("SELECT pers.idPersona AS idPersona, SUM(pers.cantFact) AS cantFact, SUM(pers.montoFact) AS montoFact, SUM(pers.unidFact) AS unidFact, SUM(pers.cantEsp) AS cantEsp, SUM(pers.montoEsp) AS montoEsp, SUM(pers.unidEsp) AS unidEsp, SUM(pers.SinVisitar) AS sinVisitar, SUM(pers.visitados) AS visitados FROM ((".$totalOrden.") UNION (".$totalReporte.")) AS pers GROUP BY pers.idPersona");

        if ($current->month !== $yesterday->month)
        {
            foreach(TPersona::where('idgrupo_persona','RFV')->where('idestatus',1)->get() as $persona)
            {
                $zonaBrick = TBrickRutaPersona::where('idPersona',$persona->idPersona)->first();
                $ciclo = TCiclo::where('idFabricante',$persona->idFabricante)->first();
                TCoberturaRv::create([
                    'idOperador'=>$persona['idOperador'],
                    'idFabricante'=>$persona['idFabricante'],
                    'idRFV'=>$persona['idPersona'],
                    'idsupervisor'=>$persona['idsupervisor'],
                    'idzona'=>$zonaBrick['idzona'],
                    'idbrick'=>$zonaBrick['idbrick'],
                    'idestado'=>$persona['idestado'],
                    'idciudad'=>$persona['idciudad'],
                    'fechaRegistro'=>$current,
                    'cant_esperada'=>0,
                    'cant_realizada'=>0,
                    'productoEsperado'=>0,
                    'productoFacturado'=>0,
                    'cant_ordenes_realizadas'=>0,
                    'cant_ordenes_parciales'=>0,
                    'porce_cobertura'=>0,
                    'monto_facturado'=>0,
                    'monto_esperado'=>0,
                    'numero_ciclo'=>$ciclo->id,
                    'MesRegistro'=>$current->month."/".$current->year,
                    'idestatus'=>1
                ]);
            }
        }

        foreach ($totalPersonas as $persona)
        {
            $update = "UPDATE `t_cobertura_rfv` SET cant_esperada = cant_esperada + ".$persona->sinVisitar.", cant_realizada = cant_realizada + ".$persona->visitados.", monto_facturado = monto_facturado + ".$persona->montoFact.", cant_ordenes_realizadas = cant_ordenes_realizadas + ".$persona->cantFact.", cant_ordenes_parciales = cant_ordenes_parciales + ".$persona->cantEsp.", monto_esperado = monto_esperado + ".$persona->montoEsp.", productoEsperado = productoEsperado + ".$persona->unidEsp.", productoFacturado = productoFacturado + ".$persona->unidFact.", porce_cobertura = ( (cant_realizada + cant_esperada - cant_esperada)*100/(cant_realizada + cant_esperada)), fechaRegistro = '".$current."' WHERE MONTH(`fechaRegistro`) = MONTH('".$current."') AND `idRFV` = '".$persona->idPersona."'";
            DB::statement($update);
        }
    }
    /**
     * Esta funcion hara la consulta estadistica, filtrado de las visitas que ha realizado
     * el cliente por zona, supervisor, RFV y por ultimo cliente, hara un calculo porcentual
     * de la cobertura total de los rfv de acuerdo al ciclo consultado.
     * Se puede introducir un arreglo con los parametros de busqueda que se necesiten.
     * El arreglo debe contener obligatoriamente la zona, las demas opciones como
     * brick son opcionales
     * @param $request es un objeto instancia /Request
     **/
    public function Estadistico (Request $request)
    {
        $validar=Validator::make($request->all(),[
            'filtros'=>'required',
            'filtros.fechaIni' => 'required|date|before:tomorrow',
            'filtros.fechaFin' => 'required|date|after_or_equal:fechaInicio'
        ],
            [
                'required'=>'Se necesita el :attribute'
            ]);

        if($validar->fails()){
            return response()->json($validar->errors()->all(), 400);
        }
        $queryString = $this->busquedaRFV($request->filtros);

        $rfvs = DB::select($queryString);

        if (empty($rfvs))
            return response(['mensaje'=>'no se han encontrado Representantes'],200);

        return response($rfvs,200);
    }

}