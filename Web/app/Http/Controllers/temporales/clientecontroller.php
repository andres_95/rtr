<?php
     namespace App\Http\Controllers\temporales;

     use Validator;
    // use App\Http\Resources\bricksrutaResource as ruta;
     use Illuminate\Http\Request;
     use App\Http\Controllers\Controller;
     use Illuminate\Support\Facades\Auth;
     use \Carbon\Carbon;
     use App\modelos\TTmpClienteRepresentante;
     use Exception;

class clientecontroller extends Controller
{
    

      public function store(Request $request){
        TTmpClienteRepresentante::create([
            'idOperador'=>$request->operador,
            'idfabricante'=>$request->fabricante,
            'idciclos'=>$request->ciclo,
            'tipo_Compania'=>$request->tipoCompania,
            'idcadena'=>$request->cadena,
            'idRFV'=>$request->rfv,
            'nombres_RFV'=>$request->nombreRFV,
            'apellidos_RFV'=>$request->apellidoRFV,
            'idCliente'=>$request->cliente,
            'nombre_cliente_Visita'=>$request->nombreCliente,
            'idranking'=>$request->ranking,
            'tipo_especialidad'=>$request->tipoEspecialidad,
            'especialidad'=>$request->especialidad,
            'linea'=>$request->linea,
            'idestado'=>$request->idestado,
            'idciudad'=>$request->idciudad,
            'idzona'=>$request->idzona,
            'direccion'=>$request->direccion,
            'telefono'=>$request->telefono,
            'idBrick'=>$request->brick,
            'email_cliente'=>$request->emailcliente,
            'idhorarios'=>$request->horario,
            'idfrecuencia'=>$request->frecuencia,
            'iddias_visita'=>$request->diavisita,
            'Alta_Baja_Update'=>$request->abu,
            'idestatus'=>'1'
        ]);
          return response()->json([
            
             'success'=>'cliente registrado'
          ],200);
      }
    public function update(Request $request){
          $cliente=TTmpClienteRepresentante::where('idcliente','=',$request->id)->where('idestatus','=','1');
             if($cliente->count()==0)
               return response()->json(['error'=>'cliente no encontrado'],404);
            
         $cliente->update([
            'idOperador'=>$request->operador,
            'idfabricante'=>$request->fabricante,
            'idciclos'=>$request->ciclo,
            'tipo_Compania'=>$request->tipoCompania,
            'idcadena'=>$request->cadena,
            'idRFV'=>$request->rfv,
            'nombres_RFV'=>$request->nombreRFV,
            'apellidos_RFV'=>$request->apellidoRFV,
            'idCliente'=>$request->cliente,
            'nombre_cliente_Visita'=>$request->nombreCliente,
            'idranking'=>$request->ranking,
            'tipo_especialidad'=>$request->tipoEspecialidad,
            'especialidad'=>$request->especialidad,
            'linea'=>$request->linea,
            'idestado'=>$request->idestado,
            'idciudad'=>$request->idciudad,
            'idzona'=>$request->idzona,
            'direccion'=>$request->direccion,
            'telefono'=>$request->telefono,
            'idBrick'=>$request->brick,
            'email_cliente'=>$request->emailcliente,
            'idhorarios'=>$request->horario,
            'idfrecuencia'=>$request->frecuencia,
            'iddias_visita'=>$request->diavisita,
            'Alta_Baja_Update'=>$request->abu,
            'idestatus'=>'1'
         ]);
           return response()->json(['success'=>'cliente actualizado'],200);
                     
    }
   public function delete(Request $request){
            $cliente=TTmpClienteRepresentante::where('idcliente','=',$request->id)->where('idestatus','=','1');
           
            if($cliente->count()==0)
               return response()->json(['error'=>'cliente no encontrado'],404);

               $cliente->update([
                'idestatus'=>'0'
             ]);
               return response()->json(['success'=>'cliente eliminado'],200);
   }

 
}
