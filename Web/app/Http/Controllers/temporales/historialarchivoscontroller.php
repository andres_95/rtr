<?php

namespace App\Http\Controllers;

use Validator;
use App\modelos\TArchivosRecibido;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use App\Http\Resources\DataFileResource as files;

class historialarchivoscontroller extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
  
    //
     

     public function store(Request $request){
          do{ $id=rand(); }while(TArchivosRecibido::find($id)!=null);

        TArchivosRecibido::create([
            'idOperador'=>'4', 
            'idfabricante'=>'4' ,
            'idArchivo'=>$id,
            'idPersona'=>'4',
		    'fecha_recepcion'=>Carbon::now()->format("Y-m-d"),
		    'mes_year'=>Carbon::now()->format("Y-m"),
            'idestatus'=>'1'
        ]);
        
          return response()->json(['success'=>'OK']);
     }
    
     public function procesar(Request $request)
     {
         $archivo=TArchivosRecibido::where('idArchivo','=',$request->id)->where('idestatus','=','1');

            if($archivo->count()==0)
              return response()->json([
                   'error'=>'no encontrado'
                
                   
              ]);

            $archivo->update([
                'fecha_procesamiento'=>Carbon::now()->format("Y-m-d"),
                'idestatus'=>'2'
            ]);

               return response()->json(['success'=>'archivo procesado']);

     }

     public function index(Request $request){
          return files::collection(TArchivosRecibido::where($request->idPersona)->where('idestatus','<>','0')->get());
     }
}
