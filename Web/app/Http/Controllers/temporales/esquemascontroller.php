<?php
     namespace App\Http\Controllers\temporales;

     use Validator;
    // use App\Http\Resources\bricksrutaResource as ruta;
     use Illuminate\Http\Request;
     use App\Http\Controllers\Controller;
     use Illuminate\Support\Facades\Auth;
     use \Carbon\Carbon;
     use App\modelos\TTmpEsquemaPromocionale;
     use App\Http\Resources\tmpEsquemaResource;
     use Exception;

class esquemascontroller extends Controller
{
    
      public function index(){
           $esquema=TTmpEsquemaPromocionale::where('idestatus','=','1')->get();
           return tmpEsquemaResource::collection($esquema);
      }
      public function store(Request $request){
        TTmpEsquemaPromocionale::create([
                'idOperador'=>$request->operador,
                'idfabricante'=>$request->fabricante ,
                //'idRFV'=>$request->rfv,
                'idesqprom'=>$request->esquema , 
                'idProducto'=>$request->producto ,
                'linea'=>$request->line ,
                'idranking'=>$request->rank ,
                'cantidad_desde'=>$request->cantidadDesde ,
                'cantidad_hasta'=>$request->cantidadHasta ,
                'monto_desde'=>$request->montoDesde ,
                'monto_hasta'=>$request->montoHasta,
                'cantidad_promocion'=>$request->cantidadPromocion ,
                'descuento'=>$request->descuento ,
                'Alta_Baja_Update'=>$request->abu ,
                'idestatus'=>'1' 
        ]);
          return response()->json(['success'=>'esquema promocional registrado'],200);
      }
    public function update(Request $request){
         $esquema=TTmpEsquemaPromocionale::where('idesqprom','=',$request->id);
                  

                    if($esquema->count()==0)
                       return response()->json(['error'=>'esquema promocional no encontrado'],404);
          
            $esquema->update([
                'idOperador'=>$request->operador,
                'idfabricante'=>$request->fabricante ,
             //   'idRFV'=>$request->rfv,
                'idesqprom'=>$request->esquema , 
                'idProducto'=>$request->producto ,
                'linea'=>$request->line ,
                'idranking'=>$request->rank ,
                'cantidad_desde'=>$request->cantidadDesde ,
                'cantidad_hasta'=>$request->cantidadHasta ,
                'monto_desde'=>$request->montoDesde ,
                'monto_hasta'=>$request->montoHasta,
                'cantidad_promocion'=>$request->cantidadPromocion ,
                'descuento'=>$request->descuento ,
                'Alta_Baja_Update'=>$request->abu ,
                'idestatus'=>'1' 
            ]);
                return response()->json(['success'=>'esquema promocional actualizado'],200);  
                     
    }
   public function delete(Request $request){
        $esquema=TTmpEsquemaPromocionale::where('idesqprom','=',$request->id)->where('idestatus','=','1');
       

        if($esquema->count()==0)
            return response()->json(['error'=>'esquema promocional no encontrado'],404);

            
            $esquema->update([
                'idestatus'=>'0'
            ]);
                return response()->json(['success'=>'esquema promocional eliminado'],200);  
        
   }

 
}
