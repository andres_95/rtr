<?php
namespace App\Http\Controllers\temporales;

    use Validator;
    use App\modelos\TTmpMuestra;
    use Illuminate\Http\Request;
    use Illuminate\Support\Facades\Auth;
    use App\Http\Controllers\Controller;

       class muestracontroller extends Controller{

            public function index(){

            }

            public function store(Request $request){
                TTmpMuestra::create([
                    'idfabricante' =>$request->fabricante,
                    'idRFV'=>$request->rfv,
                    'nombre_RFV'=>$request->nombre,
                    'idCliente'=>$request->cliente,
                    'nombre_cliente_Visita'=>$request->nombre_cliente,
                    'muestra'=>$request->muestra,
                    'lote'=>$request->lote,
                    'maximo_unidades'=>$request->unidades,
                    'Alta_Baja_Update'=>$request->abu,
                    'idestatus'=>'1'
                ]);
                 return response()->json(['success'=>'muestra agregada']);
            }
           public function update(Request $request){
                $muestra=TTmpMuestra::where('idRFV','=',$request->id)
                           ->orwhere('idfabricante','=',$request->id)
                           ->orwhere('idCliente','=',$request->id)
                           ->where('idestatus','=','1');
                       if($muestra->count()==0)
                          return response()->json([
                              'error'=>'no se encuentra la muestra'
                          ],404);

                          $muestra->update([
                            'idfabricante' =>$request->fabricante,
                            'idRFV'=>$request->rfv,
                            'nombre_RFV'=>$request->nombre,
                            'idCliente'=>$request->cliente,
                            'nombre_cliente_Visita'=>$request->nombre_cliente,
                            'muestra'=>$request->muestra,
                            'lote'=>$request->lote,
                            'maximo_unidades'=>$request->unidades,
                            'Alta_Baja_Update'=>$request->abu,
                            'idestatus'=>'1'
                        ]);
                           return response()->json(['success'=>'muestra actualizada'],200);
           }
        public function delete(Request $request){
               $muestra=TTmpMuestra::where('idRFV','=',$request->id)
                       ->orwhere('idfabricante','=',$request->id)
                       ->orwhere('idCliente','=',$request->id)
                       ->where('idestatus','=','1');
               
                    if($muestra->count()==0)
                        return response()->json(['error'=>'muestra no encontrada'],404);
                
                $muestra->update(['idestatus'=>'0']);

                   return response()->json(['success'=>'muestra borrada'],200);

             
          }  
        
       }