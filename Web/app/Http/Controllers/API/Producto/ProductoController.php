<?php

namespace RTR\Http\Controllers\API\Producto;

use Illuminate\Http\Request;
use RTR\Http\Controllers\Controller;
use RTR\Http\Resources\ProductoRecurso;
use RTR\modelos\TProducto;
use Carbon\Carbon;
use Validator;


class ProductoController extends Controller
{
    public function index()
    {

        $productos = ProductoRecurso::collection(TProducto::List()->get());
        return response()->json(['productos'=>$productos],200);

    }

    public function listprod(Request $request)
    {

            return ProductoRecurso::collection(TProducto::withoutGlobalScopes()->where('idcategorias',$request->categoria)->paginate(10))->response();
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function create(Request $request){

        $now = new Carbon();
        $now = Carbon::now();
    
        $fechaExpedicion = Carbon::parse($request->fechaExp);
        $fechaVencimiento = Carbon::parse($request->fechaVenc);
        $FechaInicioPublicacion = Carbon::parse($request->Finicpub);
        $FechaFinPublicacion = Carbon::parse($request->Ffinpub);

        $nuevoProducto=TProductos::create([
            'idOperador'          => $request->operador,
            'idfabricante'        => $request->fabricante,
            'idproducto'          => $request->producto,
            'idPersona'           => $request->persona,
            'idMayorista'         => $request->mayorista,
            'idpais'              => $request->pais,
            'ididioma'            => $request->idioma,
            'idmoneda'            => $request->moneda,
            'idlinea_producto'    => $request->linea,
            'idtipo_producto'     => $request->tipo,
            'nombre_producto'     => $request->nombre,
            'descripcion_producto'=> $request->descripcion,
            'principio_producto'  => $request->principio,
            'Precio_producto'     => $request->precio,
            'Descuento_producto'  => $request->descuento,
            'idcategorias'        => $request->categoria,
            'idunidades'          => $request->unid,
            'cantidad_producto_existente' => $request->cantidad,
            'fechaRegistro_producto'      => $now,
            'fechaExpedicion_producto'    => $fechaExpedicion,
            'fechaVencimiento_producto'   => $fechaVencimiento,
            'dias_publicacion'            => $request->dias,
            'idpromocion'                 => $request->promocion,
            'FechaInicioPublicacion'      => $request->finicpublicacion,
            'FechaFinPublicacion'         => $request->ffinpublicacion,
            'estatus_producto'            => $request->estatus_producto,
        ]);

        return response(["exito"=>"producto creado"],200);

    }

    public function store(Request $request)
    {
        //
    }


    public function show(Request $request){


         }


    public function edit($id)
    {
        //
    }

    public function update(Request $request)
    {
        $producto = TProductos::find($request->id);
        //hacer para descuento y precio.
        if ($request->has('cantidad'))
            $producto->update(['cantidad_producto_existente'=>$request->cantidad]);

        if ($request->has('precio'))
            $producto->update(['Precio_producto'=>$request->precio]);

        if ($request->has('descuento'))
            $producto->update(['Descuento_producto'=>$request->descuento]);

        return response(["exito"=>"el producto ha sido modificado exitosamente"],200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {   
        $target = TProductos::find($request->id)->where('estatus_producto','1');

        if (!$target)
            return response(["fracaso"=>"El producto no ha sido encontrado."],200);

        $target->update(['estatus_producto'=>'0']);

        return response(["exito"=>"el producto ha sido eliminado exitosamente"],200);
    }
}
