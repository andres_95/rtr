<?php

namespace RTR\Http\Controllers\API\Actividades;

use Carbon\Carbon;
use Illuminate\Http\Request;
use RTR\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use RTR\modelos\TPersona;
use RTR\modelos\TTipoActividade;
use RTR\modelos\TPlanificadore;
use RTR\modelos\TProducto;
use RTR\modelos\TActividadesRepresentante;
use RTR\Http\Resources\ReportesRecurso;
use Validator;

class ReporteController extends Controller
{
    public function __construct()
    {
        $this->middleware(function ($request, $next) {
            $this->user = Auth::user();
            return $next($request);
        });
    }

    public function new(Request $request)
    {

        $validator = Validator::make(
            $request->all(),
            [
                'tipo' => 'required|numeric',
                'idcliente' => 'required|string',
                'incidentes' => 'required|numeric',
                'muestras' => 'array',
            ],
            [
                'required' => ':attribute es requerido',
                'numeric'=> ':attribute de ser numerico',
                'string' => ':attribute , formato invalido'
            ]
        );

        if($validator->fails())
        {
            return response()->json($validator->errors(),400);
        }

        $date = Carbon::now();
        $date = $date->format('Y-m-d');
            
        $Planificacion = TPlanificadore::create([
                'idFabricante'=>$this->user->idFabricante,
                'idOperador'=>$this->user->idOperador,
                'idRFV'=> $this->user->idPersona,
                'idCliente'=> $request->idcliente,
                'fecha_agenda' => $date,
                'hora'=>Carbon::now(),
                'observacion_agenda' => $request['comentario']?$request['comentario']:"",
                'idSupervisor'=>$this->user->idsupervisor,
                'idestatus'=>2,
            ]);

        $actividad=TActividadesRepresentante::create([
                'idOperador'=>$this->user->idOperador,
                'idFabricante'=>$this->user->idFabricante,
                'idtipo_actividades'=>$request->tipo,
                'idPersona'=>$this->user->idPersona,
                'idRFV'=>$this->user->idPersona,
                'idCliente'=>$request->idcliente,
                'idtipo_incidentes'=>$request->incidentes,
                'fecha_actividad' => Carbon::now()->toDateTimeString(),
                'coordenadas_l'=>$request->lat,
                'coordenadas_a'=>$request->long,
                'Firma_cliente'=>$request->firma?$request->firma:null,
                'observaciones_cliente'=>$request->comentario,
                'idestatus'=>'1'
            ]);
        $Planificacion->idreporte = $actividad->idreporte;
        $Planificacion->save();
            

        foreach($request->muestras as $muestra){
            $muestraF= TProducto::find($muestra['idproducto']);
            if($muestraF->cantidad_producto_existente)
            {
            	$muestraF->cantidad_producto_existente -= $muestra['cantidad'];
            	$muestraF->save();
            }
            $actividad->Muestras()->attach($muestra['idproducto'],[
                        'cantidad'=>$muestra['cantidad'],
                        'idOperador'=>$muestraF['idOperador'],
                        'idFabricante'=>$muestraF['idfabricante']
            ]);
        }
            
           
        return response()->json(['success'=>'Actividad reportada'],200);
    }

    public function store(Request $request)
    {
        $validator = Validator::make(
            $request->all(),
            [
                'tipo' => 'required|numeric',
                'idcliente' => 'required|string',
                'idagenda' => 'required',
                'incidentes' => 'required|numeric',
                'muestras' => 'array',
                'RFV' => 'required'
            ],
            [
                'required' => ':attribute es requerido',
                'numeric'=> ':attribute debe ser numerico',
                'string' => ':attribute , formato invalido'
            ]
        );

        if($validator->fails())
        {
            return response()->json($validator->errors(),400);
        }

        $tipo=TTipoActividade::find($request->tipo);
            if($tipo==null)
                return response()->json([ "error" => "No hay actividades." ],404);

        
        $agenda=TPlanificadore::find($request->idagenda);

        $actividad=TActividadesRepresentante::create([

                'idtipo_actividades'=>$request->tipo,
                'idFabricante'=>$this->user->idFabricante,
                'idOperador'=>$this->user->idOperador,
                'idPersona'=>$this->user->idPersona,
                'idRFV'=>$request->RFV,
                'idCliente'=>$request->idcliente,
                'idtipo_incidentes'=>$request->incidentes,
                'coordenadas_l'=>$request->lat,
                'coordenadas_a'=>$request->long,
                'Firma_cliente'=>$request->firma,
                'fecha_actividad' => $agenda->fecha_agenda.' '. $agenda->hora,
                'observaciones_cliente'=>$request->comentario,
                'idestatus'=>'1'
            ]);



        if($request->has('muestras')){
            foreach($request->muestras as $muestra){
                $muestraF= TProducto::find($muestra['idproducto']);
                $muestraF->cantidad_producto_existente -= $muestra['cantidad'];
                $muestraF->save();
                $actividad->muestras()->attach($muestra['idproducto'],[
                    'cantidad'=>$muestra['cantidad'],
                    'idOperador'=>$muestraF['idOperador'],
                    'idFabricante'=>$muestraF['idfabricante']
                ]);
            }
        }


        $agenda->idreporte= $actividad->idreporte;
        $agenda->idestatus=2;
        $agenda->save();

        return response()->json(['success'=>'actividad Procesada'],200);
    }

    public function index(){

        $Empresa=TPersona::withoutGlobalScopes()
            ->where('idOperador',$this->user->idOperador)
            ->where('idgrupo_persona','FABR')
            ->select(['nombre_completo_razon_social','idPersona'])
            ->get();
        return view('Admin.ordenes.lista_reportes',compact('Empresa'));
    }

    public function all_admin(Request $request){
        $orden=TActividadesRepresentante::withoutGlobalScopes()->where('idFabricante',$request->fabricante)->get();

        return ReportesRecurso::collection($orden);
    }

    public function destroy(Request $request){
        $orden=TActividadesRepresentante::withoutGlobalScopes()->find($request->id);
        if($orden==null || $orden->idestatus==2)
            return response()->json(['error'=>'pedido no encontrado'],404);

        $orden->idestatus=2;
        $orden->save();

        return response()->json(['success'=>'pedido eliminado'],200);
    }
}
