<?php

namespace RTR\Http\Controllers\API\Actividades;

use Illuminate\Http\Request;
use RTR\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use RTR\modelos\TActividadesRepresentante;
use RTR\modelos\TPerfile;
use RTR\modelos\TPersona;
use RTR\modelos\TProducto;
use RTR\modelos\TPlanificadore;
use RTR\modelos\TTipoActividade;
use RTR\Http\Resources\ActividadesRecurso;
use RTR\Http\Resources\actividadResource as actividad;
use RTR\Http\Resources\ReporteRecurso;
use RTR\Http\Resources\MaterialesReporte;
use RTR\modelos\TBrickRutaPersona;
use Illuminate\Support\Facades\DB;
use Validator;

use Carbon\Carbon;

class ActividadController extends Controller
{
    public function __construct()
        {
            $this->middleware(function ($request, $next) {
                $this->user = Auth::user();
                $this->perfil = TPerfile::find($this->user->idperfil);
                return $next($request);
            });
        }

    public function index(Request $request){

         $actividades=TActividadesRepresentante::where('idRFV',$this->user->idPersona);

                   if($actividades->count()==0)
                      return response()->json(['error'=>'no hay reportes generados'],404);

           return response()->json(['actividades'=>ActividadesRecurso::collection($actividades->get())],200);

    }

    public function show(Request $request){
         $detalle=TActividadesRepresentante::where('idRFV','=',$this->user->idPersona)
                  ->where('idCliente','=',$request->cliente)
                  ->where('fecha_actividad','=',$request->fecha)
                  ->where('idestatus','<>','0');

                 if($detalle->count()==0)
                    return response()->json(['error'=>'disculpe esta actividad es inexistente'],404);

              return new actividad($detalle->get());
    }

    public function saveFirma(Request $request){
        $file = $request->file('imagen');

            $contents = $file->openFile()->fread($file->getSize());

            $actividad=TActividadesRepresentante::where('idReporte','=',$request->idreporte)->where('idestatus','=','1');

                if($actividad->count()==0)
                    return response()->json(['error'=>'actividad no encontrada'],404);

            $actividad->update([
                'Firma_cliente'=>$contents
            ]);

            return response()->json(['nombre'=>'firma registrada. Veamos si funciona'],200);
    }

    public function loadFirma(Request $request){

        $data=$request->idreporte;
           $actividad=TActividadesRepresentante::where('idReporte','=',$request->idreporte)->where('idestatus','=','1');

                     if($actividad->count()==0)
                         return response()->json(['error'=>'actividad no encontrada'],404);

            $img = Image::make($actividad->first()->Firma_cliente);

              $data=base64_encode($actividad->first()->Firma_cliente);
               return response()->json(['success'=>'firma cargada',
                                       'imagen previa'=>$data],200);

    }

    

    public function update(Request $request){   
    }

    public function viewFirma(Request $request){
         $data=base64_decode($request->firma);
         $img=Image::make($data);
            return $img->response();
    }

    public function actividades(Request $request){
              if($request->has('ruta'))
                 $personas=ruta::where('idbrick','=',$request->ruta)
                          ->where('idgrupo_persona','=','RFV')
                          ->first()->personas();
              else  {
                 $rfvs=TGrupopersona::where('idgrupo_persona','=','RFV')->first()
                        ->personas();
                    }

           $actividades=TActividadesRepresentante::all();

                 if($request->has('ciclo') && $rfvs->count()>0)
                      $rfvs=$rfvs->where('idciclos','=',$request->ciclo);

                 if($request->has('idPersona' ) && $rfvs->count()>0)
                    $rfvs=$rfvs->where('idPersona','=',$request->idPersona)->first();



                if($request->has('fecha_minimo') && $request->has('fecha_maximo') && $rfvs->count()>0){


                     $actividades=$actividades ->where('fecha_actividad','>=',$request->fecha_minimo)
                     ->where('fecha_actividad','<=',$request->fecha_maximo);
                }


                if($request->has('muestra'))
                   $actividades=$actividades->where('muestra','=',$request->muestra);

               if($request->has('actividad'))
                  $actividades=$actividades->where('idtipo_actividades','=',$request->actividad);

               if($request->has('situacion'))
                  $actividades=$actividades->where('idtipo_incidentes','=',$request->situacion);

              if($request->has('mayorista'))
                  $actividades=$actividades->where('idCliente','=',$request->mayorista);


                $actividades->whereIn('idPersona',$rfvs->get(['idPersona']));

              //   dd($actividades);

           return response()->json(['actividades'=>$actividades->toArray()],200);

    }

    public function firmadas(Request $request){
        $firmadas=TPersona::where('idPersona','=',$request->rfv)
                  ->where('idestatus','=','1')
                  ->Actividades()
                  ->where('idestatus','=','5')->get();
          return vista::collection($firmadas);
    }

    public function detalleReporte(Request $request){
        

        $validator = Validator::make(
            $request->all(),
            [
                'reporte' => 'required|numeric',
                'rfv'=> 'required'
            ],
            [
                'required' => ':attribute es requerido',
                'numeric'=> ':attribute debe ser numerico',
            ]
        );

        if($validator->fails())
        {
            return response()->json($validator->errors(),400);
        }


        $actividad = TActividadesRepresentante::withoutGlobalScopes()->where('idPersona',$request->rfv)->find($request->reporte,[
            'idCliente','idreporte','idtipo_actividades','fecha_actividad','observaciones_cliente','Firma_cliente'
        ]); 
        $actividad['cliente']=TPersona::withoutGlobalScopes()->find($actividad['idCliente'])['nombre_completo_razon_social'];
        $actividad['actividad']=TTipoActividade::withoutGlobalScopes()->find($actividad['idtipo_actividades'])['descripcion_tipo_actividades'];
        $muestras = collect([]);
        $estado = TTipoActividade::withoutGlobalScopes()->where('descripcion_tipo_actividades','ENTREGA MATERIALES')->first();
        if ( $actividad['idtipo_actividades'] == $estado->idtipo_actividades ){
            foreach($actividad->Muestras()->withoutGlobalScopes()->get() as $muestra)
                $muestras->push([
                    'codigo'=>$muestra['idproducto'],
                    'descripcion'=>$muestra['nombre_producto'],
                    'cantidad'=>$muestra->pivot['cantidad'],
                    'lote'=>$muestra['lote']
                    ]);
            $actividad['muestras']=$muestras;
        }    
        return $actividad;
    }



    /**
     * Consulta personalizada de reportes del rfv por fecha, actividad y productos
     * si la actividad es entrega de muestras, ademas de la zona brick.
     */
    public function consultaReporte(Request $request)
    {

      $validator = Validator::make($request->all(),[
        'actividad' => 'present',
        'brick' => 'present',
        'especialidad' => 'present',
        'fechaInicio' => 'required|date|before:tomorrow',
        'fechaFin' => 'required|date|after_or_equal:fechaInicio',
        'incidente' => 'present',
        'ranking' => 'present',
        'rfv' => 'present',
        'zona' => 'present'
      ]);

      if($validator->fails())
        {
            return response()->json($validator->errors(),400);
        }

    $value= '';
    if($this->perfil->tipo == 'ADM' || $this->perfil->tipo == 'OPE'){
        $value = $request->session()->get('user.teams');
    }else
        $value=$this->user->idFabricante;
        
    //dd($value);
      $subquery = "( select nombre_completo_razon_social, idactividad_negocio,idestado,idciudad, idranking, t_personas.idPersona from t_personas";

     if($request->zona != null || $request->brick != null)
      {
        $subquery.=" inner join t_brick_ruta_personas on t_personas.idPersona=t_brick_ruta_personas.idPersona";
        if ($request->zona != null) {
            $auxz=[];
            foreach($request->zona as $item){
                array_push($auxz,"'".$item['idzona']."'");
            }
          $subquery.= " and t_brick_ruta_personas.idzona IN (".implode(",",$auxz).")";
        }
        if ($request->brick != null) {
            $auxb=[];
            foreach($request->brick as $item){
                array_push($auxb,"'".$item['idbrick']."'");
            }
          $subquery.= " and t_brick_ruta_personas.idbrick IN (".implode(",",$auxb).")";
        }
        $subquery .= " and t_personas.idOperador = '".$this->user->idOperador."'";
        $subquery .= " and t_personas.idFabricante = '".$value."'";

      }else
      {
        $subquery .= " where idOperador = '".$this->user->idOperador."'";
        $subquery .= " and idFabricante = '".$value."'";
      }


      if ($request->especialidad != null) {
          $auxe=[];
          foreach($request->especialidad as $item){
              array_push($auxe,"'".$item['id']."'");
          }
        $subquery.= " and t_personas.idactividad_negocio IN (".implode(",",$auxe).")";
      }

      if ($request->ranking != null) {
          $auxr=[];
          foreach($request->ranking as $item){
              array_push($auxr,"'".$item['id']."'");
          }
        $subquery.= " and t_personas.idranking IN (".implode(",",$auxr).")";
      }

      $subquery .= ' ) Q ';

      $query = "t_actividades_representante inner join ".$subquery." on t_actividades_representante.idCliente=Q.idPersona";

      //return $query;  
      $result = DB::table(DB::raw($query))
                ->whereRAW("DATE(fecha_actividad) BETWEEN '".$request->fechaInicio." 00:00' AND '".$request->fechaFin." 23:59'");

      if($request->actividad != null){
          $auxa=[];
          foreach($request->actividad as $item){
              array_push($auxa,$item['idtipo_actividades']);
          }
          $result->whereIn('idtipo_actividades',$auxa);
      }
      if($request->incidente != null){
          $auxi=[];
          foreach($request->incidente as $item){
              array_push($auxi,$item['idtipo_incidentes']);
          }
          $result->whereIn('idtipo_incidentes',$auxi);
      }

      if($request->rfv != null){
          $aux=[];
          //dd($aux,$request->rfv['idPersona']);
          if($this->perfil->tipo!='RFV'){
              foreach($request->rfv as $item){
                  array_push($aux,$item['idPersona']);
              }
          }else{
              array_push($aux,$request->rfv['idPersona']);
          }
          //dd($aux,$request->rfv['idPersona']);
        $result->whereIn('idRFV',$aux);
      }else
      {
          if($this->perfil->tipo=='OPE' || $this->perfil->tipo=='ADM')
              $rfvs= TPersona::withoutGlobalScopes()->where('idFabricante',$request->session()->get('user.teams'))->TypeRFV()->get()->toArray();
          else
            $rfvs= TPersona::where('idsupervisor',$this->user->idPersona)->get()->toArray();

          $in = [];
          foreach ($rfvs as $key => $value) {
            array_push($in, $value['idPersona']);
          }
          array_push($in, $this->user->idPersona);
          $result->whereIn('idRFV',$in);
      }



      $result = $result->get();
      $entregas = [];

      foreach ($result as $key => $value) {
          array_push($entregas, $value->idreporte);
      }

        $select = DB::raw('idreporte as Reporte,nombre_producto as Material, cantidad as Cantidad, lote as Lote ');
        $materiales = DB::table('r_actividades_productos')
            ->select($select)
            ->whereIn('idreporte',$entregas)
            ->join('t_productos','t_productos.idproducto','=','r_actividades_productos.idproducto')
            ->get();





        return response()->json(['reportes'=>ReporteRecurso::collection($result), 'materiales'=>$materiales]);

      /*
      $query = TActividadesRepresentante::where('idRFV',$request->rfv)
                                        ->whereBetween('fecha_actividad', [$request->fechaInicio, $request->fechaFin]);

      $keys = [
        'actividad' => 'idtipo_actividades',
        'incidente' => 'idtipo_incidentes'
      ];

      foreach ($request->only(['actividad','incidente']) as $key => $value) {
        if ($value) {
          $query->where($keys[$key],$value);
        }
      }

      
      $query->join('t_personas', function ($join)  use($request){
              $join->on('t_actividades_representante.idCliente', '=', 't_personas.idPersona');
              if ($request->especialidad) {
                $join->where('t_personas.idactividad_negocio', $request->especialidad);
              }
              if($request->ranking)
              {
                $join->where('t_personas.idranking',$request->ranking);
              }
              /*$join->whereExists(function ($subquery) use($request) {
                        $subquery->from('t_brick_ruta_personas');
                        if($request->zona)
                          $subquery->where('idzona',$request->zona);
                        if ($request->brick) {
                          $subquery->where('idbrick',$request->brick);
                        }
              });
      });/*


      /*$query->whereHas('Cliente', function ($subquery) use($request) {
          if($request->especialidad)
            $subquery->where('idactividad_negocio', $request->especialidad);
          if($request->ranking)
            $subquery->where('idranking',$request->ranking);
      });*/

      /*$query->select([
        'idreporte',
        'idCliente',
        'nombre_completo_razon_social',
        'idRFV',
        'idtipo_actividades',
        'idactividad_negocio',
        'idranking',
        'idtipo_incidentes',
        'observaciones_cliente',
        'Firma_cliente',
        'fecha_actividad'
      ]);*/


      //return ReporteRecurso::collection($query->get());
    }
}