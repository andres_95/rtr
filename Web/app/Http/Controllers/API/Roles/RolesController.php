<?php

namespace RTR\Http\Controllers\API\Roles;

use Validator;

use RTR\modelos\TRole;

use Illuminate\Http\Request;
use RTR\Http\Controllers\Controller;

use Exception;


class RolesController extends Controller{

    public function count (){
        $roles = TRole::all();
        $respuesta = [];
        foreach($roles as $key=>$rol){
            $respuesta[ $rol ['descripcion'] ] = TRol::find($rol['id'])->persona()->all()->count();
        }
        return response (['Roles' => $respuesta]);
    }

    public function destroy(Request $request){

        $validar=Validator::make($request->all(),[
            'idRol'=>'required|int|exists:descripcionRol',
        ],
            [
                'required'=>'Se necesita el :attribute'
            ]
        );
        $rol=TRol::where('idRol','=',$request->id)->where('estatus','=','1');
        $rol->update(['estatus'=>'0']);
        return response('esta elimina el item',200);
    }

    public function show (){
        return response(["roles" => TRole::all(['idrol','descripcion_rol'])],200);

    }

    public function index(){

        $roles=TRole::all(['idrol','descripcion_rol']);
        foreach ($roles as $item){
            $item->id=$item->idrol;
            $item->descripcion=$item->descripcion_rol;
            $item->alias='';
        }
        $T="Roles";
        return view('Admin.Seguridad.rolesperfil',compact('roles','T'));
    }

}
?>