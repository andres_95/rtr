<?php

namespace RTR\Http\Controllers\API\Notificaciones;

use Illuminate\Http\Request;
use RTR\modelos\TNotificacion;
use RTR\Http\Controllers\Controller;
use Illuminate\Support\Facades\Notification;
use RTR\Http\Resources\NotificacionRecurso as notificaciones;
use Illuminate\Support\Facades\Auth;
use Validator;


class NotificacionController extends Controller
{
    
       public function __construct(){

        $this->middleware(function ($request, $next) {
            $this->user = Auth::user();
            return $next($request);
        });
       }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
          $validator=Validator::make($request->all(),[
              'descripcion'=>'required'     
          ]);
              if($validator->fails())
                 return response()->json(['error'=>$validator->errors()->all()],404);
        
        //return response()->json(['success'=>$this->user->idOperador.'RFV1'],200);
        if($request->idrfv!=""){
            $rfv=$request->idrfv;
        }else{
            $rfv=$this->user->idOperador.'RFV3';
        }

        TNotificacion::create([
            'idOperador'=>$this->user->idOperador,
            'idFabricante'=>$request->idfabricante,
            'idPersona'=>$rfv,
            'descripcion_notoficacion'=>$request->descripcion,
            'idestatus'=>'0',
            'idtipo'=>1
        ]); 
           return response()->json(['success'=>'notificacion creada'],200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    public function notificar(Request $request){
          $validator=Validator::make($request->all(),[
              'idnotificacion'=>'required',
              'actividad_notificacion'=>'required'
          ]);
              if($validator->fails())
                 return response()->json(['error'=>$validador->errors()->all()],200);

        $notificacion=TNotificacion::find($request->idnotificacion);
        $notificacion->idestatus=$request->actividad_notificacion;
        $notificacion->save();

          return response()->json(['success'=>'listo'],200);
             
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        //

        $validator=Validator::make($request->all(),[
            'descripcion'=>'required',
            'id'=>'required'     
        ]);
            if($validator->fails())
               return response()->json(['error'=>$validator->errors()->all()],404);
               
          $notificacion=TNotificacion::where('id','=',$request->idnotificacion)
                          ->where('idestatus','=','1');
                        
                         
                      

                if($notificacion->count()==0)
                    return response()->json(['error'=>'Notificacion no encontrada'],404);
                
                $notificacion->update([
                    'idRFV'=>($request->has('idrfv'))?$request->idrfv:'General',
                    'descripcion_notoficacion'=>$request->descripcion
                ]); 

                   return response()->json(['success'=>'Notificacion actualizada'],200);
            }
        public function delete(Request $request){
               
            $validator=Validator::make($request->all(),[
                'id'=>'required'     
            ]);
                        
            if($validator->fails())
            return response()->json(['error'=>$validator->errors()->all()],404);

            $notificacion=TNotificacion::where('id','=',$request->idnotificacion)
            ->where('idestatus','=','1');
                   

                    if($notificacion->count()==0)
                        return response()->json(['error'=>'Notificacion no encontrada'],404);
                    
                    $notificacion->update([
                        'idestatus'=>'0'
                    ]); 

                    return response()->json(['success'=>'Notificacion actualizada'],200);
        }

      public function index(Request $request){
           $notificaciones=TNotificacion::where('idPersona','=',$this->user->idPersona);
             if($notificaciones->count()==0)
                return response()->json(null,404);
               return notificaciones::collection($notificaciones->get());
              //  return response()->json(['notificaciones'=>$notificaciones->get()],200);
      }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
