<?php

namespace RTR\Http\Controllers\API\Persona;

use Illuminate\Http\Request;
use RTR\Http\Controllers\Controller;
use RTR\Http\Resources\MayoristaRecurso;
use RTR\modelos\TPersona;

class MayoristaController extends Controller
{
    public function index()
    {
        return response(['Mayoristas'=>MayoristaRecurso::collection(TPersona::TypeMayorista()->get())],200);
    }
}
