<?php

namespace RTR\Http\Controllers\API\Persona;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use RTR\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use RTR\modelos\TPerfile;
use RTR\modelos\TPersona;
use Validator;

class ClienteController extends Controller
{
    
    public function __construct()
    {
        $this->middleware(function ($request, $next) {
            $this->user = Auth::user();
            $this->perfil = TPerfile::find($this->user->idperfil)->tipo;
            return $next($request);
        });
    }


    public function index(Request $request)
    {
        if ($this->perfil=='RFV')
                $clientes =  $this->user->Clientes()->get(['idPersona','nombre_completo_razon_social']);
        else if ($this->perfil=='SUP')
                $clientes = TPersona::TypeCliente()->get(['idPersona','nombre_completo_razon_social']);

        return response()->json(['clientes'=>$clientes],200); 
    }


     public function search(Request $request){

        $validator = Validator::make($request->all(),[
            'data'=>'required'
        ],
            [
                'data.required'=> 'La data es requerida'
            ]);

        if ($validator->fails()) {
            return response()->json($validator->errors()->all(), 400);
        }


            $user = Auth::user();
            $data = $request->data;
            if ($this->perfil=='RFV')
                $clientes =  $user->Clientes()->Search($data)->get(['idPersona','nombre_completo_razon_social']);
            else if ($this->perfil=='SUP')
                $clientes = TPersona::TypeCliente()->Search($data)->get(['idPersona','nombre_completo_razon_social']);
            return response()->json($clientes,200);


    }

    public function show(Request $request)
    {
        $persona=TPersona::select('idPersona','nombre_completo_razon_social')
            ->where('idPersona','=',$request['data'])->first();

        if($persona->count()==0)
            return $this.search($request);
    }

    public function SearchByID(Request $request)
    {
       $validator= Validator::make($request->all(),
            [
                'id' => 'required|string',
            ]);
        if ($validator->fails()) {
            return response()->json("Data invalida", 400);
        }

        $cliente = TPersona::find($request['id']);
        return response()->json($cliente,200);
    }

    public function SearchByName(Request $request)
    {
        return $this.search($request);
    }


    public function get_materiales(Request $request)
    {
        $validator= Validator::make($request->all(),
            [
                'id' => 'required|string',
            ]);
        if ($validator->fails()) {
            return response()->json("Data invalida", 400);
        }
        
        $productos = TPersona::find($request['id'])->Materiales()->get();

        foreach($productos as $p)
        {
            $p['idproducto']=$p->pivot->idproducto;
        }
        return response()->json(['materiales'=>$productos]);
    }

    public function get_clientes_by_rfv()
    {

        $clientes = $user->Clientes()->get(['idPersona','nombre_completo_razon_social']);
        if($clientes)
            return response()->json($clientes,200);
        else
            return response()->json(["Error"=>"no se poseen clientes"],404);
    }

    public function muestracount()
    {
        $clientes = $user->Clientes()->get(['idPersona'])->toArray();
        $list=DB::table('r_clientes_materiales')->whereIn('idcliente',$clientes)->get();
        $list->count();
        return response($list);
    }
}
