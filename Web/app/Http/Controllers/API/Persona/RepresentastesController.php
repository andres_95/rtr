<?php

namespace RTR\Http\Controllers\API\Persona;

use Illuminate\Http\Request;
use RTR\Http\Controllers\Controller;
use RTR\modelos\TGrupopersona;
class RepresentastesController extends Controller
{
    public function index(){
            $representantes=TGrupopersona::where('idgrupo_persona','=','RFV')->first()->Personas()
            ->get(['idPersona','nombre_completo_razon_social']);

                if($representantes->count()==0)
                  return response()->json(['error'=>'no hay representantes de ventas'],404);

              return response()->json(['representantes'=>$representantes],200);
    }
}
