<?php

namespace RTR\Http\Controllers\API\Ordenes;

use Illuminate\Http\Request;
use RTR\Http\Controllers\Controller;

use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use RTR\Http\Resources\OrdenRecursoConciliar;
use RTR\Http\Resources\FacturaRecurso;
use RTR\modelos\TOrdene;
use RTR\modelos\TFactura;
use RTR\modelos\TPerfile;
use RTR\modelos\TPersona;
use Illuminate\Support\Facades\DB;
use RTR\modelos\TProducto;
use RTR\modelos\TEstatusOrdene;
use RTR\modelos\TEsquemaPromocionale;
use Validator;


use RTR\Http\Resources\OrdenRecurso;

class FacturarController extends Controller
{
    //

	public function __construct()
    {
        $this->middleware(function ($request, $next) {
            $this->user = Auth::user();
            $this->perfil = TPerfile::find($this->user->idperfil);
            return $next($request);
        });
    }

   private function descEsquema($fabricante,$mayorista,$id,$cantidad,$monto){
           
                $esquema=TEsquemaPromocionale::withoutGlobalScopes()
                        ->where('idProducto',$id)
                        ->where('idFabricante',$fabricante)
                        ->where('idMayorista',$mayorista)
                        ->where('idestatus',1)
                        //;
                        ->get();
                $descuento_esquema=0;
                $descuento_productos=0;
                $cantidad=0;
                //  dd($esquema->count());
                foreach($esquema as $key=>$value){
                          dd(($value['cantidad_desde']>=$cantidad && $cantidad<=$value['cantidad_hasta']));
                        if(($value['cantidad_desde']>=$cantidad && $cantidad<=$value['cantidad_hasta'])
                                ||
                                ($value['monto_desde']>=$monto && $monto<=$value['monto_hasta'])
                            ){
                            if($value['porcentaje']>0){
                                $descuento_esquema=$monto*$value['porcentaje']/100;
                                $descuento_productos+=$descuento_esquema;
                            }
                            if($value['cantidad']>0){
                                $cantidad+=$value['cantidad'];
                            }
                            }
                }
           return ['descuento'=>$descuento_productos,'cantidad'=>$cantidad];
   } 
      private function descMayorista($fabricante,$mayorista,$id,$cantidad,$monto){
           $esquema=array('cantidad'=>0,'descuento'=>0);
            // dd($mayorista);
            foreach($mayorista as $itemMayorista){
                $item=$this->descEsquema($fabricante,$itemMayorista['idPersona'],$id,$cantidad,$monto); 
                $esquema['cantidad']+=$item['cantidad'];
                $esquema['descuento']+=$item['descuento'];
               // echo 'entrando';
            }   

             return $esquema;
      }
   
    public function conciliarFactura (Request $request){

        $validar=Validator::make($request->all(),[
                'norden'=>'required|numeric',
                'nfactura' =>'required',
                'formaPago'=>'required|numeric',
                'almacen'=>'required|string',
                'productos'=>'required|array',
            ],
            [
                'required'=>'Se necesita el :attribute',
                'numeric'=> 'El :attribute debe ser entero',
                'string'=>'El formato de :attribute no es valido',
                'array'=> 'El :attribute debe ser un array valido'
        ]);

        if($validar->fails()){
            return response()->json($validar->errors()->all(), 400);
        }

        $orden = TOrdene::withoutGlobalScopes()->find($request->norden);

        if(!$orden) return response()->json(["Orden no encontrada"],404);

        $estatus = TEstatusOrdene::withoutGlobalScopes()->find($orden->idestatus);


        if (!$estatus || $estatus->descripcion=="Facturada") {

            return response(["Invalido"=>"Esta orden fue facturada previamente."],200);
        }

        if (!$estatus || $estatus->descripcion=="Anulada") {

            return response(["Invalido"=>"Esta orden fue anulada previamente."],200);
        }


        $mayoristas = $orden->Mayoristas()->withoutGlobalScopes()->get();
        $descuento_mayorista= $mayoristas[0]->pivot->item_descuento;
        if($orden['idMayorista']) $mayoristas = $mayoristas->diff([$mayoristas->find($orden->idMayorista)]);


        $ordenProductoFaltante = collect([]);
        $itemProducto= $orden->Productos()->withoutGlobalScopes();
        $descuento_productos=0;
        $descuento_esquema=0;
        $TotalC=0;
        $TotalF=0;
        $total=0;
        $totalnueva=0;
        foreach ($request->productos as $key => $value) {
           
            $itemProducto->updateExistingPivot($value['id'], [
                                'cantidad_conciliada'=>$value['conciliada'],
                                'item_total'=>$value['SubTotal'],
                                'item_descuento'=>$value['descuento'],
                                'cantidad_faltante'=>$value['Faltante'],
                                'item_price'=>$value['precio'],
                            ]);
            $total+=$value['SubTotal'];
            $TotalC+=$value['conciliada'];
            $TotalF+=$value['Faltante'];
            $desc=$value['precio']*$value['descuento']/100;
            $descuento_productos+=$desc;
            /*  if(($esquema['cantidad_desde']>=$value['conciliada'] && $value['conciliada']<=$esquema['cantidad_hasta'])
                ||
                ($esquema['monto_desde']>=$value['SubTotal'] && $value['SubTotal']<=$esquema['monto_hasta'])
               ){
                  $descuento_esquema=$value['precio']*$esquema['porcentaje']/100;
                  $descuento_productos+=$descuento_esquema;
               }*/
             //  dd($orden->Mayoristas()->withoutGlobalScopes()->count());
               $esquema=$this->descMayorista($orden->idFabricante,$mayoristas,$value['id'],$value['conciliada'],$value['SubTotal']);
            //  dd($esquema);
               if ($desc==0) {
                $desc=$value['precio']*$descuento_mayorista/100;
                $descuento_productos+=$desc+$esquema['descuento'];
                
            }


            $produc = $orden->Productos()->withoutGlobalScopes()->find($value['id']);
               $result=$produc->cantidad_producto_existente - $value['conciliada'] - $esquema['cantidad'];
            $produc->update([
                'cantidad_producto_existente'=>($result<0)?0:$result
            ]);

            if($value['Faltante'])
            {
            	$produc->pivot->newcosto= ($value['precio']*$value['Faltante']);
            	$totalnueva+=$produc->pivot->newcosto;
                $ordenProductoFaltante->push($produc->pivot);
            }
        }

        $imp = $request->impuesto?$request->impuesto:12;

        TFactura::create([
            'idOperador' =>$orden->idOperador,
            'idFabricante' =>$orden->idFabricante,
            'idfactura'=>$request->nfactura,
            'idordenes'=>$orden->idorden,
            'idPersona_solicitante'=>$orden->idPersona_solicitante,
            'idformaPago'=>$request->formaPago,
            'fechaFactura'=>$request->fecha?$request->fecha:Carbon::now(),
            'Fecha_pago_factura'=>$request->fecha?$request->fecha:Carbon::now(),
            'DireccionCliente'=>TPersona::withoutGlobalScopes()->find($orden->idPersona_solicitante)['direccion_domicilio'],
            'ReferenciaFactura'=>$orden->idorden,
            'DescripcionFactura'=>$request->descripcion?$request->descripcion:"",
            'Impuesto_iva'=>$imp,
            'Descuento'=>$descuento_productos,
            'Porc_Descuento'=>$descuento_mayorista,
            'MontoNeto_Factura'=>($total*$imp/100)+$total,
            'Monto_Factura'=>($total*$imp/100)+$total-$descuento_productos,
            'idMayorista_despacho'=>$orden->idMayorista,
            'idestatus'=>1,
            'cantidad_Faltante'=>$TotalF,
            'cantidad_Pagada'=>$TotalC
        ]);


        $current = Carbon::now();
      
        $orden->update([
            "idfactura" => $request->nfactura,
            'idestatus' => TEstatusOrdene::withoutGlobalScopes()
                                            ->where('descripcion','Facturada')
                                            ->where('idFabricante',$orden->idFabricante)
                                            ->first()->idestatus,
            'registro_entrega' => $current,
            'fecha_entrega' => $current,
        ]);
        

        if ( $mayoristas->count()=== 0 || $ordenProductoFaltante->count() === 0)
            return response(["exito"=>"La orden ha sido conciliada al completo"],200);

        return $this->generar_nueva($orden,$ordenProductoFaltante,$mayoristas,$totalnueva,$imp);

    }

    public function generar_nueva($orden,$productos,$mayoristas,$totalnueva,$impuesto=12){

    	 $current = Carbon::now();
        $costo=$totalnueva;
        $registrada = TEstatusOrdene::withoutGlobalScopes()->where('idFabricante',$orden->idFabricante)->wherein('descripcion',['Generada','Generadas','generada','generadas'])->first();
        $nuevaOrden=TOrdene::create([
            'idOperador'=>$orden->idOperador,
            'idFabricante'=>$orden->idFabricante,
            'idPersona'=>$orden->idPersona,
            'idpasadopor'=> $orden->idpasadopor,
            'idMayorista'=>$mayoristas[0]['idPersona'],
            'idPersona_solicitante'=>$orden->idPersona_solicitante,
            'idpais'=>$orden->idpais,
            'costoTotal'=>$costo,
            'coordenadas_l' =>$orden->lat,
            'coordenadas_a'=>$orden->lon,
            'ididioma'=>$orden->ididioma,
            'idmoneda'=>$orden->idmoneda,
            'tipo_operacion'=>1,
            'impuesto'=>$impuesto,
            'fechaOrden'=>$current,
            'fecha_envio_orden'=>$current,
            'idestatus'=>$registrada->idestatus,
            'TotalUnidades'=>0
        ]);

        $costo=0;
        foreach( $productos as $index=>$item ) {
            $nuevaOrden->Productos()->attach(
                $item['idproducto'],[
                'idOperador'=>$item['idOperador'],
                'idFabricante'=>$item['idFabricante'],
                'idMayorista'=>$mayoristas[0]['idPersona'],
                'iditem_orden'=>$index+1,
                'item_price'=>$item['item_price'],
                'nombreproducto'=>$item['nombreproducto'],
                'cantidad_solicitada'=>$item['cantidad_faltante'],
                'idunidades'=>$item['idunidades'],
                'item_descuento' => $item['item_descuento'],
                'item_total' => $item['newcosto'],
                'item_impuesto' => $impuesto,
                'idestatus' => 1
            ]);
            $nuevaOrden->TotalUnidades+=$item['cantidad_faltante'];
            $costo+=$item['item_price']*$item['cantidad_faltante'];
        }

        foreach( $mayoristas as $mayo ) {
            $total = $costo;
            $nuevaOrden->Mayoristas()->attach(
                $mayo['idPersona'],[
                'nombre_mayorista'=>$mayo['nombre_completo_razon_social'],
                'idOperador'=>$mayo['idOperador'],
                'idFabricante'=>$mayo['idFabricante'],
                'idpais'=>$mayo['idpais'],
                'orden_total'=>$total,
                'impuesto'=>$impuesto,
                'idCliente'=>$mayo['pivot']['idCliente'],
                'idmoneda'=>$mayo['idmoneda'],
                'ididioma'=>$mayo['ididioma'],
                'item_descuento' => $mayo['descuento'],
                'idestatus' => 1
            ]);
        }
        $nuevaOrden->save();
        $Request = new Request();
       return new OrdenRecursoConciliar($nuevaOrden);
    }


    public function show(Request $request)
    {
        $validar=Validator::make($request->all(),[
                'id'=>'required'
            ],
            [
                'required'=>'Se necesita el :attribute'
        ]);

        if($validar->fails()){
            return response()->json($validar->errors()->all(), 400);
        }
        $orden = TOrdene::withoutGlobalScopes()->find($request->id);
        if(!$orden) return response()->json("Not Found",404);

        return new OrdenRecursoConciliar($orden);

    }


    public function consultaFactura(Request $request)
        {
        $validator = Validator::make($request->all(),[
        'fechaInicio' => 'required|date|before:tomorrow',
        'fechaFin' => 'required|date|after_or_equal:fechaInicio'
        ]);
        if($validator->fails())
        {
        return response()->json($validator->errors(),400);
        }

        $total = 0;
        $unidades = 0;

        $value= '';
        if($this->perfil->tipo == 'ADM' || $this->perfil->tipo == 'OPE'){
            $value = $request->session()->get('user.teams');
        }else
            $value=$this->user->idFabricante;
        //dd($request->rfv);
        if ($request->rfv != null)
        {
            $aux=[];
            if($this->perfil->tipo!='RFV'){
                foreach($request->rfv as $item){
                    array_push($aux,$item['idPersona']);
                }
            }else{
                array_push($aux,$request->rfv['idPersona']);
            }

        $ordenes = TOrdene::whereIn('idPersona',$aux)->pluck('idorden');
        $facturas = TFactura::whereIn('idordenes',$ordenes);
        }else if($this->perfil->tipo=='OPE' || $this->perfil->tipo=='ADM'){
            $facturas = DB::table("t_personas")
                ->where('t_ordenes.idFabricante',$value)
                ->join("t_ordenes","t_personas.idPersona","=",'t_ordenes.idPersona')
                ->select('t_ordenes.idorden')
                ->join('t_facturas','t_ordenes.idorden','t_facturas.idordenes')
                ->select('t_facturas.*');
        }else
        $facturas = DB::table("t_personas")
        ->where('idsupervisor',$this->user->idPersona)
        ->join("t_ordenes","t_personas.idPersona","=",'t_ordenes.idPersona')
        ->select('t_ordenes.idorden')
        ->join('t_facturas','t_ordenes.idorden','t_facturas.idordenes')
        ->select('t_facturas.*');

        $facturas = $facturas->whereBetween('fechaFactura',[$request->fechaInicio,$request->fechaFin]);
        /**
        * busqueda por numero de factura
        **/
        if ($request->nfactura != "")
        $facturas = TFactura::where('idfactura',$request->nfactura);
        /**
        * busqueda por estatus de factura
        **/
        if ($request->estatus != null){
            $aux2=[];
            foreach ($request->estatus as $item){
                array_push($aux2,$item['id']);
            }
            //dd($aux2);
            $facturas = $facturas->whereIn('t_facturas.idestatus',$aux2);
        }

        $facturas = $facturas->get();

        if ($facturas->isNotEmpty())
        {
        /*foreach($facturas as $fact)
        {
        $total+=$fact->Monto_Factura;
        $unidades+=$fact->cantidad_Pagada;
        }*/
        return response(array("facturas"=>(FacturaRecurso::collection($facturas)),"total"=>$total ,"unidades"=>$unidades),200);
        } else 
        return response(["mensaje"=>"No hay facturas que cumplan con estos parametros"],200);
    }


    public function destroy(Request $request){
        $factura=TFactura::find($request->id);
           
           if($factura==null || $factura->idestatus==0)
            return response()->json(['error'=>'factura no encontrada'],404);

        $factura->idestatus=0;
        $factura->save();

            return response()->json(['success'=>'factura eliminada'],200);
    }

}
