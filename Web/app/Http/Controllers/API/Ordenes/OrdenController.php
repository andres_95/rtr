<?php

namespace RTR\Http\Controllers\API\Ordenes;

use Illuminate\Http\Request;
use RTR\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use RTR\modelos\TPerfile;
use RTR\modelos\TEstatusOrdene;
use RTR\modelos\TOrdene;
use RTR\modelos\TPersona;
use Illuminate\Support\Facades\DB;
use RTR\Http\Resources\OrdenRecurso;
use RTR\Http\Resources\OrdenCollectionRecurso;
use RTR\Http\Resources\OrdenRecursoConciliar;
use RTR\Http\Resources\OrdenDetalle;
use Validator;
use Carbon\Carbon;


class OrdenController extends Controller
{

	public function __construct()
    {
        $this->middleware(function ($request, $next) {
            $this->user = Auth::user();
            $this->perfil = TPerfile::find($this->user->idperfil);
            return $next($request);
        });
    }

    public function index(){

        $Empresa=TPersona::withoutGlobalScopes()
                                ->where('idOperador',$this->user->idOperador)
                                ->where('idgrupo_persona','FABR')
                                ->select(['nombre_completo_razon_social','idPersona'])
                                ->get();
        return view('Admin.ordenes.lista_ordenes',compact('Empresa'));
    }

    public function index_json(){
        $ordenes = OrdenCollectionRecurso::collection(TOrdene::where('idPersona',$this->user->idPersona)->get());
        return response()->json($ordenes,200);
    }

    public function show(Request $request)
    {
        $validar=Validator::make($request->all(),[
                'id'=>'required'
            ],
            [
                'required'=>'Se necesita el :attribute'
        ]);

        if($validar->fails()){
            return response()->json($validar->errors()->all(), 400);
        }
        $orden = TOrdene::withoutGlobalScopes()->find($request->id);
        if(!$orden) return response()->json("Not Found",404);

        return new OrdenDetalle($orden);
    }

    public function all_admin(Request $request){
        $orden=TOrdene::withoutGlobalScopes()->where('idFabricante',$request->fabricante)->get();
        
        return OrdenRecursoConciliar::collection($orden);
    }


    public function FilterEstatus(Request $request)
    {
        $validar=Validator::make($request->all(),[
                'id'=>'required'
            ],
            [
                'required'=>'Se necesita el :attribute'
        ]);

        $value= '';
        if($this->perfil->tipo == 'ADM' || $this->perfil->tipo == 'OPE'){
            $value = $request->session()->get('user.teams');
        }else
            $value=$this->user->idFabricante;

        if($validar->fails()){
            return response()->json($validar->errors()->all(), 400);
        }
        if ($this->perfil->tipo=='RFV') {
            return OrdenRecurso::collection(TOrdene::where('idestatus',$request->id)->where('idPersona',$this->user->idPersona)->get());
        }elseif($this->perfil->tipo=='OPE' || $this->perfil->tipo=='ADM')
            $rfvs= TPersona::withoutGlobalScopes()->where('idFabricante',$value)->TypeRFV()->get()->toArray();
        else
            $rfvs= TPersona::where('idsupervisor',$this->user->idPersona)->get()->toArray();
            
            $in = [];
            foreach ($rfvs as $key => $value1) {
                array_push($in, $value1['idPersona']);
            }
            
            return OrdenRecurso::collection(TOrdene::withoutGlobalScopes()->where('idFabricante',$value)->where('idestatus',$request->id)->whereIn('idPersona',$in)->get());


    }

    public function crear(Request $request){
        $validar=Validator::make($request->all(),[
                'productos'=>'array|required',
                'RFV' => 'required',
                'costo' => 'required',
                'impuesto' => 'required|int',
                'mayoristas' => 'required|array'
            ],
            [
                'required'=>'Se necesita el :attribute',
                'int'=> 'El :attribute debe ser entero',
                'min'=>'debe introducir una cantidad de :attribute'
        ]);

        if($validar->fails()){
            return response()->json($validar->errors()->all(), 400);
        }

        if (sizeof($request->mayoristas) === 0)
            return response(["exito"=>"no ingreso mayoristas"],400);


        if (sizeof($request->productos) === 0)
            return response(["exito"=>"no ingreso productos"],400);

        $current = Carbon::now();
        $costo=$request->costo;
        
        $registrada = TEstatusOrdene::wherein('descripcion',['Registrada','Registradas','registrada','registradas'])->first();
      // dd($registrada);
        $nuevaOrden=TOrdene::create([
            'idOperador'=>$this->user->idOperador,
            'idFabricante'=>$this->user->idFabricante,
            'idPersona'=>$request->RFV,
            'idpasadopor'=> $this->user->idPersona,
            'idMayorista'=>$request->mayoristas[0]['id'],
            'idPersona_solicitante'=>$request->cliente,
            'idpais'=>$this->user->idpais,
            'costoTotal'=>$costo,
            'coordenadas_l' =>$request->lat,
            'coordenadas_a'=>$request->lon,
            'ididioma'=>$this->user->ididioma,
            'idmoneda'=>$this->user->idmoneda,
            'tipo_operacion'=>1,
            'impuesto'=>$request->impuesto,
            'fechaOrden'=>$current,
            'fecha_envio_orden'=>$current,
            'idestatus'=>1,//$registrada->idestatus,
            'descuento'=>$request->mayoristas[0]['descuento'],
            'TotalUnidades'=>$request->cantidad,
            'comentario_entrega'=>$request->comentario
        ]);

        foreach( $request->productos as $index=>$item ) {
            $precio=0;
            $precio = $item['precio']*$item['cantidad'];
            $nuevaOrden->Productos()->attach(
                $item['id'],[
                'idOperador'=>$this->user->idOperador,
                'idfactura'=>$nuevaOrden->idfactura,
                'idFabricante'=>$this->user->idFabricante,
                'idMayorista'=>$request->mayoristas[0]['id'],
                'iditem_orden'=>$index+1,
                'item_price'=>$item['precio'],
                'nombreproducto'=>$item['nombre'],
                'cantidad_solicitada'=>$item['cantidad'],
                'idunidades'=>$item['unidades'],
                'item_descuento' => $item['descuento']>0?$item['descuento']:$request->mayoristas[0]['descuento'],
                'item_total' => $precio,
                'item_impuesto' => $request->impuesto,
                'idestatus' => 1
            ]);
        }

        
        foreach( $request->mayoristas as $mayo ) {
            $total = $costo;
            $nuevaOrden->Mayoristas()->attach(
                $mayo['id'],[
                'nombre_mayorista'=>$mayo['nombre'],
                'idOperador'=>$this->user->idOperador,
                'idFabricante'=>$this->user->idFabricante,
                'idpais'=>$this->user->idpais,
                'orden_total'=>$total,
                'impuesto'=>$request->impuesto,
                'idCliente'=>$request->cliente,
                'idmoneda'=>$this->user->idmoneda,
                'ididioma'=>$this->user->idioma,
                'item_descuento' => $mayo['descuento'],
                'idestatus' => 1
            ]);
        }

        return response (['exito'=>'la orden ha sido registrada correctamente',"norden"=>$nuevaOrden->idorden, "orden"=>$nuevaOrden],200);
    }


    /**
     * Esta es la consulta personalizada de ordenes
     * por estatus, numero de orden y mayorista
     * Route::post('/consultaOrden','API\Ordenes\OrdenController@consultaOrden');
     **/
    public function consultaOrden(Request $request)
    {
        $validator = Validator::make($request->all(),[
        'fechaInicio' => 'required|date|before:tomorrow',
        'fechaFin' => 'required|date|after_or_equal:fechaInicio'
        ]);
        if($validator->fails())
        {
        return response()->json($validator->errors(),400);
        }

        $total = 0;
        $unidades = 0;

        $value= '';
        if($this->perfil->tipo == 'ADM' || $this->perfil->tipo == 'OPE'){
            $value = $request->session()->get('user.teams');
        }else
            $value=$this->user->idFabricante;

        if ($request->rfv != null){
            $aux=[];
            if($this->perfil->tipo!='RFV'){
                foreach($request->rfv as $item){
                    array_push($aux,$item['idPersona']);
                }
            }else{
                array_push($aux,$request->rfv['idPersona']);
            }
            $ordenes = TOrdene::whereIn('idPersona',$aux);
        }
        else if($this->perfil->tipo=='OPE' || $this->perfil->tipo=='ADM'){
            $ordenes = DB::table("t_personas")
                ->where('t_ordenes.idFabricante',$value)
                ->where('idgrupo_persona','RFV')
                ->join("t_ordenes","t_personas.idPersona","=",'t_ordenes.idPersona')
                ->select('t_ordenes.*');
        }else
            $ordenes = DB::table("t_personas")
                ->where('idgrupo_persona','RFV')
                ->where('idsupervisor',$this->user->idPersona)
                ->join("t_ordenes","t_personas.idPersona","=",'t_ordenes.idPersona')
                ->select('t_ordenes.*');

        $ordenes = $ordenes->whereBetween('fechaOrden',[$request->fechaInicio,$request->fechaFin]);

        //return $ordenes->get();
        /**
        * busqueda por numero de orden
        **/
        if ($request->norden!="")
        {
        $ordenes = $ordenes->where('idorden',$request->norden);
        }

        /**
        * busqueda por estatus de orden
        **/

        if ($request->estatus != null) {
            $aux2=[];
            foreach($request->estatus as $item){
                array_push($aux2,$item['idestatus']);
            }
            $ordenes = $ordenes->whereIn('t_ordenes.idestatus',$aux2);
        }

        /**
        * busqueda por mayoristas
        **/
        if ($request->mayorista!=null)
        {
            $aux3=[];
            foreach($request->mayorista as $item){
                array_push($aux3,$item['idPersona']);
            }
            $ordenes = $ordenes->whereIn('idMayorista',$aux3);
        }

        $ordenes = $ordenes->get();

        if ($ordenes->count() == 0)
        return response(["error"=>"No hay actividades que cumplan con estos parametros"],200);
        /*foreach ($ordenes as $fil)
        {
        $unidades+=$fil->TotalUnidades;
        $total+=$fil->costoTotal;
        "productos"=>$productos,
        }*/
        $norden=[];

        foreach ($ordenes as $key => $value) {
            array_push($norden, $value->idorden);
        }

        $select = DB::raw('idorden as Orden, nombreproducto as Nombre, idMayorista as Mayorista, sum(cantidad_solicitada) as Solicitado, sum(item_price*cantidad_solicitada) as Monto_Solicitado , sum(cantidad_Faltante) as Faltante, sum(item_price*cantidad_Faltante) as Monto_Faltante, sum(cantidad_conciliada) Conciliado , sum(item_price*cantidad_conciliada) as Monto_Conciliado');

        $productos = DB::table('t_item_ordenes')
            ->select($select)
            ->whereIn('idorden',$norden)
            ->groupBy(['nombreproducto', 'idMayorista','idorden'])
            ->get();

        return response(["ordenes"=>OrdenRecurso::collection($ordenes),'total'=>$total,'unidades'=>$unidades, 'productos'=>$productos],200);
    }

     public function destroy(Request $request){
          $orden=TOrdene::withoutGlobalScopes()->find($request->id);
              if($orden==null || $orden->idestatus==6)
               return response()->json(['error'=>'pedido no encontrado'],404);

         $orden->idestatus=6;
         $orden->save();

          return response()->json(['success'=>'pedido eliminado'],200);
     }
}
