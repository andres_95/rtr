<?php

namespace RTR\Http\Controllers\API\Agenda;

use Illuminate\Http\Request;
use RTR\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use RTR\modelos\TPlanificadore;
use RTR\modelos\TPerfile;
use RTR\modelos\TPersona;
use RTR\Http\Resources\AgendaCollection;
use Validator;
class AgendaController extends Controller
{
    
    public function __construct(){
        $this->middleware(function ($request, $next) {
            $this->user = Auth::user();
            $this->perfil = TPerfile::find($this->user->idperfil);
            return $next($request);
        });
    }

    public function index()
    {
        if ($this->perfil->tipo=='RFV') {
            $clientes = $this->user->Clientes()->paginate(15, ['idPersona', 'nombre_completo_razon_social']);
        }else
            $clientes = TPersona::TypeCliente()->paginate(15, ['idPersona', 'nombre_completo_razon_social']);

        foreach($clientes as $cli){
            if($this->perfil->tipo=='RFV'){
                $citas = TPlanificadore::where(['idRFV'=>$this->user->idPersona,'idCliente'=>$cli['idPersona']])->get(['idAgenda','fecha_agenda','hora','idestatus']);
            }else
                $citas = TPlanificadore::where(['idCliente'=>$cli['idPersona']])->get(['idAgenda','fecha_agenda','hora','idestatus']);


            if ($citas->count() == 0)
                $cli['citas'] = "no hay citas agendadas";
            $cli['citas'] = $citas;
        }
        return response()->json($clientes,200);
    }

    public function detalle_cliente(Request $request)
    {
    	$validator = Validator::make($request->all(),
    		[
    			'idCliente' => 'required',
    		],
    		[
    			'required' => 'El ::attribute es requerido'
    		]
    	);

    	if($validator->fails())
        {
            return response()->json($validator->errors(),400);
        }
        $aux=TPersona::where('idsupervisor',$this->user->idPersona)->get();
        $in = [];
        foreach ($aux as $key => $value) {
            array_push($in, $value['idPersona']);
        }
        array_push($in, $this->user->idPersona);
        //dd($in);

        $agenda_detalle_cliente = TPlanificadore::where(['idCliente'=>$request->idCliente,'idestatus'=>1])->whereIn('idRFV',$in)->get();
        return AgendaCollection::collection($agenda_detalle_cliente);
    }
}
