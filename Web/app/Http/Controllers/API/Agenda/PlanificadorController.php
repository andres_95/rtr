<?php

namespace RTR\Http\Controllers\API\Agenda;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use RTR\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use RTR\Http\Resources\CitaRecurso;
use RTR\Http\Resources\PoragendarRecurso;
use RTR\modelos\TPerfile;
use RTR\modelos\TPersona;
use RTR\modelos\TPlanificadore;
use RTR\modelos\TTmpPlanificadore;
use Validator;

class PlanificadorController extends Controller
{
    
    public function __construct(){
        $this->middleware(function ($request, $next) {
            $this->user = Auth::user();
            $this->perfil = TPerfile::find($this->user->idperfil);

            return $next($request);
        });
    }

    public function index(Request $request)
    {
        if ($this->perfil->tipo=='RFV') {
            $tmp=TTmpPlanificadore::where('idRFV',$this->user->idPersona)->get();
            //return ['Poragendar'=>PoragendarRecurso::collection($tmp),'Agendado'=>CitaRecurso::collection(TPlanificadore::where('idRFV', $this->user->idPersona)->get())];
            return CitaRecurso::collection(TPlanificadore::where('idRFV', $this->user->idPersona)->get());
        }else
        {
            $tmp=TTmpPlanificadore::where(['idSupervisor'=>$this->user->idPersona])->get();
            return ['Agendado'=>CitaRecurso::collection(TPlanificadore::all()),'Poragendar'=>PoragendarRecurso::collection($tmp)];
            //return [CitaRecurso::collection(TPlanificadore::all()),];
        }
    }

    public function index2(Request $request)
    {
        if ($this->perfil->tipo=='RFV') {
            $tmp=TTmpPlanificadore::where('idRFV',$this->user->idPersona)->get();
            return ['Poragendar'=>PoragendarRecurso::collection($tmp),'Agendado'=>CitaRecurso::collection(TPlanificadore::where('idRFV', $this->user->idPersona)->get())];
            //return CitaRecurso::collection(TPlanificadore::where('idRFV', $this->user->idPersona)->get());
        }else
        {
            $tmp=TTmpPlanificadore::where(['idSupervisor'=>$this->user->idPersona])->get();
            $citas=TPlanificadore::where(['idSupervisor'=>$this->user->idPersona])->get();
            $mias=TPlanificadore::where(['idRFV'=>$this->user->idPersona])->get();
            return ['Agendado'=>CitaRecurso::collection($mias),'Sup'=>CitaRecurso::collection($citas),'Poragendar'=>PoragendarRecurso::collection($tmp)];
            //return [CitaRecurso::collection(TPlanificadore::all()),];
        }
    }

    public function create(Request $request){
        $validar=Validator::make($request->all(),[
            'cliente'=> 'required',
            'fecha'  => 'required',
            'hora'   => 'required'
        ],
            [
                'required'=>'Se necesita la :attribute',
            ]);

        if($validar->fails()){
            return response()->json($validar->errors()->all(), 400);
        }

        $plan=TPlanificadore::where('idCliente','=',$request->cliente)
            ->where('fecha_agenda','=',$request->fecha)
            ->where('hora','=',$request->hora);

        if($plan->count()>0)
            return response()->json(['error'=>'Este cliente ya tiene cita registrada'],400);

        $rfv = TPersona::find($request->rfv);
        $fecha = $request->fecha;
        $hora = $request->fecha.' '.$request->hora;

        if (TPlanificadore::where('fecha_agenda',$request->fecha)->where('hora',$hora)->get()->count() !== 0)
            return response(['error'=>'la actividad ya ha sido agendada'],200);

        $tmp=TTmpPlanificadore::find($request->id);
        if($tmp){
            $tmp->idstatus=2;
            $tmp->delete();
        }

        TPlanificadore::create([
            'idOperador'   => $this->user->idOperador,
            'idFabricante' => $this->user->idFabricante,
            'idRFV'        => $rfv->idPersona,
            'idCliente'    => $request->cliente,
            'fecha_agenda' => $fecha,
            'hora'         => $hora,
            'observacion_agenda' =>$request->comentarios?$request->comentarios:'',
            'idSupervisor' => $this->user->idsupervisor,
            'idestatus' => 1,
        ]);

        //return response(['exito'=>'la actividad ha sido agendada satisfactoriamente'],200);
        $tmp=TTmpPlanificadore::where(['idSupervisor'=>$this->user->idPersona])->get();
        return ['Agendado'=>CitaRecurso::collection(TPlanificadore::all()),'Poragendar'=>PoragendarRecurso::collection($tmp)];
    }

    public function TMP(Request $request){
        $validar=Validator::make($request->all(),[
            'cliente'=> 'required',
            'fecha'  => 'required',
            'hora'   => 'required'
        ],
            [
                'required'=>'Se necesita la :attribute',
            ]);

        if($validar->fails()){
            return response()->json($validar->errors()->all(), 400);
        }

        $plan=TTmpPlanificadore::where('idCliente','=',$request->cliente)
            ->where('Fecha','=',$request->fecha)
            ->where('Hora','=',$request->hora);

        if($plan->count()>0)
            return response()->json(['error'=>'Este cliente ya tiene cita registrada'],400);

        if($this->perfil->tipo=='RFV') {
            $rfv = $this->user->idPersona;
            $sup = $this->user->idsupervisor;
        }else{
            $rfv=$request->rfv;
            $sup=$request->sup;
        }

        $fecha = $request->fecha;
        $hora = $request->fecha.' '.$request->hora;

        if (TTmpPlanificadore::where('Fecha',$request->fecha)->where('Hora',$hora)->get()->count() !== 0)
            return response(['error'=>'la actividad ya ha sido agendada'],200);

        TTmpPlanificadore::create([
            'idOperador'   => $this->user->idOperador,
            'idFabricante' => $this->user->idFabricante,
            'idRFV'        => $rfv,
            'idCliente'    => $request->cliente,
            'Fecha'        => $fecha,
            'Hora'         => $hora,
            'idSupervisor' => $sup,
            'idstatus'     => 1,
        ]);

        if ($this->perfil->tipo=='RFV')
            $tmp=TTmpPlanificadore::where('idRFV',$this->user->idPersona)->get();
        else
            $tmp=TTmpPlanificadore::where(['idSupervisor'=>$this->user->idPersona])->get();

        return response(['Poragendar'=>PoragendarRecurso::collection($tmp)]);
    }

    public function tmpdelete(Request $request){
        $tmp=TTmpPlanificadore::find($request->id);
        $tmp->delete();

        $tmp=TTmpPlanificadore::where(['idSupervisor'=>$this->user->idPersona])->get();

        return response(['Poragendar'=>PoragendarRecurso::collection($tmp)]);
    }

    public function update(Request $request){
        $plan=TPlanificadore::where('idAgenda','=',$request->idAgenda);


         if($plan->count()==0)
            return response()->json(['error'=>'Este cliente no se encuentra'],404);

            $plan->update(
            [ 'idRFV'=>$request->idrfv,
                'idCliente'=>$request->idcliente,
                'fecha_agenda'=>$request->fecha,
                'hora'=>$request->hora,
                'idbrick'=>$request->idbrick,
                'observacion_agenda'=>$request->observacion,
                'idSupervisor'=>$request->idsupervisor,
                'idestatus'=>'1'
                ]
            );

             return response()->json(['success'=>'planificacion actualizada'],200);
    }

    public function delete(Request $request){
        $plan=TPlanificadore::where('idAgenda','=',$request->idAgenda);

         if($plan->count()==0)
            return response()->json(404);

            $plan->update(
            [
                'idestatus'=>'0'
                ]
            );

            return response()->json(['success'=>'planficiacion eliminada'],200);
    }

    public function mostrarDisponible(Request $request){
        if ($this->perfil->tipo=='RFV') {
            $clientes = $this->user->Clientes()->get();
        }
        else
            if($request->idPersona)
                $clientes = TPersona::find($request->idPersona)->Clientes()->get();
            else
                $clientes = TPersona::TypeCliente()->get();

        return response()->json(['Clientes'=>$clientes],200);
    }

    public function rfvs(Request $request){
            $rfv = TPersona::where('idSupervisor',$this->user->idPersona)->get();

        return response()->json(['RFV'=>$rfv],200);
    }

    public function citaReporte(Request $request){
        $clientes = $this->user->Clientes()->get(['idPersona','nombre_completo_razon_social']);

        foreach($clientes as $cli){
            $citas = TPlanificadore::where(['idRFV'=>$this->user->idPersona,'idCliente'=>$cli['idPersona']])->get(['idAgenda','fecha_agenda','hora','idestatus']);
            if ($citas->count() == 0)
                $cli['citas'] = "no hay citas agendadas";
            $cli['citas'] = $citas;
        }
        return response($clientes,200);
    }
}
