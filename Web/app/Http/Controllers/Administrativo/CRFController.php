<?php

namespace RTR\Http\Controllers\Administrativo;

use Illuminate\Http\Request;
use RTR\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

use RTR\Http\Resources\CRFRecurso;
use RTR\modelos\TCiclo;
use RTR\modelos\TPaise;
use RTR\modelos\TRankingCliente as Rank;
use RTR\modelos\TFrecuenciaVisita;
use RTR\modelos\TPersona;

use RTR\modelos\TRankingCliente;
use Validator;

class CRFController extends Controller{

    public function __construct()
    {
        $this->middleware(function ($request, $next) {
            $this->user = Auth::user();
            return $next($request);
        });
    }

    public function index(Request $request)
    {
        $Empresa=TPersona::withoutGlobalScopes()
                                ->where('idOperador',$this->user->idOperador)
                                ->where('idgrupo_persona','FABR')
                                ->select(['nombre_completo_razon_social','idPersona'])
                                ->get();
        $T="Ciclos";
        $fabrica = $request->session()->get('user.teams');
        return view('Admin.estadistico.CiRaFr',compact('Empresa','T','fabrica'));
    }

    public function showciclo(Request $request){

        $ciclo=TCiclo::withoutGlobalScopes()->where('idFabricante',$request->fabricante)
                      ->where('idestatus',1) 
                      ->get();
        foreach ($ciclo as $item)
            $item->tipo='c';

        $hola=CRFRecurso::collection($ciclo)->toJson();
        return response($hola,200);
    }

    public function indexF(Request $request)
    {
        $Empresa=TPersona::withoutGlobalScopes()
            ->where('idOperador',$this->user->idOperador)
            ->where('idgrupo_persona','FABR')
            ->select(['nombre_completo_razon_social','idPersona'])
            ->get();
        $T="Frecuencias";
        $pais=TPaise::all();
        $fabrica = $request->session()->get('user.teams');
        return view('Admin.estadistico.CiRaFr',compact('Empresa','T','pais','fabrica'));
    }

    public function showfrec(Request $request){

        $frec=TFrecuenciaVisita::withoutGlobalScopes()->where('idFabricante',$request->fabricante)
                   ->where('idestatus',1)   
                   ->get();
        foreach ($frec as $item){
            $item->id=$item->idfrecuencia;
            $item->tipo='f';
        }
        $hola=CRFRecurso::collection($frec)->toJson();
        return response($hola,200);
    }

    public function indexR(Request $request)
    {
        $Empresa=TPersona::withoutGlobalScopes()
            ->where('idOperador',$this->user->idOperador)
            ->where('idgrupo_persona','FABR')
            ->select(['nombre_completo_razon_social','idPersona'])
            ->get();
        $T="Ranking";
        $pais=TPaise::all();
        $fabrica = $request->session()->get('user.teams');
        return view('Admin.estadistico.CiRaFr',compact('Empresa','T','pais','fabrica'));
    }

    public function showrank(Request $request){

        $rank=TRankingCliente::withoutGlobalScopes()->where('idFabricante',$request->fabricante)
                     ->where('estatus',1) 
                     ->get();
        foreach ($rank as $item){
            $item->tipo='r';
        }
        $hola=CRFRecurso::collection($rank)->toJson();
        return response($hola,200);
    }

    public function storeC (Request $request){
        //dd($request->all());
         $validator=Validator::make($request->all(),TCiclo::$validators);
            if($validator->fails())
               return response()->json($validator->errors()->all(),404);

        $operador=TPersona::withoutGlobalScopes()->find($request->fabricante)->idOperador;
        $ciclo=TCiclo::create([
            'idOperador'=>$operador,
            'idFabricante'=>$request->fabricante,
            'descripcion_ciclos'=>$request->descripcion,
            'idestatus'=>1
        ]);

      //  $crf=TCiclo::all()->last();
      //  $crf->tipo='c';
        //dd($crf);
      //  dd($ciclo['id']);
        return response()->json(['success'=>'ciclo agregado','id'=>$ciclo['id']],200);
    }

    public function updateC (Request $request){
      //  dd($request->all());
        $validator=Validator::make($request->all(),
        array(
             'descripcion'=>TCiclo::$validators['descripcion'],
              'id'=>'required|exists:t_ciclos,id' 
        ));
        if($validator->fails())
           return response()->json($validator->errors()->all(),404);


        $crf=TCiclo::withoutGlobalScopes()->find($request->id);
        $crf->descripcion_ciclos=$request->descripcion;
        $crf->save();
        return response($crf,200);
    }

    public function deleteC (Request $request){
        $validator=Validator::make($request->all(),array('id'=>'required|exists:t_ciclos,id'));

        if($validator->fails())
           return response()->json($validator->errors()->all(),404);

        $crf=TCiclo::withoutGlobalScopes()->find($request->id);
        $crf->idestatus=0;
        $crf->save();

        //$crf->delete();
        return response()->json(['success'=>'ciclo eliminado'],200);
    }

    public function storeF(Request $request)
    {
           $validator=Validator::make($request->all(),TFrecuenciaVisita::$validators);
              
           if($validator->fails())
               return response()->json($validator->errors()->all(),404);

        //
     /*   $data=$request->all();
        $data['idOperador']=$this->user->idOperador;
        $data['idestatus']=1;*/

        $frecuencia=TFrecuenciaVisita::create(
            [
                'idOperador'=>$this->user->idOperador,
                'idfrecuencia'=>$request->id,
                'descripcion_frecuencia_visitas'=>$request->descripcion,
                'idFabricante'=>$request->fabricante,
                'idestatus'=>1

            ]
        );
         
        return response()->json(['success'=>'se ha registrado un frecuencia de visitas','id'=>$frecuencia['idfrecuencia']],200);
    }
 
    public function updateF(Request $request)
    {
        //
        $validator=Validator::make($request->all(),array(
            'descripcion'=>TFrecuenciaVisita::$validators['descripcion'],
            'id'=>'required|exists:t_frecuencia_visitas,idfrecuencia'
        ));
        
        if($validator->fails())
            return response()->json($validator->errors()->all(),404);


          $frecuencia=TFrecuenciaVisita::withoutGlobalScopes()->find($request->id);
           
             if($frecuencia==null || $frecuencia->idestatus==0)
                return response()->json(['error'=>'frecuencia de visita no encontrada'],404);
         /* if( !TFrecuenciaVisita::where('idfrecuencia',$request->id)
            ->update(
            [
                'idfrecuencia'=>$request->id,
                'descripcion_frecuencia_visitas'=>$request->descripcion

            ]))*/
           // $frecuencia->idfrecuencia=$request->id;
            $frecuencia->descripcion_frecuencia_visitas=$request->descripcion;
            $frecuencia->save();

           return response()->json(['success'=>'se ha hecho cambios en una de las frecuencias de visita'],200);

    }

    public function destroyF(Request $request)
    {
        //
        $validator=Validator::make($request->all(),array(
            'id'=>'required|exists:t_frecuencia_visitas,idfrecuencia'
        ));
        
        if($validator->fails())
            return response()->json($validator->errors()->all(),404);

        $frecuencia=TFrecuenciaVisita::withoutGlobalScopes()->find($request->id);
        
          if($frecuencia==null || $frecuencia->idestatus==0)
              return response()->json(['error'=>'frecuencia de visita no encontrada'],404);

         $frecuencia->idestatus=0;
         $frecuencia->save();

         return response()->json(['success'=>'se ha eliminado una de las frecuencias de visita'],200);
    }

    public function storeR(Request $request)
    {
            $validator=Validator::make($request->all(),Rank::$validators);
                
            if($validator->fails())
                return response()->json($validator->errors()->all(),404);

         $ranking=Rank::create([
             'idOperador'=>$this->user->idOperador,
             'idFabricante'=>$request->fabricante,
             'descripcion_ranking_cliente'=>$request->descripcion,
             'estatus'=>1
         ]);

         return response()->json(['success'=>'ranking registrado','id'=>$ranking['id']],200);
         
         
    }

    public function updateR(Request $request)
    {
        $validator=Validator::make($request->all(),array(
            'descripcion'=>Rank::$validators['descripcion'],
            'id'=>'required|exists:t_ranking_clientes,id'
        ));
        
        if($validator->fails())
            return response()->json($validator->errors()->all(),404);
        //
       // $operador=TOperadore::find($this->user->idOperador);

            $rank=Rank::withoutGlobalScopes()->find($request->id);

                if(!$rank)
                   return response()->json(['error'=>'ranking no encontrado'],404);

         
             $rank->descripcion_ranking_cliente=$request->descripcion;
             $rank->save();
              return response()->json(['success'=>'ranking actualizado'],200);
    }

    public function destroyR(Request $request)
    {
        //
        $validator=Validator::make($request->all(),array(
            'id'=>'required|exists:t_ranking_clientes,id'
        ));
        
        if($validator->fails())
            return response()->json($validator->errors()->all(),404);
       
        $rank=Rank::withoutGlobalScopes()->find($request->id);
        
                        if(!$rank)
                           return response()->json(['error'=>'ranking no encontrado'],404);
                           
                  //  $descripcion=$request->only('descripcion_ranking_clientecol');
                     $rank->estatus=0;
                     $rank->save();
                  
                      return response()->json(['success'=>'ranking eliminado'],200);     
                
                  
    }


}