<?php

namespace RTR\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use RTR\modelos\TPersona;
use RTR\modelos\TProducto;
use RTR\modelos\TEstatusOrdene;
use RTR\modelos\TOrdene;
use RTR\modelos\TFormaPago;

class CTController extends Controller
{
    //
    public function Pedido(){
        $productos=TProducto::where('idcategorias','like','P'.'%')           
                    ->get();

        $RFV=TPersona::where('idgrupo_persona','=','RFV')->where('idPersona','!=',Auth::user()->idPersona)->get();

        return view('CT.TomaPedidos',compact('productos','RFV'));
    }

    public function Seguimiento(){
        $estatus = TEstatusOrdene::all()->toJson();
        return view('CT.Seguimiento',compact('estatus'));
    }

    public function Pedir(){

        //dd($request->all());

        return redirect()->action('CTController@Pedido');
    }

    public function Facturar(){
        $formapago = TFormaPago::all();
        $status= TEstatusOrdene::where('descripcion','Facturada')->first();
        $sta= TEstatusOrdene::where('descripcion','Anulada')->first();
        $Corden = TOrdene::where('idestatus','!=',$status->idestatus)->where('idestatus','!=',$sta->idestatus)->get(['idorden']);

        //dd($Corden);
        return view('CT.Facturar',compact('formapago','Corden'));
    }
}
