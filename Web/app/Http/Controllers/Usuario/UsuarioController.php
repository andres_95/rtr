<?php

namespace RTR\Http\Controllers\Usuario;

use RTR\modelos\TGrupopersona;
use RTR\modelos\TPersona;
use RTR\modelos\TTipoPersona;
use Illuminate\Http\Request;
use RTR\Http\Controllers\Controller;
use RTR\modelos\TPaise;
use validate;
class UsuarioController extends Controller
{
    //

    public function index()
    {
        $Empresa=TPersona::withoutGlobalScopes()->where('idFabricante','')->get(['idOperador','idPersona']);
        return view('Admin.usuarios.home_personas',compact('Empresa'));
    }

    public function viewcrear()
    {
        $paises = TPaise::all();
        $tipopersona = TTipoPersona::all();
        $grupopersona = TGrupopersona::all();
        return view('Admin.usuarios.crear',compact('paises','tipopersona','grupopersona'));
    }

    public function store(Request $request)
    {
        dd($request->all());
    }
}
