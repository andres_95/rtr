<?php

namespace RTR\Http\Controllers\Web;

use RTR\modelos\TCiudade;
use RTR\modelos\TEspecialidade;
use Illuminate\Http\Request;
use RTR\Http\Controllers\Controller;
use RTR\modelos\TEstado;
use RTR\modelos\TPerfile;
use RTR\modelos\TPersona;
use RTR\modelos\TBrickRuta;
use RTR\modelos\TZona;
use RTR\modelos\TCiclo;
use Illuminate\Support\Facades\Auth;

class ConsultReporteController extends Controller
{
    //
    public function __construct()
    {
        $this->middleware(function ($request, $next) {
            $this->user = Auth::user();
            $this->perfil = TPerfile::find($this->user->idperfil);
            return $next($request);
        });
    }

    public function index()
    {
        /* General */
        $RFV=TPersona::TypeRFV()->get();

        /* Reportes */
        $ranking=TRankingCliente::all();
        $actividades = TTipoActividade::all();
        $incidentes = TTipoIncidente::all();
        $especialidades = TEspecialidade::all();
        $zona=TZona::all();
        $brick=TBrickRuta::all();

        /*Ordenes*/
        $Mayorista=TPersona::TypeMayorista()->get();
        $ordenes=TOrdene::all(['idorden']);
        $estatus = TEstatusOrdene::all();

        /*Facturas*/
        $facturas=TFactura::all(['idfactura']);
        return view('Home',compact('RFV','ranking','actividades','incidentes','especialidades','zona','brick','estatus','ordenes','Mayorista','facturas'));
    }

    public function consulGerencial(Request $request)
    {
        /* General */
        if($this->perfil->tipo == 'ADM' || $this->perfil->tipo == 'OPE'){
            $fabrica = $request->session()->get('user.teams');
        }else
            $fabrica=$this->user->idFabricante;

        $zona=TZona::withoutGlobalScopes()->where('idFabricante',$fabrica)->get();
        foreach ($zona as $item){
            $item->idestado=TEstado::find($item->idestado)->nombreCiudad;
            $item->idciudad=TCiudade::find($item->idciudad)->nombreCiudad;
        }
        $brick=TBrickRuta::withoutGlobalScopes()->where('idFabricante',$fabrica)->get();/*get(['idbrick','Descripcion'])*/;

        /* Personas */
        $sup=TPersona::withoutGlobalScopes()->where('idFabricante',$fabrica)->where('idgrupo_persona','SUP')->get(['idPersona','nombre_completo_razon_social']);
        $rfv=TPersona::withoutGlobalScopes()->where('idFabricante',$fabrica)->where('idgrupo_persona','RFV')->get(['idPersona','nombre_completo_razon_social']);

        //dd($sup,$rfv);

        return view('Consultas.Gerencial',compact('zona','brick','ciclo','sup','rfv','fabrica'));
    }
}
