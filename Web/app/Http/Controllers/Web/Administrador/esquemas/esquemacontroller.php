<?php

namespace RTR\Http\Controllers\Web\Administrador\esquemas;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use RTR\modelos\TPersona;
use RTR\modelos\TProducto;
use RTR\modelos\TEsquemaPromocionale;


use Validator;

use RTR\Http\Controllers\Controller;

class esquemacontroller extends Controller
{

    public function __construct()
    {
        $this->middleware(function ($request, $next) {
            $this->user = Auth::user();
            
            return $next($request);
        });
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $productos=TProducto::all();
        $personas=TPersona::where('idgrupo_persona','MAY')->get();
        $promociones=TEsquemaPromocionale::all();
       // dd($personas);
         return view('Admin.Caracteristicas_productos.esquema',compact('productos','personas','promociones'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
       //  $persona=TPersona::where('idPersona',$request->idRFV)->where('idestatus',1);
         $producto=TProducto::where('idProducto',$request->idProducto)->where('estatus_producto',1);

              if($producto->count()==0)
                 return response()->json(['error'=>'persona no encontrada'],404);
        $data=$request->except('idProducto');
        $data['idestatus']=1;
        $data['idOperador']=$this->user->idOperador;
        
        $producto->first()->promocionar($this->user,$data);

           return response()->json(['success'=>'se ha creado una promocion para este producto'],200);
           
            
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $producto=TProducto::where('idProducto',$id)->where('estatus_producto',1);
        
                      if($producto->count()==0)
                         return response()->json(['error'=>'producto no encontrada'],404);

       $data=$request->except('id');
       
       $producto->first()->actualizarPromocion($data,$request->id);

        return response()->json(['success'=>'se ha hecho una actualizacion al promocion'],200);

    }

      

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request,$id)
    {
        //
        $producto=TProducto::where('idProducto',$id)->where('estatus_producto',1);
        
                      if($producto->count()==0)
                         return response()->json(['error'=>'producto no encontrada'],404);

       $data=$request->except('id');
       
       $producto->first()->cancelarPromocion($request->id);

        return response()->json(['success'=>'promocion cancelada'],200);
    }
}
