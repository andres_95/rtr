<?php

namespace RTR\Http\Controllers\Web\Procesos;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use RTR\Http\Controllers\Controller;
use RTR\Http\Utilities\Monitor;

use Illuminate\Support\Facades\Input;
use RTR\modelos\TPersona;
use Validator;

class MonitorController extends Controller
{
    //
    public function __construct()
    {
        $this->middleware(function ($request, $next) {
            $this->user = Auth::user();
            return $next($request);
        });
    }

    public function index(){
        $empresas = $Empresa=TPersona::withoutGlobalScopes()
            ->where('idOperador',$this->user->idOperador)
            ->where('idgrupo_persona','FABR')
            ->select(['nombre_completo_razon_social','idPersona'])
            ->get();
        return view('Procesos.Monitor',compact('empresas'));
    }

    public function uploading(){


        $modelo = new Monitor();

        $rules = array(
            'archivo' => 'max:100000',
            'entradas' => 'required',
            //'nombre' => 'required|max:100',
            //'nit' => 'required|min:5|max:20',
        );

        $validator = Validator::make(Input::all(), $rules);

        if( $validator->fails() ){
            return back()
                ->withErrors( $validator );
        }else {
            $file = Input::file('archivo');
            $seleccion = Input::get('entradas');

            //$data = json_decode($file);

            $modelo->jsonAnalizer($file, $seleccion);

            return 'Ok';

        }

    }

}
