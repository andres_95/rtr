<?php

namespace RTR\Http\Controllers\Web\Facturacion;

use Illuminate\Http\Request;
use RTR\Http\Requests\FacturaFabricante;
use RTR\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use RTR\modelos\TPersona;
use RTR\modelos\TFormaPago;
use RTR\modelos\TEstatusOrdene;
use RTR\modelos\TOrdene;
use Validator;

class FacturaController extends Controller
{
    
    public function __construct()
    {
        $this->middleware(function ($request, $next) {
            $this->user = Auth::user();
            return $next($request);
        });
    }

    public function facturar()
    {
        $Fabricantes = TPersona::withoutGlobalScopes()
                                ->where('idOperador',$this->user->idOperador)
                                ->where('idgrupo_persona','FABR')
                                ->get();

        return view('Admin.Facturacion.facturaoption',compact('Fabricantes'));
    }
    public function show_view_conciliar(FacturaFabricante $request)
    {
        
        $formapago = TFormaPago::all();
        $status= TEstatusOrdene::withoutGlobalScopes()
                                ->OperadorFabricante($this->user->idOperador,$request->Fabricante)
                                ->where('descripcion','Facturada')->first();
        $sta= TEstatusOrdene::withoutGlobalScopes()
                                ->OperadorFabricante($this->user->idOperador,$request->Fabricante)
                                ->where('descripcion','Anulada')->first();
        $Corden = TOrdene::withoutGlobalScopes()
                                ->OperadorFabricante($this->user->idOperador,$request->Fabricante)
                                ->where('idestatus','!=',$status->idestatus)
                                ->where('idestatus','!=',$sta->idestatus)
                                ->get(['idorden']);

        return view('Admin.Facturacion.Facturar',compact('formapago','Corden'));
    }
}
