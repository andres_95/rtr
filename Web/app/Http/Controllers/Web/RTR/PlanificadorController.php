<?php

namespace RTR\Http\Controllers\Web\RTR;

use Illuminate\Http\Request;
use RTR\Http\Controllers\Controller;

class PlanificadorController extends Controller
{
    public function index()
    {

        return view('RTR.Planificador');
    }
}
