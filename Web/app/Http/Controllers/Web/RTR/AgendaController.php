<?php

namespace RTR\Http\Controllers\Web\RTR;

use Illuminate\Http\Request;
use RTR\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use RTR\modelos\TPerfile;
use RTR\modelos\TPersona;
use RTR\modelos\TPlanificadore;
use RTR\modelos\TCiclo;
use RTR\Http\Resources\AgendaRTR;
use RTR\modelos\TFrecuenciaVisita;

class AgendaController extends Controller
{
    public function __construct()
    {
        $this->middleware(function ($request, $next) {
            $this->user = Auth::user();
            $this->perfil = TPerfile::find($this->user->idperfil);
            return $next($request);
        });
    }

    public function show_resume_agenda()
    {
        $Dhabiles= null;
        if ($this->perfil->tipo=='RFV') {
            $Cliente = $this->user->Clientes()->get();
            $Dhabiles=TCiclo::find($this->user->idciclos)->descripcion_ciclos;
            if($Dhabiles=='MENSUAL' || $Dhabiles=='Mensual'){
                $Dhabiles=20;
            }
        }
        else
            $Cliente = TPersona::TypeCliente()->get();

        $visitados=0;
        $Tvisit=0;
        foreach($Cliente as $item){
            if($this->perfil->tipo=='RFV'){
                $Citas=TPlanificadore::withoutGlobalScopes()->where(['idRFV'=>$this->user->idPersona,'idCliente'=>$item->idPersona,'idestatus'=>2])->get();
            }else
                $Citas=TPlanificadore::withoutGlobalScopes()->where(['idCliente'=>$item->idPersona,'idestatus'=>2])->get();$item->visitado=$Citas->count();
            if($item->visitado>0){
                $visitados+=1;
            }
            $item['dias_visita'] = $item->Dias()->get();
            //dd($item['dias_visita']);
            $Tvisit+=(float)substr(TFrecuenciaVisita::find($item->idfrecuencia)->descripcion_frecuencia_visitas,0,1);
        }
        if($Tvisit>0){
            $ciclo=$Tvisit;
        }else {
            $ciclo = 1;
        }

        $Agenda=AgendaRTR::collection($Cliente)->toJson();
        return view('RTR.Agenda',compact('Agenda','ciclo','visitados','Dhabiles'));
    }
}
