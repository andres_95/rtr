<?php

namespace RTR\Http\Controllers\Web\RTR;

use Illuminate\Http\Request;
use RTR\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use RTR\modelos\TPerfile;
use RTR\modelos\TPersona;
use RTR\modelos\TCiclo;
use RTR\modelos\TPlanificadore;
use RTR\Http\Resources\ReporteList;
use RTR\modelos\TActividadesRepresentante;

class ReportesController extends Controller
{

	public function __construct()
    {
        $this->middleware(function ($request, $next) {
            $this->user = Auth::user();
            $this->perfil = TPerfile::find($this->user->idperfil);
            return $next($request);
        });
    }


    public function index(Request $request)
    {
        $value= '';
        if($this->perfil->tipo == 'ADM' || $this->perfil->tipo == 'OPE'){
            $value = $request->session()->get('user.teams');
        }else
            $value=$this->user->idFabricante;
            
        $Dhabiles= null;
        if ($this->perfil->tipo=='RFV') {
            $ActiviD= ReporteList::collection(TActividadesRepresentante::where('idRFV',Auth::user()->idPersona)->get())->toJson();
            $Cliente=$this->user->Clientes()->get();
            $Dhabiles=TCiclo::find($this->user->idciclos)->descripcion_ciclos;
            if($Dhabiles=='MENSUAL' || $Dhabiles=='Mensual'){
                $Dhabiles=20;
            }
        }else{
            if($this->perfil->tipo=='OPE' || $this->perfil->tipo=='ADM' || $this->perfil->tipo=='GRT'){
                $rfvs= TPersona::withoutGlobalScopes()->where('idFabricante',$value)->TypeRFV()->get()->toArray();
            }else
                $rfvs= TPersona::where('idsupervisor',$this->user->idPersona)->get()->toArray();

            $in = [];
            foreach ($rfvs as $key => $value) {
                array_push($in, $value['idPersona']);
            }
            array_push($in,$this->user->idPersona);
            $ActiviD = ReporteList::collection(TActividadesRepresentante::withoutGlobalScopes()->whereIn('idPersona',$in)->get())->toJson();
            $Cliente= $Cliente = TPersona::withoutGlobalScopes()->where('idFabricante',$value)->TypeCliente()->get();
        }

        $visitados=0;
        $Tvisit=0;

        foreach($Cliente as $item){
            if($this->perfil->tipo=='RFV'){
                $Citas=TPlanificadore::withoutGlobalScopes()->where(['idRFV'=>$this->user->idPersona,'idCliente'=>$item->idPersona,'idestatus'=>2])->get();
            }else
                $Citas=TPlanificadore::withoutGlobalScopes()->where(['idCliente'=>$item->idPersona,'idestatus'=>2])->get();
            $item->visitado=$Citas->count();
            if($item->visitado>0){
                $visitados+=1;
            }
            $Tvisit+=$item->idfrecuencia;
        }
        $visit=$visitados;
        return view('RTR.Actividades',compact('ActiviD','visit','Tvisit','Dhabiles','value'));
    }
    
}
