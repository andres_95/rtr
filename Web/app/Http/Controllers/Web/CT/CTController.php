<?php

namespace RTR\Http\Controllers\Web\CT;

use Illuminate\Http\Request;
use RTR\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use RTR\modelos\TPerfile;
use RTR\modelos\TPersona;
use RTR\modelos\TProducto;
use RTR\modelos\TEstatusOrdene;
use RTR\modelos\TOrdene;
use RTR\modelos\TFormaPago;

class CTController extends Controller
{

    public function __construct()
    {
        $this->middleware(function ($request, $next) {
            $this->user = Auth::user();
            $this->perfil = TPerfile::find($this->user->idperfil);
            return $next($request);
        });
    }
    public function Pedido(){
        $productos=TProducto::where('idcategorias','like','P'.'%')           
                    ->get();


        if($this->perfil->tipo=='RFV')
            $RFV=TPersona::where('idgrupo_persona','=','RFV')->where('idPersona','!=',Auth::user()->idPersona)->get();
        else
            $RFV=TPersona::where('idsupervisor',$this->user->idPersona)->get();

        return view('CT.TomaPedidos',compact('productos','RFV'));
    }

    public function Seguimiento(Request $request){
        if($this->perfil->tipo == 'ADM' || $this->perfil->tipo == 'OPE'){
            $value = $request->session()->get('user.teams');
        }else
            $value=$this->user->idFabricante;
        
        $estatus = TEstatusOrdene::withoutGlobalScopes()->where('idFabricante',$value)->get()->toJson();
        return view('CT.Seguimiento',compact('estatus','value'));
    }

    public function Pedir(){

        //dd($request->all());

        return redirect()->action('CTController@Pedido');
    }

    public function Facturar(){
        $formapago = TFormaPago::all();
        $status= TEstatusOrdene::where('descripcion','Facturada')->first();
        $sta= TEstatusOrdene::where('descripcion','Anulada')->first();
        $Corden = TOrdene::where('idestatus','!=',$status->idestatus)->where('idestatus','!=',$sta->idestatus)->get(['idorden']);

        //dd($Corden);
        return view('CT.Facturar',compact('formapago','Corden'));
    }
}
