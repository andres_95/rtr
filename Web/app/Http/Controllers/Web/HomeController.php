<?php

namespace RTR\Http\Controllers\Web;

use RTR\modelos\TPerfile;
use RTR\modelos\TPersona;
use RTR\modelos\TTipoActividade;
use RTR\modelos\TTipoIncidente;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use RTR\Http\Controllers\Controller;

class HomeController extends Controller
{
    public function __construct()
    {
        $this->middleware(function ($request, $next) {
            $this->user = Auth::user();
            $this->perfil = TPerfile::find($this->user->idperfil);
            return $next($request);
        });
    }

        
	public function index()
	{
        $Fabricante = null;
        $Supervisor = null;
        $Empresa=TPersona::withoutGlobalScopes()
                        ->where('idOperador',$this->user->idOperador)
                        ->where('idgrupo_persona','FABR')
                        ->select(['nombre_completo_razon_social','idPersona'])
                        ->get();

        if($this->user->idFabricante){
            $Fabricante= TPersona::find($this->user->idFabricante);
        }
        if($this->user->idsupervisor)
            $Supervisor = TPersona::find($this->user->idsupervisor);

       // dd($this->user);
   		return view('Home',[
            'Nombre'=>$this->user->nombre_persona,
            'Apellido'=>$this->user->apellido_persona,
            'Perfil' => $this->perfil,
            'Fabricante'=>$Fabricante,
            'Supervisor'=>$Supervisor,
            'Empresa'=>$Empresa
        ]);
	}
}
