<?php

namespace RTR\Http\Controllers;
use Validator;
use function League\Csv\delimiter_detect;
use Illuminate\Http\Request;
use League\Csv\Reader;
use Maatwebsite\Excel\Facades\Excel;
use RTR\modelos\TPersona;
use RTR\ExcelExport\Personas;

class prueba extends Controller
{

   

    public function export()
    {
         return (new Personas)->operador('01')->tipo('RFV')->download('invoices.xlsx');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function parseImport(Request $request)
    {
        $path = $request->file('csv_file')->getRealPath();
        $csv = Reader::createFromPath($path,'r');
        $csv->setDelimiter(';');
        $csv->setHeaderOffset(0);
        $records = $csv->getRecords();
        $errores = [];
        foreach ($records as $offset => $record) {

            $v = Validator::make($record,[
                'a'=> 'required',
                'b'=> 'required',
                'c'=> 'required',
                'd'=> 'required',
                'e'=> 'required',
                'f'=> 'required',
                'g'=> 'required',
                'h'=> 'required',
                'i'=> 'required',
            ]);
            if($v->fails()){
                array_push($errores,['error'=>'Error en registro '.$offset,'details'=>$v->errors()->all()]);
            }
        }

        return dd($errores);

    }
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
