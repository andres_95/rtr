<?php

namespace RTR\Http\Resources;

use Illuminate\Http\Resources\Json\Resource;
use RTR\modelos\TActividadesRepresentante;

class reporteResource extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
      //  return parent::toArray($request);
         $materiales=TActividadesRepresentante::where('idreporte','=',$this->idreporte)->first()->muestras();
           
               
         
         return [
             'idreporte'=>$this->idreporte,
        
             'nombre_rfv'=>$this->rfv()->first()->nombre_persona,
            
             'nombre_cliente'=>$this->cliente()->first()->nombre_persona,
             'fecha'=>$this->fecha_actividad,
             'tipo_actividad'=>$this->tipo()->first()->descripcion_tipo_actividades,
             'tipo_incidente'=>$this->incidente()->first()->descripcion_tipo_incidentes,
             $this->mergeWhen($this->idtipo_actividades=='005',
              [
                 'materiales'=>($materiales->count()>0)?$materiales->get(['cantidad','nombre_producto','lote']):'no tiene muestras'
              ]
             )
              ];
    }
}
