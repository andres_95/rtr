<?php

namespace RTR\Http\Resources;

use Illuminate\Http\Resources\Json\Resource;
use Illuminate\Support\Facades\Auth;
use RTR\modelos\TProducto;

class esquemaRecurso extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $producto=TProducto::withoutGlobalScopes()->find($this->idProducto)->nombre_producto;
        return [
            'id' => $this->idesqprom,
            'idproducto'=> $this->idProducto,
            'nombreproducto'=>$producto,
            'cantD'=>$this->cantidad_desde,
            'cantH'=>$this->cantidad_hasta,
            'montoD'=>$this->monto_desde,
            'montoH'=>$this->monto_hasta,
            'porcentaje'=>$this->porcentaje,
            'mayorista'=>$this->idMayorista,
            'cant'=>$this->cantidad
        ];
    }

}
