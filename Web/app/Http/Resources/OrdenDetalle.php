<?php

namespace RTR\Http\Resources;

use Illuminate\Http\Resources\Json\Resource;
use RTR\modelos\TEstatusOrdene;
use RTR\modelos\TPersona;
use RTR\Http\Resources\MayoristaRecurso;
use RTR\Http\Resources\ProductoOrdenRecurso;

class OrdenDetalle extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return array(
            'nOrden' => $this->idorden,
            'estatus'=> TEstatusOrdene::withoutGlobalScopes()->find($this->idestatus)->descripcion,
            'cliente'=> TPersona::withoutGlobalScopes()->find($this->idPersona_solicitante)['nombre_completo_razon_social'],
            'persona'=> TPersona::withoutGlobalScopes()->find($this->idPersona)['nombre_completo_razon_social'],
            'mayoristas'=>MayoristaRecurso::collection($this->Mayoristas()->withoutGlobalScopes()->get()),
            'productos'=>ProductoOrdenRecurso::collection($this->Productos()->withoutGlobalScopes()->get()),
            'Registrado_Por'=>TPersona::withoutGlobalScopes()->find($this->idpasadopor)['nombre_completo_razon_social'],
            'fecha'  => $this->fechaOrden->format('d/m/Y'),
            'Total'=>$this->costoTotal,
            'Impuesto' => $this->impuesto,
            'cantidad'=>$this->TotalUnidades,
            'comentario'=>$this->comentario_entrega
        );
    }
}