<?php

namespace RTR\Http\Resources;

use Illuminate\Http\Resources\Json\Resource;

class RecursoActividad extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'=>$this->idtipo_actividades,
            'nombre' => $this->descripcion_tipo_actividades
        ];
    }
}
