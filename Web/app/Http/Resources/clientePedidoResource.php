<?php

namespace RTR\Http\Resources;

use Illuminate\Http\Resources\Json\Resource;
use RTR\Http\Resources\representanteResource as representantes;

class clientePedidoResource extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
       $rfvs=$this->rfv()->get(); 
       return [
           'idPersona'=>$this->idPersona,
           'nombre'=>$this->nombre_persona,
           'representantes'=>representantes::collection($rfvs)
       ];
    }
}
