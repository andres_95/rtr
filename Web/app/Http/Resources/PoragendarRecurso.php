<?php
/**
 * Created by PhpStorm.
 * User: usuario
 * Date: 14/05/2018
 * Time: 12:04
 */
namespace RTR\Http\Resources;

use Illuminate\Http\Resources\Json\Resource;
use RTR\modelos\TPersona;

class PoragendarRecurso extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return array(
            'id'=>$this->Id,
            'rfv'=>$this->idRFV,
            'start'=>$this->Fecha.' '.$this->Hora,
            'fecha'=>$this->Fecha,
            'hora'=>$this->Hora,
            'cliente'=>$this->idCliente,
            'title'=>TPersona::find($this['idCliente'])['nombre_completo_razon_social']
        );
    }
}