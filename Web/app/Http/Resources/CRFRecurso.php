<?php

namespace RTR\Http\Resources;

use Illuminate\Http\Resources\Json\Resource;
use Illuminate\Support\Facades\Auth;
use RTR\modelos\TBrickRuta;
use RTR\modelos\TCadena;
use RTR\modelos\TCategoria;
use RTR\modelos\TCiclo;
use RTR\modelos\TDiasFeriado;
use RTR\modelos\TDiasVisita;
use RTR\modelos\TEstatus;
use RTR\modelos\TEstatusOrdene;
use RTR\modelos\TFrecuenciaVisita;
use RTR\modelos\THorario;
use RTR\modelos\TLineaProducto;
use RTR\modelos\TListanegra;
use RTR\modelos\TGrupopersona;
use RTR\modelos\TLocalizacione;
use RTR\modelos\TPaise;
use RTR\modelos\TPersona;
use RTR\modelos\TRankingCliente;
use RTR\modelos\TRedesSociale;
use RTR\modelos\TTipoActividade;
use RTR\modelos\TTipoIncidente;
use RTR\modelos\TTipoLocalizacione;
use RTR\modelos\TTipoNotificacion;
use RTR\modelos\TTipoProducto;
use RTR\modelos\TZona;
use RTR\modelos\TParametro;

class CRFRecurso extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $fecha='';
        $pais='';
        $alias='';
        $persona='';
        $zona='';
        $meta='';
        $desviacion='';
        $categoria='';
        if($this->tipo=='c'){
            $id=$this->id;
            $descripcion=TCiclo::withoutGlobalScopes()->find($id)->descripcion_ciclos;
        }
        if($this->tipo=='f'){
            $id=$this->id;
            $descripcion=TFrecuenciaVisita::withoutGlobalScopes()->find($id)->descripcion_frecuencia_visitas;
        }
        if($this->tipo=='r'){
            $id=$this->id;
            $descripcion=TRankingCliente::withoutGlobalScopes()->find($id)->descripcion_ranking_cliente;
        }
        if($this->tipo=='tp'){
            $id=$this->idtipo_producto;
            $descripcion=TTipoProducto::withoutGlobalScopes()->find($id)->descripcion_tipo_producto;
        }
        if($this->tipo=='tn'){
            $id=$this->id;
            $descripcion=TTipoNotificacion::withoutGlobalScopes()->find($id)->titulo;
        }
        if($this->tipo=='tl'){
            $id=$this->cod_tipo_localizacion;
            $descripcion=TTipoLocalizacione::withoutGlobalScopes()->find($id)->descripcion_tipo_localizacion;
        }
        if($this->tipo=='ti'){
            $id=$this->idtipo_incidentes;
            $descripcion=TTipoIncidente::withoutGlobalScopes()->find($id)->descripcion_tipo_incidentes;
        }
        if($this->tipo=='ta'){
            $id=$this->idtipo_actividades;
            $descripcion=TTipoActividade::withoutGlobalScopes()->find($id)->descripcion_tipo_actividades;
        }
        if($this->tipo=='rs'){
            $id=$this->cod_redes_sociales;
            $descripcion=TRedesSociale::withoutGlobalScopes()->find($id)->descripcion_redes_sociales;
        }
        if($this->tipo=='l'){
            $id=$this->cod_tipo_localizacion;
            $descripcion=TTipoLocalizacione::withoutGlobalScopes()->find($id)->descripcion_tipo_localizacion;
        }
        if($this->tipo=='eo'){
            $id=$this->idestatus;
            $descripcion=TEstatusOrdene::withoutGlobalScopes()->find($id)->descripcion;
        }
        if($this->tipo=='e'){
            $id=$this->idestatus;
            $descripcion=TEstatus::withoutGlobalScopes()->find($id)->descripcion_estatus;
        }
        if($this->tipo=='ca'){
            $id=$this->id;
            $descripcion=TCadena::withoutGlobalScopes()->find($id)->descripcion;
        }
        /* campos adicionales */
        if($this->tipo=='ln'){
            $id=$this->idlistanegra;
            $persona=TPersona::withoutGlobalScopes()->find($this->idPersona)->nombre_persona;
            $descripcion=TListanegra::withoutGlobalScopes()->find($id)->Causas_listanegra;
            $idgrupo=TPersona::withoutGlobalScopes()->find($this->idPersona)->idempresa_Grupo;
            $categoria=($idgrupo!=null)?TGrupopersona::find($idgrupo)->descripciongrupoPersona:'sin grupo';
            $fecha=$this->fecha_registro;
        }
        if($this->tipo=='lp'){
            $id=$this->id;
            $descripcion=TLineaProducto::withoutGlobalScopes()->find($id)->descripcion_linea_producto;
            $alias=$this->alias;
        }
        if($this->tipo=='h'){
            $id=$this->idhorarios;
            $descripcion=THorario::withoutGlobalScopes()->find($id)->descripcion_horarios;
            $pais=TPaise::find($this->idpais)->nombrePais;
        }
        if($this->tipo=='dv'){
            $id=$this->iddias_visita;
            $descripcion=TDiasVisita::withoutGlobalScopes()->find($id)->descripcion_dias_visita;
            $pais=TPaise::find($this->idpais)->nombrePais;
        }
        if($this->tipo=='df'){
            $id=$this->iddias_feriados;
            $descripcion=TDiasFeriado::withoutGlobalScopes()->find($id)->descripcion_dias_Feriados;
            $pais=TPaise::find($this->idpais)->nombrePais;
            $fecha=$this->fecha_dias_Feriados;
        }
        if($this->tipo=='b'){
            $id=$this->idbrick;
            $descripcion=TBrickRuta::withoutGlobalScopes()->find($id)->descripcion;
            $zona=TZona::find($this->idzona)->descripcion_zona;
        }
        if($this->tipo=='z'){
            $id=$this->idzona;
            $descripcion=TZona::withoutGlobalScopes()->find($id)->descripcion_zona;
        }
        if($this->tipo=='p'){
            $id=$this->idparametro;
            $parametro=TParametro::withoutGlobalScopes()->find($id);
            $descripcion=$parametro->descripcion;
            $meta=$parametro->valor_meta;
            $desviacion=$parametro->Porcentaje_Desviacion;
        }
          
        return [
            'id' => $id,
            'descripcion' => $descripcion,
            $this->mergeWhen(($this->tipo=='ln'),[
                'categoria'=>$categoria
             ]),
            $this->mergeWhen(($this->tipo=='p'),[
               'meta'=>$meta,
               'desviacion'=>$desviacion
            ]),
            'pais'=>$pais,
            'fecha'=>($fecha!='')?$fecha->format('Y-m-d'):null,
            'alias'=>$alias,
            'persona'=>$persona,
            'zona'=>$zona
        ];
    }
}