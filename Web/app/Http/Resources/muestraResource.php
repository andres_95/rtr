<?php

namespace RTR\Http\Resources;

use Illuminate\Http\Resources\Json\Resource;


class muestraResource extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {

        return [
            'codigo'=>$this->idproducto,
            'muestra'=>$this->nombre_producto,
            'unidades'=>$this->idunidades
            
        ];
     //   return parent::toArray($request);
    }
}
