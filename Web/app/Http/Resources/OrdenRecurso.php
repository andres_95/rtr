<?php

namespace RTR\Http\Resources;

use Illuminate\Http\Resources\Json\Resource;
use RTR\modelos\TPersona;
use RTR\modelos\TEstado;
use RTR\modelos\TEstatusOrdene;

use Carbon\Carbon;

class OrdenRecurso extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $cliente = TPersona::withoutGlobalScopes()->find($this->idPersona_solicitante);
        $fecha = Carbon::parse($this->fechaOrden)->toDateString();
        return array(
        'nOrden' => $this->idorden,
        'estatus'=> TEstatusOrdene::withoutGlobalScopes()->find($this->idestatus)->descripcion,
        'cliente'=> $cliente['nombre_completo_razon_social'],
        'persona'=> TPersona::withoutGlobalScopes()->find($this->idPersona)['nombre_completo_razon_social'],
        'estado'=> TEstado::withoutGlobalScopes()->find($cliente['idestado'])->nombreCiudad,
        'ciudad'=> $cliente['idciudad'],
        'mayoristas'=>TPersona::withoutGlobalScopes()->find($this->idMayorista)['nombre_completo_razon_social'],
        'unidades'=>$this->TotalUnidades,
        'totalOrden'=>(float)$this->costoTotal,
        'fecha' => $fecha
        );
    }
}
