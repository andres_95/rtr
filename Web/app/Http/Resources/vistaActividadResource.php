<?php

namespace RTR\Http\Resources;


use Illuminate\Http\Resources\Json\Resource;
use RTR\modelos\TPersona;
use RTR\modelos\TProducto;

class vistaActividadResource extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $rfv=TPersona::where('idPersona','=',$this->idRFV)->where('idestatus','=','1')->get();
        $cliente=TPersona::where('idPersona','=',$this->idCliente)->where('idestatus','=','1')->get();
        $producto=TProducto::where('idProducto','=',$this->muestra)->where('idestatus','=','1')->get();
    //    $incidente=TTipoIncidente::find($this->idtipo_incidentes);
        return [
            'rfv'=>$rfv['nombre_completo_razon_social'],
           // 'telefono_rfv'=>$rfv['telefono_persona'],
            //'direccion_rfv'=>$rfv['direccion_domicilio'],
           // 'email_rfv'=>$rfv['email_persona'],
            'cliente'=>$cliente['nombre_completo_razon_social'],
           // 'telefono_cliente'=>$cliente['telefono_persona'],
           // 'direccion_cliente'=>$cliente['direccion_domicilio'],
           // 'email_cliente'=>$cliente['email_persona'],
            'codigo'=>$this->muestra,
           // 'nombre'=>$producto['nombre_producto'],
           // 'cantidad'=>$producto['cantidad_producto_existente'],
           // 'lote'=>$producto['lote'],
            'comentario'=>$this->observaciones_cliente,
            'fecha'=>$this->fecha_actividad
           // 'incidente'=>$incidente->descripcion_tipo_incidente
        ];
    }
}
