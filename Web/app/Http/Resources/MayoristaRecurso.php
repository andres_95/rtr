<?php

namespace RTR\Http\Resources;

use Illuminate\Http\Resources\Json\Resource;

class MayoristaRecurso extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            "nombre"   =>$this->nombre_completo_razon_social,
            "id"       =>$this->idPersona,
            "descuento"=>$this->descuento?$this->descuento:0,

        ];
    }
}
