<?php

namespace RTR\Http\Resources;

use Illuminate\Http\Resources\Json\Resource;

class ProductoOrden extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'        => $this->idproducto,
            'nombre'    => $this->nombre_producto,
            'cantidad'  => $this->pivot->cantidad_solicitada,
            'precio'    => $this->pivot->item_price,
            'unidades'  => $this->idunidades,
            'descuento' => $this->pivot->item_descuento,
            'conciliada' => $this->pivot->cantidad_conciliada?$this->pivot->cantidad_conciliada:$this->pivot->cantidad_solicitada
        ];
    }
}
