<?php

namespace RTR\Http\Resources;

use Illuminate\Http\Resources\Json\Resource;

class FormatoProducto extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            "id"      =>$this['pivot']->idproducto,
    		"cantidad"=>$this['pivot']->cantidad_solicitada,
    		"nombre"  =>$this['pivot']->nombreproducto,
    		"precio"  =>$this['pivot']->item_price,
    		"unidades"=>$this['pivot']->idunidades
        ];
    }
}
