<?php

namespace RTR\Http\Resources;

use Illuminate\Http\Resources\Json\Resource;

class personaResource extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'idPersona'=>$this->idPersona,
            'nombre'=>$this->nombre_persona.' '.$this->apellido_persona
        ];
    }
}
