<?php

namespace RTR\Http\Resources;

use Illuminate\Http\Resources\Json\Resource;
use RTR\modelos\TPersona;
use RTR\modelos\TRankingCliente;
use RTR\modelos\TFrecuenciaVisita;
use RTR\modelos\TCiclo;
use RTR\modelos\TEspecialidade;

class listaRecurso extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
          $ranking=TRankingCliente::withoutGlobalScopes()->find($this->idranking);
          $frecuencia=TFrecuenciaVisita::withoutGlobalScopes()->find($this->idfrecuencia);
          $especialidad=TEspecialidade::withoutGlobalScopes()->find($this->idactividad_negocio);
        return [
            'nombre'=>$this->nombre_persona,
            'apellido'=>$this->apellido_persona,
            'tipo'=> $this->cod_tipo_persona,
            'CI'=>$this->idPersona,
            'email'=>$this->email_persona,
            'telf'=>$this->telefono_persona,
            'genero'=>$this->sexo_genero_persona,
            'direccion'=>$this->direccion_domicilio,
            //cliente
            'especialidad'=>$this->idactividad_negocio,
            'clase'=>$this->idclase_persona,
            'ranking'=>$this->idranking,
            'frecuencia'=>$this->idfrecuencia,
            //Mayorista
            'descuento'=>$this->descuento,
            //RFV / SUP
            'username'=>$this->username,
            'password'=>'',
            'ciclo'=>$this->idciclos,
            'supervisor'=>$this->idsupervisor,
            //localizacion
            'pais'=>$this->idpais,
            'estado'=>$this->idestado,
            'ciudad'=>$this->idciudad
        ];
    }
}