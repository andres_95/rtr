<?php

namespace RTR\Http\Resources;

use Illuminate\Http\Resources\Json\Resource;

class ProductoRecurso extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'        => $this->idproducto,
            'nombre'    => $this->nombre_producto,
            'cantidad'  => $this->cantidad_producto_existente,
            'linea'     => $this->idlinea_producto,
            'descripcion'=> $this->descripcion_producto,
            'principio'=> $this->principio_producto,
            'presentacion'=>$this->presentacion,
            'tipo'=>$this->idtipo_producto,
            'expedicion'=>date_format(date_create($this->fechaExpedicion_producto),'Y-m-d'),
            'vencimiento'=>date_format(date_create($this->fechaVencimiento_producto),'Y-m-d'),
            'lote'      =>$this->lote,
            'categoria' => $this->idcategorias,
            'precio'    => $this->Precio_producto,
            'unidades'  => $this->idunidades,
            'descuento' => $this->Descuento_producto,
            'mayorista' => $this->idMayorista
        ];
    }
}
