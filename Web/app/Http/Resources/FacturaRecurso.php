<?php

namespace RTR\Http\Resources;

use Illuminate\Http\Resources\Json\Resource;
use RTR\modelos\TPersona;
use RTR\modelos\TOrdene;
use RTR\modelos\TEstado;

class FacturaRecurso extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $idpersona = TOrdene::withoutGlobalScopes()->find($this->idordenes)['idPersona'];
        $persona = TPersona::withoutGlobalScopes()->find($idpersona)['nombre_completo_razon_social'];
        $cliente = TPersona::withoutGlobalScopes()->find($this->idPersona_solicitante);

        //dd($this->idordenes, $idpersona);
        return array(
        "id" => $this->idfactura,
        "orden"=>$this->idordenes,
        "RFV"=>$persona,
        "cliente"=>$cliente['nombre_completo_razon_social'],
        "ciudad"=>$cliente['idciudad'],
        'estado'=> TEstado::find($cliente['idestado'])->nombreCiudad,
        "monto"=>(float)$this->Monto_Factura,
        "pagado"=>$this->cantidad_Pagada,
        "faltante"=>$this->cantidad_Faltante,
        "descripcion"=>$this->DescripcionFactura,
        "estatus"=>$this->idestatus,
        "fecha"=>$this->fechaFactura
        );
    }
}
