<?php

namespace RTR\Http\Resources;

use Illuminate\Http\Resources\Json\Resource;
use RTR\modelos\TActividadesRepresentante;
use RTR\modelos\TPersona;

class ReportesRecurso extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        if($this->idestatus == 1)
            $status='Activa';
        else
            $status='Inactiva';

        return [
            'idreporte'=>$this->idreporte,
            'persona'=>TPersona::withoutGlobalScopes()->find($this->idRFV)->nombre_persona,
            'cliente'=>TPersona::withoutGlobalScopes()->find($this->idCliente)->nombre_persona,
            'fecha'=>$this->fecha_actividad->format('d-m-Y'),
            'estatus'=>$status
        ];
    }
}
