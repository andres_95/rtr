<?php

namespace RTR\Http\Resources;

use Illuminate\Http\Resources\Json\Resource;

class personaRecurso extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $this
     * @return array
     */
    public function toArray($request)
    {   
      /*  "nombre":this.editedItem.nombre,
        "apellido":this.editedItem.apellido,
        "CI":this.editedItem.CI,
        "email":this.editedItem.email,
        "telf":this.editedItem.telf,
        "direccion":this.editedItem.direccion,
        "genero":this.editedItem.genero,
        "especialidad":this.editedItem.especialidad,
        "clase":this.editedItem.clase,
        "ranking":this.editedItem.ranking,
        "frecuencia":this.editedItem.frecuencia,
        "descuento":this.editedItem.descuento,
        "username":this.editedItem.username,
        "password":this.editedItem.password,
        "ciclo":this.editedItem.ciclo,
        "supervisor":this.editedItem.supervisor,
        "pais":this.editedItem.pais,
        "estado":this.editedItem.estado,
        "ciudad":this.editedItem.ciudad,
        "categoria":this.categ,
        "fabricante":this.fabricante*/
        return [
            'idOperador'=>$this->operador,
            'idFabricante'=>$this->fabricante,
            'idPersona'=>$this->idPersona,
            'username'=>$this->username,
            'password'=>$this->password,
            'idsupervisor'=>$this->supervisor,
            'idpais'=>$this->pais,
            'idespecialidad'=>$this->especialidad,
          
            'documento_identidad'=>$this->CI,
            'idgrupo_persona'=>$this->categoria,
            'idclase_persona'=>$this->clase,
            'idranking'=>$this->ranking,
        
            'nombre_persona'=>$this->nombre,
            'apellido_persona'=>$this->apellido,
            'nombre_completo_razon_social'=>$this->nombre.' '.$this->apellido,
            'sexo_genero_persona'=>$this->genero,
            'fecha_nacimiento_registro',
            'telefono_persona'=>$this->telf,
          
            'email_persona'=>$this->email,
            'idciclos'=>$this->ciclo,
            'idfrecuencia'=>$this->frecuencia,
            'direccion_domicilio'=>$this->direccion,
            'idestado'=>$this->estado,
            'idciudad'=>$this->ciudad,
        ];
    }
}
