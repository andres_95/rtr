<?php

namespace RTR\Http\Resources;

use Illuminate\Http\Resources\Json\Resource;
use RTR\modelos\TCiudade;
use RTR\modelos\TEspecialidade;
use RTR\modelos\TPersona;
use RTR\modelos\TRankingCliente;
use RTR\modelos\TTipoActividade;
use RTR\modelos\TBrickRutaPersona;
use RTR\modelos\TEstado;
use Carbon\Carbon;

class ReporteRecurso extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
       $ruta = TBrickRutaPersona::where('idPersona',$this->idCliente)->first();
       $fecha = Carbon::parse($this->fecha_actividad)->toDateString();
        return array(
            'reporte' => $this->idreporte,
            'cliente' => $this->nombre_completo_razon_social,
            'RFV' => TPersona::withoutGlobalScopes()->find($this->idRFV)->nombre_completo_razon_social,
            'actividad' => $this->idtipo_actividades,
            'especialidadCliente'=> $this->idactividad_negocio,
            'rankingCliente'=> $this->idranking,
            'incidentes' => $this->idtipo_incidentes,
            'zona' => $ruta?$ruta->idzona:'S/Z',
            'ruta' => $ruta?$ruta->idbrick:'S/R',
            'ciudad' => TCiudade::find($this->idciudad)->nombreCiudad,
            'estado' => TEstado::find($this->idestado)->nombreCiudad,
            'observaciones' =>$this->observaciones_cliente?$this->observaciones_cliente:'S/C',
            'firma' =>$this->Firma_cliente,
            'fecha' => $fecha,
        );
    }
}
