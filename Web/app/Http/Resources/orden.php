<?php

namespace RTR\Http\Resources;

use Illuminate\Http\Resources\Json\Resource;

class orden extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            "id"      =>$this->idproducto,
            "cantidad"=>$this->cantidad_solicitada,
            "nombre"  =>$this->nombreproducto,
            "precio"  =>$this->item_price,
            "unidades"=>$this->idunidades
        ];

    }
}
