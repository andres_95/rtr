<?php

namespace RTR\Http\Resources;

use Illuminate\Http\Resources\Json\Resource;
use RTR\modelos\TNotificacion;
use RTR\modelos\TTipoNotificacion;

class notificacionResource extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
          $tipo=$this->tipo()->first();
          $representante= $this->representante()->first(['nombre_completo_razon_social']); 
               
        return [
            'id'=>$this->idNotificacion,
            'notificacion'=>$this->descripcion_notoficacion,
            'rfv'=>$representante,
            'tipo'=>$tipo->titulo, //$tipo->titulo,
            'fecha'=>$this->fecha_registro,
            'estatus'=>$this->idestatus
        ];
    }
}
