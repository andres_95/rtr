<?php

namespace RTR\Http\Resources;

use Illuminate\Http\Resources\Json\Resource;


class listaNegraRecurso extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
         
        return [
            'id'=>$this->idlistanegra,
            'CI'=>$this->idPersona,
            'nombre'=>$this->personas()->nombre_completo_razon_social,
            'causas'=>$this->Causas_listanegra,
            'fecha'=>$this->fecha_registro
        ];
    }
}
