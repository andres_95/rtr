<?php

namespace RTR\Http\Resources;

use Illuminate\Http\Resources\Json\Resource;
use Illuminate\Support\Facades\Auth;
use RTR\modelos\TFrecuenciaVisita;
use RTR\modelos\TPersona;
use RTR\modelos\TRankingCliente;
use RTR\modelos\TEspecialidade;
use RTR\modelos\TPlanificadore;

class ADMVisita extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'RFV'=>TPersona::withoutGlobalScopes()->find($this->id_RFV)->nombre_completo_razon_social,
            'Cliente'=>TPersona::withoutGlobalScopes()->find($this->id_cliente)->nombre_completo_razon_social,
        ];
    }
}