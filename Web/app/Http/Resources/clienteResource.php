<?php

namespace RTR\Http\Resources;

use Illuminate\Http\Resources\Json\Resource;
use RTR\modelos\TPersona;

class clienteResource extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {   
        $rfv=TPersona::where('idPersona','=',$this->idRFV)->first(['nombre_persona','apellido_persona']);
         
        
        return [
            'codigo'=>$this->idPersona,
            'nombre'=>$this->nombre_persona,
            'representante'=>$rfv['nombre_persona'].''.$rfv['apellido_persona']
        ];
    }
}
