<?php

namespace RTR\Http\Resources;

use Illuminate\Http\Resources\Json\Resource;
use RTR\modelos\TPersona;

class ActividadesRecurso extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
           'id'=>$this->idreporte,
           'tipo_actividad'=>$this->tipo()->first(['descripcion_tipo_actividades']),
           'tipo_incidente'=>$this->incidente()->first(['descripcion_tipo_incidentes']),
           'fecha'=>$this->fecha_actividad,
           'cliente'=>TPersona::find($this->idCliente)->nombre_completo_razon_social,
           'Firma'=>$this->Firma_cliente,
           'observacion'=>$this->observaciones_cliente,
           'materiales'=>$this->muestras()->get(['nombre_producto','cantidad'])
        ];
    }
}
