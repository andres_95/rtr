<?php

namespace RTR\Http\Resources;

use Illuminate\Http\Resources\Json\Resource;
use RTR\modelos\TPersona;

class CitaRecurso extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return array(
            'start'=>$this->fecha_agenda.' '.$this->hora,
            'title'=>TPersona::find($this['idCliente'])['nombre_completo_razon_social']
        );
    }
}
