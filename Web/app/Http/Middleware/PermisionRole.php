<?php

namespace RTR\Http\Middleware;
use RTR\Http\Utilities\OptionsRole;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;
use Closure;
use RTR\modelos\TPerfile;

class PermisionRole
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        return $next($request);
        if(in_array(Route::currentRouteName(),['home','contacto','manual','FQ','videos','unauthorized'])){
            return $next($request);
        }
        if($this->ChekRole(Route::currentRouteName()))
        {
            return $next($request);
        }else{
            if($request->ajax()) return response()->json('Unauthorized action.', 403);
            return redirect()->route('unauthorized');
        }
        
        //return response(Route::currentRouteName());
        
    }
    public function Permisions(){
        $Options = new OptionsRole();
        return $Options->Options();
    }

    public function ChekRole($action){
        $perfil = Auth::user()->perfil()->first();
        if (!$perfil) {
            return false;
        }
        if($perfil->id==1) return true;
        $index = array_search($action,$this->Permisions());
        if($index){
            return  $perfil->roles()->find($index);
        }
       else return false;
    }
}
