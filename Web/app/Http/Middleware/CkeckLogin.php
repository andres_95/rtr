<?php

namespace RTR\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class CkeckLogin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(Auth::check()  or $request->is("/"))
        {
            return $next($request);
        }else {
            return redirect()->guest('/');
        }
    }
}
