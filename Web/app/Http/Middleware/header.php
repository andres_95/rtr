<?php 
namespace RTR\Http\Middleware;
use Closure;
class header {
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
            if($request->isMethod('options'))
            {
                return response("")->header('Access-Control-Allow-Origin', '*')
                ->header('Access-Control-Allow-Methods', 'POST,GET,OPTIONS,PUT,DELETE')
                ->header('Access-Control-Allow-Headers', 'Content-Type,X-XSRF-TOKEN,Authorization,X-Auth-Token,x-requested-with,X-CSRF-Token')
                ->header('Accept', 'aplication/json,application/octet-stream; charset=utf-8,multipart/form-data');
            }

            return $next($request);
            ;
        }
}