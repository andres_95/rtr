<?php

namespace RTR\Http\Utilities;

use JsonSchema\Validator;
use JsonSchema\Constraints\Constraint;
use RTR\Http\Utilities\MonitorUtils;



class Monitor
{

    public function jsonAnalizer($file, $seleccion){

        $jsonAray = MonitorUtils::read_CSV($file);

        $validator = new Validator();

        $ruta = MonitorUtils::get_EsquemaAsociado($seleccion);
        if($ruta === "notFound"){

            echo "No se encontro el esquema valido!!!" ;

        }else{

            $totalErrors = 0;
            $linea = -1;


            foreach($jsonAray as $jsonItem){

                $linea = $linea + 1;

                $validator->validate(
                    $jsonItem,
                    (object)['$ref' => $ruta],
                    Constraint::CHECK_MODE_APPLY_DEFAULTS
                );



                if($validator->getErrorMask() != 0){
                    $errorArray[] = array($linea,$validator->getErrors());
                    $validator->reset();
                }


                //$errorList[] = array_combine($linea, $errorArray);

            }



            if (sizeof($errorArray) == 0) {
                //echo "Es Valido y bien formado";
            } else {








                $tabla = "<table>";


                foreach($errorArray as $listaErrores){

                    foreach($listaErrores[1] as $hallazgos){



                        $linea = "Linea " . $listaErrores[0] . " Campo:" . $hallazgos['property'] . " Descripcion: ". $hallazgos["message"];
                        //echo $linea ."\n";

                        $tabla .= "<tr><td>{$linea}</td></tr>";

                        $linea = "";


                        //echo "\$errorArray[$linea] => $errores.\n";
                    }



                }

                $tabla .= "</table>";

                echo $tabla;
            }
        }
    }
}



